#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Gimp-plugin
#
# Ein Dialogfenster, um eine gic-Eingabe auf Bilder in einem Verzeichnis
# anzuwenden
#
# oli.s. 9/2018
#
# GIMP Python


import sys
import os

# Suchpfad für import erweitern ...
sys.path.append( sys.argv[ 0 ][ : sys.argv[ 0 ].rfind( os.sep ) ] +
				 os.sep + "gic" + os.sep + "lib" )

from gimpfu import *

import gic_lib.interpreter as gic
import gic_lib.locale      as LOC


# Vorgabewert für Kommando
SGIC_DEFAULT = LOC.CMD_SAVE + " " + LOC.VAR_DEST



################################################################################
# Hauptfunktion
#
# 'sgic'    	  Befehle, die die Bildbearbeitung betreffen
# 'pathSource'    Quellpfad
# 'filterSource'  Wildcards für Suche, z.B. *.jpg
# 'pathDest'      Zielpfad
# 'extDest'       Dateityp der Zieldateien. z.B. "jpg"
#                 Ersetzt die Dateierweiterung aus der Quelldatei durch diese
#                 Angabe
################################################################################
def gic_dlg_folder( sgic, pathSource, filterSource, pathDest, extDest ):

   # 'pathSource' überprüfen
   if pathSource == None or len( pathSource ) < 2:
      pathSource = gic.path_get( gic.FOLDER_DEMO_PICS )

   # 'pathDest' überprüfen
   if pathDest == None or len( pathDest ) < 2:
      pathDest = gic.path_get( gic.FOLDER_TMP )
      #pathDest = ""

   # Speicherbefehl
   sgic2 = SGIC_DEFAULT

   # gic-Befehl zur Stapelverarbeitung erstellen
   sgic = gic.code_create( LOC.CMD_EXEC_DIR,
   						   [ sgic,
							 pathSource,
							 filterSource,
							 pathDest,
							 extDest,
							 sgic2 ],
						   [] )

   # Ausführen
   gic.run_direct( sgic )



# Gimp-Registrierung
register(
    "python_fu_gic_dlg_folder",
    LOC.DLG_FOLDER_INFO,
    LOC.DLG_FOLDER_INFO,
    "osloin",
    "osloin",
    "2018",
    LOC.DLG_FOLDER_LABEL,
    "",
    [
        (PF_STRING,  "sgic",         LOC.DLG_FOLDER__SGIC, 	  "" 	        ),
        (PF_DIRNAME, "pathSource",   LOC.SOURCEPATH,          "" 			),
        (PF_STRING,  "filterSource", LOC.FILEFILTER,          "*.*"			),
        (PF_DIRNAME, "pathDest",     LOC.DESTPATH,            "" 			),
        (PF_STRING,  "extDest",      LOC.DLG_FOLDER__EXTDEST, "jpg" 		)
    ],
    [],
    gic_dlg_folder, menu="<Image>/" + LOC.MNU_TOP
    )

main()
