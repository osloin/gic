#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Gimp-plugin
#
# Ein Dialogfenster zur Direkteingabe von gic.
#
# oli.s. 9/2018
#
# GIMP Python

import sys
import os

from gimpfu import *

# Suchpfad für import erweitern ...
sys.path.append( sys.argv[ 0 ][ : sys.argv[ 0 ].rfind( os.sep ) ] +
				 os.sep + "gic" + os.sep + "lib" )

import gic_lib.interpreter as gic
import gic_lib.locale      as LOC



################################################################################
# Hauptfunktion
#
# 'image'
# 'drawable'
# 'sgic'    		gic-Code, der ausgeführt werden soll
################################################################################
def gic_dlg_direct( image, drawable, sgic ) :

   # Eingabe ausführen ...
   gic.run_direct( sgic, [ image, drawable ] )



#####################
# Gimp-Registrierung
#####################
register(
    "python_fu_gic_dlg_direct",
    LOC.DLG_DIRECT_INFO,
    LOC.DLG_DIRECT_INFO,
    "osloin",
    "osloin",
    "2018",
    "<Image>/" + LOC.MNU_TOP + "/" + LOC.DLG_DIRECT_LABEL,
    "RGB*, GRAY*",
    [
        (PF_STRING, "sgic", LOC.DLG_DIRECT__SGIC, LOC.DLG_DIRECT__SGIC_V )
    ],
    [],
    gic_dlg_direct )

main()
