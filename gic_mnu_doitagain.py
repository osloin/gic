#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Gimp-plugin
#
# Führt die letzte Eingabe erneut aus.
#
# osloin. 1/2019


import sys
import os

# Suchpfad für import erweitern ...
sys.path.append( sys.argv[ 0 ][ : sys.argv[ 0 ].rfind( os.sep ) ] +
				 os.sep + "gic" + os.sep + "lib" )

from gimpfu import *

import gic_lib.interpreter as gic
import gic_lib.locale      as LOC



###############################################################################
# Hauptfunktion
#
# 'image'       Bild
# 'drawable'    Aktive Ebene
#
# Ergebnis
###############################################################################
def gic_mnu_doitagain( image, drawable ) :

	# WIE-Befehl ausführen
	gic.run_param( LOC.CMD_EXEC_AGAIN, [ image, drawable ] )



#####################
# Gimp-Registrierung
#####################

register(
    "python_fu_gic_mnu_doitagain",
    LOC.MNU_DOITAGAIN_INFO,
    LOC.MNU_DOITAGAIN_INFO,
    "osloin",
    "osloin",
    "2019",
    "<Image>/" + LOC.MNU_TOP + "/" + LOC.MNU_DOITAGAIN_LBL,
    "RGB*, GRAY*",
    [
    ],
    [],
    gic_mnu_doitagain )

main()
