#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Gimp-plugin
#
# Ein Dialogfenster, um einen Menüpunkt zu erzeugen.
#
# oli.s. 9/2018


import sys
import os

# Suchpfad für import erweitern ...
sys.path.append( sys.argv[ 0 ][ : sys.argv[ 0 ].rfind( os.sep ) ] +
                 os.sep + "gic" + os.sep + "lib" )

from gimpfu import *

import gic_lib.locale       as LOC
import gic_lib.interpreter  as gic
import gic_lib.files        as fil_
import gic_lib.convert      as con_
import gic_lib.str          as str_
import gic_lib.code         as cod_
import gic_lib.message      as msg_



# Dateiname der Vorlage
TEMPL_FILENAME = "menu.py"

# Zeichenketten, die eingefügt werden sollen
INS_FILENAME_PREFIX = "mnu_"    # Präfix für Dateinamen
INS_MNU1            = "\"<Image>/\"+LOC.MNU_TOP+\"/\"+\"%s\""
INS_REG_ITEM        = "(%s,\"%s\",\"%s\",\"%s\")"

INS_PF_STRING       = "(PF_STRING, \"%s\",\"%s\",\"%s\")"
INS_PF_SPINNER      = "(PF_SPINNER,\"%s\",\"%s\",%s,(%s,%s,%s))"
INS_PF_OPTION       = "(PF_OPTION, \"%s\",\"%s\",%s, %s )"
INS_PF_FILE         = "(PF_FILE,   \"%s\",\"%s\",\"%s\")"
INS_PF_DIRNAME      = "(PF_DIRNAME,\"%s\",\"%s\",\"%s\")"

# Wird Variablennamen vorangestellt
INS_VARNAME_PREFIX = "p_"

# Zeichenketten im Code, die gesucht und ersetzt werden sollen
TEMPL_BLOCK_BEGIN  = "#BLOCK_BEGIN_%d#"
TEMPL_BLOCK_END    = "#BLOCK_END_%d#"
TEMPL_NAME         = "DUMMY"
TEMPL_MNU          = TEMPL_NAME + "_INFO"
TEMPL_TOPMNU       = "\"+LOC.MNU_TOP+\""
TEMPL_MNU1         = "\"<Image>/\"+LOC.MNU_TOP+\"/\"+\"DUMMY_LABEL\""
TEMPL_MNU2         = "#" + TEMPL_NAME + "_MNU2#"
TEMPL_FCT_NAME     = TEMPL_NAME + "_FCT"               # Funktionsname
TEMPL_FCT_PARAM    = ", " + TEMPL_NAME + "_DEC_PARAM"  # Param. in Funktionsdekl.
TEMPL_FCT_P_NAME   = TEMPL_NAME + "_PARAM_NAME"   # Parametername
TEMPL_FCT_P_LABEL  = TEMPL_NAME + "_PARAM_LABEL"  # Parametertext
TEMPL_FCT_P_VALUE  = TEMPL_NAME + "_PARAM_VALUE"  # Parameter-Vorgabewert
TEMPL_FCT_REGISTER = "(PF_STRING," +\
                       "\"" + TEMPL_FCT_P_NAME  + "\"," +\
                       "\"" + TEMPL_FCT_P_LABEL + "\"," +\
                       "\"" + TEMPL_FCT_P_VALUE + "\"" +\
                      ")" # register()
TEMPL_RUN_GIC      = "\"\"#<--GIC"       # gic für run()
TEMPL_RUN_PARAM    = "[]#<--PARAM"       # List für run(): Param.-Namen
TEMPL_RUN_TYPES    = "[]#<--TYPES"       # List für run(): Python-Typen
TEMPL_RUN_QMS      = "[]#<--QMS"         # List für run(): Anführungsz.
TEMPL_RUN_OPTIO    = "[]#<--OPTIO"       # List für run(): Optionen
TEMPL_RUN_SEPAR    = "[]#<--SEPAR"       # List für run(): Trennzeichen


# Modus (Der vom Anwender gewählte Modus)
MODE_STATIC  = 0 # Befehl mit festen Parameter
MODE_VAR     = 1 # Befehl mit variablen Parametern
MODE_CODE    = 2 # 'cmd' enthält beliebigen gic-Code
MODE_PATH    = 3 # gic-Code aus Pfadnamen übernehmen

# Parameter für convert()
C_MENU     = 0
C_FILENAME = 1
C_VAR      = 2

################################################################################
# Ersetzt problematische (für Dateinamen und Menüeinträge) Zeichen in
# Namen
#
# 'name'        Originalname
# 'conv_type'   C_MENU, C_FILENAME, C_VAR
#
# Ergebnis: Der konvertierte Name
################################################################################
def convert( name, conv_type ) :

    result =   name.replace( "/",  "" )    # z.B. H/K-Befehl
    result = result.replace( "-", "_" )

    # Leerzeichen und Punkte in Variablen und Dateinamen
    if conv_type == C_VAR and conv_type == C_FILENAME:
        result = result.replace( ".", "_" )        
        result = result.replace( " ", "_" )

    # Anführungszeichen in Hochkomma
    if conv_type == C_MENU:

        # Anführungszeichen in Hochkomma
        result = result.replace( "\"", "'" )

    # Umlaute
    if conv_type == C_VAR:

        result = result.replace( "ö", "oe" )
        result = result.replace( "ä", "ae" )
        result = result.replace( "Ö", "Oe" )
        result = result.replace( "Ä", "Äe" )
        result = result.replace( "ß", "ss" )

    return result



################################################################################
# Konvertiert Zeichenkette, die in Code eingefügt werden sollen
#
# - Backslash wird durch Doppel-Backslash ersetzt
# - Anführungszeichen werden durch Backslash + Anführungszeichen ersetzt
# - Leerzeichen v. und hinten entfernen
################################################################################
def code_convert( code ) :

    code = str_.replace_solo( code, "\\", "\\\\" )
    code = code.replace( "\"", "\\\"")
    code = code.strip()       # Leerzeichen v. und hinten entfernen
    return code



################################################################################
# Ersetzt Code in der Vorlage, ohne dass der Anwender später die Variablen-Werte
# ändern kann
#
# 'codeTpl'
# 'cmd'
# 'paras'
# 'mode'
# 'mtopname'
# 'mname'
#
# Ergebnis  Code mit den ersetzten Stellen
################################################################################
def replace_STATIC( codeTpl, cmd, paras, mode, mtopname, mname, image) :

    code = codeTpl

    # Code-Block löschen
    code = cod_.py_replace( code,
                            [TEMPL_BLOCK_BEGIN % 2,TEMPL_BLOCK_END % 2] )

    # Lösche Parameter in Funktionsdeklaration
    code = code.replace( TEMPL_FCT_PARAM, "" )

    # Dlg-Eintrag löschen, wir wollen nichts eingeben
    code = code.replace( TEMPL_FCT_REGISTER, "" )

    # gic-Code erstellen
    if mode == MODE_CODE:
        #sgic = cmd
        sgic = paras
    elif mode == MODE_STATIC:
        sgic = cmd + " " + paras
    elif mode == MODE_PATH:
        av   = pdb.gimp_image_get_active_vectors( image )
        sgic = pdb.gimp_item_get_name( av )
        sgic = code_convert( sgic )
    else:
        sgic = ""

    # Code eintragen
    code = code.replace( TEMPL_RUN_GIC, "\"%s\"" % ( sgic ) )

    # Oberer Menueintrag bei register()
    code = code.replace( TEMPL_MNU1, INS_MNU1 % convert( mname, C_MENU ) )

    # Ergebnis
    return code



################################################################################
# Ersetzt Code in der Vorlage, so dass Anwender später die Variablen-Werte
# ändern kann
#
# 'codeTpl'
# 'cmd'
# 'paras'
# 'mode'
# 'mtopname'
# 'mname'
#
# Ergebnis  Code mit den ersetzten Stellen
################################################################################
def replace_VAR( codeTpl, cmd, paras, mode, mtopname, mname) :

  # Zieldatei ermitteln
  target = gic.path_get( gic.FOLDER_INSTAL ) + \
                         INS_FILENAME_PREFIX + \
                         cmd.lower() + "." + gic.MOD_EXT_MOD
  target = fil_.unused( target )   # Freien Dateinamen ermitteln
  name   = fil_.extract( target, fil_.FE_NAME )  # Dateinamen

  # Parameter-Typ-Infos für 'cmd' ermitteln
  param_gty = gic.id_get_info( cmd, gic.MOD_ACTION_PTYPE )  # GTYP-Infos
  param_ty  = gic.convert_GTYP_type( param_gty )            # Python-Infos
  param_doc = gic.id_get_info( cmd, gic.MOD_ACTION_PDOC )   # Param.Namen
  param_mms = gic.id_get_info( cmd, gic.MOD_ACTION_PMMS )   # Wertebereich
  param_sep = gic.id_get_info( cmd, gic.MOD_ACTION_PSPLIT ) # Trennzeichen
  param_val = gic.id_get_info( cmd, gic.MOD_ACTION_PVAL )   # Param.-Werte

  # Code, der an der entsprechenden Stellen eingefügt werden soll
  code_fct_param = "" # Funktionsdeklaration
  code_register  = "" # register()

  code_run_names = "" # Liste der Parameter-Namen
  code_run_types = "" # Liste der Parameter-Python-Typen
  code_run_qms   = "" # Ob bei Zeichenketten Anführungszeichen verwendet werden
  code_run_optio = "" # ggf. Optionen
  code_run_separ = "" # Trennzeichen

  code_menu_1    = "" # Oberer Menüeintrag bei register()
  code_menu_2    = "" # Unterer Menüeintrag bei register()

  # Enthält die Einträge zwischen den eckigen Klammern im Bereich register()
  registerlist = []

  # Parameter (Trennzeichen) für split_ex()
  if param_sep == None:
      split_ex_param = gic.GIC_PARAM
  else:
      split_ex_param = param_sep

  # Parameter-str in list konvertieren
  paras = str_.split_ex( paras,
                         split_ex_param,
                         gic.GIC_QM,
                         gic.GIC_QM,
                         None,
                         False,
                         False,
                         gic.GIC_FCT1,
                         gic.GIC_FCT2 )

  # Meldung
  msg_.message( LOC.PARAMETERS + ": " + str( paras ) )

  # Akt. Parameter-Index
  i = 0

  # Alle Parameter durchlaufen und aufbereiten ...
  for para in paras:

      # Python-Typ des aktuellen Eintrags holen
      t = param_ty[ i ]

      # GTyp des aktuellen Eintrags holen
      gt = param_gty[ i ]

      # Aktuellen Parametername

      # Text, der im Dialogfenster vor dem Steuerelement erscheint
      pname_lbl = gic.format_doc( [ param_doc, i ], False, "", True )

      # Variablenname
      pname_var = INS_VARNAME_PREFIX + \
                  convert( gic.format_doc( [ param_doc, i ], False, "", True ), C_VAR )

      # Ob der akt. Parameter Optionen anbietet
      if param_val != None and len( param_val[ i ] ) > 0:
          options = param_val[ i ]
      else:
          options = None
          # Für bool-Typen erzeugen wir Optionen ...
          if t == bool:
              options = [ LOC.TRUE, LOC.FALSE ]
              para = gic.GIC_QM + para + gic.GIC_QM

      # Funktionsdekl.
      code_fct_param += ", "
      code_fct_param += pname_var

      # run()
      if i == 0:
          code_run_types = "["
          code_run_names = "["
          code_run_qms   = "["
          code_run_optio = "["
          code_run_separ = "["
      else:
          code_run_types += ", "
          code_run_names += ", "
          code_run_qms   += ", "
          code_run_optio += ", "
          code_run_separ += ", "

      # Erweitern der Parameter-Python-Typen
      code_run_types += con_.type_str( t )

      # Erweitern der Parameter-Namen
      code_run_names += pname_var

      # Erweitern der Anführungzeichen-Infos
      if gic._GTYPES[ gt ][ gic._GTY_QM ] != 0:
         code_run_qms   += "True"
      else:
         code_run_qms   += "False"

      # Optionen
      code_run_optio += str( options )

      # Trennzeichen (eingeschlossen von Anführungszeichen)
      code_run_separ += "\""
      if param_sep == None:
          code_run_separ += gic.GIC_PARAM
      else:
          code_run_separ += param_sep[ i ]
      code_run_separ += "\""

      # Anpassungen bei letzten Eintrag ...
      if i == len( paras ) - 1:           # letzter Eintrag?
          code_run_types += "]"
          code_run_names += "]"
          code_run_qms   += "]"
          code_run_optio += "]"
          code_run_separ += "]"

      # register()
      # Anzeigetext um Typinfos erweitern
      # pname_lbl += "(%s)" % con_.type_str( t ) # Typinfo

      # ggf. Vor / Hinten das Anführungszeichen entfernen
      para = str_.strip_qm( para, "\\\"" )

      # Steuerelement auswählen

      # SPINNER ...
      if param_mms != None and param_mms[i] != None and len( param_mms[i] ) > 0:
            mms = param_mms[i]
            item = INS_PF_SPINNER % (pname_var, pname_lbl, para,
                                     str(mms[0]), str(mms[1]), str(mms[2]) )
      # PF_OPTION ...
      elif options != None:
            item = INS_PF_OPTION % (pname_var,
                                    pname_lbl,
                                    "0",
                                    str( options ) )
      # PF_FILE ...
      elif gt == gic.MOD_PTYP_FILE:
          item = INS_PF_FILE % ( pname_var, pname_lbl, para )

      # PF_DIRNAME ...
      elif gt == gic.MOD_PTYP_PATH:
          item = INS_PF_DIRNAME % ( pname_var, pname_lbl, para )

      # PF_STRING ...
      else:
          item = INS_PF_STRING % ( pname_var, pname_lbl, para )


      # In Liste aufnehmen
      registerlist.append( item )

      # Nächster Index
      i += 1

  # Menüeintrag
  code_menu_1 = INS_MNU1 % mname

  # Ersetze Parameter in Funktionsdeklaration
  code = codeTpl.replace( TEMPL_FCT_PARAM, code_fct_param )

  # Ersetze Code für run()
  code = code.replace( TEMPL_RUN_GIC, "\"" + cmd + "\"" )
  code = code.replace( TEMPL_RUN_PARAM, code_run_names )
  code = code.replace( TEMPL_RUN_TYPES, code_run_types )
  code = code.replace( TEMPL_RUN_QMS,   code_run_qms   )
  code = code.replace( TEMPL_RUN_OPTIO, code_run_optio )
  code = code.replace( TEMPL_RUN_SEPAR, code_run_separ )

  # Oberer Menueintrag bei register()
  code = code.replace( TEMPL_MNU1, code_menu_1 )

  # Unterer Menueintrag bei register()
  code = code.replace( TEMPL_MNU2, code_menu_2 )

  # [] - Einträge bei register()
  code = cod_.py_replace( code, TEMPL_FCT_REGISTER, registerlist, "," )

  # Ergebnis
  return code



################################################################################
# Hauptfunktion
#
# 'image'       Gimp-Objekt
#
# 'drawable'    Gimp-Objekt
#
# 'mode'        0/MODE_STATIC: Anwender kann später die Parameterwerte nicht
#                              mehr ändern
#               1/MODE_VAR:    Anwender kann später die Parameterwerte
#                              ändern
#               2/MODE_CODE    'cmd' enthält beliebigen gic-Code
#               3/MODE_PATH    Code aus Pfadnamen
#
# 'cmd'         Je nach 'mode' ...
#               MODE_STATIC    Name eines Befehls
#               MODE_VAR       Name eines Befehls
#               MODE_CODE      Beliebiger gic-Code
#               MODE_PATH      Name eines Befehls
#
# 'paras'       Je nach 'mode' ...
#               MODE_STATIC    Die Werte der einzelnen Parameter
#               MODE_VAR       Die Vorgabewerte der einzelnen Parameter
#               MODE_CODE      Beliebiger gic-Code
#               MODE_PATH      Präfix für Funktions/Datei-Name
#
#
# 'mtopname'    Text im Hauptmenü
#
# 'mname'       Text im Menüpunkt
#
################################################################################
def gic_dlg_create_menu( image, drawable, mode, cmd, paras, mtopname, mname ) :

    # Pfad mit Dateinamen der Vorlage
    template = gic.path_get( gic.FOLDER_TEMPLATES ) + TEMPL_FILENAME

    # Vorlage laden
    f = open( template, "r")
    codeTpl = f.read()
    f.close()

    # Vorlage anpssen
    if len( codeTpl ) > 0:  # Vorlage da?

      # Dateinamenvorgabe der Zieldatei
      fname = convert( cmd, C_FILENAME ).lower() + "." + gic.MOD_EXT_MOD

      # Zieldatei ermitteln
      target = gic.path_get( gic.FOLDER_INSTAL ) +\
                             INS_FILENAME_PREFIX +\
                             fname

      # Freien Dateinamen ermitteln
      target = fil_.unused( target )

      # 'fctname' aus Dateinamen ermitteln
      fctname   = fil_.extract( target, fil_.FE_NAME )

      # Wenn ein Parameter Anführungszeichen enthält, ersetzen wir sie durch
      # Backslash + Anführungszeichen, da sie direkt in den Code eingefügt
      # werden
      cmd   = code_convert( cmd )
      paras = code_convert( paras )

      # Code-Ersetzung je nach Modus
      code = ""
      if mode == MODE_VAR:      # variabler Modus
          code = replace_VAR( codeTpl, cmd, paras, mode, mtopname, mname )
      elif mode == MODE_CODE:   # code Modus
          code = replace_STATIC( codeTpl,
                                 cmd, paras, mode, mtopname, mname, image )
      else:                     # Statischer Modus
          code = replace_STATIC( codeTpl,
                                 cmd, paras, mode, mtopname, mname, image )

      # Code-Ersetzungen unabhängig vom Modus
      code = code.replace( TEMPL_FCT_NAME, fctname ) # Funktionsname
      # Menuname bei register()
      code = code.replace( TEMPL_MNU, convert( mname, C_MENU ) )
      code = code.replace( TEMPL_TOPMNU, mtopname )  # Hauptmenu-Eintrag

      # Kommentar
      code = cod_.py_replace( code,
                        [TEMPL_BLOCK_BEGIN % 1,TEMPL_BLOCK_END % 1],
                        [" " + LOC.MSG_AUTO_CREATED % "gic_dlg_create_mnu.py"]
                        )

      # Schreiben der neuen Datei
      f = open( target, "w")
      f.write( code )
      f.close()

      # Neue Datei ausführbar machen
      fil_.make_exec( target )

      # Erfolgsmeldung
      pdb.gimp_message( LOC.DLG_CREATE_MNU_MSG % target )



#####################
# Gimp-Registrierung
#####################
register(
    "python_fu_gic_dlg_create_menu",
    LOC.DLG_CREATE_MNU_INFO,
    LOC.DLG_CREATE_MNU_INFO,
    "osloin",
    "osloin",
    "2018",
    "<Image>/" + LOC.MNU_TOP + "/" +
                 LOC.MNU_CREATE + "/" +
                 LOC.DLG_CREATE_MNU_LABEL,
    "RGB*, GRAY*",
    [
        (PF_OPTION, "mode",  LOC.DLG_CREATE_MNU__MODE, 0,
                             LOC.DLG_CREATE_MNU__MODE_V ),
        (PF_STRING, "cmd",   LOC.DLG_CREATE_MNU__CMD,
                             LOC.DLG_CREATE_MNU__CMD_V ),
        (PF_STRING, "paras", LOC.DLG_CREATE_MNU__PARAM,
                             LOC.DLG_CREATE_MNU__PARAM_V ),
        (PF_STRING, "mtop",  LOC.DLG_CREATE_MNU__MTOP,
                             LOC.DLG_CREATE_MNU__MTOP_V),
        (PF_STRING, "mname", LOC.DLG_CREATE_MNU__MNAME,
                             LOC.DLG_CREATE_MNU__MNAME_V )
    ],
    [],
    gic_dlg_create_menu )

main()
