#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Gimp-plugin
#
# Führt den im aktuellen Pfadnamen enthaltenen gic-Code aus.
#
# osloin. 9/2018


import sys
import os

# Suchpfad für import erweitern ...
sys.path.append( sys.argv[ 0 ][ : sys.argv[ 0 ].rfind( os.sep ) ] +
				 os.sep + "gic" + os.sep + "lib" )

from gimpfu import *

import gic_lib.interpreter as gic
import gic_lib.locale      as LOC



###############################################################################
# Hauptfunktion
#
# 'image'       Bild
# 'drawable'    Aktive Ebene
#
# Ergebnis
###############################################################################
def gic_mnu_path( image, drawable ) :

   # Aktiven Pfad ermitteln
   av = pdb.gimp_image_get_active_vectors( image )

   # Keine aktiver Pfad?
   if av == None:

	  pdb.gimp_message( LOC.PATH + "?" )

   else: # Aktiver Pfad wurde gefunden ...

	  # Ausführen
	  gic.run_direct( pdb.gimp_item_get_name( av ), [ image, drawable ] )




#####################
# Gimp-Registrierung
#####################

register(
    "python_fu_gic_mnu_path",
    LOC.MNU_PATH_INFO,
    LOC.MNU_PATH_INFO,
    "osloin",
    "osloin",
    "2018",
    "<Image>/" + LOC.MNU_TOP + "/" + LOC.MNU_PATH_LABEL,
    "RGB*, GRAY*",
    [
    ],
    [],
    gic_mnu_path )

main()
