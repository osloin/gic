#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# Grundgerüst eines gic-Moduls
#
# Python-Variable, die während der Ausführung für das Modul zur Verfügung
# stehen ...
#
# 'action'   (int) Zeigt an, was vom Modul verlangt wird ....
#            MOD_ACTION_DYNID   Dynamische Namenauswertung
#            MOD_ACTION_EVT     Programmereignis. Siehe EVT_...
#            MOD_ACTION_EXEC    Befehl / Funktion in 'name' ausführen
#            MOD_ACTION_EXIT    Beenden
#            MOD_ACTION_IDDOC   Infos über ID (z.B. Befehl) liefern
#            MOD_ACTION_INIT    Initalisieren
#            MOD_ACTION_ITYPE   Typ-Infos für IDs liefern
#            MOD_ACTION_LVL     Modulpriorität bei Initalisierung liefern
#            MOD_ACTION_NAME    Namen liefern
#            MOD_ACTION_PDEF    Vorgabewerte für Parameter liefern
#            MOD_ACTION_PDOC    Param.-Infos für Parameter 'name' liefern
#            MOD_ACTION_PMMS    Param.-Infos für Min-Max-Schrittweite liefern
#            MOD_ACTION_POST    Post-Init. Eine ID wurde initalisiert.
#            MOD_ACTION_PTYPE   Param.-Typen für 'name' liefern.
#            MOD_ACTION_PVAL    Param.-Infos für Werteliste liefern
#            MOD_ACTION_PSPLIT  Param.-Infos für Trennerinfos
#            MOD_ACTION_TRANS_R Konvertierung in eine Richtung
#            MOD_ACTION_TRANS_L Konvertierung in anderer Richtung
#            MOD_ACTION_VAR_G   Wert für Bezeichner 'name' liefern
#            MOD_ACTION_VAR_S   Wert für Bezeichner 'name' eintragen
#
# 'image'    (Gimp-Objekt)
#            (Nur bei MOD_ACTION_EXEC und MOD_ACTION_VAR_G)
#            Aktuelles Bild
#
# 'drawable' (Gimp-Objekt)
#            (Nur bei MOD_ACTION_EXEC und MOD_ACTION_VAR_G)
#            Aktuelle Ebene im Bild
#
# 'vars'     (dict)
#            Umgebungsvariable. Für eine Übersicht siehe in 'gic.py' unter
#            VAR_...
#
# 'name'     (str)
#            Bei ...
#            MOD_ACTION_VAR_G,
#            MOD_ACTION_VAR_S:  Variablennamen
#            MOD_ACTION_POST:   ID-Name
#            MOD_ACTION_EXEC:   ID-Name
#            MOD_ACTION_PTYPE:  ID-Name
#            MOD_ACTION_PSPLIT: ID-Name
#            MOD_ACTION_PDEF:   ID-Name
#            MOD_ACTION_PVAL:   ID-Name
#            MOD_ACTION_PMMS:   ID-Name
#            MOD_ACTION_EVT:    Ereignis-ID.
#            MOD_ACTION_DYNID:  Name, der überprüft werden soll
#
# 'param'    (list)
#            MOD_ACTION_TRANS_L und MOD_ACTION_TRANS_R
#
#               Siehe MOD_ACTION_TRANS_P_... (z.B. MOD_ACTION_TRANS_P_VALUE)
#
#            MOD_ACTION_EXEC und MOD_ACTION_VAR_S:
#
#               Auflistung der Kommando-Parameter-Werte in den bei
#               MOD_ACTION_PTYPE angegebenen Typen. Z.B. MOD_PTYP_INT
#
#            MOD_ACTION_EVT
#
#               [0] (int) Ereignis-ID
#               [1] Parameter
#
# 'result'   Je nach 'action' liefert das Modul hierüber ein Ergebnis zurück
#
#            MOD_ACTION_EVT
#
#               Je nach Ereignis
#
#            MOD_ACTION_IDDOC (str oder str-list)
#
#               (str)      Info über ID
#               (str-list) [0] Kurze Info  [1] Lange Info
#
#            MOD_ACTION_ITYPE  (int-list,int)
#
#               Spezifizierung der bei MOD_ACTION_INIT zurückgelieferten IDs
#               MOD_ITYPE_... (z.B. MOD_ITYPE_CMD)
#
#               Wird als Ergebnis genau ein Typ zurückgeliefert, sollen alle
#               Ids von diesem Typ sein.
#
#               (*)
#
#            MOD_ACTION_LVL  (int)
#
#               Legt fest, in welcher Reihenfolge die Module initalisiert
#               werden sollen.
#
#               MOD__LVL_PRIO: Modul soll vor den anderen initalisiert
#                              werden.
#               MOD__LVL_NORM: (Vorgabe) Normale Priorität
#
#            MOD_ACTION_PDOC  (str-list)
#
#               Eine knappe Info über jeden Parameter
#               Oder [0] Kurze Info  [1] Lange Info
#               (*)
#
#            MOD_ACTION_DYNID (str)
#
#               Liefert den Namen der erkannten ID zurück
#               'None', wenn der Name nicht erkannt werden konnte
#
#            MOD_ACTION_INIT  (None,list)
#
#               Wenn das Modul erfolgreich initalisiert wurde, liefert es
#               ein Zeichenketten-Array zurück, das die IDs (z.B. Befehle und
#               Vars) enthält, die das Modul behandeln möchte.
#               Bei 'None' wird das Modul nicht weiter ausgeführt.
#
#               Spezielle Werte:
#                   MOD_GROUP_DYNID  (und MOD_ACTION_ITYPE auf GTYP_GROUP )
#                                    Das Modul möchte DYNID behandeln
#                MOD_GROUP_PIXTRANS  (und MOD_ACTION_ITYPE auf GTYP_GROUP )
#                                    Das Modul kann Pixel konvertieren
#
#               Soll eine ID bestimmte Flags (VIB_FLAGS) besitzen, muss die
#               entsprechende ID als list angegeben werden, bei der der erste
#               Eintrag die ID und der zweite die Flags enthält.
#
#
#            MOD_ACTION_EXIT  (bool)
#
#               True, wenn das Modul erfolgreich beendet wurde. Wenn der Wert
#               nicht versorgt wird, wird True angenommen.
#
#            MOD_ACTION_PTYPE (type-list)
#
#               Auflistung der Typen. Für jeden Parameter muss ein entsprechen-
#               der Eintrag existieren.
#               Mögliche Werte sind MOD_PTYP_INT, MOD_PTYP_STR, usw.
#               z.b. Bei 'MOD_PTYP_STR,MOD_PTYP_INT' haben wir 2 Parameter.
#               Der erste ist vom Typ Zeichenkette, der zweite vom Typ Integer)
#               Wird eine leere Liste oder 'None' zurückgeliefert, hat der
#               Befehl keine Parameter.
#
#            MOD_ACTION_PSPLIT (str-list)
#
#               Auflistung der Trennzeichen zwischen Parametern. Falls nichts
#               zurückgeliefert wird, wird das Standardzeichen für alle
#               Positionen verwendet.
#
#            MOD_ACTION_PDEF   (list)
#
#               Auflistung der Vorgabewerte unserer Parameter.
#               (*)
#
#            MOD_ACTION_EXEC
#
#               Je nach Befehl
#
#            MOD_ACTION_NAME  (str)
#
#               Das Modul soll seinen Namen als Zeichenkette zurückliefern
#
#            MOD_ACTION_VAR_G
#
#               Wert für Bezeichner
#
#           MOD_ACTION_VAR_T
#
#               Typ (GTYP) für Bezeichner abfragen
#
#            MOD_ACTION_PMMS
#
#               Liefert eine Liste über die Min-Max-Schrittweite einzelner Para-
#               meter. Ein gültiger Eintrag hat das Format:
#               [ Minimalwert, Maximalwert, Schrittweite ]
#               (*)
#
#            MOD_ACTION_PVAL
#
#               Liefert eine Liste über die Werte-Listen einzelner Para-
#               meter. Ein gültiger Eintrag hat das Format:
#               [ "Eintrag 1", "Eintrag 2", "Eintrag 3" ] (usw.)
#               (*)
#
#
#  (*) Entweder kann nichts zurückgeliefert werden, wenn kein Parameter dieses
#      Feature verwendet, oder es muß eine Liste zurückgeliefert werden, bei der
#      für jeden Parameter ein entsprechender Eintrag existiert. Einträge, die
#      das Feature nicht verwenden, müssen dann auf 'None' gesetzt werden.

# Modul-Aktionen
if action == MOD_ACTION_INIT:   # Initalisierung
    # Auflistung der von uns unterstützten IDs (z.B. Befehle)
    result = None#<--    Auflistung
elif action == MOD_ACTION_PTYPE:# Parameter-Typen für 'name' abfragen
    result = []#<--  Auflistung unserer Parameter (Typen)
elif action == MOD_ACTION_EXEC: # Wir sollen 'name' ausführen ....
    #DUMMY1#
    result = None               # Keine Info
elif action == MOD_ACTION_NAME: # Unseren Namen abfragen
    result = "DUMMY"            # Wir liefern ihn
elif action == MOD_ACTION_IDDOC:# ID-Kommentar
    result = "?"#<--    Kommentar
elif action == MOD_ACTION_PDOC: # Parameter-Kommentar
    result = ["Param1"]
