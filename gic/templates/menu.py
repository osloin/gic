#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Gimp-plugin
#
##BLOCK_BEGIN_1#
#
# Eine Vorlage für einen automatisch von 'gic_dlg_create_mnu.py' generierten
# Menüpunkt.
#
# osloin 9/2018
#
##BLOCK_END_1#
#
#
# GIMP Python


import sys
import os

# Suchpfad für import erweitern ...
sys.path.append( sys.argv[ 0 ][ : sys.argv[ 0 ].rfind( os.sep ) ] +
                 os.sep + "gic" + os.sep + "lib" )

from gimpfu import *

import gic_lib.interpreter as gic
import gic_lib.locale   as LOC
import gic_lib.message  as msg_



################################################################################
# Menüfunktion
#
# 'image'
# 'drawable'
################################################################################
def DUMMY_FCT( image, drawable, DUMMY_DEC_PARAM ) :
    sgic = ""#<--GIC
    #BLOCK_BEGIN_2#
    param = []#<--PARAM
    types = []#<--TYPES
    qms   = []#<--QMS
    optio = []#<--OPTIO
    separ = []#<--SEPAR

    # Alle Parameter in gic-Code konvertieren
    i = 0
    for p in param:

        # Trennzeichen ...
        if i == 0:
            sgic += " "
        else:
            sgic += separ[ i - 1 ]    # + Parameter-Trennzeichen

        # Falls Optionen zur Auswahl standen, müssen wir die Auswahl (int-Index)
        # zurückkonvertieren
        if optio[ i ] != None:
            para = optio[ i ][ p ]
        else:
            para = p

        # Je nach Typ ...
        if types[ i ] == str:           # Zeichenkette?

            # Ob Zeichenkette mit Anführungzeichen umschlossen werden soll
            if qms[ i ]:
                sgic += gic.GIC_QM + para + gic.GIC_QM
            else:
                sgic += para

        else:                           # Keine Zeichenkette ...
            sgic += str( para )

        i += 1

    #BLOCK_END_2#

    # 'sgic' Ausführen
    gic.run_direct( sgic, [ image, drawable ] )




#####################
# Gimp-Registrierung
#####################
register(
    "python_fu_DUMMY_FCT",
    "DUMMY_INFO",
    "DUMMY_INFO",
    "osloin",
    "osloin",
    "2018",
    "<Image>/"+LOC.MNU_TOP+"/"+"DUMMY_LABEL",
    "RGB*, GRAY*",
    [
        (PF_STRING,"DUMMY_PARAM_NAME","DUMMY_PARAM_LABEL","DUMMY_PARAM_VALUE")
    ],
    [],
    DUMMY_FCT
    #DUMMY_MNU2#
    )

main()
