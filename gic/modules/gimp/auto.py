#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# Befehle zur automatischen Bildanpassung
#
#
# LOC.CMD_NORMALIZE / "NOR"
#
# Normalisierung
#
#
# LOC.CMD_WBALANCE / "AWA"
#
# Weißabgleich


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_NORMALIZE, LOC.CMD_WBALANCE ]

elif action == MOD_ACTION_PTYPE:
    result = []   # Beide Befehle haben keine Parameter

elif action == MOD_ACTION_EXEC:

   # Normalisierung
   if name == LOC.CMD_NORMALIZE:
      pdb.plug_in_normalize( image, drawable )
   # Weißabgleich
   elif name == LOC.CMD_WBALANCE:
      pdb.gimp_levels_stretch( drawable )

elif action == MOD_ACTION_EXIT:
   result = True

elif action == MOD_ACTION_NAME:
   result = "auto_py"

elif action == MOD_ACTION_IDDOC:

   if name == LOC.CMD_NORMALIZE:
       result = LOC.IDDOC_NORMALIZE
   elif name == LOC.CMD_WBALANCE:
       result = LOC.IDDOC_WBALANCE
