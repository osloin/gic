#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# Stellt einige (schreibgeschützte) Werte / Variable zur Verfügung
#
# LOC.VAR_W / "breite"      Die Breite des aktuellen Bildes
# LOC.VAR_H / "hoehe"       Die Höhe des aktuellen Bildes
# LOC.VAR_DPIX / "dpix"     X-DPI des aktuellen Bildes
# LOC.VAR_DPIY / "dpiy"     Y-DPI des aktuellen Bildes
# LOC.VAR_F_EXT / "ext"     Dateierweiterung
# LOC.VAR_F_NAME / "name"   Dateiname
# LOC.VAR_F_PATH / "Pfad"   Pfad

# Modul-Aktionen
if action == MOD_ACTION_INIT:

    # Wir registrieren unsere Symbole
    result = [LOC.VAR_W,
              LOC.VAR_H,
              LOC.VAR_DPIX,
              LOC.VAR_DPIY,
              LOC.VAR_F_EXT,
              LOC.VAR_F_NAME,
              LOC.VAR_F_PATH ]

elif action == MOD_ACTION_ITYPE:
    result = [MOD_ITYPE_MODVAR,
              MOD_ITYPE_MODVAR,
              MOD_ITYPE_MODVAR,
              MOD_ITYPE_MODVAR,
              MOD_ITYPE_MODVAR,
              MOD_ITYPE_MODVAR,
              MOD_ITYPE_MODVAR ]

elif action == MOD_ACTION_VAR_G:

    if   name == LOC.VAR_W:
        result = pdb.gimp_image_width( image )

    elif name == LOC.VAR_H:
        result = pdb.gimp_image_height( image )

    elif name == LOC.VAR_DPIX:
        xresolution, yresolution = pdb.gimp_image_get_resolution( image )
        result = xresolution

    elif name == LOC.VAR_DPIY:
        xresolution, yresolution = pdb.gimp_image_get_resolution( image )
        result = yresolution

    # Name, Pfad oder Erweiterung?
    elif name in [ LOC.VAR_F_NAME, LOC.VAR_F_PATH, LOC.VAR_F_EXT ]:

        # Parameter für extract() ermitteln ...
        if name == LOC.VAR_F_NAME:  # Dateiname
            extract_para = fil_.FE_NAME

        elif name == LOC.VAR_F_EXT: # Dateierweiterung
            extract_para = fil_.FE_DOTEXT

        else:                       # Dateipfad
            extract_para = fil_.FE_PATH_SEP

        # Name des aktuellen Bildes holen
        fn = pdb.gimp_image_get_filename( image )

        # Wenn kein Name verfügbar ist, testen wir auf kopierten Namen
        if fn == None:

            # Dateiname des zuletzt duplizierten Bildes vorhanden?
            fn = vars_get_none( vars, MOD.VAR_IMG_FNAME )

        # Gewünschte Info aus Namen holen
        if fn != None:
            result = fil_.extract( fn, extract_para )

elif action == MOD_ACTION_PTYPE:

    if   name == LOC.VAR_W:
        result = [GTYP_X]
    elif name == LOC.VAR_H:
        result = [GTYP_Y]
    elif name == LOC.VAR_DPIX:
        result = [GTYP_X]
    elif name == LOC.VAR_DPIY:
        result = [GTYP_Y]
    elif name == LOC.VAR_F_EXT:
        result = [GTYP_STR]
    elif name == LOC.VAR_F_NAME:
        result = [GTYP_STR]
    elif name == LOC.VAR_F_PATH:
        result = [GTYP_PATH]

elif action == MOD_ACTION_PDOC:

    if name == LOC.VAR_W:
        result = [LOC.IDDOC_VAR_W]
    elif name == LOC.VAR_H:
        result = [LOC.IDDOC_VAR_H]
    elif name == LOC.VAR_DPIX:
        result = [LOC.IDDOC_VAR_DPIX_H]
    elif name == LOC.VAR_DPIY:
        result = [LOC.IDDOC_VAR_DPIY_H]
    elif name == LOC.VAR_F_EXT:
        result = [LOC.IDDOC_VAR_F_EXT]
    elif name == LOC.VAR_F_NAME:
        result = [LOC.IDDOC_VAR_F_NAME]
    elif name == LOC.VAR_F_PATH:
        result = [LOC.IDDOC_VAR_F_PATH]

elif action == MOD_ACTION_VAR_S:

    # Alle sind schreibgeschützt - Exception!
    evt( vars, ERR_001, [ name, str( param[0] ) ] )

elif action == MOD_ACTION_IDDOC:

    if name == LOC.VAR_W:
        result = LOC.IDDOC_VAR_W
    elif name == LOC.VAR_H:
        result = LOC.IDDOC_VAR_H
    elif name == LOC.VAR_DPIX:
        result = LOC.IDDOC_VAR_DPIX_H
    elif name == LOC.VAR_DPIY:
        result = LOC.IDDOC_VAR_DPIY_H
    elif name == LOC.VAR_F_EXT:
        result = LOC.IDDOC_VAR_F_EXT
    elif name == LOC.VAR_F_NAME:
        result = LOC.IDDOC_VAR_F_NAME
    elif name == LOC.VAR_F_PATH:
        result = LOC.IDDOC_VAR_F_PATH

elif action == MOD_ACTION_NAME:
    result = "vars_py" # Unseren Name

elif action == MOD_ACTION_LVL:
    result = MOD_LVL_PRIO
