#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 1/2019
#
#
# LOC.CMD_LYR_TRANS / "EBT"
#
# Ebenentransparenz einstellen
#
#  [0] (str)    Betroffene Ebene
#  [1] (float)  0 ... 100


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_LYR_TRANS ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_STR,
               MOD_PTYP_FLOAT ]

elif action == MOD_ACTION_PDEF:  # Vorgabewerte

    result = [  None, 0.0 ]

elif action == MOD_ACTION_EXEC: # Ausführen ....

    # Ebenen
    lay = pdb.gimp_image_get_layer_by_name( image, param[ 0 ] )

    # Transparenz-Wert aus Parametern übernehmen
    opacity = param[ 1 ]

    # neue Position zuweisen
    pdb.gimp_layer_set_opacity( lay, opacity )

elif action == MOD_ACTION_NAME:
   result = "layer_trans_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_LYR_TRANS

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_LYR_TRANS

elif action == MOD_ACTION_PMMS:
    result = [ None, [ 0, 100, 1 ] ] # Wertebereich unseres Parameter
