#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# LOC.CMD_SEL / "AUP"
#
# Maske aus aktiven Pfad erzeugen (und zuschneiden). Eine alternative Schreib-
# weise ist ein beliebiger Name, der einen Doppelpunkt beinhaltet
#
# [0] (bool) Autom. Zuschneiden [True]
#
#
# LOC.CMD_SELCROP / "ZUS" / "Z"
#
# Auf Maske zuschneiden
#
#
# LOC.CMD_SELCUT / "AUA"
#
# Auswahl ausschneiden (Zwischenablage)
#
# [0] (str) Name ["gic"]
#
#
# LOC.CMD_SELCOPY / "AUK"
#
# Auswahl kopieren (Zwischenablage)
#
# [0] (str) Name ["gic"]
#
#
# LOC.CMD_SELNONE / "AUS"
#
# Auswahl aufheben
#
#
# LOC.CMD_SELPASTE / "AUE"
#
# Auswahl einfügen (Zwischenablage)
#
# [0] (int)  X
# [1] (int)  Y
# [2] (str)  Name ["gic"]
#
#
# LOC.CMD_SELRCTRATIO / "AUM"
#
# Größtes Rechteck mit einem bestimmten Seitenverhältnis auswählen
#
# [0] (int)  Seitenverhältnis, Breite  z.B. 16
# [1] (int)  Seitenverhältnis, Höhe    z.B.  9
# [2] (bool) Autom. Zuschneiden [True]


# Aktionen
if action == MOD_ACTION_INIT:

    result = [ MOD_GROUP_DYNID,
               LOC.CMD_SELCROP,
               LOC.CMD_SEL,
               LOC.CMD_SELCOPY,
               LOC.CMD_SELCUT,
               LOC.CMD_SELNONE,
               LOC.CMD_SELPASTE,
               LOC.CMD_SELRCTRATIO ]

elif action == MOD_ACTION_ITYPE:

    result = [ MOD_ITYPE_GROUP,
               MOD_ITYPE_CMD_G,
               MOD_ITYPE_CMD_G,
               MOD_ITYPE_CMD_G,
               MOD_ITYPE_CMD_G,
               MOD_ITYPE_CMD_G,
               MOD_ITYPE_CMD_G,
               MOD_ITYPE_CMD_G ]

elif action == MOD_ACTION_PTYPE:

    if name == LOC.CMD_SEL:
        result = [ MOD_PTYP_BOOL_YN ]

    elif name == LOC.CMD_SELCOPY or name == LOC.CMD_SELCUT:
        result = [ MOD_PTYP_LABEL ]

    elif name == LOC.CMD_SELPASTE:
        result = [ MOD_PTYP_X,
                   MOD_PTYP_Y,
                   MOD_PTYP_LABEL ]

    elif name == LOC.CMD_SELRCTRATIO:
        result = [ MOD_PTYP_INT,
                   MOD_PTYP_INT,
                   MOD_PTYP_BOOL_YN ]

    else:
        result = []

elif action == MOD_ACTION_PDEF:

    if name == LOC.CMD_SEL:
        result = [ True ]

    elif name == LOC.CMD_SELCOPY or name == LOC.CMD_SELCUT:
        result = [ "gic" ]

    elif name == LOC.CMD_SELPASTE:
        result = [ 0, 0, "gic" ]

    elif name == LOC.CMD_SELRCTRATIO:
        result = [ 16, 9, True]

elif action == MOD_ACTION_EXEC:

    # Auf Auswahl zuschneiden: Vorgabe Nein
    autocrop = False

    # Auf Auswahl zuschneiden?
    if name == LOC.CMD_SELCROP:
        autocrop = True

    # Auswahl aus Pfad?
    elif name == LOC.CMD_SEL:

        # CHANNEL_OP_REPLACE / 2
        pdb.gimp_image_select_item( image, \
                                    CHANNEL_OP_REPLACE, \
                                    pdb.gimp_image_get_active_vectors( image ) )
        # Autom. zuschneiden?
        if param[ 0 ]:
            autocrop = True

    # Auswahl kopieren (Zwischenablage)
    elif name == LOC.CMD_SELCOPY:
        # ggf. vorhandenen Puffer entfernen
        gim_.buffer_delete( param[ 0 ] )
        # Unter dem angegebenen Namen in die Zwischenablage kopieren
        pdb.gimp_edit_named_copy( drawable, param[ 0 ] )

    # Auswahl ausschneiden (Zwischenablage)
    elif name == LOC.CMD_SELCUT:
        # ggf. vorhandenen Puffer entfernen
        gim_.buffer_delete( param[ 0 ] )
        # Unter dem angegebenen Namen in die Zwischenablage kopieren / ausschn.
        pdb.gimp_edit_named_cut( drawable, param[ 0 ] )

    # Auswahl aufheben
    elif name == LOC.CMD_SELNONE:
        pdb.gimp_selection_none( image )

    # Auswahl einfügen (Zwischenablage)
    elif name == LOC.CMD_SELPASTE:
        # Parameter in Variable übernehmen
        x = param[ 0 ]
        y = param[ 1 ]
        n = param[ 2 ]
        # Zwischenablage als Ebene einfügen
        drw_paste = pdb.gimp_edit_named_paste( drawable, n, FALSE )
        # Neue Ebene zur Position verschieben
        pdb.gimp_layer_set_offsets( drw_paste, x, y )
        # Ebene verankern
        pdb.gimp_floating_sel_anchor( drw_paste )

    # Auswahl mit bestimmten Seitenverhältnis
    elif name == LOC.CMD_SELRCTRATIO:

        # Bildabmessungen
        iw = pdb.gimp_image_width( image )
        ih = pdb.gimp_image_height( image )

        # Größtes Rechteck mit bestimmten Seitenverhältnis ermitteln
        w , h = rct_.max_ratio( iw, ih, param[ 0 ] / float( param[ 1 ] ) )

        # Zentrieren
        rct = rct_.align( [ 0, 0, w, h ], [ 0, 0, iw, ih ] )

        # Rechteckige Auswahl
        pdb.gimp_image_select_rectangle( image,\
                                         CHANNEL_OP_REPLACE,\
                                         rct[ 0 ], rct[ 1 ], rct[ 2 ], rct[ 3 ]
                                       )

        # Autom. zuschneiden?
        if param[ 2 ]:
            autocrop = True

    # Auf Auswahl zuschneiden?
    if autocrop:
        bresult, x1, y1, x2, y2 = pdb.gimp_selection_bounds( image )
        if bresult:
             pdb.gimp_image_crop( image, x2 - x1, y2 - y1, x1, y1 )


elif action == MOD_ACTION_DYNID:
    if name.find( LOC.CMD_SELCROP_1 ) != -1:
       result = LOC.CMD_SEL

elif action == MOD_ACTION_NAME:
    result = "sel_py"

elif action == MOD_ACTION_IDDOC:

    if name == LOC.CMD_SELCROP:
        result = LOC.IDDOC_CROP

    elif name == LOC.CMD_SEL:
        result = LOC.IDDOC_MASK

    elif name == LOC.CMD_SELCOPY:
        result = LOC.IDDOC_SELCOPY

    elif name == LOC.CMD_SELCUT:
        result = LOC.IDDOC_SELCUT

    elif name == LOC.CMD_SELNONE:
        result = LOC.IDDOC_SELNONE

    elif name == LOC.CMD_SELPASTE:
        result = LOC.IDDOC_SELPASTE

    elif name == LOC.CMD_SELRCTRATIO:
        result = LOC.IDDOC_SELRCTRATIO

elif action == MOD_ACTION_PDOC:

    if name == LOC.CMD_SEL:
        result = LOC.PDOC_SEL

    elif name == LOC.CMD_SELCOPY:
        result = LOC.PDOC_SELCOPY

    elif name == LOC.CMD_SELCUT:
        result = LOC.PDOC_SELCUT

    elif name == LOC.CMD_SELPASTE:
        result = LOC.PDOC_SELPASTE

    elif name == LOC.CMD_SELRCTRATIO:
        result = LOC.PDOC_SELRCTRATIO

elif action == MOD_ACTION_PMMS:

    if name == LOC.CMD_SELRCTRATIO:
        result = [ [1,16,1], [1,16,1], None] # Unsere 2 Parameter
