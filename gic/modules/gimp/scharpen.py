#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# LOC.CMD_SHARPEN / "SCH"
#
# Schärft das aktuelle Bild
#
# ([0] (int) Prozentuale Schärfe, 0 ... 99 ) [10]


# Die Konstanten
C_SHARPEN = 10      # Vorgabewert für Schärfe.


# Aktionen
if action == MOD_ACTION_INIT:
   result = [LOC.CMD_SHARPEN]

elif action == MOD_ACTION_PTYPE:
    result = [MOD_PTYP_INT]

elif action == MOD_ACTION_PDEF:
    result = [C_SHARPEN]

elif action == MOD_ACTION_EXEC:
    sharpen = param[0]
    pdb.plug_in_sharpen( image, drawable, sharpen )

elif action == MOD_ACTION_EXIT:
   result = True

elif action == MOD_ACTION_NAME:
   result = "sharpen_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_SHARPEN

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_SHARPEN

elif action == MOD_ACTION_PMMS:
    result = [[0,99,1]]     # Wertebereich unserer Parameter
