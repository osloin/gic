#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# LOC.CMD_DEL / "EXB"
#
# Entfernt das aktuelle Bild (sofern es per gic erzeugt wurde ). Der
# Befehl wird autom. aufgerufen und muss daher nicht direkt in gic-Code
# verwendet werden.
#
#
# EVT_014 / Ressourcen freigeben
# EVT_521 / Bild geladen
#
# Um zu überprüfen, ob noch ein Bild im Speicher ist


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_DEL, MOD_GROUP_EVT % 14, MOD_GROUP_EVT % 521 ]

elif action == MOD_ACTION_EXEC:          # Ausführen ....

   # Bild aus 'vars' holen
   # img = vars_get( vars, VAR_RUN_PARA )[ MOD.VAR_RUN_PARA_IMG ]

   # Das zuletzt von gic erzeugte Bild holen
   img = vars_get_none( vars, MOD.VAR_IMG )

   # Zusatz-Meldung für Ereignis
   evtmsg = ""

   # Bild-Status holen
   img_state = vars_get( vars, MOD.VAR_IMG_STATE )

   # Wenn es ein sichtbares Bild ist, wird es durch Entfernen des 'display'
   # per 'gimp_display_delete()' entfernt.
   # Ansonsten geschieht es durch 'gimp_image_delete()'.
   if img_state == 1:                     # Sichtbares Bild?

       # 'Display' holen und entfernen
       display = vars_get( vars, MOD.VAR_DISPLAY )      # holen
       if pdb.gimp_display_is_valid( display ) == TRUE: # Gültig?
           pdb.gimp_display_delete( display )           # Löschen

       # Eintrag aus 'vars' Entfernen
       vars_del( vars, MOD.VAR_DISPLAY )

       # Zusatz-Meldung
       evtmsg = "(DIS)"

   # Noch ein unsichtb. geladenes oder dupliziertes Bild vorhanden?
   elif img_state == 2 or img_state == 3:

       # Bild entfernen
       pdb.gimp_image_delete( img )

       # Zusatz-Meldung
       evtmsg = "(DEL)"

   else:
       evtmsg = LOC.ERROR

   # Ereignis: Bild entfernt
   evt( vars, EVT_528, evtmsg )

   # Status: Unbekannt (durch ext. Aufruf)
   vars_set( vars, MOD.VAR_IMG_STATE, 0 )

   # Wir setzen 'Image' und 'Drawable' wieder auf die ursprünglichen Werte
   param_bak  = vars_get( vars, VAR_RUN_PARA_BAK )
   param      = vars_get( vars, VAR_RUN_PARA )
   param[ 0 ] = param_bak[ 0 ]  # Image
   param[ 1 ] = param_bak[ 1 ]  # Drawable
   vars_set( vars, VAR_RUN_PARA, param )

# Ereignis EVT_014 (Ressourcen freigeben) und EVT_521 (Bild geladen)
elif action == MOD_ACTION_EVT:

    # Status des letzten Bildes holen
    img_state = vars_get( vars, MOD.VAR_IMG_STATE )

    # Noch ein unsichtb. geladenes oder dupliziertes Bild vorhanden?
    if img_state == 2 or img_state == 3:

        #d( vars, ["del_py (Noch was da!)", img_state ] )

        # Befehl zum entfernen des aktuellen Bildes ausführen
        run( LOC.CMD_DEL, vars )

elif action == MOD_ACTION_NAME:
   result = "del_py"

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_DEL

elif action == MOD_ACTION_ITYPE:
    result =[ MOD_ITYPE_CMD_G, MOD_ITYPE_GROUP, MOD_ITYPE_GROUP ]
