#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# LOC.CMD_EXEC_PATH / "DUP"
#
# Ausführen von qic-Code in Pfadname(n)
#
# [0] (str) Filter, um Pfad zu identifizieren
#           Der hier angegebene Text wird immer mit der ersten Zeichenkette im
#           Pfadnamen verglichen. z.B. "fkt1"; DRE 18000; "fkt2"
#
# [1] (str) Filtermodus-Modus
#           "gleich"    Alle Einträge, die Filter haben
#           "ungleich"  Alle Einträge, die Filter NICHT haben
#           "alle"      Alle Einträge (kein Filter wird verwendet)
#          ["exakt"]    Alle Einträge die Filter exakt entsprechen
#           "nexakt"    Alle Einträge die Filter NICHT exakt entsprechen
#
# [2] (bool) Bestimmt, ob der Pfad vor Ausführung ausgewählt werden soll
#          [True]: Aktuellen Pfad-Eintrag auswählen
#           False: Aktuellen Pfad-Eintrag NICHT auswählen
#
# [3] (str) gic, der ausgeführt werden soll, wenn kein entsprechender Pfad
#           gefunden wurde [""]

# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_EXEC_PATH ]

elif action == MOD_ACTION_PTYPE: # Unser Parameter
   result = [ MOD_PTYP_STR, MOD_PTYP_LABEL, MOD_PTYP_BOOL_YN, MOD_PTYP_SGIC ]

elif action == MOD_ACTION_PDEF:
    result = [ None, LOC.PVAL_EXEC_PATH_1[ 3 ], True, "" ]     # Vorgabewerte

elif action == MOD_ACTION_EXEC:         # Ausführen ....

    # Wie oft gic-Code in einem Pfadnamen ausgeführt wurde
    run_cnt = 0

    # vectors / Pfade
    num_vectors, vector_ids = pdb.gimp_image_get_vectors( image )

    # vectors / Pfade vorhanden?
    if num_vectors > 0:

        # Parameter
        filter       = param[ 0 ]
        fmode        = lis_.get_idx( LOC.PVAL_EXEC_PATH_1, param[ 1 ], 3, 1 )
        amode        = param[ 2 ]
        sgic_default = param[ 3 ]

        # Alle Vektoren / Pfade durchlaufen ...
        for id in vector_ids:

            vector = gimp.Vectors.from_id( id )
            item   = pdb.gimp_item_get_name( vector )

            # Code, der ausgeführt werden soll
            sgic = None

            # Bereich zwischen den ersten beiden Anführungszeichen als
            # Filterüberprüfungsbereich wählen und testen, ob Filter
            if filter != None:

                found  = 0 # (0) Nicht gefu. (1) Enthalten (2) Exakt

                # Suche im Filternamen nach Anführungszeichen
                pos_start = item.find( GIC_QM )

                # Anführungszeichen gefunden?
                if pos_start != -1:         # Ja - SK-Start Gefunden?

                    pos_end = item.find( GIC_QM, pos_start + 1 )

                    if pos_end != -1:       # ZK-Ende Gefunden?

                        # Bereich zwischen Anführungszeichen holen
                        extr = item[ pos_start + 1 : pos_end ]

                        if extr == filter:
                            found = 2       # Exakt

                        elif extr.find( filter ) != -1:
                            found = 1   # Enthalten

            # Je nach Modus
            if fmode == 2:
                sgic = item

            elif fmode == 0 and found != 0:
                sgic = item

            elif fmode == 1 and found == 0:
                sgic = item

            elif fmode == 3 and found == 2:
                sgic = item

            elif fmode == 4 and found != 2:
                sgic = item

            if sgic != None:

                # ggf. Pfad auswählen
                if amode == 0:
                    pdb.gimp_image_set_active_vectors( image, vector )

                # Ereignis: Pfad ausführen
                evt( vars, EVT_523, item )

                # Ausführen
                run( sgic, vars )

                # Anz der Ausführungen erhöhen
                run_cnt += 1

        # Wenn kein Pfad-gic ausgeführt wurde ...
        if run_cnt == 0 and len( sgic_default ):

            # Ausführen des als Parameter angegebenen Codes
            run( sgic_default, vars )

elif action == MOD_ACTION_NAME:
   result = "exec_path_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_EXEC_PATH

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_EXEC_PATH

# elif action == MOD_ACTION_PMMS:
#     result = [ None, None, [0,4,1], [0,1,1] ]

elif action == MOD_ACTION_PVAL:
    result = [ [], LOC.PVAL_EXEC_PATH_1, [], [] ]

elif action == MOD_ACTION_PDEF:
    result = [ None, None, True, None ]  # Vorgabewerte

elif action == MOD_ACTION_ITYPE:
    result = MOD_ITYPE_CMD
