#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# LOC.CMD_SATURATION / "SÄT" / "F"
#
# Farbsättigung (und Helligkeit) anpassen
#
#  [0] (int) Sättigung,  -100 ... +100 [0.0]
#  [1] (int) Helligkeit, -100 ... +100 [0.0]


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_SATURATION ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_INT, MOD_PTYP_INT ]

elif action == MOD_ACTION_PDEF:
    result = [ 0.0, 0.0 ]    # Vorgabewerte

elif action == MOD_ACTION_EXEC:
    saturation = param[0]
    lightness  = param[1]
    pdb.gimp_hue_saturation( drawable, 0, 0, lightness, saturation )

elif action == MOD_ACTION_EXIT:
   result = True

elif action == MOD_ACTION_NAME:
   result = "saturation_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_SATURATION

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_SATURATION

elif action == MOD_ACTION_PMMS:
    result = [ [-100,100,1], [-100,100,1] ] # Wertebereich unserer Parameter
