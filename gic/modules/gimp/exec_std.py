#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 1/2019
#
#
# LOC.CMD_EXEC_GIC / "GIC"
#
# Führt das im aktuelle Bild enthaltene gic aus. Enthält das Bild keinen
# entsprechenden Code, wird die Ausführung normal fortgesetzt
# Enthält 'MOD.VAR_POST_GIC' Code, wird dieser aber auf jeden Fall ausgeführt.
#
#
# EVT_537 / Vorbehandlung eines Bildes im Stapelbetrieb
#
# Veranlasst die Ausführung von "GIC"


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_EXEC_GIC, MOD_GROUP_EVT % 537 ]

elif action == MOD_ACTION_EXEC:          # Ausführen ....

    # DUP-Befehl zum Ausführen von Code im Gimp-Pfadnamen "gic" zusammenstellen
    # [], um LOC.IMG_EVT_GIC mit Anführungzeichen zu umschliessen
    sgic = code_create( LOC.CMD_EXEC_PATH, [ LOC.IMG_EVT_GIC ], [] )

    # DUP-Befehl ausführen
    run_param( sgic, [ image, drawable ], vars )

    # Ggf. Code aus 'MOD.VAR_POST_GIC' ausführen

    if vars_in( vars, MOD.VAR_POST_GIC ):           # Da?

        sgic = vars_get( vars, MOD.VAR_POST_GIC )   # Laden
        run( sgic ,vars )                       # Ausführen

elif action == MOD_ACTION_EVT:    # EVT_537 / Vorbehandlung im Stapelbetrieb

    # Bild aus Ereignis-Parameter holen
    img = param[ MOD_EVT_P_PARAM ]
    # img = image

    # Drawable
    drw = pdb.gimp_image_get_active_drawable( img )

    # DUP-Befehl zum Ausführen von Code im Gimp Pfadnamen "gic"
    # zusammenstellen
    # [], um LOC.IMG_EVT_GIC mit Anführungzeichen zu umschliessen
    sgic = code_create( LOC.CMD_EXEC_PATH, [ LOC.IMG_EVT_GIC ], [] )

    # DUP-Befehl ausführen
    run_param( sgic, [ img, drw ], vars )

    # Ggf. Code in 'MOD.VAR_POST_GIC' ausführen
    if vars_in( vars, MOD.VAR_POST_GIC ):           # Da?

        # gic-Code aus Var laden
        sgic = vars_get( vars, MOD.VAR_POST_GIC )

        # Ausführen
        run_param( sgic, [ img, drw ], vars )

elif action == MOD_ACTION_NAME:
   result = "exec_std_py"

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_EXEC_GIC

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_CMD_G, MOD_ITYPE_GROUP ]
