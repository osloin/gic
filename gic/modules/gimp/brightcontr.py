#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# LOC.CMD_BRIGHTNCONTR / "HEK"
#
# Helligkeit und Kontrast einstellen
#
# [0] (int) Helligkeit, -127 ... +127
# [1] (int) Kontrast,   -127 ... +127


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ MOD_GROUP_DYNID, LOC.CMD_BRIGHTNCONTR ]

elif action == MOD_ACTION_DYNID:
   if name.find( LOC.CMD_BRIGHTNCONTR_2 ) != -1:
       result = LOC.CMD_BRIGHTNCONTR

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_GROUP, MOD_ITYPE_CMD_G ]

elif action == MOD_ACTION_PTYPE:
    if name == LOC.CMD_BRIGHTNCONTR:
        result = [ MOD_PTYP_INT, MOD_PTYP_INT ]  # Unsere 2 Parameter

elif action == MOD_ACTION_PDEF:
    result = [ 0, 0 ]       # Vorgabewerte

elif action == MOD_ACTION_EXEC:
    pdb.gimp_brightness_contrast( drawable, param[ 0 ], param[ 1 ] )

elif action == MOD_ACTION_EXIT:
    result = True

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_BRIGHTNCONTR

elif action == MOD_ACTION_NAME:
    result = "brightcontr_py"

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_BRIGHTNCONTR

elif action == MOD_ACTION_PMMS:
    result = [[-127,127,1],[-127,127,1]] # Unsere 2 Parameter
