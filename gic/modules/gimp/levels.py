#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# Einen oder mehrere der Farbwert(e) anpassen
#
#
#  LOC.CMD_LEVELS / "FAR"
#
#  Farbwerte einstellen
#
#  [0] (int)   Niedriger Wert   0 ... 255 [0]
# ([1] (float) Gammawert      0,1 ... 10  [1.0]
# ([2] (int)   Hoher Wert       0 ... 255 [255]
#
#
# LOC.CMD_GAMMA / "GAM" / "G"
#
# Gammawert einstellen
#
# [0] (float)  Gammawert, 0,1 ... 10
#
#
# LOC.CMD_LEVELS_HI / "WEI"
#
# Hohen Farbwert einstellen
#
# [0] (int)    Hoher Wert, 0 ... 255

# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_LEVELS,
              LOC.CMD_GAMMA,
              LOC.CMD_LEVELS_HI ]

elif action == MOD_ACTION_PTYPE:

   if name == LOC.CMD_LEVELS:       # Farbwerte-Kommando?
        result = [ MOD_PTYP_INT, MOD_PTYP_FLOAT, MOD_PTYP_INT ] # Unsere Param.

   elif name == LOC.CMD_GAMMA:      # Gamma-Kommando?
        result = [ MOD_PTYP_FLOAT ] # Unser Parameter

   elif name == LOC.CMD_LEVELS_HI:  # Hi-Wert-Kommando?
        result = [ MOD_PTYP_INT ]   # Unser Parameter

elif action == MOD_ACTION_PDEF:     # Vorgabewerte

    if name == LOC.CMD_LEVELS:      # Farbwerte-Kommando?
        result = [ 0, 1.0, 255 ]    # Vorgabewerte

elif action == MOD_ACTION_EXEC:

   low = 0                          # Std-Low-Wert

   if name == LOC.CMD_LEVELS:       # Farbwerte-Kommando?
        pdb.gimp_levels( drawable,
                         HISTOGRAM_VALUE,
                         param[0],
                         param[2],
                         param[1],
                         0,
                         255 )

   elif name == LOC.CMD_GAMMA:      # Gamma-Kommando?
        high = 255                  # Std-Hi-Wert
        pdb.gimp_levels( drawable,
                         HISTOGRAM_VALUE,
                         low,
                         high,
                         param[0],
                         0,
                         255 )

   elif name == LOC.CMD_LEVELS_HI:  # Hi-Wert-Kommando?
        gamma = float(1)            # Std-Gamma-Wert
        pdb.gimp_levels( drawable,
                         HISTOGRAM_VALUE,
                         low,
                         param[0],
                         gamma,
                         0,
                         255 )

elif action == MOD_ACTION_NAME:
   result = "levels_py"

elif action == MOD_ACTION_PDOC:

   if name == LOC.CMD_LEVELS:       # Farbwerte-Kommando?
        result = LOC.PDOC_LEVELS

   elif name == LOC.CMD_GAMMA:      # Gamma-Kommando?
        result = LOC.PDOC_GAMMA

   elif name == LOC.CMD_LEVELS_HI:  # Hi-Wert-Kommando?
        result = LOC.PDOC_LEVELS_HI

elif action == MOD_ACTION_IDDOC:

   if name == LOC.CMD_LEVELS:       # Farbwerte-Kommando?
        result = LOC.IDDOC_LEVELS

   elif name == LOC.CMD_GAMMA:      # Gamma-Kommando?
        result = LOC.IDDOC_GAMMA

   elif name == LOC.CMD_LEVELS_HI:  # Hi-Wert-Kommando?
        result = LOC.IDDOC_LEVELS_HI

elif action == MOD_ACTION_PMMS:     # Wertebereiche unserer Parameter

   if name == LOC.CMD_LEVELS:       # Farbwerte-Kommando?
        result = [ [0,255,1], [0.1,10,0.1], [0,255,1] ]

   elif name == LOC.CMD_GAMMA:      # Gamma-Kommando?
        result = [ [0.1, 10, 0.1] ]

   elif name == LOC.CMD_LEVELS_HI:  # Hi-Wert-Kommando?
        result = [ [0, 255, 1] ]
