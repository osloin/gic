#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# LOC.CMD_TXT / "TXT" / "T"
#
# Textausgabe an einer bestimmten Position mit einem bestimmten Font in der
# aktuellen Farbe (sofern keine Farbe angegeben wurde)
#
# [0] (float) x
# [1] (float) y
# [2] (str)   Ausgabetext   [""]
# [3] (float) size          [20.0]
# [4] (str)   Font-Name     ["Sans"]
# [5] (int)   Rahmenbreite  [0]
# [6] (str)   Name          [""]
# [7] (str)   Farbe         [""]
#
# z.B. TXT 100/100/"Text"/30


# Aktionen
if action == MOD_ACTION_INIT:
   result = [LOC.CMD_TXT]

elif action == MOD_ACTION_PTYPE:  # Unsere Parameter
    result = [ MOD_PTYP_X,
               MOD_PTYP_Y,
               MOD_PTYP_STR,
               MOD_PTYP_FLOAT,
               MOD_PTYP_FONT,
               MOD_PTYP_INT,
               MOD_PTYP_STR,
               MOD_PTYP_COLOR ]

elif action == MOD_ACTION_PDEF:
    result = LOC.PDEF_TXT      # Vorgabewerte

elif action == MOD_ACTION_EXEC:                     # Ausführen ....

    # Farbnamen aus Parametern
    col_name = param[ 7 ]

    # Farbname angegeben?
    if len( col_name ) > 0:

        # Akt. Farbe retten
        col_bak = pdb.gimp_context_get_foreground()

        # Farbwert aus Variable holen
        col = vars_get( vars, col_name )

        # Akt. Farbe setzen
        pdb.gimp_context_set_foreground( ( col[0], col[1], col[2] ) )

    else:
        col_bak = None

    # Texterzeugung ...
    lay = pdb.gimp_text_fontname( image,
                                  drawable,
                                  param[ 0 ], # x,FLOAT
                                  param[ 1 ], # y,FLOAT
                                  param[ 2 ], # txt, STRING
                                  param[ 5 ], # border
                                  TRUE,       # Antial.
                                  param[ 3 ], # size, FLOAT
                                  PIXELS,
                                  param[ 4 ]  # Font, STRING
                                 )

    # Layer/Text verankern ...
    if( lay != None ):

        pdb.gimp_floating_sel_to_layer( lay )

        # Ggf. Ebenenname festlegen
        if len( param[ 6 ] ) > 0:

            pdb.gimp_item_set_name( lay, param[ 6 ] )

    # ALte Farbe wiederherstellen
    if type( col_bak ) != type( None ):
        
        pdb.gimp_context_set_foreground( col_bak )

elif action == MOD_ACTION_NAME:
   result = "txt_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_TXT

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_TXT
