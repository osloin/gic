#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# LOC.CMD_SAVE / "SPE"
#
# Speichert / Exportiert das aktuelle Bild.
#
# Löst folgende Ereignisse aus: EVT_514, EVT_529, EVT_530 und EVT_033.
#
# [0] (str)   Pfad mit Bildnamen und Erweiterung (die das Datei-Format bestimmt
#             z.B. ".jpg", "xcf", "xcf.bz2" ). Falls der Bildname schon vergeben
#             ist, wird ein freien Name gesucht. Wenn kein Pfad angegeben wurde,
#             wird der aktuelle Dateinamen verwendet. Sollte auch das nicht mög-
#             lich sein, wird ein Name im Temporär-Verzeichnis von gic gebildet.
#             Es kann z.B. auch nur ".jpg" verwendet werden, um ein Bild, das
#             schon einen Dateinamen besitzt, in diesem Format zu speichern
#
# [2] (str)   gic-Code, der vor dem Speichern (aber nach EVT_514 und EVT_529)
#             ausgeführt werden soll [""]
#
# [3] (float) 0 ... 1, Qualität bei jpg, [1.0]
#
# [4] (str)   Kommentar bei jpg, [""]


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_SAVE ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_FILE,
               MOD_PTYP_SGIC,
               MOD_PTYP_FLOAT,
               MOD_PTYP_STR ]

elif action == MOD_ACTION_PDEF:
    result = [ "", "", 1.0, "" ]

elif action == MOD_ACTION_EXEC:

    # Erster Parameter enthält den Dateinamen
    filename = param[ 0 ]

    # Zweiter Parameter enthält gic
    sgic = param[ 1 ]

    # Wenn leerer Dateiname? (Namen aus Bild holen)
    if len( filename ) == 0:            # Leerer Dateiname?
        if image.filename != None:      # Dateiname im Bild vorhanden?
            filename = image.filename   # Als Vorgabe übernehmen
        else:                           # Auch kein Bildname ...
            filename = path_tmp_get( vars ) + "tmp.xcf"

    # Wenn nur eine Erweiterung angegeben wurde ...
    elif filename[ 0 ] == ".":

        # Dateiname des aktuellen Bildes vorhanden?
        if image.filename != None and len( image.filename ) > 1:
            # Dateinamen ohne Erweiterung holen und neue Erw. anhängen...
            filename = fil_.extract( image.filename, fil_.FE_PATH_NAME ) + \
                       filename
        else:
            # Dateiname des zuletzt duplizierten Bildes vorhanden?
            fnamedpl = vars_get_none( vars, MOD.VAR_IMG_FNAME )
            if fnamedpl != None:
                # Dateinamen ohne Erweiterung holen und neue Erw. anhängen...
                filename = fil_.extract( fnamedpl, fil_.FE_PATH_NAME ) + \
                           filename
            else:
                # Ereignis: Es konnte kein Dateiname ermittelt werden
                evt( vars, ERR_039 )

    # Überprüfen, ob Bildname schon existiert
    isf = fil_.isfile( filename )     # False: Noch nicht da

    # Vorgehensweise, wenn Bildname schon existiert
    if isf == True:
        # Namen erweitern
        filename = fil_.unused( filename )

    # Ereignis: Bild soll gespeichert werden
    evt_result = evt( vars, EVT_514, filename )

    # Wenn nicht durch Ereignis-Ergebnis abgebrochen wurde ...
    if evt_result != False:

        # Bild duplizieren
        imageNew = pdb.gimp_image_duplicate( image )

        # Ereignis: Bild wird gespeichert.
        # (Es können jetzt vorletzte Arbeitsschritte durchgeführt werden)
        evt( vars, EVT_529, imageNew )

        # Wenn kein Abbruchsignal, wird jetzt gespeichert ...
        if vars_get_signal( vars, MOD.VAR_SAVE_NO ) == None:

            # Ebenen zusammenfügen
            layerNew = pdb.gimp_image_merge_visible_layers( imageNew,
                                                            CLIP_TO_IMAGE )

            # Jetzt wird ggf. der gic-Code aus den Parametern angewandt
            if len( sgic ) > 0:         # Code da?

                # Ausführen
                run_param( sgic, [ imageNew, layerNew ] ,vars )


            # Speichern
            if filename[ -3 : ] == "jpg":

                # jpg-Parameter
                fQuality = param[ 2 ]
                sComment = param[ 3 ]

                # Als jpg speichern ...
                pdb.file_jpeg_save( imageNew, # Bild
                                    layerNew, # drawable
                                    filename, # filename
                                    filename, # raw_filename
                                    fQuality, # (quality)     0...1 Beste Quali
                                    0,        # (smoothing)   Keine Glättung
                                    1,        # (optimize)    Ja
                                    1,        # (progressive) Ja
                                    sComment, # (comment)
                                    2,        # (subsmp) 2 == 4:4:4 (Beste Q.)
                                    1,        # (baseline) Ja
                                    16,       # (restart)
                                    2         # (dct)         FLOAT (2)
                                  )

            else: # Nicht jpg speichern ...
                pdb.gimp_file_save( imageNew, layerNew, filename, filename )

            # Ereignis: Bild wurde gespeichert
            evt( vars, EVT_530, filename )

        else: # Abbruchsignal MOD.VAR_SAVE_NO ...

            # Ereignis: Bild-Speicherung wurde per Befehl verhindert
            evt( vars, EVT_033, filename )

        # Dupliziertes Bild wieder freigeben
        pdb.gimp_image_delete( imageNew )


elif action == MOD_ACTION_NAME:
   result = "save_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_SAVE

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_SAVE

elif action == MOD_ACTION_ITYPE:
    result = MOD_ITYPE_CMD_G
