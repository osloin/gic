#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# LOC.CMD_LOADLAYER / "LAE"
#
# Bild als Ebene laden
#
# [0] (str)     Dateiname
# [1] (float)   x                                                            [0]
# [2] (float)   y                                                            [0]
# [3] (float)   Auf diese Breite bringen  (*)                                [0]
# [4] (float)   Auf diese Höhe bringen    (*)                                [0]
# [5] (str)     Ebenenname                                                  [""]
#
# (*) Wenn beide Werte auf 0 gesetzt werden, wird die Orginalgröße verwendet.
#     Wenn nur einer der beiden Werte auf 0 gesetzt wird, wird versucht, das
#     Seitenverhältnis beizubehalten.


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_LOADLAYER ]

elif action == MOD_ACTION_PTYPE:
   result = [ MOD_PTYP_FILE,
              MOD_PTYP_X,
              MOD_PTYP_Y,
              MOD_PTYP_X,
              MOD_PTYP_Y,
              MOD_PTYP_STR
            ]

elif action == MOD_ACTION_EXEC:          # Ausführen ....

   # Parameter in lokale Variable übernehmen
   filename   = param[ 0 ]
   x          = param[ 1 ]
   y          = param[ 2 ]
   w          = param[ 3 ]
   h          = param[ 4 ]
   layer_name = param[ 5 ]

   # Bild als Layer laden
   layer_new = pdb.gimp_file_load_layer( image, filename )

   # Ereignis
   evt( vars, EVT_521, filename )

   # Layer aufnehmen
   pdb.gimp_image_insert_layer( image, layer_new, None, 0 )

   # Layer-Name versorgen
   if len( layer_name ) > 0:    # Name angegeben?
       pdb.gimp_item_set_name( layer_new, layer_name )

   # Namen des neuen Layers holen. (Auch für den Fall, dass wir ihn selbst
   # vergeben hatten, denn falls er schon vorhanden war, wurde er autom.
   # geändert)
   layer_name = pdb.gimp_item_get_name( layer_new )

   # Position zuweisen
   pdb.gimp_layer_set_offsets( layer_new, x, y )

   # Wenn nicht Breite und Höhe auf 0 gesetzt wurden ...
   if not (w == 0.0 and h == 0.0):

       # gic im Bild ausführen
       run_param( LOC.CMD_EXEC_GIC, [ image, layer_new ], vars )

       # Befehl zur Ebenenskalierung zusammenstellen
       cmd = code_create( LOC.CMD_SCALE, [ w, h, layer_name ] )

       # Skalierungsbefehl ausführen
       run( cmd, vars )


elif action == MOD_ACTION_NAME:
   result = "load_layer_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_LOADLAYER

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_LOADLAYER

elif action == MOD_ACTION_ITYPE:
    result = MOD_ITYPE_CMD_G

elif action == MOD_ACTION_PDEF:
    result = [ None, 0, 0, 0, 0, "" ]
