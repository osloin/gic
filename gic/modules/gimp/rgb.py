#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# LOC.CMD_RGB / "RGB"
#
# Globaler Variablen ein Farbwert zuweisen - bzw. Vorder- oder Hintergrund-
# farbe einstellen.
#
# [0] (str)     Variablenname - Erzeugt eine globele Variable mit diesem Namen
#               'vordergrund' Akt. Gimp-Vordergrundfarbe setzen
#               'hintergrund' Akt. Gimp-Hintergrundfarbe setzen
# [1] (float)   Rot
# [2] (float)   Grün
# [3] (float)   Blau


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_RGB ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_LABEL,
               MOD_PTYP_FLOAT,
               MOD_PTYP_FLOAT,
               MOD_PTYP_FLOAT ]

elif action == MOD_ACTION_EXEC:

    # Farben aus Parametern holen
    col_r = param[ 1 ]
    col_g = param[ 2 ]
    col_b = param[ 3 ]
    # Farbnamen aus Parametern holen
    col_name =param[ 0 ]

    # Ggf. autom. Farben setzen
    if col_name == LOC.VAR_COL_FORE:
        # Festlegen
        pdb.gimp_context_set_foreground( ( col_r, col_g, col_b ) )
    elif col_name == LOC.VAR_COL_BACK:
        # Festlegen
         pdb.gimp_context_set_background( ( col_r, col_g, col_b ) )
    
    # In vars eintragen ...
    vars_set( vars,
              col_name,
              [ col_r, col_g, col_b ],
              VIB_VALUE,
              GTYP_COLOR )

    #d( vars, str( pdb.gimp_context_get_foreground() ) )


elif action == MOD_ACTION_NAME:
   result = "rgb_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_RGB

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_RGB

elif action == MOD_ACTION_PMMS:
    result = [ None, [0,255,1], [0,255,1], [0,255,1] ] # Wertebereich

elif action == MOD_ACTION_PSPLIT:
    result = [ GIC_ASS, GIC_PARAM, GIC_PARAM ]    # Trenner
