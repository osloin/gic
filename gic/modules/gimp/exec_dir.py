#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# LOC.CMD_EXEC_DIR / "DUV"
#
# Bilder in einem Verzeichnis abarbeiten ( und ggf. gic-Code auf jedes Bild
# anwenden )
#
# Während der Ausführung stehen folgende Variable zur Verfügung:
# - 'ziel'   enthält einen möglichen Dateinamen (inkl. Pfad und Dateiendung)
#            Der Name wird aus dem Wert hinter 'TARGET' aus 'locale.py' ge-
#            bildet. (in Kleinbuchstaben). 'ziel' wird nur versorgt, wenn ein
#            Ziel-Pfad angegeben wurde.
# - 'posnr'  enthält eine laufende Nummer, Standardmäßig beginnend mit 1.
#           'VAR_POS' aus 'locale.py'
#
# [0] (sgic) Befehle, die auf jedes Bild angewandt werden sollen. SPE-Befehle
#            sollten bei Parameter 5 eingetragen werden.
#            Hinweis: Hochkomma in der Zeichenkette werden als Anführungzeichen
#            übersetzt [None]
# [1] (str)  Quell-Pfad [None]
# [2] (str)  Filter, z.B. "*.jpg" ["*.*"]
# [3] (str)  Ziel-Pfad [""]
#     (None) Ziel-var wird dann nicht versorgt
# [4] (str)  Dateityp der Ziel-Datei ["jpg"]
# [5] (sgic) Befehle, mit dem die bearbeiteten Bilder gespeichert werden
#            sollen. Der Vorgabewert veranlasst, dass das Bild unter dem
#            generierten Namen gespeichert wird ["SPE ziel"]


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_EXEC_DIR ]

elif action == MOD_ACTION_PTYPE:
   result = [ MOD_PTYP_SGIC,
              MOD_PTYP_PATH,
              MOD_PTYP_STR,
              MOD_PTYP_PATH,
              MOD_PTYP_STR,
              MOD_PTYP_SGIC ]

elif action == MOD_ACTION_PDEF:
    result = LOC.PDEF_EXEC_DIR # Vorgabewerte

elif action == MOD_ACTION_EXEC:          # Ausführen ....

   # Parameter in  Variable übernehmen
   pathSource   = param[ 1 ]
   filterSource = param[ 2 ]
   pathDest     = param[ 3 ]
   extDest      = param[ 4 ]

   sgic_gra     = param[ 0 ]
   sgic_sav     = param[ 5 ]

   # Ziel und Quelle dürfen nicht identisch sein
   # if pathSource == pathDest:
   #    evt( vars, ERR_033, pathSource, "exec_batch.py" )

   # Wenn 'SPE "ziel"'' aber kein Ziel-Pfad
   if pathDest == "" and sgic_sav == LOC.PDEF_EXEC_DIR[ 5 ]:
       sgic_sav = ""

   # Speicher-Anweisungen übertragen und Anführungszeichen entfernen
   sgic_sav = str_.strip_qm( sgic_sav )

   # Ggf. Grafik-Anweisungen in 'MOD.VAR_POST_GIC' übertragen.
   # 'MOD.VAR_POST_GIC' wird (ggf. nach der Selbstanpassung von Bildern)
   #  vom 'LOC.CMD_EXEC_GIC' / "GIC" - Befehl ausgeführt
   if sgic_gra != None and len( sgic_gra ) > 0:
       vars_set( vars, MOD.VAR_POST_GIC, str_.strip_qm( sgic_gra ) )

   # Ob letztes Zeichen das von Betriebssystem verwendetes Trennzeichen ist
   if pathSource[-1] != os.sep: # Nein?
      pathSource += os.sep      # ergänzen!

   # pathDest überprüfen
   if pathDest != None:

       if len( pathDest ) > 0:  # Zielverzeichnis vorhanden?

           # Ob letztes Zeichen das von Betriebssystem verwendete Zeichen ist
           if pathDest[-1] != os.sep:
              pathDest += os.sep

           # Datei-Erweiterung des Ziels
           extDest = extDest.lower()    # in Kleinbuchstaben
           if extDest == "bz2":
              extDest = "xcf.bz2"

       else:    # Kein Zielverzeichnis ...
           pathDest = None

   # Suchpfad mit Wildcards ...
   source = pathSource + filterSource

   # Konvertierung
   source = fil_.convert( source )

   # Betroffenen Dateien ermitteln
   files = glob.glob( source )

   # Die Dateinamen alphabetisch sortieren
   if len( files ) > 1:

       files.sort()

       # Ereignis: Stapelverarbeitung wird gestartet
       evt_result = evt( vars, EVT_027, [ files, sgic_gra, len( files ) ] )

       # Ggf. Dateiliste durch Ereignis-Ergebnis ersetzen
       if type( evt_result ) == list:
           files = evt_result

   # Keine Dateien?
   if len( files ) < 1:

       # Ereignis: Keine Dateien gefunden
       evt( vars, EVT_038, source )

   # Alle Dateien durchlaufen...
   for filename in files:

       # Laufvariable anpassen / inkrementieren
       vars_inc( vars, LOC.VAR_POS )

       filename = str( filename )

       # LAD-Befehl zum unsichtbaren Laden eines Bildes zusammenstellen
       # 0: Ersten Parameter mit Anführungszeichen umschliessen
       cmd = code_create( LOC.CMD_LOAD, [ filename, LOC.NO ], 0 )

       # Lade-Befehl ausführen
       run( cmd, vars )

       # Der Lade-Befehl hat das geladene Bild in VAR_RUN_PARA
       # gespeichert ...
       img = vars_get( vars, VAR_RUN_PARA )[ MOD.VAR_RUN_PARA_IMG ]

       # Wurde Bild geladen?
       if img != None:

          # Ebenen zusammenfügen
          pdb.gimp_image_merge_visible_layers( img, CLIP_TO_IMAGE )

          # Speichern vorbereiten
          drw = pdb.gimp_image_get_active_drawable( img )

          # Zielname zusammenstellen ...
          if pathDest != None:

              filename_dest = pathDest + \
                              fil_.extract( filename, fil_.FE_NAME ) + "." + \
                              extDest

          # Ereignis: Vorbehandlung eines Bildes im Stapelbetrieb
          evt( vars, EVT_537, img )


          # Ggf. gic-Speicher-Code auf das geladene Bild anwenden
          if sgic_sav != None and len( sgic_sav ) > 0:

              # Zielnamen in Variable übernehmen
              if pathDest != None:
                   vars_set( vars, LOC.VAR_DEST, filename_dest )

              # Ausführen
              run_param( sgic_sav, [ img, drw ], vars )

          # Bild-Entfernen-Befehl (EXB) ausführen
          run( LOC.CMD_DEL, vars )


   # ggf. 'MOD.VAR_POST_GIC' wieder aus 'vars' entfernen
   vars_in_del( vars, MOD.VAR_POST_GIC )

   # Laufvariable zurücksetzen
   vars_set( vars, LOC.VAR_POS, LOC.VAR_POS_INIT )

elif action == MOD_ACTION_NAME:
   result = "exec_dir_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_BATCH

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_EXEC_DIR

elif action == MOD_ACTION_ITYPE:
    result = MOD_ITYPE_CMD
