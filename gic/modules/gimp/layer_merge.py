#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 1/2019
#
#
# LOC.CMD_LYR_MERGE / "EBZ"
#
# Ebenen zusammenfügen


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_LYR_MERGE ]

elif action == MOD_ACTION_EXEC:

    # Ebenen zusammenfügen
    layerNew = pdb.gimp_image_merge_visible_layers( image, CLIP_TO_IMAGE )

    # drawable in 'vars' speichern
    # drw = pdb.gimp_image_get_active_drawable( image )
    vars_set_list( vars, VAR_RUN_PARA, MOD.VAR_RUN_PARA_DRW, layerNew )

    # Wenn es sich um das gleiche Bild wie das in VAR_RUN_PARA_BAK handelt,
    # ändern wir auch da den Wert
    img_bak = vars_get( vars, VAR_RUN_PARA_BAK )
    if image == img_bak[ 0 ]:
        img_bak[ 1 ] = layerNew
        vars_set( vars, VAR_RUN_PARA_BAK, img_bak )

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_CMD_G ]

elif action == MOD_ACTION_NAME:
    result = "layer_merge_py"

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_LYR_MERGE
