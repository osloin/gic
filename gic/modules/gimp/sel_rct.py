#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# LOC.CMD_SELRCT / "AUR" / "R"
#
# Rechteckige Auswahl durchführen
#
# [0] (float)  x
# [1] (float)  y
# [2] (float)  Breite
# [3] (float)  Höhe
# [4] (str)    Modus:
#               "zufuegen"
#               "abziehen"
#              ["ersetzen"]
#               "ueberschneiden"
# [5] (bool) Autom. Zuschneiden             [False]


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_SELRCT ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_X,
               MOD_PTYP_Y,
               MOD_PTYP_X,
               MOD_PTYP_Y,
               MOD_PTYP_LABEL,
               MOD_PTYP_BOOL_YN  ]

elif action == MOD_ACTION_PDEF:
    result = [ None, None, None, None, LOC.PVAL_RCT_4[2], False ]  # Vorgabew.

elif action == MOD_ACTION_EXEC: # Ausführen ....
    x         = param[ 0 ]
    y         = param[ 1 ]
    w         = param[ 2 ]
    h         = param[ 3 ]
    operation = param[ 4 ]
    autocrop  = param[ 5 ]
    if len( operation ) == 0:
        operation = 2
    else:
        operation = lis_.get_idx( LOC.PVAL_RCT_4, operation, -1, 1 )

    pdb.gimp_image_select_rectangle( image, operation, x, y, w, h )

    # Auf Auswahl zuschneiden?
    if autocrop:
        bresult, x1, y1, x2, y2 = pdb.gimp_selection_bounds( image )
        if bresult:
             pdb.gimp_image_crop( image, x2 - x1, y2 - y1, x1, y1 )

elif action == MOD_ACTION_NAME:
   result = "sel_rct_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_RCT

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_RCT

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_CMD_G ]

elif action == MOD_ACTION_PVAL:
    result = [ None, None, None, None, LOC.PVAL_RCT_4, None ]
