#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 1/2019
#
#
# LOC.CMD_LYR_MOVE / "EBV"
#
# Ebene verschieben
#
# [0] (str)     Ebenenname
# [1] (float)   x
# [2] (float)   y


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_LYR_MOVE ]

elif action == MOD_ACTION_PTYPE:
   result = [ MOD_PTYP_STR,
              MOD_PTYP_X,
              MOD_PTYP_Y ]

elif action == MOD_ACTION_EXEC:          # Ausführen ....

   # Parameter in Variable übernehmen
   layer_name = param[ 0 ]
   x          = param[ 1 ]
   y          = param[ 2 ]

   # Ebene für Namen holen
   lay = pdb.gimp_image_get_layer_by_name( image, layer_name )

   # Position zuweisen
   if lay != None:

       pdb.gimp_layer_set_offsets( lay, x, y )

elif action == MOD_ACTION_NAME:
   result = "layer_move_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_LYR_MOVE

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_LYR_MOVE

elif action == MOD_ACTION_ITYPE:
    result = MOD_ITYPE_CMD_G
