#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# LOC.CMD_ROTATE / "DRE" / "D"
#
# Rotation eines Bildes, Bildausschnitts oder einer Ebene
#
# [0] (float) Winkel, um den nach rechts gedreht werden soll
# [1] (str)   Wenn angegeben, wird diese Ebene (statt des Bildes oder der Aus-
#             wahl) gedreht [""]


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_ROTATE ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_FLOAT, MOD_PTYP_STR ]

elif action == MOD_ACTION_EXEC:

    # Grad aus Parametern holen
    deg = param[ 0 ]

    # Ebenenname
    obj_name = param[ 1 ]

    # Auswählen, was gedreht werden soll ...
    if len( obj_name ) > 0:                     # Ebenenname angegeben?
        # Ebene
        obj = pdb.gimp_image_get_layer_by_name( image, obj_name )
    else:
        # Bild oder Auswahl
        obj = drawable

    # Kontext einrichten
    pdb.gimp_context_set_interpolation( 3 )       # 3:INTERPOLATION-LANCZOS
    pdb.gimp_context_set_transform_direction( 0 ) # 0:vor 1:zurück
    pdb.gimp_context_set_transform_resize( 0 )    # 0: TRANSFORM-RESIZE-ADJUST
    pdb.gimp_context_set_transform_recursion( 3 ) # 3 soll gut sein

    # Grad in Radiant umrechnen
    rad = math.radians( deg )

    # Transformation durchführen
    item = pdb.gimp_item_transform_rotate( obj, rad, TRUE, 0, 0 )

elif action == MOD_ACTION_PDEF:
    result = [ 0.0, "" ]

elif action == MOD_ACTION_NAME:
   result = "rotate_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_ROTATE

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_ROTATE
