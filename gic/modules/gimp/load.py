#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# LOC.CMD_LOAD / "LAD"
#
# Bild laden.
# Hinweis: Ein unsichtbar geladenes Bild sollte nach seiner Verwendung mit
# "EXB" wieder freigegeben werden.
#
# [0] (str)  Dateiname
# [1] (bool) Bild in die Anzeige bringen [True]


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_LOAD ]

elif action == MOD_ACTION_PTYPE:
   result = [ MOD_PTYP_FILE, MOD_PTYP_BOOL_YN ]

elif action == MOD_ACTION_EXEC:          # Ausführen ....

   # Dateiname aus erstem Parameter
   filename  = param[ 0 ]

   # Anzeige Ja/Nein
   viewpic  = param[ 1 ]

   # Problematische Zeichen ersetzen
   #filename = filename.decode( sys.getfilesystemencoding(), 'replace')
   filename =  fil_.convert( filename )

   #d( vars, [ "LOAD--->", filename ] )

   # Bild laden
   img = pdb.gimp_file_load( filename, filename )

   # Wurde Bild geladen?
   if img != None:

       # Ereignis
       evt( vars, EVT_521, filename )

       # Bearbeitungstatus entfernen
       pdb.gimp_image_clean_all( img )

       # Ggf. das Bild in die Anzeige bringen
       if viewpic == True:                        # Anzeigen?

           # 'display' ermitteln
           display = pdb.gimp_display_new( img )

           # 'display' Speichern
           vars_set( vars, MOD.VAR_DISPLAY, display )

           # Status: Sichtbar geladen durch gic
           vars_set( vars, MOD.VAR_IMG_STATE, 1 )

       else:  # Insichtbares Bild ...

           # Falls 'MOD.VAR_DISPLAY' noch vorhanden, entfernen
           vars_get_signal( vars, MOD.VAR_DISPLAY )

           # Status: Unsichtbar geladen durch gic
           vars_set( vars, MOD.VAR_IMG_STATE, 2 )

       # Geladenes Bild im ersten Eintrag von VAR_RUN_PARA in 'vars' speichern
       vars_set_list( vars, VAR_RUN_PARA, MOD.VAR_RUN_PARA_IMG, img )

       # Das zuletzt von gic erzeugte Bild versorgen
       vars_set( vars, MOD.VAR_IMG, img )

       # drawable in 'vars' speichern
       drw = pdb.gimp_image_get_active_drawable( img )
       vars_set_list( vars, VAR_RUN_PARA, MOD.VAR_RUN_PARA_DRW, drw )

       # Unser Ergebnis
       result = True

   else: # Bild wurde nicht geladen ...

       # Bild im ersten Eintrag von VAR_RUN_PARA in 'vars' löschen
       vars_set_list( vars, VAR_RUN_PARA, MOD.VAR_RUN_PARA_IMG, None )

       # Ebene im zweiten Eintrag von VAR_RUN_PARA in 'vars' löschen
       vars_set_list( vars, VAR_RUN_PARA, MOD.VAR_RUN_PARA_DRW, None )

       # Unser Ergebnis
       result = False


elif action == MOD_ACTION_NAME:
   result = "load_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_LOAD

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_LOAD

elif action == MOD_ACTION_ITYPE:
    result = MOD_ITYPE_CMD_G

elif action == MOD_ACTION_PDEF:
    result = [ None, True ]
