#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
# Skalierungsfunktionen
#
#
# LOC.CMD_SCALE / "SKA" / "K"
#
# Skaliert das aktuelle Bild oder die aktuelle Ebene
#
# [0] (float) Breite, 1 ... 524288 (*)
# [1] (float) Höhe,   1 ... 524288 (*)
#            (*) Wenn einer der beiden Werte auf 0 gesetzt wird, wird versucht,
#                 das Seitenverhältnis beizubehalten.
# [2] (str)   Wenn angegeben, wird nur diese Ebene skaliert
# [3] (str)   Interpolationswert [3/INTERPOLATION_LANCZOS]
#
#
# LOC.CMD_DPI / "DPI"
#
# DPI-Wert einstellen
#
# [0] (float) DPI


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_SCALE, LOC.CMD_DPI ]

elif action == MOD_ACTION_PTYPE:
    if name == LOC.CMD_SCALE:
        result = [ MOD_PTYP_X,
                   MOD_PTYP_Y,
                   MOD_PTYP_LABEL,
                   MOD_PTYP_LABEL ]

    elif name == LOC.CMD_DPI:
        result = [ MOD_PTYP_FLOAT ]

elif action == MOD_ACTION_PDEF:

    if name == LOC.CMD_SCALE:
        result = [ 0.0, 0.0, "", LOC.PVAL_SCALE_3[ 3 ] ]

elif action == MOD_ACTION_EXEC:

   if name == LOC.CMD_SCALE:

       # Ebenenname aus Parametern
       layer_name = param[ 2 ]

       # Koordinate aus Parametern
       x = int( param[ 0 ] )
       y = int( param[ 1 ] )

       # Interpolation
       interpol = lis_.get_idx( LOC.PVAL_SCALE_3, param[ 3 ], 0, 1 )

       # Entweder ganzes Bild oder eine Ebene skalieren
       if layer_name != None and len( layer_name ) > 0: # Ebenenname?

           # Bildebene skalieren
           gim_.scale_layer( image, layer_name, x, y, interpol )

       else:          # Es wurde kein Ebenenname angegeben ...

           # Bild skalieren
           gim_.scale( image, x, y, interpol )


   # Wir führen ggf. aus ....
   elif name == LOC.CMD_DPI:

      # Parameter
      dpi = param[ 0 ]

      # Auflösung anpassen
      pdb.gimp_image_set_resolution( image, dpi, dpi )


elif action == MOD_ACTION_NAME:
   result = "scale_py"

elif action == MOD_ACTION_PDOC:

    if name == LOC.CMD_SCALE:
        result = LOC.PDOC_SCALE
    elif name == LOC.CMD_DPI:
        result = LOC.PDOC_DPI

elif action == MOD_ACTION_IDDOC:

    if name == LOC.CMD_SCALE:
        result = LOC.IDDOC_SCALE
    elif name == LOC.CMD_DPI:
        result = LOC.IDDOC_DPI

elif action == MOD_ACTION_PVAL:
    if name == LOC.CMD_SCALE:
        result = [None,None,None, LOC.PVAL_SCALE_3 ]

elif action == MOD_ACTION_PMMS:
    if name == LOC.CMD_SCALE:
        result = [ [1,524288,1],[1,524288,1], None, None ]
