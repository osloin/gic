#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 12/2018
#
#
# LOC.CMD_DUPLI / "DPL"
#
# Dupliziert das aktuelle Bild im Speicher und macht es zum aktuellen Bild, so
# daß sich alle nachfolgenden Grafikbefehle auf diese (unsichtbare) Bild
# auswirken. Das Bild wird autom. freigegeben.


# Aktionen
if action == MOD_ACTION_INIT:
   result = [LOC.CMD_DUPLI]

elif action == MOD_ACTION_EXEC:

    # Bild duplizieren
    img = pdb.gimp_image_duplicate( image )

    # Das Ausgangsbild war schon dupliziertes Bild?
    if vars_get( vars, MOD.VAR_IMG_STATE ) == 3:

        # Ausgangsbild entfernen
        run( LOC.CMD_DEL, vars )

    # Dupliziertes Bild im ersten Eintrag von VAR_RUN_PARA in 'vars' speichern
    vars_set_list( vars, VAR_RUN_PARA, MOD.VAR_RUN_PARA_IMG, img )

    # Status: Dupliziertes Bild
    vars_set( vars, MOD.VAR_IMG_STATE, 3 )

    # Das zuletzt von gic erzeugte Bild versorgen
    vars_set( vars, MOD.VAR_IMG, img )

    # Dateinamen des zuletzt duplizierten Bildes versorgen
    if image.filename != None:
        vars_set( vars, MOD.VAR_IMG_FNAME, image.filename )

    # drawable in 'vars' speichern
    drw = pdb.gimp_image_get_active_drawable( img )
    vars_set_list( vars, VAR_RUN_PARA, MOD.VAR_RUN_PARA_DRW, drw )

    # Ereignis : Ein Bild wurde im Speicher dupliziert
    evt( vars, EVT_538, img )

elif action == MOD_ACTION_NAME:
   result = "duplicate_py"

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_DUPLI
