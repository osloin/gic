#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 1/2019
#
#
# LOC.CMD_LYR_ALIGN / "EBA"
#
# Ebene ausrichten
#
#  [0] (str)    Ebene die ausgerichtet werden soll
#  [1] (str)    Ebene an der ausgerichtet werden soll
#               "", um am Bild auzurichten [""]
#  [2] (str)    Bestimmt, wie horizontal ausgerichtet werden soll ["mitte"]
#  [3] (str)    Bestimmt, wie vertikal ausgerichtet werden soll ["mitte"]
#  [4] (float)  Zusätzlicher horizonaler Abstand [0.0]
#  [5] (float)  Zusätzlicher vertikaler Abstand [0.0]


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_LYR_ALIGN ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_STR,
               MOD_PTYP_STR,
               MOD_PTYP_LABEL,
               MOD_PTYP_LABEL,
               MOD_PTYP_X,
               MOD_PTYP_Y ]

elif action == MOD_ACTION_PDEF:  # Vorgabewerte

    result = [  None,
                "",
                LOC.PVAL_LYR_ALIGN_2[ 1 ],
                LOC.PVAL_LYR_ALIGN_3[ 1 ],
                0.0,
                0.0 ]

elif action == MOD_ACTION_EXEC: # Ausführen ....

    # Abmessungen der auszurichtenden Ebene
    alignName = param[ 0 ]
    layer1 = pdb.gimp_image_get_layer_by_name( image, alignName )
    layrct1 = [ layer1.offsets[ 0 ],
                layer1.offsets[ 1 ],
                layer1.width,
                layer1.height ]

    # Abmessungen des anderen Objekts
    alignToName = param[ 1 ]
    if alignToName == "":   # An Bild ausrichten?
        layrct2 = [ 0, 0, layer1.image.width, layer1.image.height ]
    else:                   # An anderer Ebene ausrichten ...
        layer2 = pdb.gimp_image_get_layer_by_name( image, alignToName )
        layrct2 = [ layer2.offsets[ 0 ],
                    layer2.offsets[ 1 ],
                    layer2.width,
                    layer2.height ]

    # Ausrichtungsmodus
    modeH = lis_.get_idx( LOC.PVAL_LYR_ALIGN_2, param[ 2 ], 1 )
    modeV = lis_.get_idx( LOC.PVAL_LYR_ALIGN_3, param[ 3 ], 1 )

    # Abstönde
    spaceH = param[ 4 ]
    spaceV = param[ 5 ]

    # Ausrichtung durchführen
    rctresult = rct_.align( layrct1, layrct2, modeH, modeV, spaceH, spaceV )

    # neue Position zuweisen
    pdb.gimp_layer_set_offsets( layer1, rctresult[ 0 ], rctresult[ 1 ] )


elif action == MOD_ACTION_NAME:
   result = "layer_align_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_LYR_ALIGN

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_LYR_ALIGN

elif action == MOD_ACTION_PVAL:
    result = [ [], [], LOC.PVAL_LYR_ALIGN_2, LOC.PVAL_LYR_ALIGN_3, [], [] ]
