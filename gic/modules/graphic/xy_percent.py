#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
# Stellt Prozent - Pixel - Transformierung zur Verfügung


# Aktionen
if action == MOD_ACTION_TRANS_L:

    # Wert, der Konvertiert werden soll
    value = param[ MOD_ACTION_TRANS_P_VALUE ]

    # Typ, der Konvertiert werden soll
    ty = param[ MOD_ACTION_TRANS_P_TYP ]

    # Je nach Typ ...
    if ty == GTYP_X:
        hun = vars_get( vars, LOC.VAR_W )
    else:
        hun = vars_get( vars, LOC.VAR_H )

    # Die Pixelanzahl errechnen
    result = hun / 100.0 * value

elif action == MOD_ACTION_INIT:
    result = [ MOD_GROUP_PIXTRANS ]

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_GROUP ]

elif action == MOD_ACTION_NAME:
    result = LOC.PERCENT

elif action == MOD_ACTION_IDDOC:
    result =  "Pixel <--> " + LOC.PERCENT
