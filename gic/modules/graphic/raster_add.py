#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# LOC.CMD_RASTER_ADD / "RAA"
#
# Passt die per RAS erzeugten Rechtecke in ihrer Position und / oder
# Größe an
#
# [0] (str)    Prefix der erzeugten Listen
# [1] (float)  Anpassung der X-Position (*)
# [2] (float)  Anpassung der Y-Position (*)
# [3] (float)  Anpassung der Breite (*)
# [4] (float)  Anpassung der Höhe (*)
#              (*) Der angegebene Wert wird jeweils addiert


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_RASTER_ADD ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_LABEL,
               MOD_PTYP_X,
               MOD_PTYP_Y,
               MOD_PTYP_X,
               MOD_PTYP_Y ]


elif action == MOD_ACTION_EXEC: # Ausführen ....

    # Parameter
    pre   = param[ 0 ]  # Variablen-Prefix
    x_add = param[ 1 ]  # X-Anpassung
    y_add = param[ 2 ]  # Y-Anpassung
    w_add = param[ 3 ]  # Wert der Breitenanpassung
    h_add = param[ 4 ]  # Wert der Höhenanpassung

    li = vars_get( vars, pre + LOC.RASTER_X, VIB_VALUE_DI )
    li = lis_.items_ope( li, x_add )
    vars_set( vars, pre + LOC.RASTER_X, li, VIB_VALUE_DI )

    li = vars_get( vars, pre + LOC.RASTER_Y, VIB_VALUE_DI )
    li = lis_.items_ope( li, y_add )
    vars_set( vars, pre + LOC.RASTER_Y, li, VIB_VALUE_DI )

    li = vars_get( vars, pre + LOC.RASTER_H, VIB_VALUE_DI )
    li = lis_.items_ope( li, h_add )
    vars_set( vars, pre + LOC.RASTER_H, li, VIB_VALUE_DI )

    li = vars_get( vars, pre + LOC.RASTER_W, VIB_VALUE_DI )
    li = lis_.items_ope( li, w_add )
    vars_set( vars, pre + LOC.RASTER_W, li, VIB_VALUE_DI )


elif action == MOD_ACTION_NAME:
   result = "raster_add_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_RASTER_ADD

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_RASTER_ADD
