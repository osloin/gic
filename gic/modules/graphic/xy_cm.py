#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
# Stellt cm - Pixel - Transformierung zur Verfügung


# Aktionen
if action == MOD_ACTION_TRANS_L:

    # Wert, der Konvertiert werden soll
    value = param[ MOD_ACTION_TRANS_P_VALUE ]

    # Typ, der Konvertiert werden soll
    ty = param[ MOD_ACTION_TRANS_P_TYP ]

    # Je nach Typ ...
    if ty == GTYP_X:
        dpi = vars_get( vars, LOC.VAR_DPIX )
    else:
        dpi = vars_get( vars, LOC.VAR_DPIY )

    # Umrechnungswert (Entspricht einem Inch/Zoll)
    mp = 2.45

    # Die Pixelanzahl errechnen
    result = value / mp * dpi


elif action == MOD_ACTION_INIT:
    result = [ MOD_GROUP_PIXTRANS ]

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_GROUP ]

elif action == MOD_ACTION_NAME:
    result = LOC.CM

elif action == MOD_ACTION_IDDOC:
    result =  "Pixel <--> " + LOC.CM
