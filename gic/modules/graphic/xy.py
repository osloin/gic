#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
# LOC.VAR_XY / "xy"
#
# Befehl, um ein Transformierungsmodul allen X/Y-Parametern zuzuweisen bzw. die
# Zuweisung wieder aufzuheben.
#
# Parameter ...
# [0] Modulname
#     'std', um eine Zuweisung wieder aufzuheben.


# Aktionen
if action == MOD_ACTION_INIT:
    result = [ LOC.VAR_XY ]

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_MODVAR ]   # Wir sind gesteuerte Variable

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_LABEL ]    # Parameter ist Modulname

elif action == MOD_ACTION_POST:
    vars_set( vars, LOC.VAR_XY, "(?)", VIB_VALUE_DI )

elif action == MOD_ACTION_VAR_G:
    result = vars_get( vars, LOC.VAR_XY, VIB_VALUE_DI )

elif action == MOD_ACTION_VAR_S:

    # Modulname
    modname = param[ 0 ]

    # Parameter allen Parametern vom Typ GTYP_X und GTYP_Y ermitteln
    pxy = id_get_param_list( [ GTYP_X, GTYP_Y ], vars )

    # Modulname angegeben?
    if modname != LOC.VAR_XY_STD:

        # Typ überprüfen
        type = vars_get( vars, modname, VIB_GTYP )

        # Typüberprüfung
        if type != GTYP_MODULE:                             # Kein Modul?
            evt( vars, ERR_027, [ modname, type, GTYP_MODULE ] )

        # Zuweisen
        transform_set( pxy, modname, None, vars )

    else:

        # Zuweisen aufheben
        transform_del( pxy, vars )

# elif action == MOD_ACTION_PDEF:
#    result = [""]

elif action == MOD_ACTION_NAME:
   result = "xy_py"

elif action == MOD_ACTION_PDOC:
   result = LOC.PDOC_XY

elif action == MOD_ACTION_IDDOC:
   result = LOC.IDDOC_VAR_XY
