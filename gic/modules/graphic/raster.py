#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# LOC.CMD_RASTER / "RAS"
#
# Erzeugt für das aktuelle Bild ein Raster aus Rechtecken mit einer bestimmten
# Anzahl an Spalten und Zeilen. Das erzeugt Raster kann über RAA nachbearbeitet,
# und per SOLANGE durchlaufen werden. (Siehe 'demo_RAS')
#
# [0] (str)    Prefix der erzeugten Listen. Es werden vier Listen erzeugt.
#              Wenn das Prefix 'ra' lautet ...
#              1. 'rax' Liste der x-Koordinaten
#              2. 'ray' Liste der y-Koordinaten
#              3. 'rab' Liste der Breiten
#              4. 'rah' Liste der Höhen
# [1] (int)    Spalten
# [2] (int)    Zeilen
#              Fläche, in dem das Raster errichtet werden soll ...
# [3] (float)  x       [0] (*)
# [4] (float)  y       [0] (*)
# [5] (float)  Breite  [0] (*)
# [6] (float)  Höhe    [0] (*)
#              (*) Werden alle Werte auf 0 gesetzt, wird die gesamte Bildgröße
#                  verwendet



# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_RASTER ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_LABEL,
               MOD_PTYP_INT,
               MOD_PTYP_INT,
               MOD_PTYP_X,
               MOD_PTYP_Y,
               MOD_PTYP_X,
               MOD_PTYP_Y
             ]

elif action == MOD_ACTION_PDEF:
    result = [ None, None, None, 0, 0, 0, 0 ] # Vorgabewerte

elif action == MOD_ACTION_EXEC: # Ausführen ....

    # Anz der Spalten und Zeilen
    col_count = param[ 1 ]
    row_count = param[ 2 ]

    # Fläche
    x = param[ 3 ]
    y = param[ 4 ]
    w = param[ 5 ]
    h = param[ 6 ]

    # Wenn gesamte Bildfläche verwendet werden soll
    if x == 0 and y == 0 and w == 0 and h == 0:

        w = vars_get( vars, LOC.VAR_W )    # Bildbreite
        h = vars_get( vars, LOC.VAR_H )    # Bildhöhe

    # Abmessung eines Rechtecks
    w_item  = w / col_count            # Breite eines Feldes
    h_item  = h / row_count            # Höhe eines Feldes

    # Aktuelle Spalte
    col = 0

    # Anzahl der Felder
    cnt = col_count * row_count

    # Aktueller Feld-Index
    idx = 0

    # Auflistungen der Werte der ermittelten Rechtecke initalisieren
    x_list = [ 0 ] * cnt    # Alle x-Koordinaten
    y_list = [ 0 ] * cnt    # Alle y-Koordinaten
    w_list = [ 0 ] * cnt    # Alle Breiten
    h_list = [ 0 ] * cnt    # Alle Höhen

    # Alle Spalten durchlaufen
    while col < col_count:

        # Aktuelle Zeile
        row = 0

        # Alle Zeilen durchlaufen
        while row < row_count:

            # Werte der Ergebnislisten ergänzen
            x_list[ idx ] = w_item * col
            y_list[ idx ] = h_item * row
            w_list[ idx ] = w_item
            h_list[ idx ] = h_item

            # Nächste Zeile
            row += 1

            # Nächster Index
            idx += 1

        # Nächste Spalte
        col += 1

    # Ergebnislisten in 'vars' übertragen
    # VIB_LIST_MODE: True, um autom. Position festzulegen
    # VIB_LIST_POS:  Auf erste Position

    pre = param[ 0 ] # Variablen-Prefix aus Parametern übernehmen

    vars_set( vars, pre + LOC.RASTER_X, x_list, VIB_VALUE, GTYP_LIST, VARS_GLOB)
    vars_set( vars, pre + LOC.RASTER_X, True  , VIB_LIST_MODE )
    vars_set( vars, pre + LOC.RASTER_X, 0     , VIB_LIST_POS )

    vars_set( vars, pre + LOC.RASTER_Y, y_list, VIB_VALUE, GTYP_LIST, VARS_GLOB)
    vars_set( vars, pre + LOC.RASTER_Y, True  , VIB_LIST_MODE )
    vars_set( vars, pre + LOC.RASTER_Y, 0     , VIB_LIST_POS )

    vars_set( vars, pre + LOC.RASTER_W, w_list, VIB_VALUE, GTYP_LIST, VARS_GLOB)
    vars_set( vars, pre + LOC.RASTER_W, True  , VIB_LIST_MODE )
    vars_set( vars, pre + LOC.RASTER_W, 0     , VIB_LIST_POS )

    vars_set( vars, pre + LOC.RASTER_H, h_list, VIB_VALUE, GTYP_LIST, VARS_GLOB)
    vars_set( vars, pre + LOC.RASTER_H, True  , VIB_LIST_MODE )
    vars_set( vars, pre + LOC.RASTER_H, 0     , VIB_LIST_POS )


elif action == MOD_ACTION_NAME:
   result = "raster_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_RASTER

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_RASTER
