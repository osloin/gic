#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 1/2019
#
#
# LOC.CMD_DBG / "DBG"
#
# Befehl zum Testen von Python-Code
#
# [0] (?) Ein Parameter

# Aktionen
if action == MOD_ACTION_INIT:
    result = [ [ LOC.CMD_DBG, VF_INV ] ]        # "DBG"

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_DIRECT ]    # Unser Parameter

elif action == MOD_ACTION_PDEF:
    result = [ "" ]                 # Vorgabewert für Parameter

elif action == MOD_ACTION_EXEC:     # Ausführen ....

    result = result
    # raise RuntimeError( "dbg.py" )

elif action == MOD_ACTION_NAME:
    result = "dbg_py"               # Modulname

elif action == MOD_ACTION_PDOC:     # Kommentar für Parameter
    result = ["Param"]

elif action == MOD_ACTION_IDDOC:    # Kommentar für Modul
    result = LOC.IDDOC_DBG
