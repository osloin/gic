#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# Mathematische Funktionen
#
# LOC.FCT_INT / "ganz()"  Ganzzahl ermitteln
# LOC.FCT_SIN / "sin()"   Sinuswert


# Aktionen
if action == MOD_ACTION_INIT:
    result = [ LOC.FCT_INT, LOC.FCT_SIN ]

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_FCT, MOD_ITYPE_FCT ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_FLOAT ]  # Unser Parameter

elif action == MOD_ACTION_EXEC:
    if name == LOC.FCT_INT:       # int()
        result = int( param[ 0 ] )
    elif name == LOC.FCT_SIN:     # sin()
        result = math.sin( param[ 0 ] )

elif action == MOD_ACTION_NAME:
    result = "math_py"

elif action == MOD_ACTION_PDOC:
    result = [ LOC.FLOATPOINT ]

elif action == MOD_ACTION_IDDOC:
    if name == LOC.FCT_SIN:          # sin()
        result = LOC.IDDOC_SIN
    elif name == LOC.FCT_INT:        # int()
        result = LOC.IDDOC_INT
