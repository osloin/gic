#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# LOC.CMD_STOP / "STOP"
#
# Beendet die Ausführung
#
# [0] (str) Grund für den Abbruch

# Aktionen
if action == MOD_ACTION_INIT:
    result = [ LOC.CMD_STOP ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_STR ]        # Wir haben keine Parameter

elif action == MOD_ACTION_EXEC:      # Ausführen ....

    # Abbruchgrund
    reason = param[ 0 ]

    # Ereignis
    evt( vars, EVT_022, reason )

    # Abbruchbefehl eintragen
    vars_set( vars,
              VAR_STOP,
              [ LOC.CMD_STOP, reason ],
              VIB_VALUE,
              GTYP_SIGNAL )

elif action == MOD_ACTION_PDEF:
    result = [ "?" ]        # Vorgabewert unseres Parameters

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_STOP

elif action == MOD_ACTION_NAME:
    result = "stop_py"

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_STOP

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_CTRL ]
