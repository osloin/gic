#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# LOC.CMD_EXEC_STR / "DUZ"
#
# Führt den Inhalt der angegebenen Zeichenkette aus
#
# [0] (str) Zeichenkette, die gic-Code enthält. Hochkomma werden durch
#           Anführungszeichen ersetzt
#
# Beispiel: DUZ "INF 'a' + 'b'"


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_EXEC_STR ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_SGIC ]

elif action == MOD_ACTION_EXEC:

    # gic aus Parameter
    sgic = param[ 0 ]

    # Ok?
    if sgic != None:
        run( sgic, vars, True )   # Ausführen

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_CMD ]

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_EXEC_STR

elif action == MOD_ACTION_NAME:
    result = "exec_str_py"

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_EXEC_STR
