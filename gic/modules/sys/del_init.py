#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# LOC.CMD_DELI / "EXI"
#
# Den per SEI angelgeten Initalisierungswert einer Variablen wieder entfernen
#
# [0] (str) Bezeichner



# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_DELI ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_LABEL ]

elif action == MOD_ACTION_EXEC:

    filename = path_get( FOLDER_SCRIPTS ) + MOD_AUTOEXEC_EXT

    # Variablenname
    var_name = param[ 0 ]

    # Ereignis
    evt_result = evt( vars, EVT_016, var_name )

    # Wenn kein Abbruch durch Ereignis ...
    if evt_result == None or evt_result != False:

        # autoexec-Datei laden
        autoexec, state = file_read( filename, None, None, vars )

        # Wenn geladen ...
        if state == fil_.ST_OK:

            # Varzuweisung ersetzen
            autoexec = code_gic_setvar( autoexec, var_name, None, vars )

            # autoexec-Datei schreiben
            file_write( filename, autoexec, "del_init.py", vars )


elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_CMD ]

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_DELG

elif action == MOD_ACTION_NAME:
    result = "del_init_py"

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_DELG
