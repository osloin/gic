#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 2/2019
#
#
# LOC.FCT_EXISTS / "hole"
#
# Hierüber kann ein bestimmter numerischer Wert (Ganzzahl) abgefragt werden.
#
# [0] (str) Was abgefragt werden soll. (Der Parameter braucht nicht in voller
#           Länge angegeben zu werden - es reicht auch 'v' oder 'ver', anstatt
#           'version'.
#
#   'tag'     Akt. Tag
#   'monat'   Akt. Monat
#   'jahr'    Akt. Jahr
#   'stunde'  Akt. Stunde
#   'min'     Akt. Minute
#   'sek'     Akt. Sekunde
#   'schlaf'  Sekundenschlaf. Der zweite Parameter bestimmt die Sekunden
#   'ver'     gic-Version. Der zweite P. bestimmt von 0 ... 3 die Stelle
#   'zufall'  Zufallszahl von 0 bis zum Parameter
#
# [1] (int) Ggf. ein zweiter Parameter [0]

# Aktionen
if action == MOD_ACTION_INIT:

   result = [ LOC.FCT_GET ]

elif action == MOD_ACTION_PTYPE:

    result = [ MOD_PTYP_LABEL, MOD_PTYP_INT ]

elif action == MOD_ACTION_EXEC:             # Ausführen ....

    # Was soll ermittelt werden
    inf = param[ 0 ]

    # Parameter
    p = param[ 1 ]

    # Index ermitteln (Werte gelten als gefunden, wenn der Anfang stimmt)
    idx = lis_.get_idx( LOC.PVAL_GET, param[ 0 ], -1, 1 )

    # Var soll überprüft werden?
    if idx == 0:                        # 'sek'
        result = time.localtime()[ 5 ]

    elif idx == 1:                      # 'min'
        result = time.localtime()[ 4 ]

    elif idx == 2:                      # 'stunde'
        result = time.localtime()[ 3 ]

    elif idx == 3:                      # 'tag'
        result = time.localtime()[ 2 ]

    elif idx == 4:                      # 'monat'
        result = time.localtime()[ 1 ]

    elif idx == 5:                      # 'jahr'
        result = time.localtime()[ 0 ]

    elif idx == 6:                      # 'schlaf' (Sekundenschlaf)
        time.sleep( p )
        result = p

    elif idx == 7:                      # 'ver' - gic-Version
        result = _VERSION[ p ]          # 0 ... 3

    elif idx == 8:                      # Zufallszahl
        result = random.randint( 0, p )

    else:
        result = 0

        # Gibts nicht
        evt( vars, ERR_011, inf )


elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_FCT ]

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_GET

elif action == MOD_ACTION_PDEF:
    result = [ None, 0 ]

elif action == MOD_ACTION_PVAL:
    result = [ LOC.PVAL_GET, None ]

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_GET

elif action == MOD_ACTION_NAME:
   result = "fct_get_py"
