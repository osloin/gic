#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 1/2019
#
#
# LOC.FCT_SIZE / "groesse"
#
# Hierüber kann die Größe von etwas abgefragt werden
#
# [0] (str) Name der überprüft werden soll
# [1] (str) Was im ersten Parameter angegeben wurde
#           ['liste']   Anz der Einträge einer Liste
#           'breite'    Breite einer Ebene
#           'hoehe'     Höhe einer Ebene


# Aktionen
if action == MOD_ACTION_INIT:

   result = [ LOC.FCT_SIZE ]

elif action == MOD_ACTION_PTYPE:

    result = [ MOD_PTYP_DIRECT, MOD_PTYP_LABEL ]

elif action == MOD_ACTION_EXEC:             # Ausführen ....

    # Objekt, das überprüft werden soll
    obj = param[ 0 ]

    # Objekt-Typ
    objtype = param[ 1 ]

    # Vorgabewert unseres Rückgabewerts
    result = 0

    # Liste ...
    if objtype == LOC.PVAL_SIZE[ 0 ]:         # 'liste'

        # In lokalen od. globalen Vars?
        space = vars_find( vars, obj )

        # Gefunden?
        if space != -1:
            list = vars_get( vars, obj, VIB_VALUE_DI, space )
            result = len( list )

    # Ebenenbreite ...
    elif objtype == LOC.PVAL_SIZE[ 1 ]:       # 'breite_e'

        # Parameter aufbereiten
        lay_name = evalV( obj, GTYP_STR, vars )

        # Ebene für Namen
        lay = pdb.gimp_image_get_layer_by_name( image, lay_name )

        # Ebenenbreite abfragen
        if lay != None:
            result = lay.width

    # Ebenenhöhe ...
    elif objtype == LOC.PVAL_SIZE[ 2 ]:       # 'hoehe_e'

        # Parameter aufbereiten
        lay_name = evalV( obj, GTYP_STR, vars )

        # Ebene für Namen
        lay = pdb.gimp_image_get_layer_by_name( image, lay_name )

        # Ebenenbreite abfragen
        if lay != None:
            result = lay.height


elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_FCT ]

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_SIZE

elif action == MOD_ACTION_PDEF:
    result = [ None, LOC.PVAL_SIZE[ 0 ] ]

elif action == MOD_ACTION_PVAL:
    result = [ None, LOC.PVAL_SIZE ]

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_SIZE

elif action == MOD_ACTION_NAME:
   result = "fct_size_py"
