#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
# Veranlasst das Laden von 'autoexec.gic'



# Aktionen
if action == MOD_ACTION_INIT:

    # Dateinamen der Autostart-Datei ermitteln
    filename = path_get( FOLDER_SCRIPTS ) + MOD_AUTOEXEC_EXT

    # Datei laden
    # 0: Im Fehlerfall kein EVT_020 auslösen
    autoexec, state = file_read( filename, "", None, vars, 0 )

    # Der inhalt von 'autoexec' wird in eine Var übertragen
    # und dann beim nächsten run() ausgeführt ...
    if state == fil_.ST_OK and len( autoexec ) > 0:
        vars_set( vars, VAR_AUTOEXEC, autoexec )

elif action == MOD_ACTION_NAME:
     result = "autoexec_py"
