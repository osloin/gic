#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# LOC.FCT_CON_STR / "zk"
#
# Konvertiert einen (numerischen) Wert in eine Zeichenkette
#
# [0] (int) Wert, der konvertiert werden soll
# [1] (str) Pythonkonforme Konvertierungsangabe
#           z.B. "%d"
#     (int) Wenn [0] eine Ganzzahl ist, kann hiermit eine Formatierung mit
#           führenden Nullen mit der angegebenen Breite erreicht werden.
#           Wenn 0, wird ohne besondere Formatierung konvertiert
#
# Beispiel: 'SET a/zk( 1, 4 )' liefert '0001'



# Aktionen
if action == MOD_ACTION_INIT:

   result = [ LOC.FCT_CON_STR ]

elif action == MOD_ACTION_PTYPE:

    result = [ MOD_PTYP_MULTI, MOD_PTYP_MULTI ]

elif action == MOD_ACTION_PDEF:

    result = [ None, 0 ]


elif action == MOD_ACTION_EXEC:             # Ausführen ....

    # Ob eigene Formatangaben
    pyt = False

    # Formatierung
    pf = param[ 1 ]
    if type( pf ) == int and pf != 0:
        formats = "%0" + str( pf ) + "d"
    elif type( pf ) == str:
        formats = pf
        pyt = True
    else:
        formats = None

    if formats != None:
        result = formats % param[ 0 ]
        if pyt:
            # "." gegen "," ersetzen ...
            result = result.replace( ".", LOC.FLOAT_SEP ) 
    else:
        result = str( param[ 0 ] )

elif action == MOD_ACTION_NAME:
   result = "con_str_py"

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_CON_STR

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_FCT ]

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_CON_STR
