#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 11/2018
#
#
# LOC.CMD_WHILE / "SOLANGE"
#
# Nachfolgende Zellen abarbeiten, solange eine Bedingung erfüllt ist
#
# [0] (bool)    True, wenn der / die nachfolgende(n) Eintrag / Einträge
#               ausgeführt werden soll(en)
# [1] (int/str) Hierüber wird bestimmt, welche nachfolgenden Zellen betroffen
#               sein sollen. Eine positive Ganzzahl gibt eine Anzahl an. Ein
#               Text gibt an, das alle Einträge bis zu dieser Textmarke
#               betroffen sein sollen. [1]
#
#
# LOC.CMD_FOR / "XMAL"
#
# Nachfolgende Zellen x-mal ausführen
#
# [0] (int)     Wie oft die Schleife ausgeführt werden soll
# [1] (int/str) Hierüber wird bestimmt, welche nachfolgenden Zellen betroffen
#               sein sollen. Eine positive Ganzzahl gibt eine Anzahl an. Ein
#               Text gibt an, das alle Einträge bis zu dieser Textmarke
#               betroffen sein sollen. [1]


# Aktionen
if action == MOD_ACTION_INIT:
    result = [ LOC.CMD_WHILE, LOC.CMD_FOR ]

elif action == MOD_ACTION_PTYPE:
    if name == LOC.CMD_WHILE:
        result = [ MOD_PTYP_BOOL, MOD_PTYP_BLOCK ]     # Unser Parameter
    else:
        result = [ MOD_PTYP_INT,  MOD_PTYP_BLOCK ]     # Unser Parameter

elif action == MOD_ACTION_PDEF:
    result = [ None, 1 ]

elif action == MOD_ACTION_EXEC and name == LOC.CMD_WHILE:  # SOLANGE-Anweisung

    # LIB da?
    if vars_in( vars, VAR_LOOP, VARS_LOC ): # da?

        # LIB holen ...
        lib = vars_get( vars, VAR_LOOP, VIB_VALUE, VARS_LOC )

    else: # Neu anlegen ...

        # Infoblock anlegen
        lib = [ None] * LIB__SIZE                    # Leeren Block
        lib[ LIB_SIZE ] = param[ 1 ]                 # Anz d. Einträge
        lib[ LIB_POS ]  = vars_get( vars, VAR_POS )  # Unsere Pos
        lib[ LIB_RUN ]  = True                       # Einmal ausführen

        # Die nachfolgenden Befehle als Schleife
        # param[ 1 ] enthält die Anzahl
        vars_set( vars,
                  VAR_LOOP,
                  lib,
                  VIB_VALUE,
                  GTYP_MULTI,
                  VARS_LOC )

    # param[ 0 ] enthält die Bedingung und bestimmt, ob ausgeführt werden soll
    lib[ LIB_RUN ] = param[ 0 ] == True

elif action == MOD_ACTION_EXEC and name == LOC.CMD_FOR:  # XMAL-Anweisung

    if vars_in( vars, VAR_LOOP, VARS_LOC ): # da?

        # LIB holen ...
        lib = vars_get( vars, VAR_LOOP, VIB_VALUE, VARS_LOC )

        # Schleifenzähler
        lib[ LIB_FREE ] -= 1

        # Ggf. Abbruch
        lib[ LIB_RUN  ] = lib[ LIB_FREE ] > 0

    else: # Neu anlegen ...

        # Anzahl der Schleifendurchläufe
        cnt_loop = param[ 0 ]

        # Infoblock anlegen (Wir legen einen zusätzlichen Eintrag für unseren
        # Schleifenzähler
        lib = [ None ] * (LIB__SIZE + 1)             # Leeren Block

        lib[ LIB_SIZE  ] = param[ 1 ]                 # Anz d. Einträge
        lib[ LIB_POS  ] = vars_get( vars, VAR_POS )  # Unsere Pos
        lib[ LIB_RUN  ] = cnt_loop > 0               # ausführen?
        lib[ LIB_FREE ] = cnt_loop                   # Einmal ausführen

        # Die nachfolgenden Befehle als Schleife
        # param[ 1 ] enthält die Anzahl
        vars_set( vars,
                  VAR_LOOP,
                  lib,
                  VIB_VALUE,
                  GTYP_MULTI,
                  VARS_LOC )


elif action == MOD_ACTION_NAME:
    result = "while_py"

elif action == MOD_ACTION_PDOC:
    if name == LOC.CMD_WHILE:
        result = LOC.PDOC_WHILE
    else:
        result = LOC.PDOC_FOR

elif action == MOD_ACTION_IDDOC:
    if name == LOC.CMD_WHILE:
        result = LOC.IDDOC_WHILE
    else:
        result = LOC.IDDOC_FOR

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_CTRL, MOD_ITYPE_CTRL ]

elif action == MOD_ACTION_LVL:
    result = MOD_LVL_PRIO
