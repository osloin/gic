#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# LOC.CMD_TRANS_SET / "TRA"
#
# Zuweisung eines Transformierungsmoduls an eine ID (z.B. Befehl) oder
# einen ID-Parameter
#
# [0] (str)   Name des Moduls, das zugewiesen werden soll
# [1] (str)   Name des Befehls, dem das Modul zugewiesen werden soll
# [2] (int)   0..x. Index, des Parameters
#               -1, wenn Zuweisung an ID selbst erfolgen soll
# [3] (str)   Parameter, die an Transformierungsmodul weitergereicht werden
#             sollen.


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_TRANS_SET ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_LABEL,
               MOD_PTYP_LABEL,
               MOD_PTYP_INT,
               MOD_PTYP_STR ]

elif action == MOD_ACTION_EXEC: # Ausführen ....

    module_name = param[ 0 ]
    cmd_name    = param[ 1 ]
    cmd_idx     = param[ 2 ]
    tparam      = param[ 3 ]

    # Wenn cmd_idx >= 0: Zuweisung an einen ID-Parameter
    if cmd_idx >= 0:                              # ID-Parameter spezifiziert?

        # Betoffener Parameter
        item = [[ cmd_name, cmd_idx ]]

        # Modul an Parameter zuweisen
        transform_set( item, module_name, tparam, vars )

    else:   # Zuweisung an ID ...

        transform_set( cmd_name, module_name, tparam, vars )


elif action == MOD_ACTION_NAME:
   result = "trans_set_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_TRANS_SET

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_TRANS_SET

elif action == MOD_ACTION_ITYPE:
    result = MOD_ITYPE_CMD
