#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 12/2018
#
#
# LOC.FCT_SYSPATH / "verz"
#
# Hierüber kann eines der gic-Verzeichnisse abgefragt werden
#
# [0] (str) Verzeichnis
#           ["temp"]        Temporäres Verzeichnis (Es wird ggf. eins erzeugt!)
#           "gic"           Installationsverzeichnis
#           "demo"   Demo-Bilder


# Aktionen
if action == MOD_ACTION_INIT:

   result = [ LOC.FCT_SYSPATH ]

elif action == MOD_ACTION_PTYPE:

    result = [ MOD_PTYP_STR ]

elif action == MOD_ACTION_EXEC:             # Ausführen ....

    # temp?
    if param[ 0 ] == LOC.PVAL_SYSPATH_0[ 0 ]:
        result = path_tmp_get( vars )             # Tmp-Verz.name holen

    # gic?
    elif param[ 0 ] == LOC.PVAL_SYSPATH_0[ 1 ]:
        result = path_get( FOLDER_INSTAL )

    # Demo-Bilder?
    elif param[ 0 ] == LOC.PVAL_SYSPATH_0[ 2 ]:
        result = path_get( FOLDER_DEMO_PICS )

    # Kein passender Parameter ...
    else:
        result = "?"

elif action == MOD_ACTION_NAME:
   result = "syspath_py"

elif action == MOD_ACTION_PDEF:
    result = [ LOC.PVAL_SYSPATH_0[ 0 ] ] # Vorgabewert f. Param: Erster Eintrag

elif action == MOD_ACTION_PVAL:
    result = [ LOC.PVAL_SYSPATH_0 ] # Mögliche Parameterwerte

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_SYSPATH

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_FCT ]

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_SYSPATH
