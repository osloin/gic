#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# LOC.CMD_INFO / "INF"
#
# Infotext ausgeben
#
# [0] (str) Text der ausgeben werden soll


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_INFO ]

elif action == MOD_ACTION_PTYPE:
   result = [ MOD_PTYP_MULTI ]          # Unser Parameter

elif action == MOD_ACTION_EXEC:         # Ausführen ....

    # Was ausgegeben werden soll
    msg = param[ 0 ]

    # Sonderbehandlung für boolsche Werte
    if type( msg ) == bool:
        if msg:
            msg = LOC.TRUE
        else:
            msg = LOC.FALSE

    # Ereignis
    evt( vars, EVT_000, [ msg ] )

    # Meldung
    #msg_.message( msg, msg_.TYPE_MSG )

elif action == MOD_ACTION_NAME:
   result = "info_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_INFO

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_INFO

elif action == MOD_ACTION_ITYPE:
    result = MOD_ITYPE_CMD
