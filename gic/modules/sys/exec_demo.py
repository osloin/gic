#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# LOC.CMD_EXEC_DEMO / "DEMO"
#
# Führt das angegebene Demo-Skript aus, bzw. gibt eine Übersicht über die vor-
# handenen Skripte aus.
#
# [0] (str) Dateiname ohne Endung im gic-Demo-Verzeichnis [""]
#
# Beispiel: DEMO INF
#           (Führt INF.gic im Demo-Verzeichnis aus)

# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_EXEC_DEMO ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_LABEL ]

elif action == MOD_ACTION_EXEC:

    # Skriptname aus Parameter
    script = param[ 0 ]

    if len( script ) < 1:

        # Dateiliste zusammenstellen
        msgtxt = LOC.DUMP_CMDS_INF_2 + \
                 "\n" + \
                 fil_.filelist_doc( path_get( FOLDER_DEMO_CODE ) +
                                    "*." +
                                    MOD_EXT_GIC,
                                    fil_.FE_NAME )
    else:

        # Rückgabewert initalisieren
        vars_set( vars, LOC.VAR_RESULT, True, VIB_VALUE, GTYP_BOOL )

        # Ergebnis der Skriptdurchführung zusammenstellen
        msgtxt = LOC.DEMO + " '" + script + "': "

        # Überprüfen, ob Skript vorhanden ist ...
        if not script_is( script, FOLDER_DEMO_CODE ):   # Nein?

            msgtxt += LOC.MISSING.upper()

        else:                                           # Ja ...

            # Skript ausführen
            result = run_script( script,
                                 path_get( FOLDER_DEMO_CODE ),
                                 vars,
                                 True,
                                 [ image, drawable ] )

            # Ergebnis der Skriptdurchführung zusammenstellen
            if vars_get( vars, LOC.VAR_RESULT ):
                msgtxt += "OK"
            else:
                msgtxt += LOC.ERROR.upper()

    # Ergebnis der Skriptdurchführung ausgeben
    msg_.message( msgtxt, msg_.TYPE_INFO )

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_CMD ]

elif action == MOD_ACTION_PDEF:
    result = [""]

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_EXEC_DEMO

elif action == MOD_ACTION_NAME:
    result = "exec_demo_py"

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_EXEC_DEMO
