#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# LOC.CMD_EXEC_SCRIPT / "DUS"
#
# Führt den Inhalt der angegebenen gic-Datei aus
#
# [0] (str) Dateiname ohne Endung (im Standard-Verzeichnis)
# [1] (str) Verzeichnis [""]
#
# Beispiel: DUS export
#           (Führt export.gic im Standardverzeichnis aus)

# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_EXEC_SCRIPT ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_LABEL, MOD_PTYP_PATH ]

elif action == MOD_ACTION_EXEC:

    # Pfadangabe
    if param[ 1 ] == "":
        path = None
    else:
        path = param[ 1 ]

    # Skript ausführen
    result = run_script( param[ 0 ], path, vars, True )

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_CMD ]

elif action == MOD_ACTION_PDEF:
    result = [ None, "" ]

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_EXEC_SCRIPT

elif action == MOD_ACTION_NAME:
    result = "exec_script_py"

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_EXEC_SCRIPT
