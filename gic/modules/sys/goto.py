#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# LOC.CMD_GOTO / "GEH"
#
# Sprungbefehl
#
# [0] (str) Ausführung hinter diesem Text fortsetzen


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_GOTO ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_STR ]              # Unser Parameter

elif action == MOD_ACTION_EXEC:             # Ausführen ....

    # Position des Sprungziels ermitteln
    idx = text_getidx( param[ 0 ], vars )

    # Auf gültige Position überprüfen
    if idx == -1:                           # Sprungziel nicht gefunden?

        # Exception
        evt( vars, ERR_007, param[ 0 ] )

    # Ausführungspos auf Text setzen
    vars_set( vars, VAR_POS, idx )

elif action == MOD_ACTION_NAME:
   result = "goto_py"

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_GOTO

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_CTRL ]

elif action == MOD_ACTION_LVL:
    result = MOD_LVL_PRIO

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_GOTO
