#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# LOC.CMD_IF / "WENN"
#
# Auswertung einer Bedingung. Wenn die Bedingung nicht erfüllt ist, wird die
# Ausführung einer bestimmten Anzahl von nachfolgenden Anweisungen verhindert.
#
# [0] (bool)    True, wenn der / die nachfolgende(n) Eintrag / Einträge ausgeführt
#               werden soll(en)
# [1] (int/str) Hierüber wird bestimmt, welche nachfolgenden Zellen betroffen
#               sein sollen. Eine positive Ganzzahl gibt eine Anzahl an. Ein
#               Text gibt an, das alle Einträge bis zu dieser Textmarke
#               betroffen sein sollen. [1]


# Aktionen
if action == MOD_ACTION_INIT:
    result = [ LOC.CMD_IF ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_BOOL, MOD_PTYP_BLOCK ]     # Unsere Parameter

elif action == MOD_ACTION_PDEF:
    result = [ None, 1 ]

elif action == MOD_ACTION_EXEC:     # Ausführen ....

    # Der erste Parameter enthält die Bedingung, die ausgewertet wurde
    if param[ 0 ] == False:

        # Ausführung der nächsten Befehle verhindern
        # param[ 1 ] enthält die Anzahl
        vars_set( vars,
                  VAR_NO_EXEC,
                  param[ 1 ],
                  VIB_VALUE,
                  GTYP_INT,
                  VARS_LOC )


elif action == MOD_ACTION_NAME:
    result = "if_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_IF

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_IF

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_CTRL ]

elif action == MOD_ACTION_LVL:
    result = MOD_LVL_PRIO
