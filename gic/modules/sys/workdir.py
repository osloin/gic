#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 1/2019
#
#
# LOC.VAR_WORKDIR / "arbeitsverz"
#
# Stellt eine Variable zum festlegen eines Arbeitsverzeichnisses zur Verfügung.
#
#
# LOC.VAR_WORKEXT / "arbeitstyp"
#
# Stellt eine Variable zum festlegen eines Arbeitstyps zur Verfügung.
#
#
# EVT_100 / Nachbehandlung von GTYP_FILE-Typen
#
# Wenn bei einem GTYP_FILE-Parameter nur ein Dateiname ohne Pfad angegeben
# wurde, wird der Pfad auf das Arbeitsverzeichnis gesetzt.


# Vorzeichen um den internen Var-Namen für LOC.VAR_WORKDIR und LOC.VAR_WORKEXT
# zu bilden
VAR_WORK_PREFIX = "_"


# Modul-Aktionen
if action == MOD_ACTION_INIT:

    # Wir registrieren unsere ID und registrieren uns für Ereignis 100
    result = [ LOC.VAR_WORKDIR,
               LOC.VAR_WORKEXT,
               MOD_GROUP_EVT % 100      # GTYP_FILE - Ereignis
             ]

elif action == MOD_ACTION_EVT:

    # Dateinamen aus den Ereignisparametern holen
    filename = param[ MOD_EVT_P_PARAM ]

    # Wenn kein Pfadtrennzeichen, wird 'arbeitsverz' verwendet
    if not os.sep in filename:                  # Nein?
        if vars_in( vars, LOC.VAR_WORKDIR ):    # Arbeitsverzeichnis da?
            result = vars_get( vars, LOC.VAR_WORKDIR ) + filename # Holen

    # Wenn kein ".", wird 'arbeitstyp' verwendet
    if not "." in filename:                     # Nein?
        if vars_in( vars, LOC.VAR_WORKEXT ):    # Arbeitstyp da?
            if result != None:
                filename = result
            result = filename + "." + vars_get( vars, LOC.VAR_WORKEXT ) # Holen

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_MODVAR, MOD_ITYPE_MODVAR, MOD_ITYPE_GROUP ]

elif action == MOD_ACTION_VAR_G:

    # Wert für Namen holen ("", wenn nicht vorhanden)
    result = vars_get( vars, VAR_WORK_PREFIX + name, default = "" )

elif action == MOD_ACTION_VAR_S:

    # Pfad aus Parameter
    pathname = param[ 0 ]

    # Überprüfen, ob Pfad mit Pfadtrennzeichen endet
    if name == LOC.VAR_WORKDIR:
        if len( pathname ) > 0 and pathname[ -1 ] != os.sep:    # Nein?
            pathname += os.sep                         # Trennzeichen anhängen

    # Speichern
    vars_set( vars, VAR_WORK_PREFIX + name, pathname, VIB_VALUE, GTYP_STR )

elif action == MOD_ACTION_PTYPE:

    if name == LOC.VAR_WORKDIR or name == LOC.VAR_WORKEXT:
        result = [GTYP_STR]

elif action == MOD_ACTION_PDOC:

    if name == LOC.VAR_WORKDIR:
        result = [ LOC.WORKINGDIR ]
    elif name == LOC.VAR_WORKEXT:
        result = [ LOC.FILETYPE ]

elif action == MOD_ACTION_IDDOC:

    if name == LOC.VAR_WORKEXT:
        result = LOC.IDDOC_VAR_WORKEXT
    elif name == LOC.VAR_WORKDIR:
        result = LOC.IDDOC_VAR_WORKDIR

elif action == MOD_ACTION_NAME:
    result = "workdir_py" # Unseren Name

elif action == MOD_ACTION_LVL:
    result = MOD_LVL_PRIO
