#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# LOC.CMD_LIST_ADD / "LIA"
#
# Einer Liste einen Wert zufügen
#
# [0] (str) Listenbezeichnung
# [1] (?)   Wert, der der Liste zugefügt werden soll


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_LIST_ADD ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_LABEL, MOD_PTYP_MULTI ]  # Unsere Parameter

elif action == MOD_ACTION_EXEC:             # Ausführen ....

    # Name / ID der Liste aus Parameter holen
    id_list = param[ 0 ]

    # Wert
    value = param[ 1 ]

    # Wert in Liste aufnehmen
    if vars_in( vars, id_list ):        # Liste schon vorhanden?

        # Liste holen
        l = vars_get( vars, id_list, VIB_VALUE_DI )

        # Neuen Eintrag zufügen
        l.append( value )

        # Erweiterte Liste speichern
        vars_set( vars, id_list, l, VIB_VALUE_DI, GTYP_LIST )

    else:                               # Noch keine Liste vorhanden ...
        # Liste mit einem Eintrag
        l = [ value ]

        # Neue Liste speichern
        vars_set( vars, id_list, l, VIB_VALUE, GTYP_LIST )

        # Position auf ersten Eintrag setzen
        vars_set( vars, id_list, 0, VIB_LIST_POS )

        # Modus. 'True': autom. erhöhen der Indexposition
        vars_set( vars, id_list, True, VIB_LIST_MODE )

elif action == MOD_ACTION_NAME:
   result = "list_add_py"

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_LIST_ADD

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_CMD ]

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_LIST_ADD
