#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 1/2019
#
#
# LOC.CMD_SAVE_NO / "NEE"
#
# Kann während der Ausführung des SPE-Befehls verendet werden, um ein Speichern
# des Bildes zu verhindern.


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_SAVE_NO ]

elif action == MOD_ACTION_EXEC:

   # Abbruch-Signal
   vars_set( vars, MOD.VAR_SAVE_NO, True, VIB_VALUE, GTYP_SIGNAL )

elif action == MOD_ACTION_NAME:
   result = "save_no_py"

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_SAVE_NO

elif action == MOD_ACTION_ITYPE:
    result = MOD_ITYPE_CMD_G
