#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 1/2019
#
#
# Eingabekontrolle
#
#
# LOC.CMD_EXEC_AGAIN / "WIE"
#
#   WIE-Befehl zum wiederholen der letzten Eingabe
#
# EVT_034 / Benutzereingabe ausführen
#
# - Eine leere Eingabe wird in einen Befehl zur Befehlsauflistung umgeformt
# - Falls als erstes Zeichen ein Fragezeichen eingegeben wurde, wird das als
#   Aufforderung für Anzeige der Befehlsinfo angesehen.
#
# EVT_035 / Benutzereingabe ausgeführt
#
# - Letzte Benutzereingabe abspeichern
#
# ERR_006 / Parameter fehlt
#
# - Es wird eine Info über den Befehl und seine Parameter ausgegeben


# Signal, dass CMD_EXEC_AGAIN gerade durchgeführt wird
SIG_EXEC_AGAIN = "_SIG_EXEC_AGAIN"


# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_EXEC_AGAIN,
              MOD_GROUP_EVT % 34,
              MOD_GROUP_EVT % 35 ]
              # MOD_GROUP_EVT % -6 ]

elif action == MOD_ACTION_EXEC:         # WIE-Befehl

    # Signal, dass WIE gerade durchgeführt wird
    vars_set( vars, SIG_EXEC_AGAIN, True, VIB_VALUE, GTYP_SIGNAL )

    # Letzte Eingabe da?
    if vars_in( vars, LOC.VAR_RECENT ):

        # Letzte Eingabe holen
        sgic = vars_get( vars, LOC.VAR_RECENT )

        # Geschütze Zeichen werden wieder zu Anführungszeichen
        sgic = sgic.replace( GIC_RPL_QM, GIC_QM )

        # Ereignis: Letzte Eingabe wiederholen
        evt( vars, EVT_036, sgic )

        # Erneut ausführen
        run( sgic, vars )

    # Signal entfernen
    vars_get_signal( vars, SIG_EXEC_AGAIN )

elif action == MOD_ACTION_EVT:          # Ereignis 34 / 35

    # Benutzereingabe aus Ereignis-Parameter holen
    sgic = param[ MOD_EVT_P_PARAM ]

    # Ereignis-ID
    id_evt = param[ MOD_EVT_P_EVT ]

    # Ereignis: 34 / Benutzereingabe
    if id_evt == 34:

        # Benutzereingaben vorhanden?
        if len( sgic ) == 0:            # Nein?

            # INB-Befehl (Alle Befehle und Vars ausgeben) zurückliefern
            result = LOC.CMD_DUMP_CMDS

        else:   # Benutzereingabe vorhanden ...

            # Steht vorne ein Fragezeichen, wird Befehl nicht ausgeführt,
            # sondern nur eine Info über den Befehl ausgegeben
            if sgic[ 0 ] == "?":

                # Befehl zur Befehlsdokuanzeige zusammenstellen. Dabei werden
                # alle Zeichen hinter dem Fragezeichen bis zum Erreichen eines
                # Leerzeichens oder einer geöffneten Klammer gesammelt ...
                result = LOC.CMD_DUMP_CMDS +                    \
                            GIC_SPACE +                         \
                            str_.collect( sgic[ 1: ],           \
                                          GIC_SPACE + GIC_FCT1, \
                                          True,                 \
                                          True )

            # Überprüfen, ob es sich bei 'sgic' um einen Skriptnamen handelt
            elif script_is( sgic ) == True:	# Ja?

                # Befehl zur Skriptausführung
                result = LOC.CMD_EXEC_SCRIPT + GIC_SPACE + sgic


    # Ereignis: 35 / Benutzereingabe ausgeführt
    elif id_evt == 35:

        # Nur wenn WIE-Befehl nicht gerade ausgeführt wird
        if not vars_in( vars, SIG_EXEC_AGAIN ):

            # SEI-Befehl zusammenstellen. Alle Anführungszeichen werden durch
            # das geschützte Zeichen ersetzt
            sgic = LOC.CMD_SETINIT  + \
                   GIC_SPACE        + \
                   LOC.VAR_RECENT   + \
                   GIC_ASS          + \
                   GIC_QM + sgic.replace( GIC_QM, GIC_RPL_QM ) + GIC_QM

            # SEI-Befehl ausführen
            run( sgic, vars )

    # Ereignis: - 6 / Parameter fehlt
    elif id_evt == -6:
        d( evt, "input_ctrl_py" )


elif action == MOD_ACTION_NAME:
   result = "input_ctrl_py"

elif action == MOD_ACTION_IDDOC:
    if name == LOC.CMD_EXEC_AGAIN:
        result = LOC.IDDOC_EXEC_AGAIN
    else:
        result = "EVT_034 / EVT_035"

elif action == MOD_ACTION_ITYPE:
    result = [  MOD_ITYPE_CTRL,
                MOD_ITYPE_GROUP,
                MOD_ITYPE_GROUP ]
                #MOD_ITYPE_GROUP ]
