#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 1/2019
#
#
# LOC.CMD_IF / "XXX"
#
# Verhindert die Ausführung nachfolgender gic-Abschnitte
#
# [0] (int/str) Hierüber wird bestimmt, welche nachfolgenden Zellen betroffen
#               sein sollen. Eine positive Ganzzahl gibt eine Anzahl an. Ein
#               Text gibt an, das alle Einträge bis zu dieser Textmarke
#               betroffen sein sollen. [1]

# Aktionen
if action == MOD_ACTION_INIT:
    result = [ LOC.CMD_XXX ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_BLOCK ]     # Unser Parameter

elif action == MOD_ACTION_PDEF:
    result = [ 1 ]

elif action == MOD_ACTION_EXEC:     # Ausführen ....

    # Ausführung der nächsten Befehle verhindern
    # param[ 0 ] enthält die Anzahl
    vars_set( vars,
              VAR_NO_EXEC,
              param[ 0 ],
              VIB_VALUE,
              GTYP_INT,
              VARS_LOC )


elif action == MOD_ACTION_NAME:
    result = "xxx_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_XXX

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_XXX

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_CTRL ]

elif action == MOD_ACTION_LVL:
    result = MOD_LVL_PRIO
