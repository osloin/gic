#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# LOC.CMD_DUMP_VARS / "INV"
#
# Ausgabe der vars[]
#
#
# LOC.CMD_DUMP_CMDS / "INB"
#
# Detailinfos zu einem Befehl
#
#
# LOC.CMD_DUMP_TEMP / "KAJ"
#
# Befehlsübersicht in eine Text-Datei im Temp-Ordner ausgeben

# Modul-Aktionen
if action == MOD_ACTION_INIT:

    # Unsere Symbole
    result = [ LOC.CMD_DUMP_VARS,
               LOC.CMD_DUMP_CMDS,
               [ LOC.CMD_DUMP_TEMP, VF_INV ] ]

elif action == MOD_ACTION_ITYPE:

    result = [ MOD_ITYPE_CMD,
               MOD_ITYPE_CMD,
               MOD_ITYPE_CMD ]

elif action == MOD_ACTION_PTYPE:

   if name == LOC.CMD_DUMP_VARS:
       result = [] # keine Parameter
   elif name == LOC.CMD_DUMP_TEMP:
       result = []
   elif name == LOC.CMD_DUMP_CMDS:
       result = [ MOD_PTYP_LABEL ]

elif action == MOD_ACTION_EXEC:

   if name == LOC.CMD_DUMP_VARS:

       # Alle IDs, sortiert
       result = vars_list_ex( vars, vars_list( vars, None, True ) )

       if result != None:
           result = "\n" + result

           # Meldung
           msg_.message( result, msg_.TYPE_DUMP )

   # Zeige Befehle u.Ä.
   elif name == LOC.CMD_DUMP_CMDS or name == LOC.CMD_DUMP_TEMP:

       # Einer oder Alle?
       if name == LOC.CMD_DUMP_TEMP:
          dump_all = True
       elif param[ 0 ] == None or param[ 0 ] != "":   # Zu einem?
          dump_all = False
       else:
          dump_all = True

       # Infos zu einem Eintrag oder allen Einträgen
       if not dump_all:   # Zu einem?

           result = id_doc( param[ 0 ] )
           if result == None:
               result = "'" + param[ 0 ] + "' ?"

       else:    # Zu allen ...

           result = id_doc( None )

           # Legende
           # result += "\n"
           # i = 0
           # for t in LOC.GTYPES:
           #     if i not in GTYP__NO_P:
           #         result += "\n(%s) %s" % ( LOC.GTYPES[ i ][LOC.GTY_SHORT],
           #                                   LOC.GTYPES[ i ][LOC.GTY_LONG] )
           #     i += 1

       # Datei oder Msg?
       if name == LOC.CMD_DUMP_TEMP:
           # Datei
           file_write( path_get( FOLDER_TMP ) + LOC.DUMP_TEMP_FILE,
                       result,
                       None,
                       vars )
       else:
           # Meldung
           msg_.message( result, msg_.TYPE_DUMP )

elif action == MOD_ACTION_NAME:
    result = "dump_py" # Unser Name

elif action == MOD_ACTION_PDOC:
    if name == LOC.CMD_DUMP_CMDS:
        result = LOC.PDOC_DUMP_CMDS

elif action == MOD_ACTION_IDDOC:

    if name == LOC.CMD_DUMP_CMDS:
        result = LOC.IDDOC_DUMP_CMDS

    elif name == LOC.CMD_DUMP_VARS:
        result = LOC.IDDOC_DUMP_VARS

    else:
        result = LOC.IDDOC_DUMP_TEMP

elif action == MOD_ACTION_LVL:
    result = MOD_LVL_PRIO

elif action == MOD_ACTION_PDEF:
    if name == LOC.CMD_DUMP_CMDS:
        result = [""]
