#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# LOC.CMD_SET / "SET"
#
# Einer Variablen einen Wert zuweisen
#
# [0]    (str) Bezeichner (ohne Anführungszeichen)
# [1]    (?)   Wert
# [2]    (str) Bei der Initalisierung einer Var kann hier 'lokal' angegeben
#              werden, um die Variable als eine lokale Variable anzulegen.
#              (ansonsten ist sie global)
#              Nach der Initalierung wird diese Angabe nicht mehr ausgewertet
#              [global]
#
# Beispiel: "SET i = 5", um der Variablen i den Wert 5 zuzuweisen


# Modul-Aktionen
if action == MOD_ACTION_INIT:

    # Unsere Symbole
    result = [ LOC.CMD_SET ]

elif action == MOD_ACTION_ITYPE:

    result = [ MOD_ITYPE_CMD ]

elif action == MOD_ACTION_PTYPE:

    result = [ MOD_PTYP_LABEL, MOD_PTYP_DIRECT, MOD_PTYP_LABEL ]

elif action == MOD_ACTION_PDEF:

    result = [ None, None, LOC.PVAL_SET_2[ 1 ] ]

elif action == MOD_ACTION_EXEC:

    # Ziel / Varname
    lbl = param[ 0 ]

    # Zugriff auf System-Var verhindern ...
    if lbl[ 0 : 1 ] == VARS_SYS_PREFIX:
        evt( vars, ERR_001, [ lbl, param[ 1 ] ] )

    # Nachsehen, ob Name schon existiert
    space = vars_find( vars, lbl )

    # Sonderfall: Variable existiert als globale Variable, soll aber jetzt
    #             unter dem gleichen Namen lokal angelegt werden
    if space == VARS_GLOB and param[ 2 ] == LOC.PVAL_SET_2[ 0 ]:
        space = -1  # Var noch nicht (lokal) vorhanden

    # Zieltyp und Namensraum ermitteln
    if space != -1:                         # Ziel vorhanden?

        # Typ ermitteln
        gtyp = vars_get( vars, lbl, VIB_GTYP, space )

        # Behandlung modulverwalteter Var
        if gtyp == GTYP_MODVAR:
            gtyp = vars_get( vars, lbl, VIB_PGTYPES, space )[ 0 ]

    else:                                   # Name ist noch nicht vorhanden ...
        # Std-Typ
        gtyp = GTYP_MULTI

        # Dritter Parameter bestimmt, ob Var lokal oder global ist
        if param[ 2 ] == LOC.PVAL_SET_2[ 0 ]:
            space = VARS_LOC
        else:
            space = VARS_GLOB

    # Werte ersetzen
    value = evalV( param[ 1 ], gtyp, vars, RVQM, RV_FI, RV_RE )

    # Ereignis: Wertzuweisung
    evt( vars, EVT_006, [ lbl, value ] )

    # Wert eintragen
    vars_set( vars, lbl, value, VIB_VALUE, gtyp, space )

    # Wert als Ergebnis zurückliefern
    result = value

    # Exception, wenn ungültiger Typ (Typ nicht in VARS_TYPES-Array)
    VARS_TYPES.index( type( value ) )


elif action == MOD_ACTION_NAME:
    result = "set_py" # Unser Name

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_SET

elif action == MOD_ACTION_PSPLIT:
    result = [ GIC_ASS, GIC_PARAM ]    # Trenner

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_SET

elif action == MOD_ACTION_LVL:
    result = MOD_LVL_PRIO

elif action == MOD_ACTION_PVAL:
    result = [ [], [], LOC.PVAL_SET_2 ]
