#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# LOC.FCT_EXISTS / "da"
#
# Hierüber kann überprüft werden, ob etwas bestimmtes existiert.
#
# [0] (str) Name der überprüft werden soll
# [1] (str) Was im ersten Parameter angegeben wurde
#           ['var'] Ein Var-Name
#           'datei' Ein Dateiname
#           'pos'   Listenposition


# Aktionen
if action == MOD_ACTION_INIT:

   result = [ LOC.FCT_EXISTS ]

elif action == MOD_ACTION_PTYPE:

    result = [ MOD_PTYP_DIRECT, MOD_PTYP_LABEL ]

elif action == MOD_ACTION_EXEC:             # Ausführen ....

    # Objekt, das überprüft werden soll
    obj = param[ 0 ]

    # Objekt-Typ
    objtype = param[ 1 ]

    # Ergebnis. Vorgabewert: Nicht da
    result = False

    # Var soll überprüft werden?
    if objtype == LOC.PVAL_EXISTS[ 0 ]:         # 'var'

        # In lokalen od. globalen Vars vorhanden?
        result = vars_find( vars, obj ) != VARS__UNDEF

    # Listenpos soll überprüft werden?
    elif objtype == LOC.PVAL_EXISTS[ 2 ]:       # 'pos'

        # In lokalen od. globalen Vars?
        vspace = vars_find( vars, obj )

        # Wenn Name gefunden wurde ...
        if vspace != VARS__UNDEF:

            # Typ für Namen
            gtype = vars_get( vars, obj, VIB_GTYP, vspace )

            # Wenn korrekter Typ ...
            if gtype == GTYP_LIST:

                # Ist Pos ok ...
                lpos = vars_get( vars, obj, VIB_LIST_POS, vspace )
                if lpos != -1:

                    # Ergebnis: Listenpos gefunden
                    result = True

    # Dateiname soll überprüft werden?
    elif objtype == LOC.PVAL_EXISTS[ 1 ]:       # 'datei'

        # Parameter aufbereiten
        filename = evalV( obj, GTYP_FILE, vars )

        # Datei da oder nicht ...
        result = fil_.isfile( filename )

    else:
        # Gibts nicht
        evt( vars, ERR_011 )


elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_FCT ]

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_EXISTS

elif action == MOD_ACTION_PDEF:
    result = [ None, LOC.PVAL_EXISTS[ 0 ] ]

elif action == MOD_ACTION_PVAL:
    result = [ None, LOC.PVAL_EXISTS ]

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_EXISTS

elif action == MOD_ACTION_NAME:
   result = "fct_exists_py"
