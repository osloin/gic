#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# Registriert sich für bestimmte Programmereignisse und informiert über ihr
# auftreten.


# Namen, den wir für eine laufende Nummer verwenden
VAR_EVT_MSG_POS = "_VAR_EVT_MSG_POS"


# Aktionen
if action == MOD_ACTION_EVT:

    id_evt    = param[ MOD_EVT_P_EVT        ]
    para_evt  = param[ MOD_EVT_P_PARAM      ]
    txt_evt   = param[ MOD_EVT_P_TXT        ]
    pview_evt = param[ MOD_EVT_P_PARAM_VIEW ]

    # Parameter in Zeichenkette konvertieren
    if para_evt != None:
        ps = lis_.to_str( para_evt, idxfilter = pview_evt )
        ps = con_.roundup( ps, 70 ) # Auf 70 Zeichen begrenzen
    else:
        ps = ""

    # Je nach Ereignis
    if id_evt == 7:     # Ausführungstart

        # Startzeit speichern
        vars_set( vars, "_start_time", time.time() )

        # Position einrichten
        vars_set( vars, VAR_EVT_MSG_POS, 0 )

        # Ereignistext ersetzen
        txt_evt = LOC.GIC + " ..."

        # Ausgabe
        #msg_add = ""
        msg_add = None # Keine

    elif id_evt == 8:   # Ausführungsende

        # Startzeit holen
        start_time = vars_get( vars, "_start_time" )

        # Durchführungszeit
        sec = time.time() - start_time

        # Durchführungszeit in 'vars' vermerken
        vars_set( vars, VAR_TIME_RUN, sec )

        # Name und Version
        msg_add = "%s %d.%d.%d: " % ( _NAME,
                                      _VERSION[ 0 ],
                                      _VERSION[ 1 ],
                                      _VERSION[ 2 ] )

        # Ausführungszeit in Sekunde
        ps += " %.3f s" % sec

        # Ereignistext ersetzen
        txt_evt = LOC.EXEC_TIME

    elif id_evt == 15:   # EVT_015 / SEI

        # Wir wollen anzeigen, aber nicht letzte Eingabe
        if para_evt[ 0 ] == LOC.VAR_RECENT:
            msg_add = None                    # Keine Anzeige
        else:
            msg_add = ""                      # Anzeige
    else:
        msg_add = ""

    # Nachrichtentext zusammenstellen?
    if msg_add != None:

        # Position einrichten
        pos = vars_inc( vars, VAR_EVT_MSG_POS )

        # Nachrichtentext zusammenstellen
        # (%d) id_evt,
        msgtxt = "[%d] %s%s %s" % ( pos, msg_add, txt_evt, ps )

        # Ggf. Platzhalter des 'WIE'-Befehls ersetzen
        if id_evt == 15:
            msgtxt = msgtxt.replace( GIC_RPL_QM, "\"" )

        # Meldung
        msg_.message( msgtxt, msg_.TYPE_INFO )


elif action == MOD_ACTION_INIT:

    # Wir wollen folgende Ereignisse ...
    result = [ MOD_GROUP_EVT % 0,   # EVT_000 / Meldung
               MOD_GROUP_EVT % 7,   # EVT_007 / vars_new()
               MOD_GROUP_EVT % 8,   # EVT_008 / vars_release()
               MOD_GROUP_EVT % 9,   # EVT_009 / Zuweisen von Transf.-Modulen
               MOD_GROUP_EVT % 10,  # EVT_010 / Zuweisen von Transf.-Modulen a.
               MOD_GROUP_EVT % 15,  # EVT_015 / SEI
               MOD_GROUP_EVT % 16,  # EVT_016 / EXI
               MOD_GROUP_EVT % 22,  # EVT_022 / Abbruchbefehl
               MOD_GROUP_EVT % 26,  # EVT_026 / Ordner anlegen
               MOD_GROUP_EVT % 27,  # EVT_027 / Stapelverarbeitung
               MOD_GROUP_EVT % 28,  # EVT_028 / Schleife gestartet
               MOD_GROUP_EVT % 30,  # EVT_030 / Schleife beendet
               MOD_GROUP_EVT % 31,  # EVT_031 / Leerer Eintrag
               MOD_GROUP_EVT % 33,  # EVT_033 / Speicherung wurde verhindert
               MOD_GROUP_EVT % 36,  # EVT_036 / Letzte Eingabe erneut ausführen
               MOD_GROUP_EVT % 37,  # EVT_037 / Neue Datei angelegt
               MOD_GROUP_EVT % 521, # EVT_521 / Bild geladen
               MOD_GROUP_EVT % 523, # EVT_523 / Pfadnamen-gic ausführen
               MOD_GROUP_EVT % 528, # EVT_528 / Bild entfernt
               MOD_GROUP_EVT % 530, # EVT_530 / Bild wurde gespeichert
               MOD_GROUP_EVT % 538  # EVT_538 / Bild im Speicher dupli.
             ]

elif action == MOD_ACTION_ITYPE:

    # Alle Rückgaben bei MOD_ACTION_INIT sollen von diesem Typ sein
    result = MOD_ITYPE_GROUP

elif action == MOD_ACTION_NAME:
    result = "evt_msg_py"

elif action == MOD_ACTION_IDDOC:
    result =  LOC.EVENTS

elif action == MOD_ACTION_LVL:
    result = MOD_LVL_PRIO
