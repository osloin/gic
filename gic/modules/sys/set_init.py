#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 10/2018
#
#
# LOC.CMD_SETINIT / "SEI"
#
# Initalisierungscode einer Var festlegen, der mit EXI wieder entfernt werden
# kann.
#
# [0] (str)  Bezeichner
# [1] (str)  Wert
# [2] (bool) [False]:Der 'Wert' soll unbearbeitet in 'autoexec.gic' eingetragen
#                    werden und erst bei der nächsten Ausführung analysiert
#                    werden.
#              True: 'Wert' soll sofort analysiert werden und das Ergebnis soll
#                    dann in 'autoexec.gic' eingetragen werden.

# Aktionen
if action == MOD_ACTION_INIT:
   result = [ LOC.CMD_SETINIT ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_LABEL, MOD_PTYP_DIRECT, MOD_PTYP_BOOL ] # Unsere Param.

elif action == MOD_ACTION_EXEC:

    # Name der autoexec-Datei
    filename = path_get( FOLDER_SCRIPTS ) + MOD_AUTOEXEC_EXT

    # Variablenname
    var_name = param[ 0 ]

    # Sofort analysieren oder erst zur Laufzeit
    if not param[ 2 ]:          # Laufzeit?

        # Wert
        value = param[ 1 ]

    else:                       # Sofort analysieren ...

        # Platzhalter ersetzen
        value = evalV( param[ 1 ], GTYP_MULTI, vars, RVQM, RV_FI, RV_RE )

        # Wert für Eintragung aufbereiten
        if type( value ) == str:
            value = GIC_QM + value + GIC_QM
        elif type( value ) == bool:
            if value:
                value = LOC.TRUE
            else:
                value = LOC.FALSE
        else:
            value = str( value )

    # Wert als Ergebnis zurückliefern
    result = value

    # Ereignis: Initalisierungswert festlegen
    evt_result = evt( vars, EVT_015, [ var_name, value ] )

    # Wenn kein Abbruch durch Ereignis ...
    if evt_result == None or evt_result != False:

        # autoexec-Datei laden
        # 2: EVT_020 nur auslösen, wenn Datei existiert aber nicht gelesen w. k.
        autoexec, state = file_read( filename, "", None, vars, 2 )

        # Wenn geladen oder nicht vorhanden ...
        if state == fil_.ST_OK or state == fil_.ST_NO:

            # Varzuweisung ersetzen
            autoexec = code_gic_setvar( autoexec, var_name, value, vars )

            # autoexec-Datei schreiben
            file_write( filename, autoexec, "set_init.py", vars )


elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_SETG

elif action == MOD_ACTION_PDEF:
    result = [ None, None, False ]

elif action == MOD_ACTION_NAME:
    result = "set_init_py"

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_SETG

elif action == MOD_ACTION_ITYPE:
    result = MOD_ITYPE_CMD

elif action == MOD_ACTION_PSPLIT:
    result = [ GIC_ASS, GIC_PARAM ]   # Gleichheitszeichen als Trenner verwenden
