#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# gic-Modul
# osloin 9/2018
#
#
# Zerlegen von gic-Code
#
#
# LOC.CMD_PARSE_1 / "PARSE_1"
#
# qic-Zeichenkette in einzelne Einträge aufsplitten
#
# [0] gic-Code
#
# Ergebnis: (list) Liste der Abschnitte
#
#
# LOC.CMD_PARSE_2 / "PARSE_2"
#
# Einzelner Eintrag: Befehl von seinen Parametern trennen
#
# [0] gic-Code
#
# Ergebnis: (list) [0] ID des Befehls
#                  [1] (list) Parameter des Befehls


# Aktionen
if action == MOD_ACTION_INIT:
    result = [ LOC.CMD_PARSE_1, LOC.CMD_PARSE_2 ]

elif action == MOD_ACTION_PTYPE:
    result = [ MOD_PTYP_DIRECT ]    # Unser Parameter

elif action == MOD_ACTION_EXEC: # Ausführen ....

    # qic-Zeichenkette in Einträge aufsplitten
    if name == LOC.CMD_PARSE_1:

        # Parameter übernehmen
        sgic = param[ 0 ]

        # die cmds[]-Auflistung initalisieren
        if str_.find_ex( sgic, GIC_SEP, GIC_QM ) == -1: # Nur ein Kommando?

           result = range( 1 )        # Liste mit einem Eintrag anlegen
           result[ 0 ] = sgic.strip() # Leerzeichen vorne und hinten entfernen

        else: # mehrere Kommandos vorhanden ...

           # Die Kommandos aufsplitten
           result = str_.split_ex( sgic,
                                   GIC_SEP,
                                   GIC_QM,
                                   GIC_QM,
                                   None,
                                   True,    # strip() durchführen
                                   False,   # Leere Einträge nicht unterdrücken
                                   GIC_FCT1,
                                   GIC_FCT2 )

    # gic-Eintrag in Befehl und seine Parameter trennen
    else:   # LOC.CMD_PARSE_2 ...

        # Leerzeichen vorne und hinten entfernen
        cmd = param[ 0 ].strip()

        # Nach Trennposition 'pos' zwischen Befehlsnamen und seinen Parametern
        # in 'cmd' suchen
        pos = -1                # Noch nicht gefunden
        i   = 0                 # Akt. Index
        for c in cmd:           # Alle Zeichen ...

            # Wenn Anführungszeichen: Zeichenkette (Sprungmarke od Kommentar)
            if c == GIC_QM:
                break           # for abbrechen

            # Wenn Leerzeichen: 'cmd' ist ein Befehl mit Parametern
            if c == GIC_SPACE:
                pos = i         # Pos auf Leerzeichen

            # Wenn Zuweisungsoperator: (weggelassener) SET-Befehl
            elif c == GIC_ASS:

                # SET-Befehl zusammenstellen
                cmdpre = LOC.CMD_SET + GIC_SPACE

                # Pos auf Leerzeichen hinter "SET"
                pos = len( LOC.CMD_SET )

                # SET-Befehl voranstellen
                cmd = cmdpre + cmd

                # for abbrechen
                break

            # Wenn "a += b" -> "SET a=a+b"
            elif c == GIC_PLUS and cmd[ i + 1 ] == GIC_ASS:

                # Name
                cmdpre = cmd[ : i ]

                # SET-Befehl zusammenstellen
                cmd = LOC.CMD_SET + GIC_SPACE + \
                      cmdpre + GIC_ASS + cmdpre + GIC_PLUS + \
                      cmd[ i + 2: ]

                # Pos auf Leerzeichen hinter "SET"
                pos = len( LOC.CMD_SET )

                # for abbrechen
                break

            # Wenn "a -= b" -> "SET a=a-b"
            elif c == GIC_MIN and cmd[ i + 1 ] == GIC_ASS:

                # Name
                cmdpre = cmd[ : i ]

                # SET-Befehl zusammenstellen
                cmd = LOC.CMD_SET + GIC_SPACE + \
                      cmdpre + GIC_ASS + cmdpre + GIC_MIN + \
                      cmd[ i + 2: ]

                # Pos auf Leerzeichen hinter "SET"
                pos = len( LOC.CMD_SET )

                # for abbrechen
                break

            # Kein Sonderzeichen ...
            else:
                if pos != -1:       # Schon Sonderzeichen erkannt?
                    break           # for abbrechen

            # Nächster Index
            i += 1

        # Befehl von seinen Parametern trennen
        if pos == -1:                  # Kein Leerzeichen? -> Keine Parameter

           # Ggf. Befehlsnamen in Langform übersetzen
           cmd = id_expand( cmd, vars, LOC.CMDS_SHORT )

           paras = range( 0 )          # Auflistung ohne Einträge

        else: # Kommando hat einen od. mehrere Parameter ...

           # Parameter liegen ab dem gefundenen Leerzeichen
           pars = cmd[ pos + 1: ].strip()

           # Befehlsnamen von seinen Parametern trennen
           cmd = cmd[ :pos ].strip()

           # Ggf. Befehlsnamen in Langform übersetzen
           cmd = id_expand( cmd, vars, LOC.CMDS_SHORT )

           # Std-Trennzeichen für Parameter
           sep = GIC_PARAM

           # Überprüfen, ob das Kommando eigene Trennoperator verwenden kann
           if id_get_typeinfo( cmd, _GTY_TYPAR, vars ) != -1: # Ja?

               # Eigene Trennoperatoren abfragen
               psplits = vars_get( vars, cmd, VIB_PSPLITS )
               if psplits != None:
                   sep = psplits    # Als Wert für split_ex() übernehmen

           # Parameter-Zeichenkette in Liste konvertieren
           paras = str_.split_ex( pars,
                                  sep,
                                  GIC_QM,
                                  GIC_QM,
                                  None,
                                  True,
                                  False,
                                  GIC_FCT1,
                                  GIC_FCT2 )

        # Ergebnis zusammenstellen
        # Positionen beachten! ( CMD_PARSE_2_ID und CMD_PARSE_2_PARAM )
        result = [ cmd, paras ]

elif action == MOD_ACTION_NAME:
    result = "parse_py"

elif action == MOD_ACTION_PDOC:
    result = LOC.PDOC_PARSE

elif action == MOD_ACTION_IDDOC:
    result = LOC.IDDOC_PARSE

elif action == MOD_ACTION_ITYPE:
    result = [ MOD_ITYPE_INTERN, MOD_ITYPE_INTERN ]

elif action == MOD_ACTION_LVL:
    result = MOD_LVL_PRIO
