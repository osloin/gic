#!/usr/bin/env python
# -*- coding: utf-8 -*-
# osloin. 11/2018
#
# Hilfsfunktionen zur Behandlung von Code
#
# py_replace()      Zeichenkette durch Code ersetzen



###############################################################################
# Sucht in Python-Code nach einer Zeichenkette und ersetzt sie durch Code,
# wobei sichergestellt wird, dass bei einer mehrzeiligen Ersetzung der linke
# Originalabstand bewahrt bleibt.
#
# 'code'        (str) Python-Code
# 'replace'     (str) Zeichenkette, die ersetzt werden soll
#               (str-list) Wenn ein Array mit 2 Einträgen angegeben wird,
#                           wird der durch diese beiden Zeichenketten
#                           eingeschlossen Bereich (inklusiv der beiden Ketten
#                           selbst) ersetzt.
# 'lines'       (str-list) Code-Zeilen
# 'add'         (str) Zusätzliche Zeichenkette, die vor einem Zeilenvorschub
#                     eingefügt werden soll.
#
# return        (str) Neuer Code
###############################################################################
def py_replace( code, replace, lines = [], add = "" ):

    result = ""
    chars  = ""

    # Je nach Parametertyp
    if type( replace ) == str:          # Zeichenkette?
        pos = code.find( replace )
        posEnd = pos+len(replace)       # Position dahinter
    elif type( replace ) == list:       # str-list mit zwei Einträgen
        pos = code.find( replace[0] )
        posEnd = code.find( replace[1] )+len(replace[1])  # Position dahinter
    else:
        raise RuntimeError( "py_replace" )

    # Vor der Fundstelle
    if pos > 0:                 # gefunden?
        result = code[:pos]     # Bis zur Fundstelle

        posLF = code.rfind("\n", 0, pos)    # Suche links Zeilenvorschub
        if posLF == -1:
            posLF = 0
        else:
            posLF += 1

        chars = code[ posLF : pos ]         # Code vor Suchpos kopieren

    # Zeilen einfügen
    if pos != -1:                 # gefunden?
        i = 0
        for l in lines:
            if i > 0:
                result = result + add + "\n" + chars
            result += l
            i += 1

    # Hinter der Fundstelle
    if pos > 0:                 # gefunden?
        result = result + code[posEnd:] # Ab Fundstelle

    return result
