#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Konvertierungs-Funktionen
#
# byte_int()         bytes in int konvertieren
# image_data_write() Byte-Folge in Bild speichern
# int_byte()         int in bytes konvertieren
# roundup()          Parameter in einer Zeichenkette zusammenfassen
# str_big()          Lange Zeichenketten begrenzen
# type_idx()         Typ in Index (Integer) konvertieren
# type_str()         Typ in Zeichenkette konvertieren



import locale   as LOC



################################################################################
#                               type_idx()
################################################################################
CTI_INT     = 0
CTI_BOOL    = 1
CTI_FLOAT   = 2
CTI_STR     = 3
CTI_LIST    = 4
CTI_DICT    = 5
CTI_UNKNOWN = 6



###############################################################################
# Konvertiert eine byte-Folge in einen int-Wert
#
# 'byte_value' (bytearray) Wert
#
# Ergebnis  (int)
###############################################################################
def byte_int( byte_value ):

    result    = 0

    mask_int  = 1
    mask_byte = 1
    byte_pos  = 0

    while byte_pos < 4:

        if ( byte_value[ byte_pos] & mask_byte ) == mask_byte:

            result |= mask_int

        if mask_byte == 128:

            mask_byte = 1
            byte_pos += 1

        else:
            mask_byte <<= 1

        mask_int <<= 1

    return result


###############################################################################
# Konvertiert einen int-Wert in eine byte-Folge
#
# 'value'    (int) Wert
#
# Ergebnis  (bytearray)
###############################################################################
def int_byte( int_value ):

    result = bytearray( 4 )

    mask_int  = 1
    mask_byte = 1
    byte_pos  = 0

    while byte_pos < 4:

        if ( int_value & mask_int ) == mask_int:

            result[ byte_pos ] |= mask_byte

        if mask_byte == 128:

            mask_byte = 1
            byte_pos += 1

        else:
            mask_byte <<= 1

        mask_int <<= 1

    return result


###############################################################################
# Wandelt - soweit möglich - beliebige Parameter in eine einzeilige Zeichen-
# kette mit einer max. großen Zeichenanzahl um.
# Aus z.B. "Das ist ein Test" wird "Das ist ..."
#
# 'p'          (?)   Parameter
# 'max'        (int) Max. Länge [20]
# 'lf_replace' (str) Zeichen mit dem eventuelle Zeilenvorschübe ersetzen werden
#                    sollen. 'None', um ein Ersetzen zu verhindern. [""]
# 's_end'      (str) Zeichenkette, die ein Abschneiden symbolisiert ["..."]
#
# Ergebnis Zeichenkette
###############################################################################
def roundup( p, max = 20, lf_replace = "", s_end = " ..." ):

    # Ergebnis
    result = ""

    # Ergebnis
    result = str( p )

    # Zeilenvorschübe ersetzt
    if lf_replace != None:
        result = result.replace( "\n", lf_replace ) # ersetzen

    # ggf. auf max Länge begrenzen und mit Endmarkierung versehen
    if len( result ) > max: # zu lang?
        if max > len( s_end ):
           result = result[ 0 : max - len( s_end ) ] + s_end
        else:
           result = s_end[ -max : ]

    # Ergebnis
    return result


###############################################################################
# Eine große Zeichenkette auf eine bestimmte Länge begrenzen
#
# 's'       (str)
# 'max'     (int) Max. Länge
#
# Ergebnis Zeichenkette
###############################################################################
def str_big( s, max = 20 ):

    if len( s ) > max or "\n" in s: # Sehr lang od. mit LF
        result = "%s(%d)" % ( LOC.STRING, len( s ) )
    else:
        result = s

    return result



###############################################################################
# Konvertiert Typ in einen Integer-Wert / Index
#
# 'p'       (beliebiger Typ)
#
# Ergebnis  (int) Index
###############################################################################
def type_idx( p ):

    # Parameter in Typ
    if type( p ) == type:
        t = p
    else:
        t = type( p )

    # Typ in int
    if t == int:
        result = CTI_INT

    elif t == bool:
        result = CTI_BOOL

    elif t == float:
        result = CTI_FLOAT

    elif t == str:
        result = CTI_STR

    elif t == list:
        result = CTI_LIST

    elif t == dict:
        result = CTI_DICT

    else:
        result = CTI_UNKNOWN

    return result



###############################################################################
# Ein Typ soll in eine Zeichenkette konvertiert werden
#
# Python liefert standardmäßig für str( int ) das Ergebnis <type 'int'>
#
# 'p'       (beliebiger Typ)
#           z.B. vom Typ int
# 'ftsmode' (int)
#
# Ergebnis Zeichenkette. z.B. "int"
###############################################################################
def type_str( p ):

    # Den Typ in einen int / Index konvertieren
    idx = type_idx( p )

    # Index in Zeichenkette
    return ["int","bool","float","str","list","dict","None"][ idx ]
