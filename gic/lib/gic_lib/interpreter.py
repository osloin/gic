#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# interpreter.py
# osloin. 11/2018
#
#
#                       AUSFÜHRUNG STARTEN ...
#
# run()                 Ausführen: gic-Code
# run_direct()          Ausführen: gic-Code per Eingabe
# run_fct()             Ausführen: gic-Funktion
# run_param()           Ausführen: gic-Code mit Gimp-Parametern
# run_script()          Ausführen: Skript, ggf. mit Gimp-Parametern
#
#                       ABARBEITUNG ...
#
# evalV()               Platzhalterersetzung und Zeichenkettenanalyse
# evalV_BLOCK()         Nachbehandlung für GTYP_BLOCK - Werte
# evalV_bool()          Nachbehandlung für logisches
# evalV_file()          Nachbehandlung für Dateinamen
# evalV_name()          evalV() will einen Namen ersetzen
# run_1()               Die angegebenen Einträge abarbeiten
# run_1_loop()          Schleifendurchführung
# run_2()               Eine Zelle zerlegen
# run_3()               Befehl und Parameter wurden aufgetrennt
# run_4()               Parameterkonvertierung.
# run_4_empty()         Eintrag ist leer
# run_4_text()          Eintrag ist eine Zeichenkette
# run_5()               Modulaktion ausführen: Parameter für run_6() aufbereiten
# run_5_para()          Wie run_5() mit zusätzlichen Parametern
# run_6()               Modulaktion ausführen. 'exec()'
# text_getidx()         Sucht Text
#
#                       EREIGNISSE ...
#
# evt()                 Löst ein Ereignis aus
#
#                       IDs ...
#
# id_create()           Neue ID anlegen
# id_doc()              Informationen über einen oder alle Befehl(e) abrufen
# id_expand()           Befehl in seine Langform übersetzen
# id_get_info()         Modulverwaltete ID-Information abfragen
# id_get_module()       Modul für eine ID ermitteln
# id_get_param_list()   Auflistung von Parametern eines best. Typs ermitteln
# id_get_typeinfo()     Typinfo ermitteln
# vars_list()           Auflistung von IDs (z.B. Befehlen)
# vars_list_ex()        Auflistung von IDs (z.B. Befehlen)
#
#                       MODULE ...
#
# modules_exit()        Die Module beenden und entladen
# modules_init()        Module initalisieren
# modules_load()        Module laden
#
#                       TRANSFORMATION ...
#
# transform()           Transformierungsaktion durchführen
# transform_del()       Transformierungsmodulzuweisung aufheben
# transform_set()       Transformierungsmodul zuweisen
#
#                       'vars' ...
#
# vars_dec()            Vermindert Wert einer numerischen Var
# vars_del()            Var entfernen
# vars_find()           Sucht eine Var in den Hauptlisten
# vars_get()            Liest den Wert einer Var
# vars_get_none()       Liest (sicher) den Wert einer Var
# vars_get_signal()     Liest Wert einer Var und entfernt sie gleichzeitig
# vars_in()             Testet, ob eine bestimmte Var existiert
# vars_in_del()         Testen und entfernen
# vars_inc()            Erhöht Wert einer numerischen Var
# vars_init()           Initalisierung
# vars_init_local()     Initalisiert lokale Variablen
# vars_list()           liefert eine (ggf. sortiert/gefilterte) ID-Auflistung
# vars_list_ex()        liefert eine ID-Auflistung
# vars_new()            Neues 'vars' erzeugen
# vars_release()        'vars' freigeben
# vars_set()            Zuweisung
# vars_var()            Neuen Eintrag initalisieren
# verify()              Überprüfung eines 'vars'
# _vars()               Zugriff auf eine Hauptliste
# _vars_set()           Daten in eine Hauptliste eintragen
#
#                       CODE-BEHANDLUNG ...
#
# code_create()         Erzeugt Code zur Befehlsausführung
# code_gic_setvar()     Ersetzt Variablenzuweisung in gic-Code
#
#                       KONVERTIERUNG ...
#
# convert_GTYP_type()   Konvertierung: GTYPE in Python-Typ
# format_doc()          Formatiert einen Modul-Kommentar
#
#                       DATEIEN ...
#
# file_read()           Textdatei laden
# file_write()          Textdatei speichern
# path_get()            Eines der Standard-Verzeichnisse abfragen
# path_tmp_get()        Temporäres Verzeichnis abfragen ( und ggf. erzeugen )
# run_script()          Führt das angegebene Skript im Standardverzeichnis aus
# script_is()           Überprüft, ob das angegebene Skript existiert
#
#                       TEST ...
#
# d()                   Test-Funktion

from gimpfu import *
import sys
import os
import glob
import string
import time
import math
import random

import convert  as con_
import fcts     as fct_
import files    as fil_
import gimpfcts as gim_
import list     as lis_
import locale   as LOC
import moddef   as MOD
import message  as msg_
import rct      as rct_
import str      as str_

################################################################################
#                                      Basis
################################################################################
_NAME    = "gic"
_VERSION = [ 1, 1, 0, 0 ]
_FILE    = "interpreter.py"

################################################################################
#                             Ereignisse über evt()
################################################################################

# list-Parameter 'evt' der evt()-Fkt. .Bedeutung der einzelnen In­di­zes ...

# Eindeutige Nr. (Fehlerereignisse haben negative Werte)
EVT_P_POS   = 0

# Typ. Siehe EVT_T_...
EVT_P_TYP   = 1

# Text, der das Ereignis beschreibt
EVT_P_TXT   = 2

# Das Ereignis kann das da Angegebene als Ergebnis liefern
EVT_P_RES   = 3

# Spezifiziert die Ausgabeparameter
#   (nicht angeg.)  Alle
#   None            Keine
#   []              Alle
#   int             Nur der angegebene
#   [int,int,..]    Nur die aufgelisteten Indizes
EVT_P_VIE   = 4


# Typen: Meldungsinfos
EVT_T_DETAIL  = 0     # Detail-Ereignis über das i.d.R. nicht informiert wird
EVT_T_INFO    = 1     # Detail-Ereignis über das (zu Testzwecken) informiert
                      # werden soll
EVT_T_DUMP    = 2
EVT_T_MSG     = 3
EVT_T_WARNING = 4
EVT_T_ERROR   = 5

# Typen: Programmabbruch-Fehler
EVT_T_ERR_RUNTIME =  6    # Laufzeitfehler
EVT_T_ERR_VALUE   =  7    # Ungültige Funktionsparameter
EVT_T_ERR_GIC     =  8    # Fehler in gic-Code
EVT_T_ERR_TYPE    =  9    # Ungültige Funktionsparametertyp
EVT_T_ERR_MODULE  = 10    # Module hat fehlerhafte Daten geliefert

EVT_M = _FILE        # wir

# Evt-Typ-Tabelle
_EVTS_EXCEP = 0    # (0) Keine (1) ValueError (2) RuntimeError
_EVTS_MSG   = 1    # Nachricht ausgeben
_EVTS =[ [ 0, False ],      #  0 / EVT_T_DETAIL
         [ 0, True  ],      #  1 / EVT_T_INFO
         [ 0, False ],      #  2 / EVT_T_DUMP
         [ 0, False ],      #  3 / EVT_T_MSG
         [ 0, True  ],      #  4 / EVT_T_WARNING
         [ 0, True  ],      #  5 / EVT_T_ERROR
         [ 2, True  ],      #  6 / EVT_T_ERR_RUNTIME
         [ 1, True  ],      #  7 / EVT_T_ERR_VALUE
         [ 2, True  ],      #  8 / EVT_T_ERR_GIC
         [ 1, True  ],      #  9 / EVT_T_ERR_TYPE
         [ 2, True  ]       #  9 / EVT_T_ERR_MODULE
       ]

# Fehler, die zu einer 'Exception' / einem Programmabbruch führen

# Ereignis : Einer geschützten Var soll ein Wert zugewiesen werden
# Parameter: [0] (str) Var-Name [1] Wert
ERR_001 = [  -1, EVT_T_ERR_RUNTIME, LOC.ERR_001             ]

ERR_002 = [  -2, EVT_T_ERR_VALUE,   LOC.ERR_TXT_PARAM       ]
ERR_003 = [  -3, EVT_T_ERR_GIC,     LOC.ERR_003             ]
ERR_004 = [  -4, EVT_T_ERR_RUNTIME, LOC.ERR_TXT_RUNTIME     ]
ERR_005 = [  -5, EVT_T_ERR_MODULE,  LOC.ERR_005             ]
ERR_006 = [  -6, EVT_T_ERR_MODULE,  LOC.ERR_006             ]
ERR_007 = [  -7, EVT_T_ERR_GIC,     LOC.ERR_007             ] # Sprungziel
ERR_008 = [  -8, EVT_T_ERR_RUNTIME, LOC.ERR_TXT_RUNTIME     ]
ERR_009 = [  -9, EVT_T_ERR_VALUE,   LOC.ERR_TXT_RUNTIME     ]
ERR_010 = [ -10, EVT_T_ERR_RUNTIME, LOC.ERR_TXT_RUNTIME     ]

# Ereignis : Ungültiger Wert für einen Parameter
# Parameter: [0]
ERR_011 = [ -11, EVT_T_ERR_RUNTIME, LOC.ERR_TXT_TYPE        ] # Ungü. Wert

ERR_012 = [ -12, EVT_T_ERR_RUNTIME, LOC.ERR_TXT_LABEL       ] # Unbek. B.
ERR_013 = [ -13, EVT_T_ERR_RUNTIME, LOC.ERR_013             ]
ERR_014 = [ -14, EVT_T_ERR_RUNTIME, LOC.ERR_TXT_RUNTIME     ]

# Ereignis : Dieser Wert kann nicht gelöscht werden
# Parameter: [0] ID
ERR_015 = [ -15, EVT_T_ERR_RUNTIME, LOC.ERR_TXT_RUNTIME     ]

ERR_016 = [ -16, EVT_T_ERR_RUNTIME, LOC.ERR_TXT_LABEL       ]
ERR_017 = [ -17, EVT_T_ERR_RUNTIME, LOC.ERR_TXT_RUNTIME     ]
ERR_018 = [ -18, EVT_T_ERR_MODULE,  LOC.ERR_018             ] # Mod.Beend.
ERR_019 = [ -19, EVT_T_ERR_GIC,     LOC.ERR_019             ]
ERR_020 = [ -20, EVT_T_ERR_TYPE,    LOC.ERR_TXT_TYPE        ]
ERR_021 = [ -21, EVT_T_ERR_TYPE,    LOC.ERR_TXT_TYPE        ]
ERR_022 = [ -22, EVT_T_ERR_RUNTIME, LOC.ERR_TXT_RUNTIME     ]
ERR_023 = [ -23, EVT_T_ERR_MODULE,  LOC.ERR_023             ]
ERR_024 = [ -24, EVT_T_ERR_RUNTIME, LOC.ERR_024             ]
ERR_025 = [ -25, EVT_T_ERR_MODULE,  LOC.ERR_025             ]
ERR_026 = [ -26, EVT_T_ERR_MODULE,  LOC.ERR_TXT_MOD_VAL_NO  ]
ERR_027 = [ -27, EVT_T_ERR_VALUE,   LOC.ERR_TXT_TYPE        ]
ERR_028 = [ -28, EVT_T_ERR_VALUE,   LOC.ERR_TXT_MOD_VAL_NO  ]
ERR_029 = [ -29, EVT_T_ERR_VALUE,   LOC.ERR_029             ]
ERR_030 = [ -30, EVT_T_ERR_MODULE,  LOC.ERR_TXT_MOD_VAL_ERR ]
ERR_031 = [ -31, EVT_T_ERR_MODULE,  LOC.ERR_TXT_MOD_VAL_ERR ]
ERR_032 = [ -32, EVT_T_ERR_RUNTIME, LOC.ERR_032             ]
ERR_033 = [ -33, EVT_T_ERR_RUNTIME, LOC.ERR_033             ]

# Ereignis: Ungültige Zellenposition
ERR_034 = [ -34, EVT_T_ERR_RUNTIME, LOC.ERR_034             ] # Pos

# Ereignis: Ungültige Anz. an Schleifeneinträge
ERR_035 = [ -35, EVT_T_ERR_RUNTIME, LOC.ERR_035             ]

ERR_036 = [ -36, EVT_T_ERR_RUNTIME, LOC.ERR_036             ] # Ungültiger Bez.
ERR_037 = [ -37, EVT_T_ERR_GIC,     LOC.ERR_036             ] # Ungültiger Bez.
ERR_038 = [ -38, EVT_T_ERR_GIC,     LOC.ERR_038             ] # Ungül. List-Idx

# Ereignis: Es konnte kein Dateiname zum Speichern ermittelt werden
ERR_039 = [ -39, EVT_T_ERR_RUNTIME, LOC.ERR_039             ]

# Ereignis: Die angegebene Blockmarke konnte nicht ermittelt werden
ERR_040 = [ -40, EVT_T_ERR_RUNTIME, LOC.ERR_040             ]

# Ereignis: Eckige Klammern sind für diesen Typ nicht zulässig
ERR_041 = [ -41, EVT_T_ERR_RUNTIME, LOC.ERR_041             ]

# Programmablauf

# Ereignis : Meldung
# Parameter: [0] (str) Nachricht
EVT_000 = [   0, EVT_T_DETAIL,      LOC.EVT_000             ] # Meldung
EVT_005 = [   5, EVT_T_ERROR,       LOC.EVT_005             ] # Befehl: Fehler
EVT_001 = [   1, EVT_T_DETAIL,      LOC.EVT_001             ] # run(): Gestart.
EVT_002 = [   2, EVT_T_DETAIL,      LOC.EVT_002             ] # run(): Beendet
EVT_003 = [   3, EVT_T_DETAIL,      LOC.EVT_003             ] # Autoexec
EVT_004 = [   4, EVT_T_DETAIL,      LOC.EVT_004             ] # Befehl: Ausgef.

# Ereignis : Fehler bei Ausführung durch Modul
# Parameter: [0] (str) Modulname
EVT_005 = [   5, EVT_T_ERROR,       LOC.EVT_005             ] # Befehl: Fehler
EVT_006 = [   6, EVT_T_DETAIL,      LOC.EVT_006             ] # Wertzuweisung
EVT_007 = [   7, EVT_T_DETAIL,      LOC.EVT_007             ] # Init
EVT_008 = [   8, EVT_T_DETAIL,      LOC.EVT_008             ] # Exit

# Ereignis: Transformierungsmodule werden zugewiesen
# Parameter: (int) Anz
EVT_009 = [   9, EVT_T_DETAIL,      LOC.EVT_009             ] # Transf. (1)

# Ereignis: Transformierungsmodulzuweisung wird aufgehoben
EVT_010 = [  10, EVT_T_DETAIL,      LOC.EVT_010             ] # Transf. (2)

EVT_011 = [  11, EVT_T_DETAIL,      LOC.EVT_011             ] # Verzeichnis
EVT_012 = [  12, EVT_T_DETAIL,      LOC.EVT_012             ] # Modul: Geladen
EVT_013 = [  13, EVT_T_DETAIL,      LOC.EVT_013             ] # Modul: Init
# Ressourcen freigeben
EVT_014 = [  14, EVT_T_DETAIL,      LOC.EVT_014             ] # Res- Freigeben

# Ereignis : Initalisierungswert festlegen
# Parameter: [0] (str) Varname [1] (?) Wert
# Ergebnis : False: Speichern wird verhindert
EVT_015 = [  15, EVT_T_DETAIL,      LOC.EVT_015, False, 0   ] # Var-Init: Set
EVT_016 = [  16, EVT_T_DETAIL,      LOC.EVT_016, False      ] # Var-Init: Entf

# Ereignis : Text-Datei speichern
# Parameter: [0] (str) Datei [1] (str) Text [2] (?) Parameter
# Ergebnis : Speichern wird verhindert
#            [0] (int) Neues Ergebnis, z.B. ST_OK
#            [1] (bool) Status, ob Datei schon vorhanden war
EVT_017 = [  17, EVT_T_DETAIL,      LOC.EVT_017, list      ] # Speichern (1)

# Ereignis : Fehler beim speichern einer Text-Datei
# Parameter: [0] (str) Datei [1] (str) Text [2] (?) Parameter
EVT_018 = [  18, EVT_T_ERROR,       LOC.EVT_018, list      ] # Speichern (2)

# Ereignis : Text-Datei laden
# Parameter: [0] (str) Datei [1] (?) Vorgabewert [2] (?) Parameter
# Ergebnis : Laden wird verhindert
#            [0] Dateiinhalt
#            [1] Status, z.B. ST_OK
EVT_019 = [  19, EVT_T_DETAIL,      LOC.EVT_019, str        ] # Laden (1)

# Ereignis : Fehler beim laden
# Parameter: [0] (str) Datei [1] Status
EVT_020 = [  20, EVT_T_WARNING,     LOC.EVT_020             ] # Laden (2) Fehler

# Ereignis : Programmabbruch nicht über STO-Befehl
# Parameter: (?)
EVT_021 = [  21, EVT_T_WARNING,     LOC.EVT_021             ] # Stop (1)

# Ereignis : Programmabbruch über STO-Befehl
EVT_022 = [  22, EVT_T_DETAIL,      LOC.EVT_022             ] # Stop (2)
EVT_023 = [  23, EVT_T_WARNING,     LOC.EVT_023             ] # Evaluierungsfeh.
EVT_024 = [  24, EVT_T_DETAIL,      LOC.EVT_024             ] # Rekursion (1)
EVT_025 = [  25, EVT_T_DETAIL,      LOC.EVT_025             ] # Rekursion (2)
EVT_026 = [  26, EVT_T_DETAIL,      LOC.EVT_026, False      ] # Ordner anlegen

# Ereignis : Stapelverarbeitung
# Parameter: [0] (list) Dateien [1] (str) gic [2] (int) Anz der Dateien
# Ergebnis : Dateien, die stattdessen verwendet werden soll
EVT_027 = [  27, EVT_T_DETAIL,      LOC.EVT_027, list, [1,2]] # Stapelbetr.(1)

# Ereignis : Eine Schleife wurde gestartet
# Parameter: [0] (list) LIB (Siehe LIB_...) [1] (str) Befehl
# Ergebnis :
EVT_028 = [  28, EVT_T_DETAIL,      LOC.EVT_028, list, 1    ] # Schleife (1)

# Ereignis : Eine Schleife wurde beendet
# Parameter: [0] (list) LIB (Siehe LIB_...) [1] (str) Befehl  [2] (int) Anz
EVT_030 = [  30, EVT_T_DETAIL,      LOC.EVT_030, list, [2]  ] # Schleife (2)

# Ereignis : Leerer Eintrag (Kein Text oder Befehl)
# Parameter: [0] (int) Index [1] (str) Vorheriger Eintrag
# Ergebnis : Zeichenkette, die stattdessen verwendet werden soll
EVT_031 = [  31, EVT_T_DETAIL,      LOC.EVT_031, str        ] # Leerer Eintrag

# Ereignis : Unbekannte ID (Kein Befehl oder Text)
# Parameter: (str) Unbek. ID
# Ergebnis : (str) ID, die stattdessen verwendet werden soll
EVT_032 = [  32, EVT_T_DETAIL,      LOC.EVT_032, str        ] # Unbekannte ID

# Ereignis : Bildspeicherung wurde per NEE-Befehl verhindert
# Parameter: (str) Dateiname des betroffenen Bildes
EVT_033 = [  33, EVT_T_DETAIL,      LOC.EVT_033             ] # Bild speich.(4)

# Ereignis : Anwender will diese Eingabe ausführen lassen
# Parameter: (str) Eingabe
# Ergebnis : (str) Eingabe, die stattdessen ausgeführt werden soll
EVT_034 = [  34, EVT_T_DETAIL,      LOC.EVT_034, str        ] # Benutzerein.(1)

# Ereignis : Anwender hat diese Eingabe ausführen lassen
# Parameter: (str) Eingabe
EVT_035 = [  35, EVT_T_DETAIL,      LOC.EVT_035             ] # Benutzerein.(2)

# Ereignis : Letzte Eingabe erneut ausführen lassen
#            Ausgabe wird unterdrückt, da die Parameter Zeichen enthalten
#            könnten, die bei einer Meldung zu Schwierigkeiten führen könnten
# Parameter: (str) Letzte Eingabe
EVT_036 = [  36, EVT_T_DETAIL,      LOC.EVT_036, None, None ] # Benutzerein.(3)

# Ereignis : Eine neue (Text-) Datei angelegt
# Parameter: (str) Datei
EVT_037 = [  37, EVT_T_DETAIL,      LOC.EVT_037             ] # Speichern (3)

# Ereignis : Keine Dateien für Stapelbetrieb gefunden
# Parameter: (str) Verzeichnis
EVT_038 = [ 38, EVT_T_WARNING,      LOC.EVT_038             ] # Stapelbetr.(3)


# Ab hier liegen Typ-Ereignisse

# Ereignis : Nachbehandlung von GTYP_FILE-Typen
# Parameter: (str) Ggf. der konvertierte Eintrag
EVT_100 = [ 100, EVT_T_DETAIL,      LOC.EVT_100             ] # GTYP_FILE

# Ereignis : GTYP_CMD_G soll ausgeführt werden
# Parameter: 'False', wenn nicht ausgeführt werden soll
EVT_101 = [ 101, EVT_T_DETAIL,      LOC.EVT_101             ] # GTYP_CMD_G

# Ab hier liegen grafische Modul-Ereignisse

# Ereignis : Bild soll gepeichert werden.
# Parameter: Dateiname
# Ergebnis : False, wenn das Speichern verhindert werden soll
EVT_514 = [ 514, EVT_T_DETAIL,      LOC.EVT_514, False      ] # Bild speich.(1)

EVT_521 = [ 521, EVT_T_DETAIL,      LOC.EVT_521             ] # Bild geladen
EVT_523 = [ 523, EVT_T_DETAIL,      LOC.EVT_523             ] # Gimp-Pfadausf.
EVT_528 = [ 528, EVT_T_DETAIL,      LOC.EVT_528             ] # Bild entfernt

# Ereignis : Bild wird gepeichert
# Parameter: (Gimp) 'image'
EVT_529 = [ 529, EVT_T_DETAIL,      LOC.EVT_529             ] # Bild speich.(2)

# Ereignis : Bild wurde gepeichert
# Parameter: (str) Dateiname
EVT_530 = [ 530, EVT_T_DETAIL,      LOC.EVT_530             ] # Bild speich.(3)

# Ereignis : Vorbehandlung eines Bildes im Stapelbetrieb
# Parameter: (Gimp) 'image'
EVT_537 = [ 537, EVT_T_DETAIL,      LOC.EVT_537             ] # Stapelbetr.(2)

# Ereignis : Ein Bild wurde im Speicher dupliziert
# Parameter: (Gimp) 'image'
EVT_538 = [ 538, EVT_T_DETAIL,      LOC.EVT_538, None, None ] # Dupliziert

################################################################################
#                      Sonderzeichen in gic-Code
################################################################################

GIC_APO   = "'"                 # Hochkomma
GIC_ASS   = "="                 # Zuweisungsoperator
GIC_FCT1  = "("                 # Zeichen, das Funktionsparameter einleitet
GIC_FCT2  = ")"                 # Zeichen, das Funktionsparameter abschließt
GIC_LF    = "\n"                # Zeilenvorschub
GIC_PARAM = "/"                 # Trennt (Befehls-)Parameter voneinander
GIC_QM    = "\""                # Zeichen, das Zeichenketten umschliesst
GIC_SEP   = ";"                 # Trennt Einträge voneinander
GIC_SPACE = " "                 # Leerzeichen
GIC_SQB1  = "["                 # Eckige Klammer auf
GIC_SQB2  = "]"                 # Eckige Klammer zu
GIC_DIV   = ":"                 # Zeichen für eine Division
GIC_FLOAT = LOC.FLOAT_SEP       # Zeichen, das bei Fließkommawerten verwendet w.
GIC_MIN   = "-"                 # Minuszeichen
GIC_PLUS  = "+"                 # Pluszeichen

GIC_COMP  = "="                 # Vergleichsoperator
GIC_TRUE  = LOC.TRUE            # Wahr
GIC_FALSE = LOC.FALSE           # Falsch
GIC_AND   = LOC.AND             # Vergleichsoperation
GIC_OR    = LOC.OR              # Vergleichsoperation

GIC_PROT  = "~"                 # Geschützes Zeichen, das regulär nicht vor-
                                # kommen darf, da es für interne Operationen
                                # vorbehalten ist.

GIC_RPL_QM = "$$"               # Neben dem Hochkomma werden diese Zeichen in-
                                # tern als Platzhalter für Anführungszeichen
                                # in Zeichenketten verwendet.


################################################################################
#                               Daten-Typen
#
# Bei Änderung anpassen: _GTYPES und LOC.GTYPES
################################################################################
GTYP_UNDEF  = 0    # (?)     Undefiniert
GTYP_MULTI  = 1    # (?)     Parameter beliebigen Typs
GTYP_INT    = 2    # (int)   Ganzzahl
GTYP_FLOAT  = 3    # (float) Fließkommawert
GTYP_BOOL   = 4    # (bool)  Logischer Wert. Wahr / Falsch
GTYP_DIRECT = 5    # (str)   Unbehandelte Zeichenkette mit Anführungsz.
GTYP_STR    = 6    # (str)   Zeichenkette mit Anführungsz.
GTYP_X      = 7    # (float) X-Koordinate oder Breitenangabe
GTYP_Y      = 8    # (float) Y-Koordinate oder Höhenangabe
GTYP_LABEL  = 9    # (str)   Namen eines Bezeichners (ohne Anführungsz.)
GTYP_PATH   = 10   # (str)   Pfadname
GTYP_FONT   = 11   # (str)   Schriftart
GTYP_MODVAR = 12   # Modulgesteuerte Variable   (*)

GTYP_CMD    = 13   # (str)   Befehl               (*)
GTYP_CMD_G  = 14   # (str)   Grafischer Befehl (MOD_ITYPE_DEFAULT)  (*)
GTYP_CTRL   = 15   # (str)   Steuerungs-Befehl    (*)
GTYP_INTERN = 16   # (str)   Interner-Befehl      (*)

GTYP_TRANS  = 17   # (str)   Transformations-Client
                   # Das zugehörige Modul unterstützt
                   # MOD_ACTION_TRANS_R und MOD_ACTION_TRANS_L
GTYP_REPLACE= 18   # (str)   Konstante / Direkte Code-Ersetzung
GTYP_GROUP  = 19   # (str)   Modulgruppe. Modul-Liste in VIB_PARA1
GTYP_MODULE = 20   # (str)   Modul.
                   # VIB_VALUE: Name
                   # VIB_PARA1: Pfad
GTYP_FOLDERS= 21   # (str)   Pfad-Liste. Liste in VIB_PARA1
GTYP_FCT    = 22   # (str)   Funktion
# GTYP_LIST Eine Liste mit Einträge beliebigen Typs und einer Position, die
# auf einen Listeneintrag verweist. Siehe auch VIB_LIST_POS und VIB_LIST_MODE
GTYP_LIST   = 23   # (list)
GTYP_FILE   = 24   # (str)   Dateiname
GTYP_SGIC   = 25   # (str)   gic-Code mit Anführungszeichen
GTYP_BOOL_YN= 26   # (bool)  Boolischer Wert. Ja / Nein
GTYP_BOOL_OO= 27   # (bool)  Boolischer Wert. An / Aus
GTYP_SIGNAL = 28   # (?)     Signal
GTYP_COLOR  = 29   # (float-list[3]) RGB-Farbwert
GTYP_INT_PO = 30   # (int)   Positive Ganzzahl
# Spezifiziert einen Codeblock ab der aktuellen Ausführungsposition
# (int) Positive Ganzzahl, um die betroffenen Zellen anzugeben
# (str) Ein Textname, der den Block abschliesst
GTYP_BLOCK  = 31

# Interne Typ-Tabelle (Siehe auch LOC.GTYPES)
_GTY_TYPE    = 0 # (type) Entspricht am ehestem diesem Python-Typ
_GTY_VALUE   = 1 # (?) Initalisierungswert, wenn ungleich 'None'
_GTY_PARA1   = 2 # (?) Initalisierungswert, wenn ungleich 'None'
_GTY_PARA2   = 3 # (?) Initalisierungswert, wenn ungleich 'None'
_GTY_TYPAR   = 4 # (int) -1: Dieser Typ hat keine Parameter-Typ-Infos
                 #  0 oder größer: Das zuständige Modul muss eine Liste
                 #  mit mindestens dieser Anzahl an Einträgen zur Verfügung
                 #  stellen (MOD_ACTION_PTYPE). Gespeichert werden sie
                 #  unter VIB_PGTYPES. Ausserdem kann dann das Modul
                 #  MOD_ACTION_PSPLIT-Infos zur Verfügung stellen, die dann
                 #  in VIB_PSPLITS gespeichert werden
_GTY_QM      = 5 # (int) Ob evalV_name() den Wert mit Anführungszeichen um-
                 # schliessen soll ...
                 # 0: Niemals
                 # 1: Immer
                 # 2: Nach Typ-Test
_GTY_REPLV   = 6 # (int) evalV-Flags ...
                 # 1: Ersetzung durchführen
                 # 2: Ob eval() durchgeführt werden soll
                 # 4: Hochkomma soll vor Beh. durch Anführungsz. ersetzt w.
                 # 8: evalV_bool() soll angewandt werden
_GTY_EVAL_NO = 7 # (bool) Ob eine geplante Python-Evaluierung verhindert werden
                 # soll, wenn ein Wert dieses Typs ersetzt wurde.
                 # evalV_name()


# Der erste Index dieser Tabelle ist ein GTYP. Der zweite ein _GTY_...
_GTYPES =[
[None ,None  ,None  ,None, -1 , 0, 0    ,True ], #  0 / GTYP_UNDEF
[None ,None  ,None  ,None, -1 , 2, 1|2|8,False], #  1 / GTYP_MULTI
[int  ,0     ,None  ,None, -1 , 0, 1|2  ,False], #  2 / GTYP_INT
[float,0.0   ,None  ,None, -1 , 0, 1|2  ,False], #  3 / GTYP_FLOAT
[bool ,False ,None  ,None, -1 , 0, 1|2|8,False], #  4 / GTYP_BOOL (W./F.)
[str  ,""    ,None  ,None, -1 , 0, 0    ,True ], #  5 / GTYP_DIRECT
[str  ,""    ,None  ,None, -1 , 1, 1|2  ,False], #  6 / GTYP_STR
[float,0.0   ,None  ,None, -1 , 0, 1|2  ,False], #  7 / GTYP_X
[float,0.0   ,None  ,None, -1 , 0, 1|2  ,False], #  8 / GTYP_Y
[str  ,""    ,None  ,None, -1 , 0, 0    ,True ], #  9 / GTYP_LABEL
[str  ,""    ,None  ,None, -1 , 1, 1|2  ,False], # 10 / GTYP_PATH
[str  ,"Sans",None  ,None, -1 , 1, 0    ,False], # 11 / GTYP_FONT
[None ,None  ,None  ,None,  1 , 0, 0    ,False], # 12 / GTYP_MODVAR
[str  ,""    ,[]    ,[]  ,  0 , 0, 0    ,False], # 13 / GTYP_CMD
[str  ,""    ,[]    ,[]  ,  0 , 0, 0    ,False], # 14 / GTYP_CMD_G
[str  ,""    ,[]    ,[]  ,  0 , 0, 0    ,False], # 15 / GTYP_CTRL
[str  ,""    ,[]    ,[]  ,  0 , 0, 0    ,False], # 16 / GTYP_INTERN
[str  ,""    ,None  ,None, -1 , 0, 0    ,False], # 17 / GTYP_TRANS
[str  ,""    ,None  ,None, -1 , 0, 0    ,True ], # 18 / GTYP_REPLACE
[str  ,""    ,[]    ,None, -1 , 0, 0    ,True ], # 19 / GTYP_GROUP
[str  ,""    ,""    ,None, -1 , 0, 1|2  ,True ], # 20 / GTYP_MODULE
[str  ,""    ,[]    ,None, -1 , 1, 0    ,True ], # 21 / GTYP_FOLDERS
[str  ,""    ,[]    ,None,  0 , 0, 1|2  ,False], # 22 / GTYP_FCT
[list ,[]    ,-1    ,False,-1 , 2, 0    ,False], # 23 / GTYP_LIST
[str  ,""    ,None  ,None, -1 , 1, 1|2  ,False], # 24 / GTYP_FILE
[str  ,""    ,None  ,None, -1 , 1, 1|2|4,True ], # 25 / GTYP_SGIC
[bool ,False ,None  ,None, -1 , 0, 1|2|8,False], # 26 / GTYP_BOOL_YN (Ja/Ne.)
[bool ,False ,None  ,None, -1 , 0, 1|2|8,False], # 27 / GTYP_BOOL_OO (An/Aus)
[None ,True  ,None  ,None, -1 , 0, 1|2  ,False], # 28 / GTYP_SIGNAL
[list ,[]    ,None  ,None, -1 , 0, 0    ,True ], # 29 / GTYP_COLOR
[int  ,0     ,None  ,None, -1 , 0, 1|2  ,False], # 30 / GTYP_INT_PO
[int  ,0     ,None  ,None, -1 , 0, 1|2  ,False]  # 31 / GTYP_BLOCK
]

################################################################################
#                                  Module
################################################################################

# Dateien und Dateisystem
MOD_EXT_MOD  = "py"                 # Datei-Erweiterung der Befehls-Module
MOD_EXT_GIC  = "gic"                # Datei-Erweiterung der gic-Skripte
MOD_WILDCARD = "*." + MOD_EXT_MOD   # Wildcards für die Modulsuche
MOD_ID       = "MOD_ACTION_INIT"    # Modulidentifizierer
MOD_AUTOEXEC = "autoexec"           # Autostart-Skript
MOD_AUTOEXEC_EXT = MOD_AUTOEXEC + "." + MOD_EXT_GIC # Autostart-Datei

# Konstante für einzelne MOD_ACTION_...

# MOD_ACTION_EVT
# param[]
MOD_EVT_P_EVT        = 0            # (int) Eindeutige Ereignisnummer
MOD_EVT_P_TXT        = 2            # (str) Text
MOD_EVT_P_PARAM      = 1            # (?)   Parameter je nach Ereignis
MOD_EVT_P_PARAM_VIEW = 3            # (?)   Visualisierung d. Param. (EVT_P_VIE)

# MOD_ACTION_INIT
# Std-Modulgruppen
MOD_GROUP_DYNID     = "_DYNID_GRP"  # DYNID-Gruppe
MOD_GROUP_PIXTRANS  = "PIXTRANS"    # Maßeinheiten für X/Y-Werte
MOD_GROUP_EVT       = "_%d"         # %d wird durch die Ereignisnr ersetzt
MOD_GROUP_EVT_NPOS  = 1             # Ab dieser Pos beginnt bei MOD_GROUP_EVT
                                    # der eigenliche Wert.

# MOD_ACTION_ITYPE
# Mögliche Rückgabewerte eines Moduls ...
# (Kompatibel zu GTYP)
MOD_ITYPE_CMD    = GTYP_CMD
MOD_ITYPE_CMD_G  = GTYP_CMD_G
MOD_ITYPE_CTRL   = GTYP_CTRL
MOD_ITYPE_GROUP  = GTYP_GROUP
MOD_ITYPE_INTERN = GTYP_INTERN
MOD_ITYPE_MODVAR = GTYP_MODVAR
MOD_ITYPE_TRANS  = GTYP_TRANS
MOD_ITYPE_FCT    = GTYP_FCT

# Vorgabewert für den Fall, daß ein Modul keinen Typ für eine ID angibt
# MOD_ACTION_ITYPE
MOD_ITYPE_DEFAULT = MOD_ITYPE_CMD_G

# MOD_ACTION_LVL
MOD_LVL_PRIO  = 1            # Init vor den anderen Mod.
MOD_LVL_NORM  = 2            # Normale Init-Priorität

# MOD_ACTION_PTYPE
# Mögliche Rückgabewerte für Parametertypen ...
# (Kompatibel zu GTYP)
MOD_PTYP_BLOCK   = GTYP_BLOCK
MOD_PTYP_BOOL    = GTYP_BOOL
MOD_PTYP_BOOL_OO = GTYP_BOOL_OO
MOD_PTYP_BOOL_YN = GTYP_BOOL_YN
MOD_PTYP_COLOR   = GTYP_COLOR
MOD_PTYP_DIRECT  = GTYP_DIRECT
MOD_PTYP_FILE    = GTYP_FILE
MOD_PTYP_FLOAT   = GTYP_FLOAT
MOD_PTYP_FONT    = GTYP_FONT
MOD_PTYP_INT     = GTYP_INT
MOD_PTYP_INT_PO  = GTYP_INT_PO
MOD_PTYP_LABEL   = GTYP_LABEL
MOD_PTYP_MULTI   = GTYP_MULTI
MOD_PTYP_PATH    = GTYP_PATH
MOD_PTYP_SGIC    = GTYP_SGIC
MOD_PTYP_STR     = GTYP_STR
MOD_PTYP_X       = GTYP_X
MOD_PTYP_Y       = GTYP_Y

# MOD_ACTION_TRANS_R / MOD_ACTION_TRANS_L
# param[]
MOD_ACTION_TRANS_P_VALUE = 0  # Wert, der konvertiert werden soll
MOD_ACTION_TRANS_P_TYP   = 1  # GTYP
MOD_ACTION_TRANS_P_PARAM = 2  # Werden an Modul weitergereicht

# Modulaktionen
# Werte für die Variable 'action', über die ein Modul gesteuert wird
# Siehe 'module.py' für weitere Infos

# Kategorie: Ablauf
MOD_ACTION_EVT    = 20 # Programmereignis. Siehe EVT_...
                       # name     Siehe MOD_GROUP_EVT
                       # param[MOD_EVT_P_PARAM]
                       # param[MOD_EVT_P_EVT]
                       # param[]
                       # param[MOD_EVT_P_PARAM_VIEW]
                       # result   Je nach Ereignis

# Kategorie: Modul
MOD_ACTION_LVL    = 8  # Abfrage: Modulpriorität für MOD_ACTION_INIT
                       # result   (int)
                       #          MOD__LVL_PRIO - Vor MOD__LVL_NORM
                       #          MOD__LVL_NORM (normale Priorität)
MOD_ACTION_INIT   = 7  # Modul initalisieren
                       # result   (None) Nicht (weiter verwenden)
                       #          (str-list) ID-Liste
MOD_ACTION_POST   = 19 # Post-Init. Initalierung einer ID erfolgreich
                       # name     ID
MOD_ACTION_EXIT   = 4  # Modul beenden
                       # result   (bool) False, wenn Fehler aufgetreten ist
MOD_ACTION_NAME   = 9  # Abfrage: Modulname
                       # result   (str) Eindeutiger Modulname

# Kategorie: ID (z.B. Befehl oder Variable)
MOD_ACTION_DYNID  = 2  # Abfrage: ID bei dynamische ID-Identifizierung
                       # name     ID, die überprüft werden soll
                       # result   (str) 'None' oder eine neue ID
MOD_ACTION_EXEC   = 3  # Befehl ausführen
                       # name     ID
                       # result   Je nach Befehl
MOD_ACTION_IDDOC  = 6  # Abfrage: ID-Info-Text
                       # result   (str)      Info über ID
                       #          (str-list) [0] Kurze Info  [1] Lange Info
MOD_ACTION_ITYPE  = 5  # Abfrage: Typeinfos für die IDs eines Moduls
                       # result   (int-list) GTYP-list
MOD_ACTION_VAR_G  = 13 # Wert für Bezeichner (Varnamen) liefern
                       # name     ID
                       # result   Wert
MOD_ACTION_VAR_S  = 15 # Wert für Bezeichner (Varnamen) Setzen
                       # name     ID
MOD_ACTION_VAR_T  = 16 # Typ für Bezeichner (Varnamen) abfragen (GTYP)
                       # Dieser Verfahren wird z.Z. nicht verwendet. Der Typ
                       # ist statisch. Es gilt der Typ von MOD_ACTION_PTYPE!

# Kategorie: ID-Parameter-Informationen (z.B. Parameter eines Befehls)
MOD_ACTION_PDEF    = 1 # Abfrage: Vorgabewerte
                       # name     ID
                       # result   (list) Vorgabewerte
MOD_ACTION_PTYPE  = 10 # Abfrage: Typinfos für die Parameter einer ID
                       # name     ID
                       # result   (type-list) Typ-Infos
MOD_ACTION_PDOC   = 11 # Abfrage: Parameter-Info-Text für ID
                       # result   (str-list) Eine kurze Info über jeden Param.
MOD_ACTION_PMMS   = 12 # Abfrage: Parameter-Infos für Min-Max-Schrittweite
                       # name     ID
                       # result   (list) Min-Max-Schrittweite-Liste
MOD_ACTION_PVAL   = 14 # Abfrage: Parameter-Infos für Zeichenk.-Liste als idx
                       # name     ID
                       # result   (str-list) Ggf. Werte-Liste für Parameter
MOD_ACTION_PSPLIT = 21 # Abfrage: Trenninfos für die Parameter einer ID
                       # name     ID
                       # result   (str-list) Infos

# Kategorie: Transformation
# - Ein Modul kann kann bei seiner Initalisierung Transformation anbieten,
#   indem es einen Gruppennamen als ID zurückliefert und bei seinem Typ
#   MOD_ITYPE_TRANS angibt.

MOD_ACTION_TRANS_R = 17 # In eine Richtung konvertieren
MOD_ACTION_TRANS_L = 18 # In die andere Richtung konvertieren

# Aktionen-Tabelle

_ACTIONS_NAME  = 0   # Ob 'name' benötigt wird
_ACTIONS_PARAM = 1   # Ob 'param' benötigt wird
_ACTIONS_GIMP  = 2   # Ob 'image' und 'drawable' benötigt werden

_ACTIONS =[[False,False,False],      #  0
           [True, False,False],      #  1 / MOD_ACTION_PDEF
           [True, False,False],      #  2 / MOD_ACTION_DYNID
           [True, True, True ],      #  3 / MOD_ACTION_EXEC
           [False,False,False],      #  4 / MOD_ACTION_EXIT
           [False,False,False],      #  5 / MOD_ACTION_ITYPE
           [True, False,False],      #  6 / MOD_ACTION_IDDOC
           [False,False,False],      #  7 / MOD_ACTION_INIT
           [False,False,False],      #  8 / MOD_ACTION_LVL
           [False,False,False],      #  9 / MOD_ACTION_NAME
           [True, False,False],      # 10 / MOD_ACTION_PTYPE
           [True, False,False],      # 11 / MOD_ACTION_PDOC
           [True, False,False],      # 12 / MOD_ACTION_PMMS
           [True, False,True ],      # 13 / MOD_ACTION_VAR_G
           [True, False,False],      # 14 / MOD_ACTION_PVAL
           [True, True, False],      # 15 / MOD_ACTION_VAR_S
           [True, False,False],      # 16 / MOD_ACTION_VAR_T
           [True, True, False],      # 17 / MOD_ACTION_TRANS_R
           [True, True, False],      # 18 / MOD_ACTION_TRANS_L
           [True, False,False],      # 19 / MOD_ACTION_POST
           [True, True ,False],      # 20 / MOD_ACTION_EVT
           [True, False,False]       # 21 / MOD_ACTION_PSPLIT
          ]



################################################################################
# evalV()
################################################################################

# Parameter 'qm'
RVQM = "\""  # Standardwert für diesen Parameter

# Parameter 'fi' und 're'
# Ausserhalb von Zeichenketten werden die Zeichen aus 'fi'
# durch die aus 're' ersetzt. Bei RV_RE kann an einer Position GIC_PROT ver-
# wendet werden, wenn das entsprechende Zeichen durch nichts ersetzt werden
# soll.
RV_FI = GIC_FLOAT + GIC_DIV + "\n"      # 'fi'
RV_RE = "."       + "/"     + GIC_PROT  # 're'

################################################################################
# LOC.CMD_PARSE_2 - Rückgabewerte
################################################################################
CMD_PARSE_2_ID     = 0   # ID
CMD_PARSE_2_PARAM  = 1   # Parameter

################################################################################
#                                 'vars'
#
# Das vars-Objekt enthält zur Laufzeit alle System-Vars und alle durch die
# Abarbeitung von gic-Code entstandenen Vars und steht in fast allen Funk-
# tionen als Parameter zur Verfügung.
#
# (Auf 'vars'-Einträge sollte niemals direkt zugegriffen werden, sondern nur
# über vars_get(), vars_set() oder eine der anderen Hilfsfunktionen. Ein neues
# 'vars' kann mit vars_new() erzeugt werden)
################################################################################
VARS_TYPE   = list  # Typ eines 'vars'

# Parameter für _vars()
VARS_GLOB   = 0     # globale Variablen
VARS_LOC    = 1     # lokale Variablen

# Rückgabewert von vars_find(), wenn Namensraum nicht ermittelt werden konnte
VARS__UNDEF = -1

# Zeichen, die in einem Variablennamen / Schlüssel vorkommen dürfen
VARS_CHARS   = "äÄöÖüÜ"
VARS_CHARS_1 = string.ascii_letters + "_" + VARS_CHARS      # An erster Pos
VARS_CHARS_x = VARS_CHARS_1 + string.digits                 # Ab zweiter Pos

# Vorzeichen im Namen von System-Einträgen
VARS_SYS_PREFIX = "_"

################################################################################
#               'vars' - Schlüssel / Namen der Systemeinträge
################################################################################
VAR_AUTOEXEC    = "_AUTOEXEC"  # (sgic) Automatische Code-Ausführung bei run()
VAR_CIB         = "_CIB"       # (CIB) Infos über aktuelle Zelle
VAR_ITEMS       = "_ITEMS"     # (str-list) Auflistung der Einträge (roh)
                               # Ergebnis von LOC.CMD_PARSE_1
VAR_POS         = "_POS"       # (int) Aktueller Index in VAR_ITEMS
VAR_CNT_EXEC    = "_CNT_EXEC"  # (int) Wie viele Einträge abgearbeitet wurden
VAR_D_COUNT     = "_D_COUNT"   # Hilfswert fur d()
VAR_ID          = "_ID"        # Laufende Nr, die bei vars_set() an neue
                               # Einträge vergeben wird
VAR_FOLDERS_M   = "_FOLDERS_M" # (str-list) Suchpfade für Module
VAR_LIST_EOL    = "_LIST_EOL"  # (?) Ein Listenende wurde erreicht
VAR_LOC_CNT     = "_LOC_CNT"   # Anz der Mitbenutzer
VAR_LOC_PREV    = "_LOC_PREV"  # Verweis auf die vorherigen lokalen Variablen
VAR_LOOP        = "_LOOP"      # (LIB) Schleifendaten. Siehe LIB_...
VAR_MODULES     = "_MODS"      # (str-list) Auflistung der Module
VAR_MODULES_INIT= "_MODS_INIT" # (bool) Ob Module schon initalisiert wurden
VAR_NAME        = "_NAME"      # (str) Name der akt. ID (z.B. Befehl)
VAR_NO_EXEC     = "_NO_EXEC"   # (int) Wenn größer als 0, wird die Ausführung
                               # des nächsten Befehls verhindert und der Wert
                               # um eins vermindert (lokale Var)
VAR_PARAM_STR   = "_PARAM_STR" # (str-list) Auflistung der Befehlsparameter
                               # des aktuellen Befehls. Ggf. mit Platzhaltern
VAR_PARAM       = "_PARAM"     # (list) Auflistung der aktuellen Parameter-
                               # werte in den entsprechenden Typen.
VAR_RUN_CNT     = "_RUN_CNT"   # (int) Wie oft run() seit vars_init() ausge-
                               # führt wurde. Nur zu statischen Zwecken
VAR_RUN_LVL     = "_RUN_LVL"   # (int) Akt. Rekursionsebene von run()
VAR_RUN_PARA    = "_VAR_RUN_PA"# Akt. 'run' - Parameter
                               # [0] Image
                               # [1] Drawable
VAR_RUN_PARA_BAK= "_VAR_RUN_PB"# Kopie der Original VAR_RUN_PARA
VAR_STOP        = "_STOP"      # (str) Grund für Abbruch-Signal
VAR_TIME_RUN    = "_TIME_RUN"  # (float) Letzte Durchführungszeit run() in s

# Std-Einträge. Nur Werte dieser Typen lassen sich über den SET-Befehl
# eintragen
VARS_TYPES = [ bool, int, float, str ]

################################################################################
#                                   CIB
#
# Der Cell-Info-Block enthält die Analysedaten einer Zelle gic-Code
################################################################################
CIB__SIZE     = 10 # CIB-Größe

CIB_VARS      = 0  # (vars) vars
CIB_ITEMS     = 1  # (list) Kontext
CIB_POS       = 2  # (int)  Pos im Kontext 0 ... x
CIB_CELL      = 3  # (str)  Zelleninhalt
CIB_MODULE    = 4  # (str)  Zuständiges Modul
CIB_NAME      = 5  # (str)  Befehls-ID
CIB_PARAM     = 6  # (list) Parameter
CIB_PARAM_STR = 7  # (list) Parameter (unbearbeitet)
CIB_TYPE      = 8  # (GTYP) Zellentyp. z.B. GTYP_CMD_G, ein Grafikbefehl
CIB_RESULT    = 9  # (?)    Ergebnis

################################################################################
#                                   LIB
#
# Der loop-Info-Block enthält die Daten, die für einen Schleifenauftrag
# gespeichert werden. VAR_LOOP
################################################################################
LIB__SIZE   = 5  # Mindestgröße / Anz der list-Einträge. Kann durch Daten des
                 # Auftraggebers erweitert werden
LIB_POS     = 0  # (int)  Position des Auftraggebers
LIB_SIZE    = 1  # (int)  Anz der Einträge, die v. Schleife betroffen sind
LIB_RUN     = 2  # (bool) Ob die Schleife (weiter) ausgeführt werden soll
LIB_CMD     = 3  # (str)  Befehl, der gestartet hat
LIB_LOOPS   = 4  # (int)  Wie oft die Schleife ausgeführt wurde
LIB_FREE    = 5  # (?)    Ab diesem Index frei für Daten des Auftraggebers

################################################################################
#                                   VIB
#
# Der vars-Info-Block enthält die Daten, die für einen vars-Eintrag gespeichert
# werden.
################################################################################
VIB__SIZE   = 9  # Blockgröße / Anz der list-Einträge

VIB_VALUE   = 0  # Der aktuelle Wert.
VIB_FLAGS   = 1  # (int) 'Flags'. Siehe VF_...
VIB_GTYP    = 2  # (int)
                 # Der Eintragstyp
                 # GTYP_... (z.B. GTYP_CMD - Eintrag ist ein Befehl)
                 # GTYP_VAR ist der Initalisierungswert
VIB_MODULE  = 3  # (str)
                 # Das Modul, das mit der ID verbunden ist
VIB_TIB     = 4  # (TIB-list)
                 # Zugewiesenes Transformierungsmodul für den Wert
                 # Wenn keins: 'None'
VIB_TIB_P   = 5  # (list)
                 # Zugewiesene Transformierungsmodule für die einzelnen
                 # Parameter
                 # Wenn keiner: 'None'. Wenn vorhanden, muß für jeden
                 # Parameter ein (wenn auch leerer Eintrag) existieren!
VIB_PARA1   = 6  # Frei (*)
VIB_PARA2   = 7  # Frei (*)
VIB_ID      = 8  # (int) Laufende Nr
VIB_VALUE_DI = VIB__SIZE
                 # Direktzugriff auf aktuellen Wert (ohne Ersetzung o.Ä.)
                 # Zur Verwendung für Module, die eine Var steuern und hierüber
                 # Zugriff auf eigene Daten bekommen können.

# (*) Für einige Typen (Siehe _GTYPES / _GTY_TYPAR ) werden in VIB_PARA1
#     Typ-Infos ihrer Parameter gespeichert ...
VIB_PGTYPES = VIB_PARA1             # (GTYP-list) MOD_ACTION_PTYPE

# (*) Für einige Typen (Siehe _GTYPES / _GTY_TYPAR ) werden in VIB_PARA2
#     Trenn-Infos ihrer Parameter gespeichert ...
VIB_PSPLITS = VIB_PARA2             # (list) MOD_ACTION_PSPLIT

# GTYP_LIST-Einträge haben im ersten Parameter ihre Position gespeichert
# -1, wenn auf einen ungüligen Wert verwiesen wird
VIB_LIST_POS = VIB_PARA1            # (int)

# GTYP_LIST-Einträge haben im zweiten Parameter den Modus gespeichert
# True, wenn die Position nach einem Lesezugriff autom. erhöht werden soll
VIB_LIST_MODE = VIB_PARA2           # (bool)

# VIB_FLAGS ...
VF_NO  = 0   # (Keine Flags)
VF_STA = 1   # Statischer Eintrag (kann nicht geändert od. gelöscht werden)
VF_INV = 2   # Unsichtbar in der Befehlsliste ( vars_list() )

################################################################################
#                                   TIB
#
# Der Transformierungsmodul-Info-Block enthält Daten über die Verbindung
# zwischen einer ID (oder einem ID-Parameter) und einem Transformierungsmodul
################################################################################
TIB__SIZE = 2   # Größe des Blocks
TIB_MODUL = 0   # Modul
TIB_PARAM = 1   # Parameter für ausführendes Modul

################################################################################
#                               path_get()
################################################################################
FOLDER_INSTAL       = "~"                # Installationsordner
FOLDER_GIC          = _NAME              # Haupt-Ordner im Installationsordner
FOLDER_TMP          = "temp"             # Ordner für temporäre Dinge
FOLDER_MODULES_SYS  = "sys"              # Ordner für System-Module
FOLDER_MODULES_GIMP = "gimp"             # Ordner für Gimp-Module
FOLDER_MODULES_GRA  = "graphic"          # Ordner für Grafik-Module
FOLDER_MODULES_MY   = "my"               # Ordner für eigene Module
FOLDER_MODULES      = "modules"          # Ordner für Module
FOLDER_SCRIPTS      = "scripts"          # Ordner für Skripte
FOLDER_DEMO         = "demo"             # Ordner für Demos
FOLDER_DEMO_CODE    = "gics"             # Ordner für Demo-Skripte
FOLDER_DEMO_PICS    = "pics"             # Ordner für Demo-Bilder
FOLDER_TEMPLATES    = "templates"        # Ordner für Vorlagen
# Modulordner / Unterordner in FOLDER_MODULES
FOLDER__MODULES     = [FOLDER_MODULES_SYS,
                       FOLDER_MODULES_GIMP,
                       FOLDER_MODULES_GRA,
                       FOLDER_MODULES_MY ]

################################################################################
#                              vars_list_ex()
################################################################################
IG_SIMPLE     = 1   # Einfache Einträge, die nur die IDs enthalten
IG_PARAM      = 2   # Mit Parameter-Infos
IG_PARAM_NOTY = 3   # Mit Parameter-Infos (aber ohne Typ-Inf der ID)

################################################################################
#                              vars_init()
#
# Vars, die bei vars_init() angelegt und initalisiert werden sollen
# (0) ID        (str)  Name
# (1) Wert      (?)    Initalisierungswert f. ID
# (2) 'Flags'   (int)  Siehe VF_...
# (3) Typ       (GTYP) (optional)
# (4) Param1    (str)  (optional) Initalisierungswert f. Parameter
################################################################################
VARS_INIT = [ \
    [ VAR_D_COUNT       , 0               , VF_NO  , GTYP_INT       ],\
    [ VAR_ID            , 1               , VF_STA , GTYP_INT       ],\
    [ GIC_TRUE          , "True"          , VF_STA , GTYP_BOOL      ],\
    [ GIC_FALSE         , "False"         , VF_STA , GTYP_BOOL      ],\
    [ LOC.YES           , "True"          , VF_STA , GTYP_BOOL_YN   ],\
    [ LOC.NO            , "False"         , VF_STA , GTYP_BOOL_YN   ],\
    [ LOC.ON            , "True"          , VF_STA , GTYP_BOOL_OO   ],\
    [ LOC.OFF           , "False"         , VF_STA , GTYP_BOOL_OO   ],\
    [ GIC_AND           , "and"           , VF_STA , GTYP_BOOL      ],\
    [ GIC_OR            , "or"            , VF_STA , GTYP_BOOL      ],\
    [ MOD_GROUP_DYNID   , []              , VF_NO  , GTYP_GROUP, [] ],\
    [ MOD_GROUP_PIXTRANS, []              , VF_NO  , GTYP_GROUP, [] ],\
    [ VAR_ITEMS         , []              , VF_NO                   ],\
    [ VAR_POS           , 0               , VF_NO                   ],\
    [ VAR_CNT_EXEC      , 0               , VF_NO                   ],\
    [ VAR_MODULES_INIT  , False           , VF_NO                   ],\
    [ VAR_NAME          , None            , VF_NO                   ],\
    [ VAR_NO_EXEC       , 0               , VF_NO                   ],\
    [ VAR_RUN_CNT       , 0               , VF_NO  , GTYP_INT       ],\
    [ VAR_RUN_LVL       , 0               , VF_NO  , GTYP_INT       ],\
    [ VAR_FOLDERS_M     , []              , VF_NO  , GTYP_FOLDERS   ],\
    [ LOC.VAR_POS       , LOC.VAR_POS_INIT, VF_NO  , GTYP_INT       ],\
    [ MOD.VAR_IMG_STATE , 0               , VF_NO  , GTYP_INT       ],\
    [ VAR_RUN_PARA      , [ None, None ]  , VF_NO  , GTYP_MULTI     ]]


###############################################################################
# Konvertiert GTYPE (z.B. Rückgabewert(e) von MOD_ACTION_PTYPE) in einen
# Python-Typ. Es können Listen oder einzelne Werte konvertiert werden.
#
# 'pt'       (int/int-list)     GTYP_... (z.B. GTYP_INT in int)
#
# Ergebnis   (type/type-list)   Typ(en) (str,int,bool,float)
###############################################################################
def convert_GTYP_type( pt ):

    if type( pt ) == list:

        result = []
        for t in pt:
            result.append( convert_GTYP_type( t ) )

    else:

        # Python-Wert für GTYP aus Tabelle holen
        result = _GTYPES[ pt ][ _GTY_TYPE ]

    return result



###############################################################################
# debug-Funktion
#
# 'vars'    (vars) Var-Speicher [None]
# 'param'   (?)    Beliebige Parameter  [None]
# 'filter'  (int) Ausgabe soll erst ab dieser Pos erfolgen
#           (int-list) 0: Ausgabe ab dieser Position, 1:Bis dieser Pos
#           [None] Alle
# 'ids'     (str/str-list) Welche IDs ausgegeben werden soll. Z.B. VAR_NAME
#           [None], wenn keine
# 'details' (str-list) Welche Datails gezeigt werden sollen
#           "param" Parameter-Details bei VAR_PARAM_STR
#           "block" Alle Infos, die zur ID gespeichert sind
#           z.B. "block","param"
#           [None], wenn keine Details anhezeigt werden sollen
# 'sep'     (str) Trennzeichen zwischen einzelnen Infos [" "]
# Ergebnis
###############################################################################
def d( vars     = None,
       param    = None,
       filter   = None,
       ids      = None,
       details  = None,
       sep      = " " ) :

    # 'vars'?
    if vars != None:

        if type( vars ) != VARS_TYPE:

            raise ValueError( "d()" )

        # Anz unserer Aufrufe
        vars_inc( vars, VAR_D_COUNT )
        d_count = vars_get( vars, VAR_D_COUNT )

        msg = "d(%d) " % d_count

        # Nur
        if d_count >= 1000:
            return
    else:
        d_count = None
        msg = ""

    # Trennzeichen
    msg += sep

    # ggf. str in list konvertieren
    if type( ids ) == str:
        ids = [ ids ]

    # Beliebige Parameter
    if param != None:
        if type( param ) == str:
            msg += param
        elif type( param ) == list:
            msg += lis_.to_str( param, sep )
        else:
            msg += str( param )

    msg += sep

    # Alle gewünschten Keys behandeln
    if type( ids ) == list and vars != None:
        for id in ids:

            msg += "%s=%s" % \
                      ( id, str( vars_get( vars, id, VIB_VALUE_DI ) ) )

            # Sonderbehandlung für einige Einträge

            # Hinter Pos die Anz der vorhandenen Einträge vermerken
            if id == VAR_POS:
                items = vars_get( vars, VAR_ITEMS )
                if items != None:
                    msg += "(%d)" % len( items)

            # Hinter Befehlsnamen Parameterinfos vermerken
            elif id == VAR_NAME:
                sp = vars_get( vars, VAR_PARAM_STR )
                if sp != None:
                    if details != None and "param" in details:
                        msg += " " + \
                                  lis_.to_str( sp, [ "[%d]", False ] )
                    else:
                        msg += "(%d)" % len( sp )
            msg += sep

            # Gesamten Info-Block ausgeben
            if details != None and "block" in details:
                for ib in range( 0, VIB__SIZE ):
                    if ib != VIB_VALUE: # VIB_VALUE ist schon
                        value = vars_get( vars, id, ib )
                        msg += "[%d]%s" % \
                                  ( ib, con_.str_big( str( value ) ) )
                        msg += sep

    # Ob Meldung angezeigt werden soll ....
    if filter == None or d_count == None:
        viewmsg = True
    elif type( filter ) == int:
        viewmsg = d_count >= filter
    elif type( filter ) == list:
        viewmsg = d_count >= filter[ 0 ]
        if len( filter ) > 1 and viewmsg:
            viewmsg = d_count <= filter[ 1 ]
    else:
        viewmsg = True

    # Meldung
    if viewmsg:
        msg_.message( msg, msg_.TYPE_INFO )



###############################################################################
# Gibt eine Dokumentation über eine ID oder alle zurück
#
# 'id'          (str)  ID (z.B. Befehl)
#                      None, um eine Doku für alle IDs zu erzeugen
# 'vars'        (vars) Var-Speicher
#                      'None', wenn ein temporäres erzeugt werden soll
#
# Ergebnis      (str) Info
#               None, wenn die ID nicht existiert
###############################################################################
def id_doc( id, vars = None ) :

   result = None

   if vars == None:

        # 'vars'
        vars = vars_new( True )
        is_vars_new = True
   else:
        is_vars_new = False

   # verify( vars )

   # Doku über alle IDs?
   if id == None:

       # Initalierung
       result = ""

       # Skripte
       result += LOC.DUMP_CMDS_INF % LOC.SCRIPTS

       # Dateiliste zusammenstellen und 'result' zufügen
       result += fil_.filelist_doc( path_get( FOLDER_SCRIPTS ) +
                                    "*." +
                                    MOD_EXT_GIC,
                                    fil_.FE_NAME,
                                    [ MOD_AUTOEXEC ] )
       result += "\n"

       # Variablen ...
       result += LOC.DUMP_CMDS_INF % LOC.VARS
       result += vars_list_ex( vars,
                               vars_list( vars, MOD_ITYPE_MODVAR, True ),
                               False,
                               IG_PARAM_NOTY ) + "\n"

       # Maßeinheiten ...
       result += LOC.DUMP_CMDS_INF % LOC.UNITS
       result += vars_list_ex( vars,
                               vars_list( vars, MOD_GROUP_PIXTRANS, True ),
                               False,
                               IG_PARAM_NOTY ) + "\n"

       # Steuerung ...
       result += LOC.DUMP_CMDS_INF % LOC.CONTROLS
       result += vars_list_ex( vars,
                               vars_list( vars, MOD_ITYPE_CTRL, True ),
                               False,
                               IG_PARAM_NOTY ) + "\n"

       # Funktionen ...
       result += LOC.DUMP_CMDS_INF % (LOC.FUNCTIONS + LOC.DUMP_CMDS_INF_1)
       result += vars_list_ex( vars,
                               vars_list( vars, MOD_ITYPE_FCT, True ),
                               False,
                               IG_PARAM_NOTY ) + "\n"

       # Befehle ...
       result += LOC.DUMP_CMDS_INF % (LOC.CMDS + LOC.DUMP_CMDS_INF_1)
       result += vars_list_ex( vars,
                               vars_list( vars, MOD_ITYPE_CMD, True ),
                               False,
                               IG_PARAM_NOTY ) + "\n"

       # Grafische Befehle ...
       result += LOC.DUMP_CMDS_INF % (LOC.GRAPHIC + " " + LOC.CMDS +
                                      LOC.DUMP_CMDS_INF_1)
       result += vars_list_ex( vars,
                               vars_list( vars, MOD_ITYPE_CMD_G, True ),
                               False,
                               IG_PARAM_NOTY ) + "\n"

       # Hinweis ...
       result += LOC.DUMP_CMDS_INF_3 + "\n"

   else: # Eine ID (nicht alle) ...

       # Überprüfen, ob ID als Kurzform angegeben wurde
       if len( id ) == 1:
           id = id_expand( id, vars, LOC.CMDS_SHORT )

       # Überprüfen, ob ID vorhanden ist, dann ggf. die Daten zusammenstellen ...
       if vars_in( vars, id ):      # da?

           # Name (des Befehls)
           result = "\n'" + id + "'"

           # Ggf. Kurzform eines Befehls anzeigen
           short_found = False
           for key in LOC.CMDS_SHORT.iterkeys():
               if LOC.CMDS_SHORT[ key ] == id:
                    result += " (%s: '%s')" % ( LOC.SHORTFORM, key )
                    short_found = True
                    break

           # Ggf. Kurzform einer Variablen anzeigen
           if not short_found:
               for key in LOC.VARS_SHORT.iterkeys():
                   if LOC.VARS_SHORT[ key ] == id:
                        result += " (%s: '%s')" % ( LOC.SHORTFORM, key )
                        break

           # Info über die ID
           inf = id_get_info( id, MOD_ACTION_IDDOC, vars )

           # Ergebnis formatieren
           result += format_doc( inf, True, "\n\n" ) + "."

           # Hat ID-Typ Parameter?
           if id_get_typeinfo( id, _GTY_TYPAR, vars ) >= 0:

               # Infos über Parameter
               gtypes = vars_get( vars, id, VIB_PGTYPES )

               # Wenn Parameterinfos vorhanden ...
               if gtypes != None and len( gtypes ) > 0:

                   # Überschrift für Parameter ...
                   result += "\n\n" + LOC.PARAMETERS + LOC.MSG_STYLE_0

                   # Akt. Parameter-Index
                   i = 0

                   # Vorgabewert ...
                   pdefs = id_get_info( id, MOD_ACTION_PDEF, vars )

                   # Trenn-Operatoren
                   psplits = id_get_info( id, MOD_ACTION_PSPLIT, vars )

                   # Infotexte
                   pdocs = id_get_info( id, MOD_ACTION_PDOC, vars )

                   # Wertebereiche
                   pmmss = id_get_info( id, MOD_ACTION_PMMS, vars )

                   # Wertemenge
                   pvals = id_get_info( id, MOD_ACTION_PVAL, vars )

                   # Alle Parameter (Typen) durchgehen
                   for t2 in gtypes:

                       # GTYP in Python-Typ
                       pythontyp = _GTYPES[ t2 ][ _GTY_TYPE ]

                       # Zeilenvorschub + Laufende Nummer und Typ ...
                       result += "\n%d." % ( i + 1 )

                       # Infotext über den akt. Parameter ...
                       result += format_doc( [ pdocs, i ], True, " ", True )

                       # Typ ...
                       result += ", %s" % LOC.GTYPES[ t2 ][ LOC.GTY_LONG ]

                       # Vorgabewerte
                       if pdefs != None:      # Vorgabewerte?
                          inf = pdefs[ i ]    # Vorgabewert für akt. Parameter
                          if inf != None:
                              if t2 == GTYP_STR:     # Param. ist Zeichenkette ?
                                  inf = "'" + inf + "'"
                              elif t2 == GTYP_BOOL:    # Wahr / Falsch ?
                                  if inf:
                                      inf = LOC.TRUE
                                  else:
                                      inf = LOC.FALSE
                              elif t2 == GTYP_BOOL_YN: # Ja / Nein ?
                                  if inf:
                                      inf = LOC.YES
                                  else:
                                      inf = LOC.NO
                              elif t2 == GTYP_BOOL_OO: # An / Aus ?
                                  if inf:
                                      inf = LOC.ON
                                  else:
                                      inf = LOC.OFF
                              else:                    # Anderer Typ ...
                                  inf = str( inf )
                              result += ", %s: %s" % ( LOC.DEFAULT, inf )

                       # Wertebereich
                       if pmmss != None:      # Wertebereich?
                          inf = pmmss[ i ]    # Vorgabewert für akt. Parameter
                          if inf != None:
                              result += ", %s: %s ... %s" %\
                                ( LOC.RANGE, str( inf[ 0 ] ), str( inf[ 1 ] ) )

                       # Wertemenge
                       if pvals != None:      # Vorgabewerte?

                          inf = pvals[ i ]    # Vorgabewert für akt. Parameter
                          if inf != None and len( inf ) > 0:
                              result += ", %s" %\
                                ( lis_.to_str( inf, [" (%d) ",True,"(%d) "] ) )

                       # Wenn Trenn-Operatoren vorhanden sind und nicht letzter
                       # Parameter
                       if psplits != None and i < len( gtypes ) - 1:
                           # Nur Nicht-Standardtrennzeichen
                           if psplits[ i ] != GIC_PARAM:
                                result += ", %s: %s" % ( LOC.SEPARATOR, \
                                                         psplits[ i ] )

                       # Nächster Index
                       i += 1

           # Info über den Typ. Klammer wird geöffnet!
           inf = vars_get( vars, id, VIB_GTYP )
           result += "\n\n(" + LOC.TYPE + ": " +\
                     LOC.GTYPES[ inf ][LOC.GTY_LONG] + ""

           # Info über das Modul
           inf = vars_get( vars, id, VIB_MODULE )   # Modul holen

           if inf == None:                          # Kein Modul?
               result += ")"
           else:
               inf = run_5( inf, MOD_ACTION_NAME, vars )
               result += ", " + LOC.MODULE + ": '" + inf + "')"

   if is_vars_new:
       vars_release( vars )                      # Freigeben

   # Ergebnis
   return result


###############################################################################
# Ein Ereignis ist aufgetreten
#
# 'vars'      (vars)    Var-Speicher
#                       'None' ist möglich, dann steht die Modulinformation
#                       nicht mehr zur Verfügung
#
# 'evt'       (list)    Siehe EVT_P_...
#                       0 / EVT_P_POS
#                       1 / EVT_P_TYP
#                       2 / EVT_P_TXT
#                       3 / EVT_P_RES
#                       4 / EVT_P_VIE
#
# 'evtparam'  Beliebige Parameter beliebigen Typs
#
# Ergebnis:   Je nach Ereignis
###############################################################################
def evt( vars, evt, evtparam = None ) :

    # Unser Ergebnis
    result = None

    # Wenn 'vars' vorhanden
    if vars != None:

        # Ggf. Module informieren
        id_key = MOD_GROUP_EVT % evt[ EVT_P_POS ]

        # Registrierte Module?
        if vars_in( vars, id_key ):

            # ID in numerischen Wert konvertieren
            evt_num = int( id_key[ MOD_GROUP_EVT_NPOS : ] )

            # Parameter für Modul zusammenstellen
            # Index beachten: MOD_EVT_P_EVT, MOD_EVT_P_PARAM,
            #                 MOD_EVT_P_TXT, MOD_EVT_P_PARAM_VIEW
            # lis_.get() führt typsicheren Zugriff auf list durch
            param = [ evt_num,
                      evtparam, evt[ EVT_P_TXT ],
                      lis_.get( evt, EVT_P_VIE, [] ) ]

            # Alle registr. Module ...
            for m in vars_get( vars, id_key ):

                # Modul benachrichtigen
                mod_result = run_6( m, MOD_ACTION_EVT, vars, id_key, param )

                # Hat Modul ein Ergebnis geliefert?
                if mod_result != None:

                    # Wird unser Ergebnis
                    result = mod_result


    # Meldung für den Typ?
    if _EVTS[ evt[ 1 ] ][ _EVTS_MSG ]:
        view_msg = True
    else:
        view_msg = False

    # Exception für den Typ?
    excep = 0 # Ob Exception ausgelöst werden soll. Vorgabe: 0 / Nein

    if _EVTS[ evt[ EVT_P_TYP ] ][ _EVTS_EXCEP ] == 1:
        excep = 1       # Exception: ValueError
        view_msg = True # Txt wird für Exception benötigt

    elif _EVTS[ evt[ EVT_P_TYP ] ][ _EVTS_EXCEP ] == 2:
        excep = 2       # Exception: RuntimeError
        view_msg = True # Txt wird für Exception benötigt


    # Soll eine Ereignisbeschreibung erzeugt werden?
    if view_msg:

        # Start des Meldungstextes
        msg = "%s: #%d %s" % ( _NAME, evt[ EVT_P_POS ], evt[ EVT_P_TXT ] )

        # Parameter?
        if evtparam != None:

            if type( evtparam ) == str:
                msg += " " + evtparam
            elif type( evtparam ) == list:
                msg += " " + lis_.to_str( evtparam )
            else:
                msg += " " + str( evtparam )


        # Meldung für den Typ?
        if _EVTS[ evt[ 1 ] ][ _EVTS_MSG ]:

            # Meldung
            msg_.message( msg, msg_.TYPE_INFO )


    # Exception für den Typ?
    if excep == 1:
        raise ValueError( msg )
    elif excep == 2:
        raise RuntimeError( msg )

    # Ergebnis
    return result



###############################################################################
# Liest Dateiinhalt
# Gegenstück zu file_write()
#
# 'filename'  (str)  Dateiname
# 'default'   (?)    Was im Fehlerfall zurückgeliefert werden soll
# 'param'     (?)    Parameter, die an das Ereignis EVT_019 weitergereicht werden
# 'vars'      (vars) Var-Speicher
# 'raise_evt' (int)  Wann soll EVT_020 ausgelöst werden
#                    0: Nie
#                   [1]: Immer, wenn die Datei nicht geladen werden konnte.
#                    2:  Immer, wenn die Datei nicht geladen werden konnte.
#                        (aber existierte)
#
# Ergebnis: (0) (str) Der Dateiinhalt oder 'default'
#           (1) fil_.ST_OK,  wenn erfolgreich
#               fil_.ST_NO,  wenn Datei nicht vorhanden
#               fil_.ST_PM,  wenn keine Berechtigung
#               fil_.ST_ERR, wenn unbekannter Fehler auftrat
###############################################################################
def file_read( filename, default, param, vars, raise_evt = 1  ) :

    # Ereignis: Text-Datei laden
    evt_result = evt( vars, EVT_019, [ filename, default, param ] )

    # Wenn kein Abbruch durch Ereignis ...
    if evt_result == None:

        # Laden ...
        result, state = fil_.read( filename, default )

    else:

        # Ergebnisse vom Ereignis übernehmen
        result = evt_result[ 0 ]
        state  = evt_result[ 1 ]

    # Datei konnte nicht gelesen werden?
    if state != fil_.ST_OK:
        # Entscheiden, ob Ereignis ausgelöst werden soll
        if raise_evt == 1 or state != fil_.ST_NO:
            evt( vars, EVT_020, [ filename, state ] )

    # Dateiinhalt und Status zurückliefern
    return result, state


###############################################################################
# Speichert eine Zeichenkette in eine Datei
# Gegenstück zu file_read()
#
# 'filename' (str)  Dateiname
# 's'        (str)  Was gespeichert werden soll
# 'param'    (?)    Parameter, die an das Ereignis EVT_017 weitergereicht werden
# 'vars'     (vars) Var-Speicher
#
# Ergebnis: ST_OK, wenn erfolgreich
###############################################################################
def file_write( filename, s, param, vars ) :

    # Ereignis: Vor Speichern einer Text-Datei
    evt_result = evt( vars, EVT_017, [ filename, s, param ] )

    # Wenn kein Abbruch durch Ereignis ...
    if evt_result == None:

        # Ob Datei schon existiert
        f_is = fil_.isfile( filename )

        # Speichern ...
        result = fil_.write( filename, s )

    else: # Abbruch durch Ereignis ...

        result = evt_result[ 0 ]
        f_is   = evt_result[ 1 ]

    # Ergebnis auswerten ...
    if result != fil_.ST_OK:

        # Ereignis: Fehler beim Speichern
        evt( vars, EVT_018, [ filename, s, param ] )

    # Wenn Datei noch nicht da war ...
    elif not f_is:

        # Ereignis: Datei wurde neu angelegt
        evt( vars, EVT_037, filename )

    # Ergebnis
    return result


###############################################################################
# Formatiert einen Eintrag von MOD_ACTION_PDOC oder MOD_ACTION_IDDOC
# für eine Ausgabe. Es wird auf jeden Fall ein gültiger Wert geliefert.
#
# 'item'     (str) Zeichenkette, die behandelt werden soll
#           (list) [0] (list) Alle Einträge
#                  [1] (int)  Index
# 'prilong' (bool) True, wenn die Priorität auf der lang-Version liegen soll
# 'pre'     (str)  Text vor Ergebnis
# 'itemlist' (bool) True, wenn 'item' eine Liste mit Einträgen enthält
#                   [0] (list) Alle Einträge
#                   [1] (int)  Index
#
# Ergebnis  (str) Zeichenkette
###############################################################################
def format_doc( item, prilong = True, pre = "", itemlist = False ):

    # Wenn leerer Eintrag ...
    if item == None:

        result = ""

    else:

        # Wenn Liste als item-Parameter angegeben wurde
        if itemlist:
            if item[ 0 ] != None:
                item = item[ 0 ][ item[ 1 ] ]
            else:
                result = ""

        # Nur eine Version vorhanden?
        if type( item ) == str:
            result = item
        elif type( item ) == list:
            # Liste
            if prilong: # Priorität auf Lang-Version
                result = item[ 1 ]
            else:
                result = item[ 0 ]
        else:
            result = "(?)"

    if type( result ) != str:
        result = "?(Type:)" +str( type( result ) )
    else:
        if len( result ) > 0:

            result = pre + result

    return result



###############################################################################
# Sucht ein Modul für eine bestimmte ID (z.B. einen Befehl)
#
# Hinweis: Wenn das Modul über eine dynamische ID-Ermittlung identifiziert
# wurde, wird die eigentliche ID als zweiter Wert zurückgeliefert
#
# Falls keine ID ermittelt werden konnte, wird eine 'Exception' ausgelöst
#
# 'id'      (str)  id
# 'vars'    (vars) Var-Speicher
#
# Ergebnis  (str) Modul-Code
#          ((str) Ggf. neue ID))
###############################################################################
def id_get_module( id, vars ):

    if vars_in( vars, id ):
        module = vars_get( vars, id, VIB_MODULE ) # Modul holen
    else:                             # Kein Modul für ID registriert ...

        module = None

        # Dynamische ID-Überprüfung durchführen ...
        vars_set( vars, VAR_NAME, id )    # 'id' ist der unbekannte Befehl

        # Alle registr. Module ...
        for m in vars_get( vars, MOD_GROUP_DYNID ):

            # Wahre ID ermitteln ...
            id_real = run_5( m, MOD_ACTION_DYNID, vars ) #

            # Wenn wahre ID gefunden ...
            if id_real != None:

                # Rekursion
                module, id = id_get_module( id_real, vars )

                # Wirkliche ID zurückliefern
                id = id_real

                break

        # Wenn noch immer kein Modul ermittelt werden konnte, wird versucht,
        # über das Ereignis EVT_032 eins zu ermitteln
        if module == None:

            # Ereignis: Unbekannte ID
            evt_result = evt( vars, EVT_032, id )

            # Hat Ereignis eine ID geliefert?
            if type( evt_result ) == str and vars_in( vars, evt_result ):

                # Modul holen
                module = vars_get( vars, evt_result, VIB_MODULE )

        # Es konnte noch immer kein Modul ermittelt werden? -> Fehler
        if module == None:

            # Ereignis: Unbekannter Bezeichner
            evt( vars, ERR_012, id )


    return module, id



###############################################################################
# Fragt (auch von ausserhalb) die Parameter einer ID ab
#
# 'id'          (str) ID (z.B. Befehl)
# 'info'        (int) Welche Info ermittelt werden sollen
#                     Für die möglichen Werte siehe _ACTIONS_NAME
# 'vars'        (vars) Var-Speicher
#                      'None', wenn temporäres erzeugt werden soll
#
# Ergebnis      Je nach 'info'
#               'Exception', wenn 'id' nicht existiert
###############################################################################
def id_get_info( id, info = MOD_ACTION_PTYPE, vars = None ) :

    # Ob gültiger Wert
    if not _ACTIONS[ info ][ _ACTIONS_NAME ]:
        evt( vars, ERR_014 )

    # Ob 'vars' angegeben wurde
    if vars == None:

        # 'vars'
        vars = vars_new( True )
        is_vars_new = True

    else:
        is_vars_new = False

    idbak = vars_get( vars, VAR_NAME )           # Akt. ID retten

    # Modul holen (Bei Dynamischer ID kann sich auch ID ändern!)
    module, id = id_get_module( id, vars )       # Zuständiges Modul

    vars_set( vars, VAR_NAME, id )               # Neue

    result = run_5( module, info, vars )         # Param.

    vars_set( vars, VAR_NAME, idbak )            # Urspr. ID herstellen

    if is_vars_new:
       vars_release( vars )                      # Freigeben

    return result



###############################################################################
# Ermittelt eine Auflistung aller Parameter eines bestimmten Typs in den glo-
# balen Variablen.
#
# 'gtyp'    (GTYP/GTYP-list) Alle Parameter dieses Typs (dieser Typen) ermitteln
# 'vars'    (vars) Var-Speicher
#
# Ergebnis (list) Einträge bestehen aus Paaren
#                 [0] ID
#                 [1] Index des Parameters
###############################################################################
def id_get_param_list( gtyp, vars ):

    result = []

    # Alle IDs durchlaufen ...
    for id, block in _vars( vars ).items():

        # GTYP der ID
        gt = block[ VIB_GTYP ]

        # Hat der Typ Typinfos in VIB_PGTYPES?
        if _GTYPES[ gt ][ _GTY_TYPAR ] != -1: # Ja? ...

            # Typinfos für seine Parameter
            param_gtyps = block[ VIB_PGTYPES ]

            # Alle Einträge durchlaufen
            i = 0

            for pgt in param_gtyps:

                # Wenn vom gesuchten Typ
                if ( type( gtyp )  ==  int  and pgt == gtyp ) or \
                   ( type( gtyp )  ==  list and pgt in gtyp ):

                    # Neuer Eintrag
                    item = [ id, i ]

                    # In Auflistung aufnehmen
                    result.append( item )

                # Nächster Index
                i += 1


    return result



###############################################################################
# Fragt für eine ID Typ-Infos aus _GTYPES ab
#
# 'id'          (str) ID (z.B. Befehl)
# 'info'        (int) Welche Info ermittelt werden sollen
#                     Für die möglichen Werte siehe _GTY_... (z.B. _GTY_TYPAR)
# 'vars'        (vars) Var-Speicher
#
# Ergebnis      Je nach 'info'
#               'Exception', wenn 'id' nicht existiert
###############################################################################
def id_get_typeinfo( id, info, vars ) :

   # Typ der ID ermitteln
   gtyp = vars_get( vars, id, VIB_GTYP )

   # Gewünschte Info zurückliefern
   return _GTYPES[ gtyp ][ info ]



###############################################################################
# Die Module beenden und entladen
#
# 'vars'      (vars) Var-Speicher, die im Eintrag 'vars[VAR_MODULES]' die
#             geladenen Module enthält.
# Ergebnis
###############################################################################
def modules_exit( vars ) :

    # Auflistung der Module holen
    mods = vars_get( vars, VAR_MODULES )

    # Alle Module beenden
    if mods != None:
        for module in mods:

           # Modulfunktion ausführen
           result = run_5( module, MOD_ACTION_EXIT, vars )

           if result == False:      # Modul hat Fehler gemeldet?
              evt( vars, ERR_018 )  # Exception!
              result = True         # zurücksetzten

        # Var
        vars_set( vars, VAR_MODULES, None )
        vars_set( vars, VAR_MODULES_INIT, False )



###############################################################################
# Module initalisieren
#
# 'vars'     (vars) Var-Speicher
#                   Enthält Modulliste unter VAR_MODULES
#
# Ergebnis
###############################################################################
def modules_init( vars ) :

    # Schon initalisiert?
    if vars_in( vars, VAR_MODULES_INIT ) and \
            vars_get( vars, VAR_MODULES_INIT ) == True: # Ja?

        evt( vars, ERR_008 )            # Exception!

    # Alle Module durchlaufen...
    if vars_in( vars, VAR_MODULES ):    # Module da?

        modules = []                    # Alle initalisierten Module

        for module in vars_get( vars, VAR_MODULES ):# Alle Module durchlaufen

           info = ""                    # Modulinfo
           ids = run_5( module, MOD_ACTION_INIT, vars )

           # Type-Infos für seine IDs holen
           types = run_5( module, MOD_ACTION_ITYPE, vars )

           # Modulname
           mname = run_5( module, MOD_ACTION_NAME, vars )

           if type( ids ) == list:      # Typ-Ergebnis ist eine Liste?

                # Wenn ID-Liste aber nur eine Typangabe, sollen alle IDs von
                # diesem Typ sein
                if type( types ) == int:
                    types = len( ids ) * [ types ]

                # Fehlerüberprüfung
                if types != None and len( types ) != len( ids ):
                    evt( vars, ERR_030, mname )

                # Index der aktuellen ID
                i = 0

                # Alle IDs der Liste durchlaufen ...
                for id in ids:

                    # Typbehandlung
                    if types == None:             # Keine Typ-Angaben?
                        ty = MOD_ITYPE_DEFAULT    # Soll als dieser Typ gelten
                    else:
                        ty = types[ i ]           # Typ aus Angabe übernehmen

                    # Neue ID anlegen
                    id_create( id, ty, module, vars )

                    # Nächster Index
                    i += 1

           elif ids != None:            # Kein Array und nicht 'None'?
                evt( vars, ERR_010 )    # Exception!

           if ids != None:              # Modul erfolgreich initalisiert?

               # Ereignis: Modul erfolgreich initalisiert
               evt( vars, EVT_013, mname )

               # In Liste aufnehmen
               modules.append( module )

        # Wir ersetzen die Modulliste durch die Liste der erfolgreich
        # initalisierten Module ...
        vars_set( vars, VAR_MODULES, modules )

        # Init wurde durchgeführt
        vars_set( vars, VAR_MODULES_INIT, True )



###############################################################################
# Module im angegebenen Pfad suchen, laden und der Modulauflistung in
# 'vars' zufügen.
# Die geladenen Module können dann mit 'modules_init()' initalisiert werden.
#
# 'paths'       (str)       Pfad in dem die Module liegen, mit abschliessenden
#                           Trennzeichen.
#               (str-list)  Suchpfad-Liste
#               (None)      Die Verzeichnisse aus VAR_FOLDERS_M werden verwendet
# 'vars'        (vars)      Var-Speicher
#                           Die Module werden der Modulliste unter VAR_MODULES
#                           zugefügt.
#
# Ergebnis      Anz der geladenen Module
###############################################################################
def modules_load( paths, vars ) :

    # Liste der geladenen Module
    modules = []

    # 'path' behandeln
    if paths == None:                    # Keine Angabe?

        # Pfadliste aus 'vars' holen
        paths = vars_get( vars, VAR_FOLDERS_M )

    # Ggf. Zeichenkette in Zeichenketten-Liste konvertieren
    elif type( paths ) == str:

        paths = [ paths ]

    # Alle Pfade in Liste durchlaufen...
    for path in paths:

        # Wildcards für Modulsuche anhängen
        path += MOD_WILDCARD

        # Modul-Pfad-Ereignis
        evt( vars, EVT_011, path )

        # Dateienliste erstellen
        files = glob.glob( path )

        # Alle Dateien durchlaufen...
        for filename in files:

           # Modul laden, initalisieren und ggf. anhängen
           try:
              # Modul-Pfad-Ereignis
              evt( vars, EVT_012, filename )

              # Modul laden
              module, state = file_read( filename, None, None, vars )

              # Nicht erfolgreich?
              if state != fil_.ST_OK:

                 # Ereignis: Fehler beim Laden eines Moduls
                 # Programmabbruch
                 evt( vars, ERR_013, state )

              # Wenn Kennung im geladenen Modul vorhanden ...
              if MOD_ID in module:

                 # Intitalisierung-Level behandeln, damit das Modul bei
                 # modules_init() in der richtigen Reihenfolge behandelt
                 # wird
                 lvl = run_5( module, MOD_ACTION_LVL, vars )
                 if lvl == None or lvl == MOD_LVL_NORM or len(modules) == 0:
                     modules.append( module )       # Am Ende aufnehmen
                 else:
                     modules.insert( 0, module )    # Am Anfang aufnehmen

                 # Modulname holen
                 modname = run_5( module, MOD_ACTION_NAME, vars )

                 # Abbruch, wenn ungültiger Name
                 if modname == None or vars_in( vars, modname ):
                     evt( vars, ERR_023 )

                 # Neuen Moduleintrag in 'vars' anlegen

                 # Modulname eintr.
                 vars_set( vars, modname, modname )

                 # Typ eintragen
                 vars_set( vars, modname, GTYP_MODULE, VIB_GTYP )

                 # Pfad eintragen
                 vars_set( vars, modname, path, VIB_PARA1 )

                 # Modul eintragen
                 vars_set( vars, modname, module, VIB_MODULE )

              elif state != fil_.ST_OK: # Modulidentifizierung fehlerhaft?

                 # Ereignis: Fehler beim Laden eines Moduls
                 # Programmabbruch
                 evt( vars, ERR_013, MOD_ID )

           except Exception as e:

               # Ereignis: Fehler beim Laden eines Moduls
               evt( vars, ERR_013, str( e ) )

    # Module in 'vars[ VAR_MODULES ]' Übernehmen
    if VAR_MODULES in _vars( vars ):                     # Schon welche da?
        vars_get( vars, VAR_MODULES ).extend( modules )  # Zufügen
    else:                                           # Noch keine da ...
        vars_set( vars, VAR_MODULES, modules )      # Neu

    # Ergebnis
    return len( modules )



###############################################################################
# Eines des Dateiverzeichnisse abfragen
#
# 'folder'  (str) Für welchen Ordner der Pfad ermittelt werden soll
#           Siehe FOLDER_... z.B. FOLDER_MODULES
#
# Ergebnis  (str) Pfad (mit zusätzlichem Trennzeichen)
###############################################################################
def path_get( folder ) :

    # Ordner der obersten Ebene (im gic-Ordner)?
    if folder in [ FOLDER_MODULES,
                   FOLDER_SCRIPTS,
                   FOLDER_TEMPLATES,
                   FOLDER_TMP ]:

        result = fil_.extract( sys.argv[ 0 ], fil_.FE_PATH_SEP ) + \
                 FOLDER_GIC + os.sep + \
                 folder     + os.sep

    # Modulunterordner?
    elif folder in FOLDER__MODULES:

        result = fil_.extract( sys.argv[ 0 ], fil_.FE_PATH_SEP ) + \
                 FOLDER_GIC     + os.sep + \
                 FOLDER_MODULES + os.sep + \
                 folder         + os.sep

    # Basisordner / Installation?
    elif folder == FOLDER_INSTAL:

        result = fil_.extract( sys.argv[ 0 ], fil_.FE_PATH_SEP )

    # Demo-Unter-Ordner
    elif folder in [ FOLDER_DEMO_CODE, FOLDER_DEMO_PICS ]:

        result = fil_.extract( sys.argv[ 0 ], fil_.FE_PATH_SEP ) + \
                 FOLDER_GIC  + os.sep + \
                 FOLDER_DEMO + os.sep + \
                 folder + os.sep

    return result


###############################################################################
# Temporäres Dateiverzeichnis abfragen. Identisch mit path_get( FOLDER_TMP ),
# path_tmp_get() stellt zusätzlich sicher, dass dieses Verzeichnis auch
# wirklich existiert.
#
# 'vars'    (vars) Var-Speicher
#
# Ergebnis  (str) Pfad (mit zusätzlichem Trennzeichen)
###############################################################################
def path_tmp_get( vars ) :

    # Unser Ergebnis
    result = path_get( FOLDER_TMP )

    # Temp-Ordner ggf. erzeugen
    if os.path.exists( result ) == False:           # Nicht da?
        # Ereignis: Ordner anlegen
        if evt( vars, EVT_026, result ) != False:
            fil_.create_folder( result )            # Ordner anlegen

    # Unser Ergebnis
    return result


###############################################################################
# Evaluierung einer Zeichenkette. Ersetzt die enthaltenen Variablen / Platz-
# halter durch ihre Werte, dann wird die Zeichenkette ausgewertet und das
# Ergebnis zurückgeliefert.
#
# Für jeden gefundenen Bezeichner wird evalV_name() aufgerufen.
#
# 'sgic'        (str)   Zeichenkette, deren Platzhalter ersetzt werden sollen
# 'gtype'       (GTYP_) Ergebnistyp
# 'vars'        (vars)  Var-Speicher
# 'qm'          (str)   Zeichen, das eine Zeichenkette einleitet, in der keine
#                       Bezeichner erkannt werden sollen. [RVQM]
#
#  Zeichenersetzung ausserhalb von Zeichenketten ..
#
# 'fi'          (str) Eine Zeichenk., deren Zeichen durch die Zeichen aus 're',
#               die an der gleichen Position eines gefundenen Zeichens liegen,
#               ersetzt werden sollen [None]
# 're'          (str) Wenn z.B. 'fi' == 'ab' u. 're' == 'cd' ist, wird ein ge-
#               fundenes 'a' durch 'c' und eine 'b' durch 'd' ersetzt.
#               'fi' und 're' müssen immer gleich lang sein.
#               'fi' auf 'None' setzen, wenn keine Zeichen ersetzt werden
#               sollen. [None]
#
#               Es dürfen nur Zeichen, die eine Länge von 1 haben, verwendet
#               werden (z.B. nicht das 'ß' mit einer Länge von 2 )
# 'cib'         (CIB)  In welchem Kontext aufgerufen wird [None]
#
# Ergebnis      (str) Die aufbereitete Zeichenkette
###############################################################################
def evalV(  sgic,
            gtype,
            vars,
            qm  = RVQM,
            fi  = None,
            re  = None,
            cib = None ):

    # Überprüfen, ob Platzhalter ersetzt werden sollen
    if ( _GTYPES[ gtype ][ _GTY_REPLV ] & 1 ) != 1 : # Keine Behandl. durch uns?

        # Keine Ersetzungen
        result = sgic

        # Unten kein eval() ausführen!
        do_eval = False

    else:                         # Ersetzungen ...

        # Vorgabe, ob unten eval() ausführt werden soll
        do_eval = ( _GTYPES[ gtype ][ _GTY_REPLV ] & 2 ) == 2

        result = ""               # Unser Ergebnis ist eine Zeichenkette

        cnt_var  = 0              # Anzahl der gefundenen Variablen
        cnt_c    = 0              # Anzahl der analysierten Zeichen
        varname  = ""             # Aktueller Variablenname
        cadd     = None           # Zeichen, das in das Erg. aufgen. werden soll
        length   = len( sgic )    # Anz d. Zeichen, die analysiert werd. sollen

        # Wenn größer 0, sollen die Anzahl an Zeichen nicht analysiert werden
        no_analyse = 0

        # Analysierungsmodus / 'mode'
        MODE_NO  = 0              # mode: Kein Modus
        MODE_STR = 1              # mode: Zeichenkettenmodus
        MODE_VAR = 2              # mode: Variablenmodus

        # Aktueller Analysierungsmodus
        mode = MODE_NO;           # z.z. keine Zeichenkette

        # Alle Zeichen in 'sgic' abarbeiten...
        for c in sgic:

          # Anz der analysierten Zeichen
          cnt_c += 1

          if no_analyse > 0:    # Bestimmte Zeichenanz nicht analysieren?
            no_analyse -= 1     # Ein Zeichen weniger

          else:                 # Zeichen analysieren ...

            # 'cadd' enthält das Zeichen, das in Erg. aufg. werden soll
            cadd = c

            # Behandlung von Zeichenersetzungen
            if mode != MODE_STR:            # Kein Zeichenkettenmodus?
                if fi != None:              # Bestimmte Zeichen ersetzen?
                    pos = fi.find( cadd )   # Suche Zeichen in Ersetzungsliste
                    if pos != -1:           # gefunden?
                        cadd = re[ pos ]    # Durch entsprech. Zeichen ersetzen
                        if cadd == GIC_PROT:# Geschützes Zeichen?
                            cadd = None     # Zeichen ganz ersetzen

            # Je nach aktuellem Modus
            if mode == MODE_NO:            # Kein Zeichenkettenmodus?
                if c == qm:                # Beginn einer Zeichenkette?
                    mode = MODE_STR        # Zeichenkettenmodus
                else:                      # Kein Beginn einer Zeichenkette ...
                    if c in VARS_CHARS_1:  # Zeichen ist Teil eines Var-Namens?
                        varname += c       # Akt. Zeichen in V.-Namen aufnehmen
                        cadd = None        # In diesem Durchgang kein Zeichen
                                           # in das Ergebnis aufnehmen
                        mode = MODE_VAR    # Variablenmodus

            elif mode == MODE_STR:         # Zeichenkettenmodus?
                if c == qm:                # Ende einer Zeichenkette?
                    mode = MODE_NO         # Zeichenkettenmodus beenden

            elif mode == MODE_VAR:         # Variablenmodus?
                if c in VARS_CHARS_x:      # Zeichen ist Teil eines Var-Namens?
                    varname += c           # Aktuelles Zeichen in Variablennamen
                    cadd = None            # Kein Zeichen aufnehmen
                else:
                    if c == qm:            # Beginn einer Zeichenkette?
                        mode = MODE_STR    # Zeichenkettenmodus
                    else:
                        mode = MODE_NO     # Kein Modus

            # Wenn Platzhalterzeichen gefunden ...
            if len( varname ) > 0:

                # Keine Platzhalterzeichensammelmodus mehr oder letztes Zeichen
                if mode != MODE_VAR or cnt_c == length:

                    # Parameter für evalV_name()
                    # Wenn Klammern hinter dem Bezeichner identifiziert wurden,
                    # steht in 'brackets' das zwischen ihnen liegende
                    brackets = None

                    # Parameter für evalV_name(). 0 ist der Standardwert
                    eval_mode = 0

                    # Behandlung einer Funktion
                    if cnt_c != length: # Nicht letztes Zeichen?

                        # Runde Klammer / Funktion ?
                        if sgic[ cnt_c - 1 ] == GIC_FCT1: # Klammer auf?

                            # Position von Klammer auf / zu
                            bra_start, \
                            bra_end = str_.find_brackets( sgic[ cnt_c - 1: ],
                                                          GIC_FCT1,
                                                          GIC_FCT2,
                                                          GIC_QM
                                                        )

                            # Alles zwischen den äusseren Klammern ermitteln
                            start = cnt_c + bra_start
                            end   = cnt_c - 1 + bra_end
                            brackets = sgic[ start : end ]

                            # Klammern und Zeichen dazwischen nicht weiter aus-
                            # werten oder analysieren
                            no_analyse = len( GIC_FCT1 ) + \
                                         len( GIC_FCT2 ) + \
                                         end - start - 1

                            # Kein Zeichen aufnehmen
                            cadd = None

                            # Parameter für evalV_name().
                            eval_mode = 1 # Ein Klammer wurde gefunden

                        # Eckige Klammer?
                        elif sgic[ cnt_c - 1 ] == GIC_SQB1: # Klammer auf?

                            # Position von Klammer auf / zu
                            bra_start, \
                            bra_end = str_.find_brackets( sgic[ cnt_c - 1: ],
                                                          GIC_SQB1,
                                                          GIC_SQB2,
                                                          GIC_QM
                                                        )

                            # Alles zwischen den äusseren Klammern ermitteln
                            start = cnt_c + bra_start
                            end   = cnt_c - 1 + bra_end
                            brackets = sgic[ start : end ]

                            # Klammern und Zeichen dazwischen nicht weiter aus-
                            # werten oder analysieren
                            no_analyse = len( GIC_SQB1 ) + \
                                         len( GIC_SQB2 ) + \
                                         end - start - 1

                            # Kein Zeichen aufnehmen
                            cadd = None

                            # Parameter für evalV_name().
                            eval_mode = 2 # Ein Klammer wurde gefunden


                    # Ersetzungwert ermitteln lassen
                    value, \
                    no_eval_result = evalV_name( varname,
                                                 eval_mode,
                                                 brackets,
                                                 vars,
                                                 cib )

                    # vorhandene Werte erweitern
                    result += value

                    # Anz der gefundenen Platzhalter
                    cnt_var += 1

                    # 'no_eval'
                    # Überprüfen, ob evalV_name() das eval() unten
                    # verhindern möchte
                    if no_eval_result and do_eval:  # eval aktiv und geändert?
                        do_eval = False             # eval: aus!

                    # Aktuellen Var-Namen zurücksetzen
                    varname = ""

            # ggf. Ergebnis erweitern
            if cadd != None:
                result += cadd

        # evalV_bool() für diesen Typ anwenden?
        if ( _GTYPES[ gtype ][ _GTY_REPLV ] & 8 ) == 8 : # Ja?
            result = evalV_bool( result )

    # eval() ...
    if do_eval == True:

        # Ob Backslashs ersetzt wurden
        rpl_backsl = False

        # Backslashs vorhanden?
        if "\\" in result:
            result = result.replace( "\\", GIC_PROT ) # Backslash ersetzen
            rpl_backsl = True

        try:
            # Auswerten durch Python
            result = eval( result )

        # Fehler bei Auswertung durch Python  ...
        except Exception as e:

            # Ereignis: Evaluierungsfehler
            evt( vars, EVT_023, result )

            # 'Exception' auslösen
            raise ValueError( result )

        # Wenn Backslashs ersetzt wurden ...
        if rpl_backsl == True:
            result = result.replace( GIC_PROT, "\\" ) # Backsl. wiederherst.

    # Spezialbehandlung bestimmer Ergebnistypen

    # GTYP_BLOCK / Code-Block
    if gtype == GTYP_BLOCK:
        result = evalV_BLOCK( sgic, result, cib )

    # GTYP_FILE / Dateinamen
    elif gtype == GTYP_FILE:
        # Ereignis: Nachbehandlung von Dateinamen
        evt_result = evt( vars, EVT_100, result )
        if evt_result != None:                          # Ereignis - Erg.?
            result = evt_result                         # Erg übernehmen

    # GTYP_INT_PO / Positive Ganzzahlen ...
    elif gtype == GTYP_INT_PO:
        if result < 0:          # Kleiner als 0 ?
            # Ereignis: Ungültiger Wert für Type
            evt( vars, ERR_011, [sgic] )

    # Wenn kein Ergebnis ermittelt werden konnte
    if result == None:

        # Ereignis: Evaluierungsfehler
        evt( vars, EVT_023, [sgic,"?"] )

        # 'Exception' auslösen
        raise ValueError( result )

    # Ggf. Hochkomma durch Anführungszeichen ersetzten
    if ( _GTYPES[ gtype ][ _GTY_REPLV ] & 4 ) == 4 :
        result = result.replace( GIC_APO, GIC_QM )

    # Ergebnis
    return result



###############################################################################
# Zusatzliche Nachbehandlung, wenn evalV() für boolischen Wert erzeugt.
#
# gic verwendet "=" als Vergleichsoperator, Python "==", das wird jetzt
# berücksichtigt. Alle anderen Schreibweisen (z.B. >=) werden von Python
# übernommen.
#
# 'sgic'      (str)  Zeichenkette, die angepasst werden soll
#
# Ergebnis   (str) Die aufbereitete Zeichenkette
###############################################################################
def evalV_bool( sgic ):

    # Überprüfen, ob mindestens ein GIC_COMP / "=" vorhanden ist
    if sgic.find( GIC_COMP ) == -1:  # Analyse nicht notwendig?
        result = sgic                # Unverändert zurückliefern
    else:                            # Analyse notwendig ...
        # Positionen aller "=" ausserhalb von Zeichenketten ermitteln
        poslist = str_.find_ex( sgic,
                                GIC_COMP,
                                GIC_QM,
                                sfe_mode = str_.SFE_LIST )
        if len( poslist ) == 0:         # Nichts gefunden?
            result = sgic               # Unverändert zurückliefern
        else:                           # Gefunden ...
            # Das Ergebnis
            result = ""
            # Vorheriges Zeichen
            cprev = ""
            # Akt. Positionslisten-Index
            poslist_idx = 0
            # Index
            i = 0
            # Alle Zeichen in 'sgic' abarbeiten...
            for c in sgic:
                # Ob passendes "=" an aktuellem Index gefunden
                found = False
                # Liegt an der akt. Pos ein "=" ?
                if i == poslist[ poslist_idx ]: # Ja
                    # Überprüfen, ob "=" zu ">=" od. "<=" gehört
                    if cprev != ">" and cprev != "<":
                        found = True
                    # ggf. Auf nächste Listenpos
                    if poslist_idx < len( poslist ) - 1:
                        poslist_idx += 1
                # Aktuelles Zeichen in Ergebnis aufnehmen
                result += c
                # Wenn akt. Zeichen ein passendes "=" ist, erneut aufnehmen
                if found:           # Ja ...
                    result += c
                # Akt. Zeichen wird vorheriges Zeichen
                cprev = c
                # Index anpassen
                i += 1


    return result



###############################################################################
# Zusatzliche Nachbehandlung für Einträge vom Typ GTYP_BLOCK / Code-Block
#
# Hier wird der Fall behandelt, das der Parameter nicht als Zahl, sondern als
# Zeichenkette angegeben wurde.
#
# 'sgic'     (str)  Zeichenkette, die gerade evaluiert wird
# 'result'     (?)  Das aktuelle Ergebnis (int oder str)
# 'cib'      (CIB)  In welchem Kontext aufgerufen wird
#
# Ergebnis   (int)  Anzahl der betroffenen Zellen
###############################################################################
def evalV_BLOCK( sgic, result, cib ):

    # Zeichenkette?
    if type( result ) == str:

        # Werte übertragen
        items = cib[ CIB_ITEMS ]
        i     = cib[ CIB_POS   ]
        txt   = result

        # Ergebnis
        result = 0

        # Ob Ziel gefunden wurde
        found = False

        # Alle restlichen Einträge durchsuchen ...
        while i < len( items ):  # Noch nicht letzter?

            # Eintrag an aktueller Position holen
            item = items[ i ]

            # Text zwischen Anführungszeichen vergleichen
            if item != None and len(item) > 2 and item[ 1 : -1 ] == txt:
                found = True
                break

            # Ergebnis und Index anpassen
            result += 1
            i += 1

        # Ereignis: Blockende nicht gefunden
        if not found:
            evt( cib[ CIB_VARS ], ERR_040, txt )

    else: # Keine Zeichenkette ...
        # Ergebnis liegt schon als int vor
        result = result

    # Ergebnis zurückliefern
    return result


###############################################################################
# Wird durch evalV() aufgerufen, wenn ein entsprechender Varname /
# Platzhalter identifiziert wurde. Wir liefern den Wert, durch den der
# Wert ersetzt werden soll.
#
# 'name'        (str)  Gefundener Bezeichner
# 'mode'        (int)  0: Normaler Modus
#                      1: Eine runde Klammer wurde hinter dem Bez. gefunden
#                      2: Eine eckige Klammer wurde hinter dem Bez. gefunden
# 'brackets'    (str)  Wenn 'mode' auf 1 oder 2 gesetzt wird, muss hier das
#                      zwischen den Klammern Liegende übergeben werden.
#               (None) Hinter dem Bezeichner befinden sich keine Klammern
# 'vars'        (vars) Var-Speicher
# 'cib'         (vars) In welchem Kontext aufgerufen wird
#
# Ergebnis      (str) Zeichenketten-Wert durch den 'name' ersetzt werden soll.
#            + (bool) True, wenn eine Ersetzung gefunden wurde, die eine
#                     Evaluierung ausschliesst
###############################################################################
def evalV_name( name, mode, brackets, vars, cib ):

    # Unser zweiter Rückgabewert
    no_eval = False

    # Aktueller Anführungszeichen-Modus ( _GTY_QM )
    qm_mode = None

    # Behandlung einer Funktion
    if mode == 1:            # Runde Klammern / Funktion ?

        # Funktion ausführen ...
        value, gtyp = run_fct( name, brackets, vars, cib )

    else:                           # Keine Funktion ...

        # Ermitteln, ob es sich bei 'name' um einen lokalen oder globalen
        # Bezeichner handelt
        space = vars_find( vars, name )

        # Wenn 'name' nicht ermittelt werden konnte ...
        if space == VARS__UNDEF:                # Nichts gefunden?

            # Vielleicht wurde eine Kurzform angegeben?
            name_ex = id_expand( name, vars, LOC.VARS_SHORT )

            # Wurde 'name' ersetzt?
            if name_ex != name:                 # Ja?
                name  = name_ex                 # Namen ersetzen
                space = vars_find( vars, name ) # Zweiter Versuch

            if space == VARS__UNDEF:            # (Wieder) nichts gefunden?
                evt( vars, ERR_016, name )      # Exception!

        # Typ (GTYP) hinter 'name' ermitteln
        gtyp  = vars_get( vars, name, VIB_GTYP, space )

        # Behandlung eckiger Klammern
        if mode == 2:                           # Eckige Klammer?

            # Überprüfen, ob eine Liste vorliegt
            if _GTYPES[ gtyp ][ _GTY_TYPE ] != list:
                evt( vars, ERR_041, [ name, gtyp ] ) # Exception!

            # Das zwischen den Klammern ist ein numerischer Index
            idx = evalV( brackets, GTYP_INT, vars )

            # Index eintragen, damit vars_get() den richtigen Wert liefert
            vars_set( vars, name, idx, VIB_LIST_POS, GTYP_INT, space )

            # Anführungszeichen nach Typ
            qm_mode = 2

        # Wert hinter 'name' ermitteln
        value = vars_get( vars, name, VIB_VALUE, space )

        # Fehler, wenn 'name' ungültigen Wert hat
        if value == None:                       # Nichts gefunden?
            evt( vars, ERR_016, name )          # Exception!

        # Sonderfall "modulgesteuerte Variable", der Typ muss durch Typ des
        # Parameters ersetzt werden
        if gtyp == GTYP_MODVAR:
            gtyp = vars_get( vars, name, VIB_PGTYPES, space )[ 0 ]

    # Überprüfen, ob eine Evaluierungsverhinderung für den Typ definiert wurde
    if _GTYPES[ gtyp ][ _GTY_EVAL_NO ]:
        no_eval = True

    # Entscheiden, ob der Wert mit Anführungszeichen umschlossen
    # werden soll ...

    if qm_mode == None: # Wurde Modus noch nicht festgelegt?
        qm_mode = _GTYPES[ gtyp ][ _GTY_QM ]

    if qm_mode == 2:                        # Ggf. nach Test?

        if type( value ) == str:            # Zeichenkette?
            result = "\"" + value + "\""    # Mit Anführungszeichen
        else:                               # Keine Zeichenkette ...
            result = str( value )           # Std-Konvertierung in Zeichk.

    elif qm_mode == 1:                      # Immer mit Anführungsz.?
        result = "\"" + value + "\""

    else:                                   # Immer ohne ...
        result = str( value )

    # Ergebnis
    return result, no_eval



###############################################################################
# Übersetzt ggf. einen Befehl in seine Langform. Wenn keine Langform
# für den angebenen Befehl existiert, wird er unverändert zurückgeliefert.
#
# 'id'          (str)   ID
# 'vars'        (vars)  Var-Speicher
# 'dic'         (dict)  Ersetzungsliste
#
# Ergebnis      (str)   ID
###############################################################################
def id_expand( id, vars, dic ):

    if len( id ) == 1:                # Ein Zeichen?

       if id in dic:                  # Langform vorhanden?

           # Langform holen
           id = dic[ id ]

    # Ergebnis
    return id



###############################################################################
# Eine neue, von einem Modul initiierte, ID erzeugen
#
# 'id'          (str)  ID
# 'idtype'      (int)  ID-Typ (GTYP)
# 'module'      (str)  Zuständiges Modul
# 'vars'        (vars) Var-Speicher
#
# Ergebnis
###############################################################################
def id_create( id, idtype, module, vars ):

    # Überprüfen, ob ID-Flags angegeben wurden. In diesem Fall besteht die
    # ID aus einer Liste, die im ersten Eintrag die ID, im zweiten die ID-Flags
    # enthält
    if type( id ) == list:

        # Auf korrekte Listenlänge überprüfen
        if len( id ) != 2:
            evt( vars, ERR_031, [ id, "(flags)" ] )

        # Listeneinträge übernehmen
        flags = id[ 1 ] # Flags
        id    = id[ 0 ] # ID

        # Auf korrekten Flags-Typ (int) überprüfen
        if type( flags ) != int:
            evt( vars, ERR_031, [ str(id), "(flagstype)" ] )

    else:   # ID liegt als Zeichenkette vor ...

        # Keine Flags
        flags = 0

    # Auf korrekten ID-Typ (Zeichenkette) überprüfen
    if type( id ) != str:
        evt( vars, ERR_031, [ id, "(type)" ] )

    # Spezial-ID

    # GTYP_GROUP: ID bezeichnet eine Modulgruppe, in die das 'module'
    # aufgenommen werden soll
    if idtype == GTYP_GROUP:

        # Ggf. neue Modulgruppe anlegen
        if not vars_in( vars, id ):

            vars_set( vars, id, [],         VIB_VALUE )
            vars_set( vars, id, [],         VIB_PARA1 )
            vars_set( vars, id, GTYP_GROUP, VIB_GTYP  )

        # Modul in Gruppe aufnehmen ...
        vars_get( vars, id ).append( module )

        # Modul-Namen holen ...
        mname = run_6( module, MOD_ACTION_NAME, vars )

        # Modulnamen in Gruppenparameter eintragen ...
        vars_get( vars, id, VIB_PARA1 ).append( mname )

    else:   # Keine Modulgruppe ...

        # Wenn ID schon vergeben ist ...
        if vars_in( vars, id ):
            evt( vars, ERR_025, [ id ] )    # Exception!

        # Den Wert für neue ID mit dem Standard-Wert des Typs initalisieren
        valueinit = _GTYPES[ idtype ][ _GTY_VALUE ]
        if valueinit != None:
            vars_set( vars, id, valueinit, VIB_VALUE )

        # Typ für die ID festlegen
        vars_set( vars, id, idtype, VIB_GTYP )

        # Modul für die ID festlegen
        vars_set( vars, id, module, VIB_MODULE )

        # Ggf. Flags für die neue ID festlegen
        if flags != 0:
            vars_set( vars, id, flags, VIB_FLAGS )

        # Parameter-Typinfos vom Modul anfordern und in den ID-Parametern
        # speichern
        paramode = _GTYPES[ idtype ][ _GTY_TYPAR ]
        ptypes   = id_get_info( id, MOD_ACTION_PTYPE, vars )

        if ptypes != None:      # Modul hat Typinfos geliefert?

            if paramode == -1:      # Der Typ darf keine derartigen Dinge haben
                evt( vars, ERR_026, id )

            elif len( ptypes ) < paramode: # Zu wenig Typinfos?
                evt( vars, ERR_026, [ id, len( ptypes ), paramode ] )

            # Typinfo in den ID-Daten speichern
            vars_set( vars, id, ptypes, VIB_PGTYPES )

            # Splitinfos für Parameter
            psplits = id_get_info( id, MOD_ACTION_PSPLIT, vars )

            # Modul hat Infos geliefert?
            if psplits != None:

                # Splitinfo in den ID-Daten speichern
                vars_set( vars, id, psplits, VIB_PSPLITS )


        else:   # Modul hat keine Typen geliefert ...

            if paramode != -1: # Hätte aber?
                # Typinfo in den ID_Daten speichern
                # ID hat keine Parameter / Leere Liste
                vars_set( vars, id, [], VIB_PGTYPES )
                #evt( vars, ERR_026, [ id, paramode ] )

    # MOD_ACTION_IDDOC - Überprüfen, ob korrekte Werte
    iddoc = run_6( module, MOD_ACTION_IDDOC, vars, id )

    # Auf korrekte Listenlänge überprüfen
    if type( iddoc ) == list and len( iddoc ) != 2:
        evt( vars, ERR_031, [ id, "(doc)" ] )

    # Modul über erfolgreiche Initalisierung informieren
    run_6( module, MOD_ACTION_POST, vars, id )


###############################################################################
# liefert eine Auflistung unserer IDs
#
# 'vars'         (vars) Var-Speicher
# 'filter'              Filter, um nur die ID zu berücksichtigen, die dem
#                       Filter entsprechen ...
#                        (None),    um alle zu berücksichtigen
#                        (int)      GTYP_...
#                        (int-list) Um die angebenen Typen zu filtern
#                        (str)      Gruppe. Nur Einträge dieser Gruppe
# 'sort'         (bool) True: Liste alphab. nach IDs sortieren
#
# Ergebnis       (str-list / str)
###############################################################################
def vars_list( vars,
               filter = GTYP_CMD,
               sort   = False,
             ):

    # Ergebnis
    result = []

    # Init
    id_ok   = False   # Ob Eintrag in Erg aufgenommen werden soll
    pos     = 1       # Laufende Position

    # Alle IDs durchlaufen
    for id, block in _vars( vars ).items():

        # Falsche Blockgröße?
        if len( block ) != VIB__SIZE:
           evt( vars, ERR_021, [ pos, id, len( block ), VIB__SIZE ] )

        # init
        id_ok = False         # Akt. ID (noch) nicht in Ergebnis aufnehmen

        # Unsichtbar?
        if block[ VIB_FLAGS ] & VF_INV == VF_INV:

            id_ok = False                   # Nicht aufnehmen

        else:

            # Ob akt. ID dem gesuchtem Typ entspricht
            if filter == None:              # Alle?

                id_ok = True                # aufnehmen

            elif type( filter ) == int:     # Einzelner int-Wert?

                if block[ VIB_GTYP ] == filter:
                    id_ok = True

            elif type( filter ) == list:    # Liste?

                id_ok = block[ VIB_GTYP ] in filter

            elif type( filter ) == str:     # Gruppe?

                id_ok = id in vars_get( vars, filter, VIB_PARA1 )

        # Aufnehmen?
        if id_ok:

            # In Ergebnis aufnehmen
            result.append( id )

        # Pos
        pos += 1

    # Sortieren
    if sort == True and len( result ) > 1:

        result.sort()

    # Ergebnis
    return result



###############################################################################
# Liefert eine Auflistung unserer IDs
#
# 'vars'         (vars) Var-Speicher
# 'ids'          (str-list) Welche IDs angezeigt werden sollen
#                       'None', um alle anzuzeigen.
#
#                       Hier kann das Ergebnis von vars_list() verwendet
#                       werden!
# 'resultlist'   (bool) Bestimmt den Ergebnistyp
#                       True:  Als Array
#                       False: Als ein Text, bei dem jeder Eintrag in einer
#                              neuen Zeile steht
# 'vlmode'       (int)  Formatierungsmodus
#                       IG_SIMPLE   Einfache Einträge, die nur die IDs enthält
#                       IG_PARAM
# 'add_nr'       (str) 'None', wenn keine laufende Nummer vergeben werden soll,
#                       ansonsten eine Formatierungszeichenkette. z.B. "%3d."
# 'row'          (bool) True: Spaltenformatierung für ID
#
# Ergebnis       (str-list / str)
###############################################################################
def vars_list_ex( vars       = None,
                  ids        = None,
                  resultlist = False,
                  vlmode     = IG_PARAM,
                  add_nr     = "%3d. ",
                  row        = False
                ):

    # 'vars'
    if vars == None:

        vars = vars_new( True )
        is_vars_new = True

    else:
        if type( vars ) != VARS_TYPE:
            evt( vars, ERR_020, "vars" )

        is_vars_new = False

    # Vorläufiger Ergebnistyp: list
    result = []

    # Alle IDs durchlaufen
    item    = ""     # Aktueller Eintrag
    pos     = 1      # Laufende Position
    maxlen  = 0      # Größte ID-Länge
    rowchar = " "    # Erstes Zeichen hinter der ID

    # (Sortierte) IDs-Liste ermitteln
    if ids == None:
        ids = vars_list( vars, None, False )

    # Alle IDs durchlaufen ...
    for id in ids:

        # Typ
        gtyp = vars_get( vars, id, VIB_GTYP )

        # Modul
        module = vars_get( vars, id, VIB_MODULE )

        # Wenn eine der vars auf Zeichenkette gesetzt wurde, soll die
        # Info ausgeben werden
        add_iddoc = None    # Info über ID
        add_pdoc  = None    # Info über Param

        # Größte ID-Länge
        if len( id ) > maxlen:

            maxlen = len(id)

        # Je nach Ausgabemodus
        if vlmode == IG_SIMPLE:
            item = id

        elif vlmode == IG_PARAM or vlmode == IG_PARAM_NOTY:

            # ID
            item = "'%s'" % id

            # Typinfo
            if vlmode == IG_PARAM:
                item += " (%s) " % LOC.GTYPES[ gtyp ][ LOC.GTY_LONG ]

            add_iddoc = ""
            add_pdoc = ""

        else:
            evt( vars, ERR_017 )             # Exception!

        # VAR_NAME versorgen
        if add_iddoc != None or add_pdoc != None:
            vars_set( vars, VAR_NAME, id )

        # Befehls-Infos
        if add_iddoc != None and module != None:

            add_iddoc = run_5( module, MOD_ACTION_IDDOC, vars )
            add_iddoc = format_doc( add_iddoc, False )

        # Parameter-Infos
        if _GTYPES[ gtyp ][ _GTY_TYPAR ] != -1 and \
           add_pdoc != None and module != None:

            # Infos über Parameter anfordern ...
            infos = run_5( module, MOD_ACTION_PDOC, vars )  # Doku
            types = vars_get( vars, id, VIB_PGTYPES )       # Typen

            # Parameter durchlaufen
            if infos != None and types != None:

                i = 0

                for pi in infos:

                    # Infos für aktuellen Parameter ...
                    add_pdoc += format_doc( [infos,i],
                                            False,
                                            "[%d] " % ( i + 1 ),
                                            True ) + " "
                    # Index anpassen
                    i += 1

        # Info über ID
        if add_iddoc != None:   # ID-Info?
            item += " %s " % add_iddoc

        # Info über ID-Parameter
        if add_pdoc != None:   # Param-Info?
            item += " %s " % add_pdoc

        # Normale Variable
        if gtyp == GTYP_MULTI or module == None:

            value = vars_get( vars, id, VIB_VALUE )

            if type( value ) == str:
                if len( value ) > 30 or "\n" in value: # Sehr lang od. mit LF
                    item += "%s(%d)" % ( LOC.PYT_TYPE_STR, len( value ) )
                else:
                    item += "'" + value + "'"
            elif type( value ) == list:
                item += "%s(%d)" % ( LOC.PYT_TYPE_LIST, len( value ) )
            elif type( value ) == dict:
                item += "%s(%d)" % ( LOC.PYT_TYPE_DICT, len( value ) )
            else:
                item += str( value )

        # aufnehmen
        result.append( item )

        # Pos
        pos += 1

    # Module beenden
    if is_vars_new == True:         # Haben wir init veranlasst?
        vars_release( vars )        # Beenden

    # Spaltenformatierung
    if row == True:
        i = 0
        for item in result:
            pos = item.find( rowchar )
            if pos == -1:
                result[i] = item.rjust(maxlen+1)
            else:
                result[i] = item[:pos+1].rjust(maxlen+1) +\
                            item[pos:]
            i += 1

    # Laufende Nummer?
    if add_nr != None:
        i = 0
        for item in result:
            result[i] = add_nr % (i+1) + item
            i += 1

    # Ergebnis als str?
    if resultlist == False:
        result_new = ""
        for item in result:
            result_new = result_new + item + "\n"
        result = result_new

    return result



###############################################################################
# Sucht einen bestimmten Text.
#
# 'txt'          (str) Text, der gesucht werden soll.
# 'vars'         (vars) Var-Speicher
#
# Ergebnis       (int) -1 im Fehlerfall, ansonsten der Index in vars[VAR_ITEMS]
###############################################################################
def text_getidx( txt, vars ):

    i = 0

    # Unser (vorläufiges) Ergebnis
    result = -1

    for a in vars_get( vars, VAR_ITEMS ):

        if len( a ) > 2:    # 2 Zeichen sind schon die Anführungszeichen

            if a[ 1 : -1 ] == txt:

                result = i
                break

        i += 1

    return result



###############################################################################
# Erzeugt gic-Code zur Ausführung eines bestimmten Befehls.
# Nur für Befehle mit Standard-Trennzeichen "/" !
#
# 'cmd'       (str)        gic-Befehl
# 'paras'     (str-list)   Parameterliste [None]
# 'qm_pos'    (int-list)   Bestimmt, welche Parameter mit Anführungszeichen
#                          umschlossen werden sollen.
#                           'None' für keiner
#                           '[]' für alle
#                           ansonsten nur die aufgelisteten ( z.B. [0,2]: Der
#                           erste und der dritte Parameter sollen betroffen
#                           sein) [None]
#                  (int)   Gleichbedeutend mit [ int ]
#
# Ergebnis   (sgic) Code
###############################################################################
def code_create( cmd, paras = None, qm_pos = None ):

    result = cmd

    # ggf. int in int-list konvertieren
    if type( qm_pos ) == int:
        qm_pos = [ qm_pos ]

    # Parameter-Liste?
    if type( paras ) == list:

        # Parameter-Index
        i = 0

        # Alle Parameter durchlaufen
        for p in paras:

            # Passendes Trennzeichen ermitteln
            if i == 0:
                result += GIC_SPACE
            else:
                result += GIC_PARAM

            # Parameter aufnehmen
            if type( p ) == str or type( p ) == unicode:

                # Liste?
                if type( qm_pos ) != list:

                    result += p # unverändert

                else:

                    # Alle oder in Liste?
                    if qm_pos == [] or i in qm_pos:

                        # Zeichenkette soll mit Anführungszeichen umschlossen w.
                        result += GIC_QM + p + GIC_QM

                    else:

                        result += p # unverändert

            elif type( p ) == bool:

                if p:
                    result += LOC.TRUE
                else:
                    result += LOC.FALSE

            elif type( p ) == float:

                # Komma durch entsprechendes gic-Zeichen ersetzen
                result += str( p ).replace( ".", LOG.FLOAT_SEP )

            else:

                result += str( p )

            # Nächster Index
            i += 1


    # Ergebnis zurückliefern
    return result


###############################################################################
# Ersetzt in gic-Code eine bestimmte Variablenzuweisung. Falls noch keine
# existiert, wird am Code-Ende eine entsprechende Zuweisung generiert.
# Es wird nur die erste Zuweisung ersetzt!
#
# z.B. Wenn als 'varname' "Test" und als 'value' 0 angegeben wurde, wird im
# im Code 'SET Test=0;' generiert.
#
# 'code'        (str)  gic-Code
# 'varname'     (str)  Variablenname
# 'value'       (str)  gic-Code für Zuweisung, der als Wert eingetragen werden
#                      soll.
#                      Wird als Wert 'None' angegeben, wird die Variablen-
#                      zuweisung aus dem Code entfernt.
# 'vars'        (vars) Var-Speicher
#
# Ergebnis       (str) Der neue Code
###############################################################################
def code_gic_setvar( code, varname, value, vars ) :

    # Unser Ergebnis
    result = None

    # Ob ein entsprechender Variablenwert gefunden und ersetzt wurde
    replaced = False

    # Zuweisungsoperator
    ass_ope = GIC_ASS

    # Liste der Positionen aller SET-Befehle holen
    search  = LOC.CMD_SET + GIC_SPACE
    poslist = str_.find_ex( code,
                            search,
                            GIC_QM,
                            sfe_mode = str_.SFE_LIST )

    if len( poslist ) > 0:  # Position(en) gefunden?

        for p in poslist:   # Alle SET-Positionen durchgehen ...

            startpos = -1   # Ab hier soll ersetzt werden. (-1 = keine)
            endpos   = -1   # Bis hier soll ersetzt werden. (-1 = keine)

            posSET = p                  # Hier liegt SET
            p += len(search)            # Hinter SET

            # Varname an Position ermitteln ...
            vn = str_.collect( code[p:], VARS_CHARS_x + GIC_SPACE )
            searchpos = p + len( vn )   # Hinter dem Varnamen suchen
            vn = vn.strip()             # Leerzeichen vor. und hinten entfernen
            if varname == vn:           # gesuchter Name?

                if value != None:       # Neue Wertzuweisung?
                    pos = code.find( ass_ope, searchpos ) # Param.-Trenner
                    if pos != -1:               # Param.-Trenner gefunden?
                        pos += len( ass_ope )   # Hinter Trennzeichen
                        startpos = pos  # Ab hier beginnt Ersetzungsbereich
                    addendpos = 0       # Vor Trennzeichen
                else:                   # Eintrag entfernen ...
                    startpos = posSET   # Ersetzungsbereich beginnt bei "SET"
                    pos = searchpos     # Hinter Varnamen weitersuchen
                    addendpos = len( GIC_SEP ) # Trennzeichen einschliessen

                # Ende des Eintrags suchen
                endpos = str_.find_ex( code[ pos: ] + GIC_SEP,
                                       GIC_SEP,
                                       GIC_QM ) + pos

                # Wenn alle Positionen ermittelt wurden, kann jetzt die
                # Ersetzung erfolgen
                if startpos != -1 and endpos != -1:

                    # Endpos anpassen
                    endpos += addendpos

                    # Code vor der Fundstelle übernehmen
                    if startpos > 0:
                        result = code[ :startpos ]
                    else:
                        result = ""

                    # Code
                    if value != None:   # Neuen Wert eintragen?
                        result += value
                        replaced = True # Ersetzt

                    # Code hinter der Fundstelle übernehmen
                    if endpos < len( code ):
                        result += code[ endpos: ]
                    # Keine weitere Abarbeitung der Positionen
                    break


    # Wenn nichts gefunden. Gesamten Code als Ergebnis übernehmen
    if result == None:
        result = code

    # Leerzeichen vorne und hinten entfernen
    result = result.strip()

    # Nicht ersetzt? -> Zuweisungscode anhängen
    if replaced == False and value != None:

        # Wenn hinten noch kein Trennz. ?
        if len( result ) > 0 and result[-1] != GIC_SEP:

            result += GIC_SEP + GIC_LF      # Trennzeichen anhängen

        # Zuweisungsbefehl + Leerzeichen + Neuen Code
        result += LOC.CMD_SET + \
                  GIC_SPACE   + \
                  varname + ass_ope + value

    # Durch das Löschen konnten Leerzeilen entstanden sein -> entfernen
    if value == None:

        result = result.replace("\n\n","\n")

        # Sonderfall: Leerzeile in erster Zeile
        if len( result ) > 0 and result[ 0 ] == "\n":

            result = result[ 1: ]

    # Falls ein Trennzeichen am Ende steht -> entfernen
    if len( result ) > 0 and result[ -1 ] == GIC_SEP:
        result = result[ 0 : -1 ]

    # Ergebnis
    return result



###############################################################################
# Stellt sicher, dass alle wichtigen Variablen im angebenen vars
# vorhanden sind. Diese Funktion wird durch vars_new() autom.
# ausgeführt.
#
# 'VARS_INIT' enthält alle Schlüssel mit ihren Initalisierungswerten
#
# 'vars'        (vars) Var-Speicher, die initalisiert werden soll
#
# Ergebnis
###############################################################################
def vars_init( vars ) :

   # Alle Einträge von VARS_INIT durchlaufen ...
   for item in VARS_INIT:

        # (0) ID        (str)  Name
        # (1) Wert             Initalisierungswert der ID
        # (2) Flags     (int)  Flags
        # (3) Typ       (GTYP) (optional)
        # (4) Param1    (str)  (optional) Parameter 1

        id    = item[ 0 ]
        value = item[ 1 ]

        if id not in _vars( vars ):     # ID-Name noch nicht vergeben?

            # Typinfo ...
            if len( item ) > 3:         # Typinfo vorhanden?
                type = item[ 3 ]
            else:
                type = GTYP_MULTI

            # Neuen Eintrag anlegen
            vars_set( vars, id, value, VIB_VALUE, type ) # item ist der Wert

            # Ggf. Flags ...
            if item[ 2 ] != 0:
                vars_set( vars, id, item[ 2 ], VIB_FLAGS )  # Flags

            if len( item ) > 4:         # Param1 vorhanden?
                param1 = item[ 4 ]
                vars_set( vars, id, param1, VIB_PARA1 )

   # Suchpfade für Module ermitteln und in vars eintragen
   paths = []
   for p in FOLDER__MODULES: # Alle Unterordner der Module
      paths.append( path_get( p ) )
   # Suchpfade für Module in vars eintragen
   vars_set( vars, VAR_FOLDERS_M, paths )


###############################################################################
# Erzeugt ein neues vars. Nach der Verwendung sollte vars_release()
# ausgeführt werden
#
# 'initmodules'  (bool) True, wenn die std-Module geladen und initalisiert
#                       werden sollen.
#
# Ergebnis  Neues Objekt
###############################################################################
def vars_new( initmodules = False ):

    # Neues list-Objekt mit zwei dict-Objekten:
    # Muß kompatibel zu VARS_GLOB und VARS_LOC sein!
    vars = [ {}, {} ]

    # Lokale Variablen initalisieren
    vars_init_local( vars )

    # Globale Variablen initalisieren
    vars_init( vars )

    # Module initalisieren
    if initmodules:

        # Module laden
        modules_load( None, vars )

        # Module initalisieren
        modules_init( vars )

        # Initalisierungsereignis
        evt( vars, EVT_007 )

    # Neues Objekt zurückliefern
    return vars



###############################################################################
# Initalisiert lokale Variablen
#
# Ergebnis
###############################################################################
def vars_init_local( vars ):

    # Anz der Mitbenutzer initalisieren
    vars_set( vars, VAR_LOC_CNT, 0, VIB_VALUE, GTYP_INT, VARS_LOC )


###############################################################################
# Ein durch vars_new() erzeugtes Objekt wieder freigeben
#
# 'vars'    (vars) Var-Speicher
#
# Ergebnis
###############################################################################
def vars_release( vars ):

    # Ereignis: Ressourcen freigeben
    evt( vars, EVT_014 )

    # Ereignis: Ende
    evt( vars, EVT_008 )

    # Wurden Module initalisiert?
    if vars_in( vars, VAR_MODULES_INIT ):
        modules_exit( vars )


###############################################################################
# Wird immer ausgeführt, wenn ein neuer var-Eintrag angelegt werden soll
#
# 'vars'    (vars) Var-Speicher
# 'name'    (str)  Name des neuen Eintrags
# 'gtype'   (int)  Typ. z.B. GTYP_MULTI
#
# Ergebnis  Datenblock für den neuen Eintrag
################################################################################
def vars_var( vars, name, gtype ):

    # Überprüfen, ob gültiger Name vorliegt
    i = 0
    for c in name:                # Alle Buchstaben in 'name' durchgehen
        if i == 0:                # Erstes Zeichen
            if c not in VARS_CHARS_1:           # Kein zulässige Zeichen?
                evt( vars, ERR_036, [name,c] )  # Abbruch
        else:
            if c not in VARS_CHARS_x:           # Kein zulässige Zeichen?
                evt( vars, ERR_036, [name,c] )  # Abbruch
        i += 1                                  # Nächster Index

    # Leeren Info-Block erzeugen
    vb = [ None ] * VIB__SIZE

    # Info-Block initalisieren
    vb[ VIB_GTYP ]  = gtype                          # Typ
    vb[ VIB_VALUE ] = _GTYPES[ gtype ][ _GTY_VALUE ] # Vorgabewert
    vb[ VIB_PARA1 ] = _GTYPES[ gtype ][ _GTY_PARA1 ] # Vorgabewert
    vb[ VIB_PARA2 ] = _GTYPES[ gtype ][ _GTY_PARA2 ] # Vorgabewert
    vb[ VIB_FLAGS ] = 0                              # Keine Flags

    # Laufende Nr vergeben
    if vars_in( vars, VAR_ID ):   # Lauf-Var da?
        vb[ VIB_ID ] = vars_get( vars, VAR_ID ) + 1
    else:
        vb[ VIB_ID ] = 0          # Erste Nr

    # Info-Block zurückliefern
    return vb



###############################################################################
# Zugriff auf eine der Hauptlisten eines 'vars'
#
# 'vars'    (vars) Var-Speicher
# 'space'   (int)  Namensraum [VARS_GLOB]
#                  VARS_GLOB: dict der globalen Variablen
#                  VARS_LOC:  dict der lokalen Variablen
#
# Ergebnis  Je nach 'space'
###############################################################################
def _vars( vars, space = VARS_GLOB ):

    return vars[ space ]



###############################################################################
# 'vars'-Daten eintragen
#
# 'vars'    (vars) Var-Speicher
# 'space'   (int)  Namensraum
#                  VARS_GLOB: dict der globalen Variablen
#                  VARS_LOC:  dict der lokalen Variablen
# 'value'   (dict) Neues Objekt
#
# Ergebnis  Je nach 'space'
###############################################################################
def _vars_set( vars, space, value ):

    vars[ space ] = value



###############################################################################
# Vermindert einen Eintrag vom Typ int
# Wenn ein entsprechender Name nicht existiert, entsteht eine 'Exception'.
#
# 'vars'    (vars) Var-Speicher
# 'name'    (str)  Betroffener Eintrag
# 'space'   (int)  Namensraum, [VARS_GLOB]
#
# Ergebnis
###############################################################################
def vars_dec( vars, name, space = VARS_GLOB ):

    _vars( vars, space )[ name ][ VIB_VALUE ] -= 1



###############################################################################
# Eintrag aus vars entfernen
# Wenn ein entsprechender Name nicht existiert, entsteht eine 'Exception'.
# Ebenso, wenn es sich um einen statischen Eintrag handelt.
#
# 'vars'    (vars) Var-Speicher
# 'name'    (str)  Betroffener Eintrag
# 'space'   (int)  Namensraum, [VARS_GLOB]
#
# Ergebnis
###############################################################################
def vars_del( vars, name, space = VARS_GLOB ):

    # Flags holen
    flags = vars_get( vars, name, VIB_FLAGS, space )

    # Eintrag entfernen, wenn Eintrag nicht statisch ...
    if flags & VF_STA == VF_STA:
        # Ereignis: Eintrag kann nicht gelöscht werden
        evt( vars, ERR_015, [ name ] )
    else:
        # Eintrag löschen
        del _vars( vars, space )[ name ]



###############################################################################
# Sucht die angegebene Var in den lokalen und globalen Variablen
#
# 'vars'    (vars) Var-Speicher
# 'name'    (str)  Varname
#
# Ergebnis VARS_LOC     Varname ist eine lokale Variable
#          VARS_GLOB    Varname ist eine globale Variable
#          VARS__UNDEF (wenn nicht gefunden)
################################################################################
def vars_find( vars, name ):

    if vars_in( vars, name, VARS_LOC ):    # Im lokalem Bereich?
        result = VARS_LOC                  # Gefunden im lokalen Bereich

    elif vars_in( vars, name, VARS_GLOB ): # Im globalen Bereich?
        result = VARS_GLOB                 # Gefunden im globalen Bereich

    else:
        result = VARS__UNDEF               # Nicht gefunden

    return result



###############################################################################
# Lesezugriff auf 'vars'.
# Wenn ein entsprechender Name nicht existiert, entsteht eine 'Exception'.
#
# 'vars'    (vars) Var-Speicher
# 'name'    (str)  Betroffener Eintrag
# 'vib'     (int)  Betroffener Înfo-Block-Wert. Siehe VIB_.... (z.B. VIB_VALUE)
# 'space'   (int)  Namensraum [VARS_GLOB]
# 'default' (?)    Wenn hier ein Wert ungleich 'None' angegeben wird, wird
#                  keine 'Exception' für einen fehlenden Wert ausgelöst,
#                  sondern stattdessen dieser Wert zurückgeliefert. [None]
#
# Ergebnis:  Wert
###############################################################################
def vars_get( vars, name, vib = VIB_VALUE, space = VARS_GLOB, default = None ):

    # Behandlung eines angegebenen Vorgabewerts
    if default != None:     # Vorgabewert?
        if not vars_in( vars, name, space ):
            return default

    # Info-Block holen
    vb = _vars( vars, space )[ name ]

    # Je nach Typ behandeln
    if vib == VIB_VALUE_DI:              # Direkter Wert?

        result = vb[ VIB_VALUE ]

    elif vb[ VIB_GTYP ] == GTYP_MODVAR:  # Modulgesteuerte Var?

        # Bestimmte Infos müssen vom Modul ermittelt werden...
        action = None
        if vib == VIB_VALUE:             # Wert?

            action = MOD_ACTION_VAR_G   # Modulbotschaft festlegen

        if action != None:              # Modulbotschaft?

            # Wert von Modul anfordern
            result = run_6( vb[ VIB_MODULE ], action, vars, name )

            # Test, ob Modul Werte geliefert hat
            if result == None:
                evt( vars, ERR_028, [ name, vib ] )

        else:
            result = vb[ vib ]          # Std

    # Wert eines GTYP_LIST-Eintrags wird abgefragt?
    elif vib == VIB_VALUE and vb[ VIB_GTYP ] == GTYP_LIST:

        # Aktuellen Listenindex holen
        idx = vb[ VIB_LIST_POS ]

        # Anz der Listeneinträge
        cnt = len( vb[ VIB_VALUE ] )

        # Auf ungültigen Index überprüfen
        if idx < 0 or idx >= cnt:

            # Exception!
            evt( vars, ERR_038, [ name, idx, cnt ] )

        # Wert an der aktuellen Position liefern
        result = vb[ VIB_VALUE ][ idx ]

        # ggf. autom. Index auf nächste Position
        if vb[ VIB_LIST_MODE ] == True:

            if idx < cnt - 1:               # Noch nicht letzter Index?

                # Auf nächsten Index
                vb[ VIB_LIST_POS ] += 1

            else:                           # Letzter Index erreicht ...
                # Nicht (mehr) möglich
                vb[ VIB_LIST_POS ] = -1

                # VAR_LIST_EOL versorgen
                if not vars_in( vars, VAR_LIST_EOL ): # Nicht vorhanden?

                    # Neue VAR_LIST_EOL anlegen
                    vars_set( vars,
                              VAR_LIST_EOL,
                              True,
                              VIB_VALUE,
                              GTYP_BOOL )


    else:        # Keine modulverwalteter Eintrag ...

        # Keine Sonderbehandlung / Normaler Wert ...
        result = vb[ vib ]

    # Behandlung von Transformierungen
    if vb[ VIB_TIB ] != None:      # Transf. vorhanden?

        result = transform( vb[ VIB_TIB ],
                            name,
                            result,
                            vb[ VIB_GTYP ],
                            False,
                            vars )


    # Rückgabewert
    return result



###############################################################################
# Lesezugriff auf 'vars'.
# Wenn ein entsprechender Name nicht existiert, entsteht keine 'Exception',
# sondern es wird 'None' zurückgeliefert.
#
# 'vars'    (vars) Var-Speicher
# 'name'    (str)  Betroffener Eintrag
# 'vib'     (int)  Betroffener Înfo-Block-Wert. Siehe VIB_.... (z.B. VIB_VALUE)
# 'space'   (int)  Namensraum [VARS_GLOB]
#
# Ergebnis:  Wert
###############################################################################
def vars_get_none( vars, name, vib = VIB_VALUE, space = VARS_GLOB ):

    if vars_in( vars, name ):             # Da
        result = vars_get( vars, name, vib, space )
    else:
        result = None

    return result


###############################################################################
# Fragt einen Signal-Eintrag (Typischerweise GTYP_SIGNAL) ab. Wenn vorhanden,
# wird sein Wert zurückgeliefert und autom. entfernt. Wenn kein entsprechender
# Eintrag existierte, wird 'None' zurückgeliefert.
#
# 'vars'    (vars) Var-Speicher
# 'name'    (str)  Signalname
# 'space'   (int)  Namensraum [VARS_GLOB]
#
# Ergebnis  Ungleich 'None', wenn Signal vorhanden war.
###############################################################################
def vars_get_signal( vars, name, space = VARS_GLOB ):

    # Signal da?
    if vars_in( vars, name, space ):        # Ja

        result = vars_get( vars, name, VIB_VALUE, space )  # Signalwert holen

        vars_del( vars, name, space )       # Signal entfernen

        if result == None:                  # Wenn Signal keinen Wert hatte
            result = True                   # Irgendeinen Wert

    else:
        result = None                       # Kein Signal

    return result                           # Ergebnis


###############################################################################
# Überprüfen, ob eine bestimmte Variable existiert
#
# 'vars'    (vars) Var-Speicher
# 'name'    (str) Betroffener Eintrag
# 'space'   (int) Namensraum, [VARS_GLOB]
#
# Ergebnis  True, wenn ein Eintrag mit diesem Namen existiert
###############################################################################
def vars_in( vars, name, space = VARS_GLOB ):

    # Ob Name im entsprechenden dict vorhanden ist
    return name in _vars( vars, space )


###############################################################################
# Überprüfen, ob eine bestimmte Variable existiert, wenn ja, wird sie entfernt
#
# 'vars'    (vars) Var-Speicher
# 'name'    (str) Betroffener Eintrag
# 'space'   (int) Namensraum, [VARS_GLOB]
#
# Ergebnis  True, wenn ein Eintrag mit diesem Namen existierte
###############################################################################
def vars_in_del( vars, name, space = VARS_GLOB ):

    if name in _vars( vars, space ):    # da?

        vars_del( vars, name, space )   # Entfernen

        result = True
    else:
        result = False

    # Ergebnis
    return result


###############################################################################
# Inkrementiert einen Eintrag vom Typ int
# Wenn ein entsprechender Name nicht existiert, entsteht eine 'Exception'.
#
# 'vars'    (vars) Var-Speicher
# 'name'    (str)  Betroffener Eintrag
# 'inc'     (int)  Um welchen Wert inkrementiert werden soll [1]
# 'space'   (int)  Namensraum [VARS_GLOB]
# 'fast'    (bool) Ob schneller Zugriff erfolgen soll. [True]
#                  Achtung: Wenn ja, werden keine Transformationen o.ä. aus-
#                           geführt!!!
#
# Ergebnis  Der neue Wert
###############################################################################
def vars_inc( vars, name, inc = 1, space = VARS_GLOB, fast = True ):

    if fast:

        # Aktueller Wert + 1
        result = _vars( vars, space )[ name ][ VIB_VALUE ] + inc

        # Zuweisen
        _vars( vars, space )[ name ][ VIB_VALUE ] = result

    else:

        # Aktueller Wert + 1
        result = vars_get( vars, name ) + inc

        # Zuweisen
        vars_get( vars, name, result )

    # Erg
    return result



###############################################################################
# Weist einem Eintrag einen (neuen) Wert zu
#
# 'vars'    (vars) Var-Speicher
# 'name'    (str)  Betroffener Eintrag
# 'value'   (?)    Wert
# 'vib'     (int)  Betroffener Înfo-Block-Wert. Siehe VIB_.... [VIB_VALUE]
# 'gtype'   (int)  Initalisierungstyp [GTYP_MULTI]
# 'space'   (int)  Namensraum, [VARS_GLOB ]
#
# Ergebnis
################################################################################
def vars_set( vars,
              name,
              value,
              vib   = VIB_VALUE,
              gtype = GTYP_MULTI,
              space = VARS_GLOB ):


    # Info-Block ('vb') für Var mit 'name' holen
    if name not in _vars( vars, space ):            # Var noch nicht vorhanden?

        # Info-Block für neuen Eintrag anfordern
        vb = vars_var( vars, name, gtype )

        # Info-Block für neuen Eintrag eintragen
        _vars( vars, space )[ name ] = vb

    else:                                       # Var ist vorhanden ...

        # vorhandenen Info-Block übernehmen
        vb = _vars( vars, space )[ name ]

    # Behandlung statischer Einträge / Wertzuweisung nicht möglich
    if vb[ VIB_FLAGS ] & VF_STA == VF_STA:
        evt( vars, ERR_024, name )

    # Ggf. Behandlung von Transformierungen
    if vb[ VIB_TIB ] != None:         # Transf. vorhanden?

        value = transform( vb[ VIB_TIB ],
                           name,
                           value,
                           vb[ VIB_GTYP ],
                           True,
                           vars )

    # Ist eine Variable betroffen, die von einem Modul behandelt wird?
    if vib == VIB_VALUE and vb[ VIB_GTYP ] == GTYP_MODVAR:

        # Namen der Var eintragen
        vars_set( vars, VAR_NAME, name )         # Name in vars

        # Neuer Wert der Var eintragen
        vars_set( vars, VAR_PARAM, [value] )    # Wert in vars

        # Behandlung durch Modul
        run_5( vb[ VIB_MODULE ], MOD_ACTION_VAR_S, vars )

    else:   # Keine modulverwaltete Var ...

        # Direktzuweisung behandeln
        if vib == VIB_VALUE_DI:

            vib = VIB_VALUE

        # Normale Zuweisung
        _vars( vars, space )[ name ][ vib ] = value


###############################################################################
# Weist einem Listen-Eintrag einen (neuen) Wert zu
#
# 'vars'    (vars) Var-Speicher
# 'name'    (str)  Betroffener Eintrag
# 'idx'     (int)  Listen-Index
# 'value'   (?)    Wert
# 'gtype'   (int)  Initalisierungstyp [GTYP_MULTI]
# 'space'   (int)  Namensraum, [VARS_GLOB]
# 'inc'    (bool) True, wenn der angegebene Wert auf den Eintrag inkrementiert
#                 werden soll [False]
#
# Ergebnis
################################################################################
def vars_set_list(  vars,
                    name,
                    idx,
                    value,
                    space = VARS_GLOB,
                    inc = False ):

        var = vars_get ( vars, name, VIB_VALUE, space )

        if inc:
            var[ idx ] += value
        else:
            var[ idx ] = value

        vars_set ( vars, name, var )


################################################################################
# Überprüft, ob die Struktur des angegebenen 'vars' stimmt
#
# Im Fehlerfall wird eine 'Exception' ausgelöst
################################################################################
def verify( vars ):

    # Typ ...
    if vars == None or type( vars ) != VARS_TYPE:
        evt( vars, ERR_022, [ 0, "vars" ] )

    # Keine Einträge ...
    if len( vars ) < 1:
        evt( vars, ERR_022, [ 1, "?" ] )

    # Alle gloablen IDs durchlaufen ...
    i = 0
    for id, block in _vars( vars ).items():

        # Ob ID fehlt ...
        if id == None or len( id ) < 1:
            evt( vars, ERR_022, [ 2, "id", id] )

        # Falsche Blockgröße ...
        if len( block ) != VIB__SIZE:
            evt( vars, ERR_022, [ 3, i, id, len( block ), VIB__SIZE ] )

        i += 1


###############################################################################
# Zentrale Ausführungsfunktion für gic-Code
#
# 'sgic'        (str)   gic-Zeichenkette
# 'vars'        (vars)  Var-Speicher
#                       Wenn 'None', wird eine neues vars erzeugt
# 'local'       (bool)  True, um bei einem rekursiven Aufruf neue lokale
#                       Variablen zu erzeugen. Beim ersten Aufruf werden
#                       sie immer erzeugt [False]
#
# Werden Gimp-Befehle ausgeführt, müssen folgende var-Werte versorgt sein ...
# VAR_RUN_PARA[ MOD.VAR_RUN_PARA_IMG ] (gimp) Bild
# VAR_RUN_PARA[ MOD.VAR_RUN_PARA_DRW ] (gimp) Aktive Ebene, Kanal oder Ä.
#
# Ergebnis      Letzter Rückgabewert
###############################################################################
def run( sgic, vars, local = False ) :

   # Unser Funktionsergebnis
   result = None

   # 'vars'
   if vars != None:                 # vars-da?
      if type(vars) is not list:    # Falscher vars-Typ?
          evt( vars, ERR_002 )      # Exception!
      is_vars_new = False           # Unten soll nicht vars_release() erfolgen
   else:                            # Keine 'vars' angegeben ...
      vars = vars_new()             # Neue 'vars' anlegen
      is_vars_new = True            # Unten soll vars_release() erfolgen

   # gic überprüfen
   if type( sgic ) != str or len( sgic ) < 1:   # Keine gic-Befehle vorhanden?
      evt( vars, ERR_003 )                      # Exception!

   # Anpassungen
   vars_inc( vars, VAR_RUN_CNT )                # Anz der run()-Aufrufe anpassen
   pos_bak = vars_get( vars, VAR_POS )          # Akt. Ausführungspos merken

   # Rekursions-Behandlung
   lvl = vars_get( vars, VAR_RUN_LVL ) + 1      # Rekursionsebene anpassen
   vars_set( vars, VAR_RUN_LVL, lvl )           # Rekursionsebene

   # Erster run() - Aufruf / Nicht in Rekursion ?
   if lvl == 1:

       # Falls noch ein Stop-Befehl existiert: entfernen
       if vars_in( vars, VAR_STOP ):        # Stop?
          vars_del( vars, VAR_STOP )        # Stop entfernen

       # Kopie der run-Parameter anlegen
       vars_set( vars, VAR_RUN_PARA_BAK, vars_get( vars, VAR_RUN_PARA ) )

       # Module ...
       modinit = False                      # Ob wir Module initalisiert haben
       if not vars_in( vars, VAR_MODULES ) or \
        vars_get( vars, VAR_MODULES ) == None:
           modules_load( None, vars )       # Module laden

       if vars_get( vars, VAR_MODULES_INIT ) == False:  # Module n. nicht init.?
           modules_init( vars )             # Module initalisieren
           modinit = True                   # Unten wieder terminieren

       # Ereignis: Oberste Ebene gestartet
       evt( vars, EVT_001, sgic )

       # Wenn 'autoexec' Code enthält, übernehmen wir ihn
       if vars_in( vars, VAR_AUTOEXEC ) == True:

           # Autoexec
           evt( vars, EVT_003 )

           sgic = vars_get( vars, VAR_AUTOEXEC ) + GIC_SEP + sgic# Code zufügen
           vars_del( vars, VAR_AUTOEXEC )                        # entfernen

   else: # Wir sind in einer Rekursion ...

        # Ein Modul ruft uns während Init-Phase auf?
        if vars_get( vars, VAR_MODULES_INIT ) == False:
            evt( vars, ERR_004 )                  # Exception!

        # Behandlung lokaler Variablen
        if local:                                 # Neue anlegen?

            # Die lokalen Variablen retten
            local_vars_save = _vars( vars, VARS_LOC )

            # Die lokalen Variablen zurücksetzen
            _vars_set( vars, VARS_LOC, {} )

            # Lokale Variablen initalisieren
            vars_init_local( vars )

            # Verweis auf alte lokale Vars in den neuen lokalen Vars speichern
            vars_set( vars,
                      VAR_LOC_PREV,
                      local_vars_save,
                      VIB_VALUE,
                      GTYP_MULTI,
                      VARS_LOC )

        else:         # Keine neuen lokalen Vars anlegen ...

            # Anz der Mitbenutzer erhöhen
            vars_inc( vars, VAR_LOC_CNT, 1, VARS_LOC )

        # Ereignis: Rekursion wird ausgeführt
        evt( vars, EVT_024, [ lvl, sgic ] )

   # 'qic'-Zeichenkette in Einträge aufsplitten
   cmds = run_5_para( LOC.CMD_PARSE_1, [ sgic ] , vars )

   # Kein Modul hat aufgesplittet?
   if cmds == None or len( cmds ) == 0:
       evt( vars, ERR_005 )                  # Exception!

   # Schon vorhandene Einträge holen
   items = vars_get( vars, VAR_ITEMS )

   # Aktuelle Ausführungsposition anpassen
   pos_new = len( items )                   # Akt. Anzahl
   vars_set( vars, VAR_POS, pos_new )

   # Befehlsauflistung behandeln ...
   if len( items ) == 0:                    # Keine vorhanden?
       items = cmds
   else:                                    # Vorhanden ...
       items.extend( cmds )                 # erweitern

   try:
        # Alle Einträge in VAR_ITEMS abarbeiten ...
        result = run_1( vars, items )

   # Fehler bei Abarbeitung  ...
   except Exception as e:

        # Abbruchsignal
        vars_set( vars,
                  VAR_STOP,
                  "run() " + str( e ),
                  VIB_VALUE,
                  GTYP_SIGNAL )

   # Erster run() - Aufruf / Nicht in Rekursion ...
   if lvl == 1:

       # Ggf. Ereignis über vorzeitigen Programmabbruch
       if vars_in( vars, VAR_STOP ): # Stop?

           # Stop-Signal holen und entfernen
           stop_cmd = vars_get_signal( vars, VAR_STOP )

           # Wenn Stop-Signal nicht über STOP kam
           if type( stop_cmd ) != list or stop_cmd[ 0 ] != LOC.CMD_STOP:

               evt( vars, EVT_021, stop_cmd )

       # ggf. Module terminieren
       if modinit == True:                  # Haben wir Module initalisiert?
           modules_exit( vars )             # Module terminieren

       # Ereignis: Oberste Ebene beendet
       evt( vars, EVT_002 )

   else:# Wir sind in einer Rekursion ...

        # Noch Benutzer der lokalen Vars?
        if vars_get( vars, VAR_LOC_CNT, VIB_VALUE, VARS_LOC ) > 0:

            # Anz vermindern
            vars_dec( vars, VAR_LOC_CNT, VARS_LOC )

        else:   # Keine Benutzer mehr ...

            # Alte Vars holen
            local_vars_save = vars_get( vars,
                                        VAR_LOC_PREV,
                                        VIB_VALUE,
                                        VARS_LOC )

            # Die alten lokalen Variablen wiederherstellen
            _vars_set( vars, VARS_LOC, local_vars_save )

        # Ereignis: Rekursion wird beendet
        evt( vars, EVT_025, lvl )


   # Rekursion behandeln
   if len( cmds ) > 0:                        # Zugefügte Einträge?
        del vars_get( vars, VAR_ITEMS )[ -len(cmds) : ] # ... wieder entfernen

   vars_set( vars, VAR_RUN_LVL, lvl - 1 )   # Ebene runter
   vars_set( vars, VAR_POS, pos_bak )       # Alte Pos herstellen

   # vars_release()
   if is_vars_new == True:
       vars_release( vars )

   # Ergebnis
   return result



###############################################################################
# Vom Anwender eingegebenen gic-Code zur Ausführung bringen
#
# 'sgic'        (sgic)  gic
# 'param'       (?)     [0] Gimp-Image
#                       [1] Gimp-Drawable
# 'vars'        (vars)  Var-Speicher
#                       Für eine Übersicht siehe oben unter VAR_...    [None]
# 'restore'     (bool)  Bestimmt, ob die Originalparameter im 'vars' nach
#                       Durchführung wiederhergestellt werden soll. [False]
#
# Ergebnis      Letzter Rückgabewert
###############################################################################
def run_direct( sgic, param = None, vars = None, restore = False ) :

   # Abbruch, wenn geschütztes Zeichen verwendet wurde
   if sgic != None and sgic.find( GIC_PROT ) != -1:
        return None

   # Unser Rückgabewert
   result = None

   # vars
   if vars == None:            # Keine Var-Speicher als Parameter angegeben?

      is_vars_new = True
      vars = vars_new( True )  # Neues vars

   else:

      is_vars_new = False

   # Ereignis: Benutzereingabe ausführen
   evt_result = evt( vars, EVT_034, sgic )

   # Ereignis-Ergebnis auswerten
   if type( evt_result ) == str:
       sgic = evt_result    # Als Code übernehmen

   # Code vorhanden?
   if sgic != None and len( sgic ) > 0:

       # Ausführung starten
       if param == None:
           result = run( sgic, vars, False )
       else:
           result = run_param( sgic, param, vars, restore, False )

       # Ereignis: Benutzereingabe ausgeführt
       evt( vars, EVT_035, sgic )

   # vars
   if is_vars_new == True:
       vars_release( vars )

   # Unser Rückgabewert
   return result


###############################################################################
# Ein Funktion ausführen.
#
# 'fctname'     (sgic)  Funktionsname
# 'fctparam'    (sgic)  Funktionsparameter (also das zwischen den Klammern
#                                           Eingeschlossene )
# 'vars'        (vars)  Var-Speicher
# 'cib'         (CIB)   In welchem Kontext aufgerufen wird [None]
#
# Ergebnis      Rückgabewert (?)
#               Rückgabetyp  (GTYP)
###############################################################################
def run_fct( fctname, fctparam, vars, cib = None ) :

    # Std-Typ
    result_type = GTYP_MULTI

    # Funktion mit seinen Parametern zusammenstellen
    sgic_fct = fctname + GIC_SPACE + fctparam

    result = run_2( sgic_fct, vars, 0 )[ CIB_RESULT ]

    # Ergebnis
    return result, result_type


###############################################################################
# Die in der angegebenen Zeichenkette vorhandenen Kommandos sollen ab-
# gearbeitet werden.
#
# 'sgic'        (sgic)  gic-Code
# 'param'       (?)     [0] Gimp-Image
#                       [1] Gimp-Drawable
# 'vars'        (vars)  Var-Speicher
#                       Für eine Übersicht siehe oben unter VAR_... [None]
# 'restore'     (bool)  Bestimmt, ob die Originalparameter im 'vars' nach
#                       Durchführung wieder hergestellt werden soll. [False]
# 'local'       (bool)  True, um bei einem rekursiven Aufruf neue lokale
#                       Variablen zu erzeugen. Beim ersten Aufruf werden
#                       sie immer erzeugt [False]
#
# Ergebnis      Letzter Rückgabewert
###############################################################################
def run_param( sgic, param, vars = None, restore = False, local = False ) :

   # Unser Rückgabewert
   result = None

   if vars == None:            # Keine Var-Speicher als Parameter angegeben?

      # Neues vars
      vars = vars_new( True )

      # Wir haben ein neues 'vars' angelegt
      is_vars_new = True

   else:

      # Wir haben kein neues 'vars' angelegt
      is_vars_new = False

      # Ggf. Parameter sichern
      if restore:
          param_bak = vars_get( vars, VAR_RUN_PARA )

   # Parameter in vars übernehmen
   vars_set( vars, VAR_RUN_PARA, param )

   # Ausführung starten
   result = run( sgic, vars, local )

   # vars
   if is_vars_new == True:

       # Das 'vars', das wir oben angelegt hatten, wieder entfernen
       vars_release( vars )

   elif restore == True:

       # Alte Parameter-Werte herstellen
       vars_set( vars, VAR_RUN_PARA, param_bak )

   return result



###############################################################################
# Führt das angegebene Skript im Standardverzeichnis aus
#
# 'script'      (str)   Skriptname (ohne Dateierweiterung)
# 'path'        (str)   Verzeichnis (mit abschliessenden Trennzeichen).
#                       Wenn nicht angegeben, wird das Standard-Verzeichnis für
#                       Skripte verwendet [None]
# 'vars'        (vars)  Var-Speicher
#                       Wenn 'None', wird eine neues vars erzeugt [None]
# 'local'       (bool)  True, um bei einem rekursiven Aufruf neue lokale
#                       Variablen zu erzeugen. Beim ersten Aufruf werden
#                       sie immer erzeugt [False]
# 'param'       (?)     [0] Gimp-Image
#                       [1] Gimp-Drawable
# 'restore'     (bool)  Bestimmt, ob die Originalparameter im 'vars' nach
#                       Durchführung wiederhergestellt werden soll. [False]
#
# Ergebnis      Letzter Rückgabewert
###############################################################################
def run_script( script,
                path    = None,
                vars    = None,
                local   = True,
                param   = None,
                restore = False ) :

    # Unser Ergebnis
    result = None

    # Dateiname des Skripts
    filename = None

    # Kein Pfad angegeben? (In Standardordner suchen)
    if path == None:

        # Dateiname
        filename = path_get( FOLDER_SCRIPTS ) + script + "." + MOD_EXT_GIC

    else:   # Pfad angegeben ...

        # Dateiname
        filename = path + script + "." + MOD_EXT_GIC

        # Dateiname nicht vorhanden?
        if not fil_.isfile( filename ):
            filename = None


    # Wenn ein Dateiname vorhanden ist
    if filename != None:

        # gic-Datei laden
        sgic, state = file_read( filename, None, None, vars )

        # Geladen?
        if sgic != None:

            # Je nachdem, ob Parameter angegeben wurde
            if param == None:
                result = run( sgic, vars, local )   # Ausführen
            else:
                result = run_param( sgic, param, vars, restore, local ) # Ausf.

    # Unser Ergebnis
    return result



###############################################################################
# Die angegebenen Einträge sollen abgearbeitet werden.
#
# 'vars'   (vars) Var-Speicher
# 'items'  (list) Einträge, die abgearbeitet werden sollen
# 'pos'    (int)  Ausführungsposition in 'items'
#                 Wenn -1, wird sie aus VAR_POS aufgenommen
#
#
# Ergebnis      Letzter Rückgabewert
###############################################################################
def run_1( vars, items, pos = -1 ):

   # Unser Funktionsergebnis
   result = None

   # Ggf. schon vorhandene VAR_ITEMS sichern ...
   if vars_in( vars, VAR_ITEMS ):

       items_bak = vars_get_none( vars, VAR_ITEMS )
       pos_bak   = vars_get( vars, VAR_POS, VIB_VALUE, VARS_GLOB, 0 )

   else:

       items_bak = None
       pos_bak   = 0

   # Neue Liste eintragen
   vars_set( vars, VAR_ITEMS, items )

   # Aktuelle Position holen
   if pos == -1:
       pos = vars_get( vars, VAR_POS )
   else:
       vars_set( vars, VAR_POS, pos )

   # Alle Einträge in VAR_ITEMS abarbeiten ...
   while pos < len( items ):  # Noch nicht letzter?

      # Aktuellen Eintrag holen
      item = items[ pos ]

      # Eintrag zerlegen und weiterbehandeln ...
      cib = run_2( item, vars, pos, items )

      result = cib[ CIB_RESULT ]

      # Aktuelle Position neu einlesen, denn sie könnte inzwischen
      # geändert worden sein
      pos_new = vars_get( vars, VAR_POS )

      # Alte Position durch run_3() Geändert? (z.B. GEH-Befehl)
      if pos != pos_new:
          #d( vars, [ "Pos changed", pos, pos_new ] )
          pos = pos_new

      # Pos überprüfen ..
      if pos >= len( items ):

          # Ereignis: Ungültige Zellen-Pos
          evt( vars, ERR_034 )

      # Schleifenauftrag?
      if vars_in( vars, VAR_LOOP, VARS_LOC ): # Ja

          # Loop-Daten holen und entfernen
          lib = vars_get_signal( vars, VAR_LOOP, VARS_LOC )

          # LIB ergänzen ...
          lib[ LIB_CMD ]   = item    # Startenden Eintrag vermerken
          lib[ LIB_LOOPS ] = 0       # Anz der Durchläufe

          if lib[ LIB_POS ] == None: # Nicht angegeben?
             lib[ LIB_POS   ] = pos  # Auf akt. Position

          # Ggf. Schleifeneinträge ausführen ...
          if lib[ LIB_RUN ]:  # Ja?

              # Ereignis: Schleife gestartet
              evt( vars, EVT_028, [ lib, lib[ LIB_CMD ] ] )

              # Anz der Schleifeneinträge, die von der Schleife betroffen
              # sein sollen
              cnt = lib[ LIB_SIZE ]

              # Anz / Pos überprüfen ..
              if lib[ LIB_POS ] + cnt >= len( items ):

                  # Ereignis: Ungültige Anz. an Schleifeneinträge
                  evt( vars, ERR_035, [ items,
                                        len( items ),
                                        lib[ LIB_POS ],
                                        cnt ] )

              # Schleifenausführung
              result = run_1_loop( vars, cib, lib )

              # Ereignis: Schleife beendet
              evt( vars, EVT_030, [ lib, lib[ LIB_CMD ], lib[ LIB_LOOPS ] ] )

          # Ausführung hinter der Schleife fortsetzen
          pos = lib[ LIB_POS ] + lib[ LIB_SIZE ]

      # Auf Stop-Befehl testen
      if vars_in( vars, VAR_STOP ):                      # Stop?

          # while abbrechen
          break

      else:                              # Kein Stop ...

          # Auf nächsten Eintrag / Befehl
          pos += 1
          vars_set( vars, VAR_POS, pos )

   # Ggf. ursprüngliche Auflistung wiederherstellen
   if items_bak != None:
       vars_set( vars, VAR_ITEMS, items_bak )
       vars_set( vars, VAR_POS,   pos_bak   )

   # Ergebnis
   return result





###############################################################################
# run_1() hat einen Schleifenbefehl an der aktuellen Position erkannt und
# möchte die betroffenen Einträge ausführen.
#
# 'vars'     (vars)      Var-Speicher
# 'id'       (str)       Aktuelle ID / Schleifenbefehl
# 'param'    (list)      Parameter des Schleifenbefehls
# 'cib'      (CIB)       Aktuelle Zelleninfos
# 'lib'      (list)      LIB
#
# Ergebnis      Letzter Rückgabewert
###############################################################################
def run_1_loop( vars, cib, lib ):

   # Unser Funktionsergebnis
   result = None

   # Die Schleifeneinträge
   items = cib[ CIB_ITEMS ]

   # Die Schleifeneinträge
   items_new = []

   # Anz der betroffenen Einträge aus VAR_LOOP nehmen ...
   cnt = lib[ LIB_SIZE ]

   # Aktuelle Position / Pos des Schleifenbefehls
   pos = lib[ LIB_POS ]

   # Schleifeneinträge 'items_new' zusammenstellen
   i = 0                                  # Index
   while i < cnt:

       # Index
       i += 1

       # Eintrag übernehmen
       items_new.append( items[ pos + i ] )

   # Schleife (weiter) ausführen
   loop = True

   # Schleife. In einem Durchlauf werden alle Schleifenbefehle ausgeführt
   while loop:

       # Rekursion / Alle Befehle 'items_new' ausführen ...
       result = run_1( vars, items_new, 0 )

       # Schleifendaten 'lib' wiederherstellen, damit Schleifenbefehl
       # sie ggf. auswerten kann ...
       vars_set( vars,
                 VAR_LOOP,
                 lib,
                 VIB_VALUE,
                 GTYP_MULTI,
                 VARS_LOC )

       # Jetzt den Schleifenbefehl mit seinem Namen und seinen
       # Parameter herstellen ...
       vars_set( vars, VAR_PARAM_STR, cib[ CIB_PARAM ] )
       vars_set( vars, VAR_NAME,      cib[ CIB_NAME ]  )

       # Anz der Ausführungen anpassen
       lib[ LIB_LOOPS ] += 1

       # Schleifenbefehl ausführen ...
       result = run_3( cib )

       # Antwort holen
       lib = vars_get_signal( vars, VAR_LOOP, VARS_LOC )

       # Will keinen weitere Durchführung?
       if not lib[ LIB_RUN ]: # Ja?

          # Schleife abbrechen
          loop = False

   # Alte Schleifeneinträge wiederherstellen
   vars_set( vars, VAR_ITEMS, items )

   # Ergebnis
   return result


###############################################################################
# Eine Zelle soll zerlegt und weiterbehandelt werden.
#
# 'sgic'    (gic)   Zelle im gic-Format
# 'vars'    (vars)  vars
# 'pos'     (int)   Aktuelle Pos
# 'items'   (list)  Kontext [None]
#
# Ergebnis  CIB
###############################################################################
def run_2( sgic, vars, pos, items = None ) :

    # Neues CIB anlegen
    cib = [ None ] * CIB__SIZE

    # Initalierung der Einträge ...
    cib[ CIB_VARS  ] = vars
    cib[ CIB_POS   ] = pos
    cib[ CIB_CELL  ] = sgic
    cib[ CIB_ITEMS ] = items

    # CIB in vars speichern
    vars_set( vars, VAR_CIB, cib )

    # Leerer Eintrag (kann noch per Ereignis gefüllt werden)
    if len( sgic ) == 0:

          # Ereignis-Parameter für 'Leerer Eintrag'
          if pos == 0:                      # Erster?
              evtparam = "("+LOC.FIRST+")"  # Text
          else:
              evtparam = items[ pos - 1 ]   # Vorheriger Eintrag

          # Ereignis: Leerer Eintrag
          evt_result = evt( vars, EVT_031, [ pos,evtparam ] )

          # Wenn das Ereignis einen Wert geliefert hat ...
          if type( evt_result ) == str:
              sgic = evt_result            # Als Eintrag übernehmen

    # Wenn Eintrag vorhanden ...
    if len( sgic ) > 0:

          # Aktuellen Eintrag zerlegen
          parse = run_5_para( LOC.CMD_PARSE_2, [ sgic ], vars )

          # Ergebnis der Zerlegung in 'vars' eintragen
          vars_set( vars, VAR_PARAM_STR, parse[ CMD_PARSE_2_PARAM ] )
          vars_set( vars, VAR_NAME,      parse[ CMD_PARSE_2_ID    ] )

          # CIB ergänzen
          cib[ CIB_PARAM_STR ]  = parse[ CMD_PARSE_2_PARAM ]
          cib[ CIB_NAME      ]  = parse[ CMD_PARSE_2_ID    ]

    else: # Keine Eintrag

          # Ergebnis der Zerlegung in 'vars' eintragen
          vars_set( vars, VAR_PARAM_STR, [] ) # str-list
          vars_set( vars, VAR_NAME,      "" )

          # CIB ergänzen
          cib[ CIB_PARAM_STR ]  = []
          cib[ CIB_NAME      ]  = ""


    # Eintrag wurde zerlegt -> Nächste Ebene
    result = run_3( cib )

    # Ergebnis der Ausführung in CIB übernehmen
    cib[ CIB_RESULT ] = result

    # Ergebnis
    return cib


###############################################################################
# Führt eine Zelle aus, die schon vorbehandelt wurde. Der Befehl (bzw. die
# Zeichenkette) wurde ggf. von seinen Parametern getrennt. Die Parameter
# wurden auch schon in eine Liste konvertiert.
#
# 'cib'         (CIB) Zelleninfos
#
# Ergebnis      Letzter Rückgabewert
###############################################################################
def run_3( cib ) :

    # Unsere Funktionsergebnis
    result = None

    # 'vars' in lokale Variable
    vars = cib[ CIB_VARS ]

    # Ausführungsverhinderung vorhanden?
    if vars_get( vars, VAR_NO_EXEC, VIB_VALUE, VARS_LOC, 0 ) > 0: # Ja?

        # Ausführungsverhinderung vermindern
        vars_dec( vars, VAR_NO_EXEC, VARS_LOC )

    else:   # Keine Ausführungsverhinderung ...

        # Zum einfacheren Zugriff übertragen wir Inhalt von CIB_NAME
        # in eine lokale Variable
        cmd = cib[ CIB_NAME ]

        # Leerer Eintrag
        if len( cmd ) == 0:

            # Weiterverarbeitung
            result = run_4_empty( cib )

        # Wenn Anführungszeichen vorhanden, ist 'cmd' kein Befehl, sondern eine
        # Zeichenkette
        elif cmd.find( GIC_QM ) != -1:       # Zeichenkette?

            # Ergebnis
            result = cmd

            # Weiterverarbeitung
            run_4_text( cib )

        else:

            # Befehl durch Module behandeln
            result = run_4( cib )

        # Anz der Ausführungen anpassen
        vars_inc( vars, VAR_CNT_EXEC )

    # Ergebnis
    return result



###############################################################################
# Wird von run_3() ausgeführt, wenn ein Befehl durch ein Modul ausgeführt
# werden soll. Zeichenketten und leere Einträge werden hier nicht mehr
# behandelt.
#
# Veranlasst die Konvertierung der Roh-Parameter in die vom Modul benötigte
# Form.
#
# 'cib'         (CIB) Zelleninfos
#
# Ergebnis      Letzter Rückgabewert
###############################################################################
def run_4( cib ) :

    # Unser Ergebnis
    result = None

    # Wir übertragen in lokale Variable ...
    cmd       = cib[ CIB_NAME ]      # Befehlsname
    param_str = cib[ CIB_PARAM_STR ] # Original-Parameter mit Platzh.
    vars      = cib[ CIB_VARS ]      # vars

    try:
        # Modul und ggf. ersetzten Befehl holen
        module, cmd = id_get_module( cmd, vars )

        # cib ergänzen
        cib[ CIB_MODULE ] = module
        cib[ CIB_NAME   ] = cmd

    # Fehler bei Modulermittlung ...
    except Exception as e:

        # Abbruchsignal
        vars_set( vars, VAR_STOP, "run_4", VIB_VALUE, GTYP_SIGNAL )

        # Ergebnis
        result = None

    # Kein Fehler bei Modulermittlung ...
    else:

        # CIB_TYPE versorgen
        cib[ CIB_TYPE ] = vars_get( vars, cmd, VIB_GTYP )

        # Typinfos der Befehlsparameter holen
        gtypes = vars_get( vars, cmd, VIB_PGTYPES )

        # Parameter-Infos da?
        if gtypes != None:

            # Abbruch, wenn zu viele Parameter-Werte
            if len( gtypes ) < len( param_str ):
                evt( vars, ERR_019, [ cmd, len( param_str ) - len( gtypes ) ] )

            # Wir bereiten die Parameter für das Modul auf
            param = []                 # Noch keine Parameter
            i = 0                      # Aktueller Parameter-Index

            # Parameter (Typ-Infos) durchlaufen ...
            for tp in gtypes:

                # Enthält später den endgültigen Zuweisungswert f. Parameter
                value = None           # 'None' ist aber kein zulässiger Wert!

                if i < len( param_str ): # Parameter-Wert vorhanden?

                    # Variablen und Platzhalter ersetzen
                    value = evalV( param_str[ i ],
                                   tp,
                                   vars,
                                   RVQM,
                                   RV_FI,
                                   RV_RE,
                                   cib )

                else:                # kein Parameter-Wert für Index?

                    # Modul soll Vorgabewerte für seine Parameter liefern
                    pdef = run_6( module, MOD_ACTION_PDEF, vars, cmd )

                    # Kein entsprechender Vorgabewert vorhanden?
                    if pdef == None or pdef[ i ] == None:

                        # Ereignis: Parameter fehlt / Keine Vorgabewert
                        evt( vars, ERR_006, [ i + 1, cmd ] )

                    # Vorgabewert übernehmen
                    value = pdef[ i ]

                # Transformierungsmodule behandeln
                tm = vars_get( vars, cmd, VIB_TIB_P ) # Für alle Parameter
                if tm != None:

                    # tm[ i ]: Das für uns zuständige Modul
                    if tm[ i ] != None: # Parameter hat Modul?

                        value = transform( tm[ i ],
                                           cmd,
                                           value,
                                           gtypes[ i ],
                                           True,
                                           vars )

                # Es konnte kein entsprechender Wert ermittelt werden?
                if value == None:
                    evt( vars, ERR_029, [ i + 1, cmd ] )

                # 'value' als aktuellen Wert übernehmen
                param.append( value )

                # Nächster Index
                i += 1

            # Das Modul soll den aktuellen Befehl ausführen
            result = run_6( module, MOD_ACTION_EXEC, vars, cmd, param )


    # Ergebnis zurückliefern
    return result


###############################################################################
# Wird von run_3() ausgeführt, wenn ein leerer Eintrag gefunden wurde
#
# 'cib'        (CIB)
#
# Ergebnis
###############################################################################
def run_4_empty( cib ) :

    # Exception
    # evt( vars, ERR_037, param_o )

    # CIB_TYPE versorgen
    cib[ CIB_TYPE ] = GTYP_UNDEF

    return ""



###############################################################################
# Wird von run_3() ausgeführt, wenn der Eintrag eine Zeichenkette ist
#
# 'cib'        (CIB)
#
# Ergebnis
###############################################################################
def run_4_text( cib ) :

    # CIB_TYPE versorgen
    cib[ CIB_TYPE ] = GTYP_STR

    return None


###############################################################################
# Führt über das angegebene Modul die angegebene Aktion aus.
#
# 'module'      (str)  Modul, das die Ausführung übernehmen soll
# 'action'      (int)  Aktion, die von 'module' ausgeführt werden soll
#                      Siehe MOD_ACTION_... (z.B. MOD_ACTION_EXEC)
# 'vars'        (vars) Var-Speicher
#
# Ergebnis      Rückgabewert des Moduls
###############################################################################
def run_5( module, action, vars ) :

    # Umgebung für die Module schaffen

    # 'name' (Befehls oder Variablenname)
    if _ACTIONS[ action ][ _ACTIONS_NAME ]:
        name = vars_get( vars, VAR_NAME )
    else:
        name = None

    # 'param' bei Befehlsausführung oder Variablenzuweisung
    if _ACTIONS[ action ][ _ACTIONS_PARAM ]:
        param = vars_get( vars, VAR_PARAM )
    else:
        param = None

    # Ergebnis
    return run_6( module, action, vars, name, param )


###############################################################################
# Führt einen Befehl durch ein Modul aus.
# Die Parameter müssen in der korrekten Form vorliegen
#
# 'id'          (str)  Bezeichner
# 'param'       (list) Parameter, die in der korrekten Form vorliegen müssen
# 'vars'        (vars) Var-Speicher
#
# Ergebnis      Das vom Modul zurückgegebene Ergebnis
###############################################################################
def run_5_para( cmd, param, vars ) :

    # Modul für 'cmd' ermitteln
    module, cmd = id_get_module( cmd, vars )

    # Modul soll Befehl ausführen
    result = run_6( module, MOD_ACTION_EXEC, vars, cmd, param )

    # Erg zurückliefern
    return result


###############################################################################
# Führt über das angegebene Modul die angegebene Aktion aus.
#
# 'module'      (str)  Modul, das die Ausführung übernehmen soll
# 'action'      (int)  Aktion, die von 'module' ausgeführt werden soll
#                      Siehe MOD_ACTION_... (z.B. MOD_ACTION_EXEC)
# 'vars'        (vars) Var-Speicher
# 'name'        (str)  Bedeutung je nach 'action' [None]
# 'param'       (?)    Bedeutung je nach 'action' [None]
#
# Ergebnis      Rückgabewert des Moduls
###############################################################################
def run_6( module, action, vars, name = None, param = None ) :

    # Umgebung für die Module schaffen

    # GIMP - 'image','drawable'
    if _ACTIONS[ action ][ _ACTIONS_GIMP ]:

        paras = vars_get( vars, VAR_RUN_PARA )

        image    = paras[ MOD.VAR_RUN_PARA_IMG ]
        drawable = paras[ MOD.VAR_RUN_PARA_DRW ]

    # Hier trägt das Modul ggf. seinen Rückgabewert ein
    result = None

    # Ob der Befehl ausgeführt werden soll
    do_exec = True      # Vorgabe: Ja

    # Ereignis
    if action == MOD_ACTION_EXEC:       # Befehlsausführung?

        # Befehlstyp ermitteln
        cmd_type = vars_get( vars, name, VIB_GTYP )

        # Wenn grafischer Befehl ausgeführt werden soll ...
        if cmd_type == GTYP_CMD_G:

            # Ereignis: Grafischer Befehl wird ausgeführt
            if evt( vars, EVT_101 ) == False:  # Ausführung verhindern?
                do_exec = False                # Keine Ausführung!

    # Modul soll 'action' ausführen
    if do_exec:

        try:
            exec( module )

        # Fehler bei Modulausführung ...
        except Exception as e:

            # Rückgabewert retten
            result_bak = result

            # Modulname vom Modul anfordern: 'result'
            action = MOD_ACTION_NAME
            exec( module )

            # Ergebnis: Modulfehler - Befehl und Modul angegeben
            evt( vars, EVT_005, [ result, name, e ] )

            # 'Exception'
            raise RuntimeError( name )

            # Alte Rückgabewert wiederherstellen
            result = result_bak

        # Ereignis
        if action == MOD_ACTION_EXEC:       # Befehlsausführung?
            evt( vars, EVT_004, name )      # Ereignis

    # Ergebnis
    return result


###############################################################################
# Überprüft, ob das angegebene Skript im Standardordner existiert und per
# run_script() ausgeführt werden könnte.
#
# 'script'    (str) Skriptname (ohne Dateierweiterung)
# 'path'      'FOLDER_SCRIPTS' oder 'FOLDER_DEMO_CODE' ['FOLDER_SCRIPTS']
#
# Ergebnis    True, wenn ein Skript mit diesem Namen existiert
###############################################################################
def script_is( script, path = FOLDER_SCRIPTS ) :

    # Dateiname zusammenstellen
    filename = path_get( path ) + script + "." + MOD_EXT_GIC

    # Testen, ob Dateiname vorhanden
    result = fil_.isfile( filename )

    #Ergebnis zurückliefern
    return result



###############################################################################
# Führt eine Transformierungsaktion über ein Modul aus
#
# 'tib'     (tib)   TIB
# 'id'      (str)   ID
# 'value'           Wert, der konvertiert werden soll
# 'type'    (GTYP)  Typ
# 'set'     (bool)  Richtung
# 'vars'    (vars)  Var-Speicher
#
# Ergebnis          Der Konvertierte Wert
###############################################################################
def transform( tib, id, value, type, set, vars ):

    # Aktionstyp
    if set:
        action = MOD_ACTION_TRANS_L
    else:
        action = MOD_ACTION_TRANS_R

    # infos für modul zusammenstellen
    # [0] MOD_ACTION_TRANS_P_VALUE
    # [1] MOD_ACTION_TRANS_P_TYP
    # [2] MOD_ACTION_TRANS_P_PARAM
    p = [ value, type, tib[ TIB_PARAM ] ]

    # Modul soll Konvertierung durchführen
    return run_6( tib[ TIB_MODUL ], action, vars, id, p )



###############################################################################
# Transformierungsmodulzuweisung wieder aufheben.
#
# 'set'     (str)   Wird der angegebenen ID direkt zugewiesen
#           (list)  Allen ausgewählten Parametern zuweisen
#                   [["ID",0],["ID,1"],...] weißt den ersten beiden Parametern
#                   von "ID" das Modul zu.
#                   Siehe auch id_get_param_list()
# 'vars'    (vars)  Var-Speicher
#
# Ergebnis
###############################################################################
def transform_del( set, vars ):

    # Eine ID wurde angegeben?
    if type( set ) == str:

        # Ereignis: Transformierungsmodule werden entfernt
        evt( vars, EVT_010 )

        # Der ID (set) den TIB zuweisen
        vars_set( vars, set, None, VIB_TIB )

    elif type( set ) == list:

        # Ereignis: Transformierungsmodule werden entfernt
        evt( vars, EVT_010, len( set ) )

        # Auflistung durcharbeiten ...
        for item in set:

            if len( item ) == 2:        # Korrekter Anz im Eintrag?

                id       = item[ 0 ]    # Erster Wert enthält ID
                paraidx  = item[ 1 ]    # Zweiter Wert enthält Index

            else:                       # Falscher Eintrag ...
                # Ereignis : Unbekannte ID
                evt( vars, ERR_032, [ "transform_set" ] )

            # Vorhandene Zuweisungen holen
            tps = vars_get( vars, id, VIB_TIB_P )

            # Vorhanden?
            if tps != None:

                # Aktueller Index
                i = 0

                # Alle Zuweisungen durchlaufen ...
                for t in tps:

                    # Eintrag löschen
                    tps[ i ] = None

                    # Nächster Index
                    i += 1


###############################################################################
# Transformierungsmodul zuweisen.
#
# 'set'     (str)   Wird der angegebenen ID direkt zugewiesen
#           (list)  Allen ausgewählten Parametern zuweisen
#                   [["ID",0],["ID,1"],...] weißt den ersten beiden Parametern
#                   von "ID" das Modul zu.
#                   Siehe auch id_get_param_list()
# 'module_name'(str) Modul
# 'tparam'          Transformierungs-Parameter, die an das ausführende Modul
#                   weitergereicht werden
# 'vars'    (vars)  Var-Speicher
#
# Ergebnis
###############################################################################
def transform_set( set, module_name, tparam, vars ):

    # Modul für Modul-Namen
    module = vars_get( vars, module_name, VIB_MODULE )

    # Eine ID wurde angegeben?
    if type( set ) == str:

        # Ereignis: Transformierungsmodule werden zugewiesen
        evt( vars, EVT_009, 1 )

        # TIB Zusammenstellen
        # [0] TIB_MODUL
        # [1] TIB_PARAM
        tib = [ module, tparam ]

        # Der ID (set) den TIB zuweisen
        vars_set( vars, set, tib, VIB_TIB )

    elif type( set ) == list:

        # Ereignis: Transformierungsmodule werden zugewiesen
        evt( vars, EVT_009, len( set ) )

        # Auflistung durcharbeiten ...
        for item in set:

            if len( item ) == 2:        # Korrekter Anz im Eintrag?

                id       = item[ 0 ]    # Erster Wert enthält ID
                paraidx  = item[ 1 ]    # Zweiter Wert enthält Index

            else:                       # Falscher Eintrag ...
                # Ereignis : Unbekannte ID
                evt( vars, ERR_032, [ "transform_set" ] )

            # Vorhandene holen
            tps = vars_get( vars, id, VIB_TIB_P )

            # Ggf. neue Erzeugen
            if tps == None:

                l = len( vars_get( vars, id, VIB_PGTYPES ) )

                # Leere Info-Block erzeugen
                tps = [None] * l

            # TIB Zusammenstellen
            # [0] TIB_MODUL
            # [1] TIB_PARAM
            tib = [ module, tparam ]

            # Eintragen
            tps[ paraidx ] = tib   # Modul eintragen

            # Speichern
            vars_set( vars, id, tps, VIB_TIB_P )
