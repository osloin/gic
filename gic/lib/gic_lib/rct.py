#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Rechteck-Hilfsfunktionen
#
# align()     Rechteck an einem anderen R. ausrichten
# max_ratio() Größtmögliches Rechteck mit bestimmten Seitenverhältnis ermitteln


################################################################################
# Um ein Rechteck an/in einem anderen auszurichten ohne dabei seine Größe
# zu verändern
#
# Rechtecke sind eine Liste (list) mit diesen Werten ...
# [0] (int) x-Position
# [1] (int) y-Position
# [2] (int) Breite
# [3] (int) Höhe
#
# 'rct':    (int-list) Rechteck, das ausgerichtet werden soll
# 'rctTo':  (int-list) Rechteck, an dem angerichtet werden soll
# 'modeH':  (int) Bestimmt, wie horizontal ausgerichtet werden soll
#           0: keine
#           1: mitte
#           2: links ausrichten  (innen)
#           3: rechts ausrichten (innen)
#           4: links ausrichten  (außen)
#           5: rechts ausrichten (außen)
# 'modeV':  (int) Bestimmt, wie vertikal ausgerichtet werden soll
#           0: keine
#           1: mitte
#           2: oben ausrichten  (innen)
#           3: unten ausrichten (innen)
#           4: oben ausrichten  (außen)
#           5: unten ausrichten (außen)
# 'spaceH': (int) Zusätzlicher Wert, der bei der horizonalen Ausrichtung
#           addiert/subtrahiert werden soll
# 'spaceV': (int) Zusätzlicher Wert, der bei der vertikalen Ausrichtung
#           addiert/subtrahiert werden soll
#
# Ergebnis: Das ausgerichtete Rechteck
################################################################################
def align( rct, rctTo, modeH = 1, modeV = 1, spaceH = 0, spaceV = 0 ) :

    # Ergebnis-Rechteck
    result = [ 0,
               0,
               rct[ 2 ],
               rct[ 3 ] ]

    # Rechteck, an dem ausgerichtet werden soll
    xA      = rctTo[ 0 ]
    yA      = rctTo[ 1 ]
    widthA  = rctTo[ 2 ]
    heightA = rctTo[ 3 ]

    # Die beiden Zielkoordinaten initalisieren
    offx = spaceH
    offy = spaceV

    # x-Position ermitteln
    if modeH == 1:              # mittig?
        offx += xA + ( ( widthA - rct[ 2 ] ) / 2 )
    elif modeH == 2:            # links innen?
        offx += xA
    elif modeH == 3:            # rechts innen?
        offx += xA + widthA - rct[ 2 ]
    elif modeH == 4:            # links aussen?
        offx += xA - rct[ 2 ]
    elif modeH == 5:            # rechts außen?
        offx += xA + widthA
    else:
        offx += rct[ 0 ]        # unverändert

    # y-Position ermitteln
    if modeV == 1:              # mittig?
        offy += yA + ( ( heightA - rct[ 3 ] ) / 2 )
    elif modeV == 2:            # oben innen?
        offy += yA
    elif modeV == 3:            # unten innen?
        offy += yA + heightA - rct[ 3 ]
    elif modeV == 4:            # oben aussen?
        offy += yA - rct[ 3 ]
    elif modeV == 5:            # unten außen?
        offy += yA + heightA
    else:
        offy += rct[ 1 ]        # unverändert

    # neue Position zuweisen
    result[ 0 ] = offx
    result[ 1 ] = offy

    # Ergebnis
    return result

################################################################################
# Berechnet für ein Rechteck das größtmögliche Rechteck (mit einem bestimmten
# Seitenverhältnis), das in ihm gespeichert werden kann
#
# w, h = max_ratio( 4789, 3193, 1.5 )
#
# w,h           (int)
# fRatioResult  (float)
#               Der das Seitenverhältnis bestimmt.
#               z.b. 1.5 um ein 15:10 zu erzeugen
#
# Ergebnis
################################################################################
def max_ratio( w, h, fRatioResult ):

    fRatio = w / float( h )

    if fRatio != float( fRatioResult ):    # Beide haben keine gl Seit-Verhä.?

        # Rechteck, das über die volle Breite ragt, erzeugen
        h_new = int( w / fRatioResult )

        if h_new > h:                     # passt nicht? -> volle Höhe
            h_new = int( h )
            w = int( h_new * fRatioResult )

        h = h_new

    return w, h
