#!/usr/bin/env python
# -*- coding: utf-8 -*-
# osloin. 11/2018
#
# Zeichenketten - Hilfsfunktionen
#
# collect()       Sammelt bestimmte Zeichen einer Zeichenkette
# find_brackets() Durchsucht eine Zeichenkette nach einem Klammerpaar
# find_ex()       Durchsucht eine Zeichenkette nach einer anderen
# replace_solo()  Einzelnes Zeichen ersetzen
# split_ex()      Zeichenkette in Liste konvertieren
# strip_qm()      Erweiterte strip()-Funktion



import  locale as LOC



################################################################################
#                               find_ex()
################################################################################
SFE_POS  = 0    # Erste Position suchen
SFE_LIST = 1    # Alle Vorkommen suchen und als int-list zurückliefern



###############################################################################
# Sammelt Zeichen einer Zeichenkette, solange sie (nicht) einem
# bestimmten Zeichenvorrat entsprechen
#
# 's'           (str) z.B. "aabbccddeeff"
# 'inchars'     (str) z.B. "abc"
# 'stop'       (bool) True: Abbrechen bei nicht sammelbarem Zeichen [True]
# 'notinchars' (bool) True: Sammelt Zeichen, wenn sie nicht(!) in 'inchars'
#                           vorkommen [False]
#
# Ergebnis  (str) z.B. "aabbcc"
###############################################################################
def collect( s, inchars, stop = True, notinchars = False ):

    result = ""

    # Alle Zeichen durchlaufen ...
    for c in s:

        if notinchars == True:
            bCollect = c not in inchars
        else:
            bCollect = c in inchars

        if bCollect == True:
            result += c
        else:
            if stop:
                break

    return result



###############################################################################
# Durchsucht die angegebene Zeichenkette nach einem Klammerpaar. Dabei werden
# mehrfach geöffnete Klammern und Klammern in Zeichenketten berücksichtigt.
#
# 's'  (str) Zeichenkette, die analysiert werden soll
# 'b1' (str) Zeichen, Klammer auf
# 'b2' (str) Zeichen, Klammer zu
# 'qm' (str) Anführungszeichen (Klammern, die davon eingeschlossen sind, werden
#            nicht berücksichtigt)
#
# Ergebnis: start (int) Position der geöffneten Klammer
#           end   (int) Position der geschlossenen Klammer
###############################################################################
def find_brackets( s, b1, b2, qm ):

    # Alle Klammern (auf und zu) ermitteln
    brackets = find_ex( s,
                        b1 + b2,
                        qm,
                        findIn   = True,
                        sfe_mode = SFE_LIST
                      )

    # Unser Ergebnis
    start = -1
    end   = -1

    # Anz der geöffneten Klammern
    brop = 0

    # alle Klammerpositionen abgrasen ...
    for b in brackets:
        if s[ b ] == b1:
            brop += 1
            if start == -1:
                start = b
        else:
            brop -= 1
            if brop == 0:
                end = b
                break;

    # Ergebnis
    return start, end



###############################################################################
# Durchsucht eine Zeichenkette nach einer Zeichenkette oder Zeichen einer
# Zeichenkette, wobei ein Bereich definiert werden kann, der besonders
# behandelt werden soll.
#
# 's'           (str) Zeichenkette, die durchsucht werden soll
# 'find'        (str) Zeichenkette, die gesucht werden soll
# 'qm_begin'    (str) Diese Zeichenkette leitet einen Bereich ein
# 'qm_end'      (str) Diese Zeichenkette beendet den durch 'qm_begin'
#               eingeleiteten Bereich wieder. [None]
#               None, wenn 'qm_begin' identisch mit 'qm_begin' sein soll
# 'qm_mode'     (int)
#               0:  Es soll im und ausserhalb eines Bereichs gesucht werden
#               1:  Es soll nur in einem Bereich gesucht werden
#              [2]: Es soll nur ausserhalb eines Bereichs gesucht werden
# 'findIn'      (bool)
#              [False]: Es soll nach 'find' gesucht werden
#                 True: Es soll nach einem Zeichen aus 'find' gesucht werden
# 'sfe_mode'    (int) [SFE_POS], SFE_LIST
#
# Ergebnis      SFE_POS (int) Position, wenn gefunden
#               -1, wenn nichts gefunden
#
#               SFE_LIST (int-list) Auflistung der Positionen
#               [], wenn nichts gefunden
###############################################################################
def find_ex( s,
             find,
             qm_begin,
             qm_end   = None,
             qm_mode  = 2,
             findIn   = False,
             sfe_mode = SFE_POS
           ):

   # Wenn kein End-Zeichen angegeben, wird das Anfangszeichen verwendet
   if qm_end == None:           # Kein Endzeichen angegeben?

      if qm_begin == None:      # Auch kein Startzeichen angegeben?
         return s.find( find )  # Standardverfahren
      qm_end = qm_begin         # Endzeichen ist das Startzeichen

   # Vorläufiges Suchergebnis
   if sfe_mode == SFE_POS:     # Positionsmodus?
       result       = -1
   else:                       # Liste der Positionen ...
       result = []

   # Initalisierungen für Suche
   b_qm         = False         # Ob gerade ein Bereich analysiert wird
   bInitBuffer  = True          # Ob die Puffer im aktuellen Durchlauf init-
                                # alisiert werden sollen
   bCompare     = False         # Ob aktuelles Zeichen im akt. Durchlauf
                                # verarbeitet werden soll
   i            = 0             # Aktuelle Position in s
   if findIn == True:           # Nach einem Zeichen in 'find' suchen?
       l = 1                    # Länge des Suchbegriffs
   else:                        # Nach 'find' suchen
       l = len( find )          # Länge des Suchbegriffs

   # Alle Zeichen der Zeichenkette durchgehen ...
   for c in s:

      if bInitBuffer == True:                # Puffer initalisieren?

         bInitBuffer = False                 # Initalisierung beendet
         buffer = " " * l                    # Aktueller Puffer
         buffer_qm = " " * len( qm_begin )   # Aktueller qm-Puffer

      buffer_qm = buffer_qm[1:] + c          # Erstes Zeichen entfernen und + c

      if b_qm == False:                      # Kein Bereich?

         if buffer_qm == qm_begin:           # Beginn des Bereichs gefunden?
            b_qm = True                      # Bereich hat begonnen
            bInitBuffer = True               # Puffer sollen initalisiert werden
         else:                               # Kein Beginn des Bereichs gefunden
            if qm_mode == 0 or qm_mode == 2: # Ausserhalb des Bereichs suchen?
               bCompare = True               # Aktuelles Zeichen verarbeiten
      else:                                  # Bereich ...

         if buffer_qm == qm_end:             # Ende des Bereichs gefunden?
            b_qm = False                     # Bereich beendet
            bInitBuffer = True               # Puffer sollen initalisiert w.
         else:                               # Kein Bereichsende ...
            if qm_mode == 0 or qm_mode == 1: # Innerhalb des Bereichs suchen?
               bCompare = True               # Aktuelles Zeichen verarbeiten

      if bCompare == True:              # Soll akt. Zeichen verarbeitet werden?

         bCompare = False               # Aktuelles Zeichen wurde verarbeitet
         buffer   = buffer[1:] + c      # Erstes Zeichen entfernen und + c
         found    = False               # noch nicht gefunden

         if findIn == True:             # Nach einem Zeichen in 'find' suchen?
            if buffer in find:          # Zeichen in 'find'?
                found = True            # Gefunden
         else:                          # Nach 'find' suchen ...
            if buffer == find:          # gefunden?
               found = True             # Gefunden

         if found:                      # gefunden?

            if sfe_mode == SFE_POS:         # Positionsmodus?
                result = i - l + 1          # Ergebnis
                break                       # Schleife abbrechen
            else:                           # SFE_LIST ...
                result.append( i - l + 1 )  # Akt. Position als Erg. aufnehmen

      # Auf nächste Position
      i += 1

   # Ergebnis
   return result


###############################################################################
# Ersetzt alle einzelstehenden Zeichen durch eine beliebige Zeichenkette.
# Es werden also keine Zeichen ersetzt, die einen gleichen Nachbarn haben
#
# 's'           (str) Zeichenkette
# 'find'        (str) Zeichen das ersetzt werden soll
# 'replace'     (str) Hierdurch ersetzen
#
# Ergebnis      (str)
###############################################################################
def replace_solo( s, find, replace ):

    # Ergebnis
    result = ""

    # Vorheriges Zeichen
    c_prev = None

    # index
    i = 0

    cnt = len( s )

    while i < cnt:

        c = s[ i ]

        if c == find:
            solo = True
            if i != 0:                  # nicht erstes
                if s[ i - 1 ] == find:  # vorheriges
                    solo = False
            if i != cnt - 1:            # nicht letztes
                if s[ i + 1 ] == find:  # vorheriges
                    solo = False
        else:
            solo = False

        if solo:
            result += replace
        else:
            result += c

        # Index
        i += 1

    # Ergebnis
    return result


###############################################################################
# Konvertiert eine Zeichenkette in eine Liste.
#
# 's'           (str) Zeichenkette, die konvertiert werden soll
#               z.B. "1200,800,0"
# 'separator'   (str) Trennzeichen. z.B. ","
#               (str-list) Wenn für jede Position ein eigenes Trennzeichen
#                          festgelegt werden soll
# 'qm'          Trennzeichen, die in einer solchen Zeichenkette liegen, werden
#               nicht als Trennzeichen angesehen
# 'qmNew'       (str) Gefundene qm werden durch dieses Zeichen ersetzt
# 'end'         (str) Es soll nur bis zu diesem Zeichen analysiert werden
# 'dotrim'      (bool) Ob 'strip()' auf einen gefundenen Eintrag aus-
#               geführt werden soll
# 'noempty'     (bool) True, wenn leere Einträge unterdrückt werden sollen
# 'br_1'
# 'br_2'        (str)   Zeichen für Klammer-auf und Klammer-zu. Wenn angegeben,
#                       wird das 'separator'-Zeichen als Funktions-Parameter-
#                       Trennzeichen angesehen und nicht berücksichtigt
#               (None)  Wenn keine Klammerbehandlung
#
# Ergebnis      (str-list) Auflistung, die die getrennten Parameter enthält
###############################################################################
def split_ex( s,
              separator = ",",
              qm        = "\"",
              qmNew     = "\"",
              end       = None,
              dotrim    = False,
              noempty   = False,
              br_1      = "(",
              br_2      = ")" ):

    result  = range(0)          # Das Ergebnis
    bStr    = False             # Ob gerade eine Zeichenkette analysiert wird
    buffer  = ""                # Aktueller Puffer
    bAddNew = False             # Ob neuer Eintrag erkannt wurde
    i       = 0                 # Aktueller Index in 's'
    cnt_bra = 0                 # Anz d. z.Z. geöffneten Klammern
    cnt_spl = 0                 # Anz der gefundenen Trennzeichen
    err_result = 0

    # Aktuelles Trennzeichen ermitteln
    if type( separator ) == list:   # Trennzeichen-Liste?
        sep = separator[ cnt_spl ]  # Trennz. an akt. Pos
    else:
        sep = separator

    # Alle Zeichen der Zeichenkette 's' durchgehen ...
    for c in s:

        if bStr == True:                # Zeichenkettenmodus?

            if c == qm:                 # Zeichenkettenende?
                bStr = False            # Kein Zeichenkettenmodus mehr
                buffer += qmNew         # Umschliesszeichen zufügen
            else:                       # Kein Zeichenkettenende...
                buffer += c             # Neues Zeichen aufnehmen

        else:                           # Kein Zeichenkettenmodus...

            if br_1 != None and c == br_1:    # Klammer auf?
                cnt_bra += 1                  # Anz der geöffneten Klammern
            elif br_2 != None and c == br_2:  # Klammer zu?
                cnt_bra -= 1                  # Anz der geöffneten Klammern
                if cnt_bra < 0:
                    raise RuntimeError( LOC.BRACKET ) # Geöffn. Klammer fehlt
                    #evt( vars, ERR_034 )      # Geöffnete Klammer fehlt

            if c == sep:                # Passendes Trennzeichen gefunden?
                if cnt_bra == 0:        # Keine geöffnete Klammer?
                    bAddNew = True      # Neuer Eintrag gefunden
                    cnt_spl += 1        # Anz d. gefundenen Trennzeichen
                    if type( separator ) == list: # Trennzeichen-Liste?

                        if cnt_spl < len( separator ):# Max. nicht erreicht?
                            sep = separator[ cnt_spl ] # Auf nächstes Trennz.
                else:
                    buffer += c         # Trenner als neues Zeichen aufnehmen
            else:
                if c == qm:             # Zeichenkettenstart?
                    bStr = True         # Zeichenkettenmodus
                    buffer += qmNew     # Umschliesszeichen zufügen
                else:
                    if end != None and c == end:
                        break           # for abbrechen
                    buffer += c         # Neues Zeichen aufnehmen

        i += 1                          # Aktuellen Index erhöhen

        if i == len( s ):               # letztes Zeichen?
            if len( buffer ) > 0:       # Zeichen im Puffer?
                bAddNew = True          # Übernehmen

        if bAddNew == True:             # Neuen Eintrag erkannt?

            if dotrim == True:          # Zeichen vorne/hinten entfernen
                buffer = buffer.strip() # Zeichen entfernen

            if noempty == False or len( buffer ) > 0:
                result.append( buffer ) # Neuen Eintrag in die Auflistung aufil_.

            bAddNew = False             # Flag zurücksetzen
            buffer = ""                 # Puffer zurücksetzen

    if cnt_bra > 0:
        raise RuntimeError( LOC.BRACKET ) # Geschlossene Klammer fehlt
        #evt( vars, ERR_035 )            # Geschlossene Klammer fehlt

    # Ergebnis
    return result



###############################################################################
# Führt auf die angegebne Zeichenkette ein strip() aus und entfernt (falls vor-
# handen) vorne und hinten das angegebene Zeichen - ggf. mehrfach
#
# 's'     (str) Zeichenkette, die behandelt werden soll
# 'qm'    (str) Das Zeichen, das entfernt werden soll
#               Vorgabewert: Anführungszeichen
#
# Ergebnis  (str) Konvertierte Zeichenkette
###############################################################################
def strip_qm( s, qm = "\"" ):

    qm_len = len( qm )

    striped = True

    while( striped ):

        s = s.strip()

        if len( s ) > qm_len and s[ 0 : qm_len ] == qm and s[ - qm_len : ] == qm:
            striped = True
            s = s[ qm_len: len( s ) - qm_len ]
        else:
            striped = False

    return s
