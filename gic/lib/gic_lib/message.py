#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Nachricht für Anwender
#
# message() Nachricht für Anwender ausgeben



from gimpfu import *



################################################################################
#                                   message()
################################################################################
TYPE_NO      = 0                 # (Keine Meldung)
TYPE_DETAIL  = 1                 # Eine Detail-Information
TYPE_INFO    = 2                 # Eine Information. (z.B. Durchführungszeit)
TYPE_DUMP    = 3                 # Längere Auflistungen
TYPE_MSG     = 4                 # Eine Nachricht
TYPE_WARNING = 5                 # Eine Warnung
TYPE_ERROR   = 6                 # Eine Fehlermeldung



###############################################################################
# Nachricht ausgeben
#
# 'msgtxt'      (str) Text, der ausgegeben werden soll
# 'type'        (int) Siehe unter TYPE_... (z.B. TYPE_MSG)
#
# Ergebnis
###############################################################################
def message( msgtxt, type = TYPE_INFO ) :

    # Art der Darstellung
    handler = pdb.gimp_message_get_handler( ERROR_CONSOLE )
    if handler != ERROR_CONSOLE:
        pdb.gimp_message_set_handler( ERROR_CONSOLE )
    else:
        handler = -1

    # Ausgabe
    pdb.gimp_message( msgtxt )

    # Haben wir die Ausgabeart geändert?
    if handler != -1:
        pdb.gimp_message_set_handler( ERROR_CONSOLE ) # Orginal herst.
