#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Hilfsfunktionen zur Behandlung von Dateien und Dateinamen
#
# convert()       Problematische Zeichen ersetzen
# create_folder() Legt angegebenen Ordner an
# extract()       Extrahiert etwas aus einem Dateipfad
# filelist_doc()  Dateiliste im Pfad zusammenstellen
# isfile()        Überprüft, ob die angegebene Datei existiert
# make_exec()     Datei (unter Linux) ausführbar machen
# read()          Liest Dateiinhalt
# unused()        Ermittelt einen unbenutzen Dateinamen
# write()         Schreibt Zeichenkette in Datei


import os
import sys
import stat
import errno
import glob

################################################################################
#                            Lese- Schreibstatus
################################################################################
ST_OK  = 0       # Alles OK
ST_ERR = -1      # Unbekannter Fehler
ST_NO  = -2      # Datei oder Verzeichnis existiert nicht
ST_PM  = -3      # Keine Berechtigung

################################################################################
#                                 extract()
################################################################################
FE_DOTPOS        = 1 # Position, des Trenners zwischen Dateinamen und
                     # Dateierweiterung
FE_DOTEXT        = 2 # Wie FE_EXT, aber mit vorangestellten Punkt
FE_EXT           = 3 # Nur die Erweiterung ohne Punkt. Bei mehreren wird nur
                     # die letzte zurückgeliefert
FE_NAME          = 4 # Nur der Dateiname (ohne Pfad und ohne Erweiterung)
FE_NAME_EXT      = 5 # Dateiname mit Erweiterung (ohne Pfad)
FE_PATH_SEP      = 6 # Pfad ohne Dateiname (es wird sichergestellt, dass am
                     # Ende ein Trennzeichen steht)
FE_PATH_NAME     = 7 # Pfad mit Dateiname aber ohne Erweiterung
                     # (und ohne Punkt)
FE_PATH_NAME_DOT = 8 # Pfad mit Dateiname ohne Erweiterung (Stellt aber
                     # sicher, daß ein Punkt am Ende steht)



################################################################################
# Legt den angegebenen Ordner an, falls er noch nicht existieren sollte.
# (Falls schon, geschieht nichts)
#
# 'folder'    (str) Name mit Pfad
#
# Ergebnis: (str) 'folder'
################################################################################
def create_folder( folder ) :

    if os.path.exists( folder ) == False:   # Nicht da?

        os.makedirs( folder )               # Ordner anlegen

    return folder





################################################################################
# convert()
#
# Problematische Zeichen für glob.glob() oder pdb.gimp_file_load() ersetzen
#
# 'name'    (str)  Datei- oder Pfadname
#
# Ergebnis: (str) Datei- oder Pfadname
################################################################################
def convert( name ) :

    #result = name.decode( sys.getfilesystemencoding(), 'replace' )
    result = name.decode('utf-8')

    return result


################################################################################
# Extrahiert etwas aus einem Dateipfad
#
# 'filename'    (str) Pfad mit Dateiname und einer oder mehreren
#                     Dateierweiterungen
# 'extr'        (int) Was ermittelt werden sollen. Siehe oben bei den
#                     Deklarationen ...
#
#                       FE_DOTPOS
#                       FE_DOTEXT
#                       FE_EXT
#                       FE_NAME
#                       FE_PATH_SEP
#                       FE_PATH_NAME
#                       FE_PATH_NAME_DOT
#
# Ergebnis:     (str/int) Je nach 'extr'
#               Im Normalfall ist das Ergebnis von Typ Zeichenkette. Nur wenn
#               'extr' auf FE_DOTPOS gesetzt wurde, ist es Integer.
################################################################################
def extract( filename, extr ) :

    # Unser Ergebnis
    result = ""

    # Position des Trennzeichen zwischen Dateiname und seiner Erweiterung
    if extr == FE_DOTPOS:

        pos_ext = filename.rfind( "." ) # Startposition der Erweiterung

        # Sonderfall xcf.bz2, das wird zusammen als Endung betrachtet
        if pos_ext != -1:

            # Zweiten Punkt suchen
            # pos_ext2 - Startposition der Erweiterung
            pos_ext2 = filename.rfind( ".", 0, pos_ext )

            if pos_ext2 != -1:
                if filename[ pos_ext2 : ].lower() == ".xcf.bz2":
                    pos_ext = pos_ext2

        return pos_ext

    # Nur Erweiterung
    elif extr == FE_EXT or extr == FE_DOTEXT:
       pos_ext = extract( filename, FE_DOTPOS )
       if pos_ext == -1:               # Nicht gefunden?
          pos_ext = len( filename )    # Position auf Ende setzen
       elif extr == FE_DOTEXT:
            pos_ext -= 1               # Mit Punkt?

       result = filename[ pos_ext+1 : ]

    # Pfad o. Dateiname + Trennzeichen
    elif extr == FE_PATH_SEP:
       pos_sep = filename.rfind( os.sep )
       if pos_sep != -1:
          result = filename[ : pos_sep + len( os.sep ) ]

    # Pfadname + Dateiname ( + Punkt )
    elif extr == FE_PATH_NAME or extr == FE_PATH_NAME_DOT:
       pos_ext = extract( filename, FE_DOTPOS )
       if pos_ext == -1:               # Nicht gefunden?
          pos_ext = len( filename )    # Position auf Ende setzen
       result = filename[ : pos_ext ]
       if extr == FE_PATH_NAME_DOT:
           result += "."

    # Dateiname (ohne Pfad und Erweiterung)
    elif extr == FE_NAME:
       pos_ext = extract( filename, FE_DOTPOS )
       pos_sep = filename.rfind( os.sep )
       if pos_sep != -1 and pos_ext != -1:
          result = filename[ pos_sep + 1 : pos_ext ]

    # Dateiname mit Erweiterung (ohne Pfad)
    elif extr == FE_NAME_EXT:
       result = extract( filename, FE_NAME ) +\
                extract( filename, FE_DOTEXT )

    return result


################################################################################
# Einfache Liste der Dateien im Pfad zusammenstellen, die dann zur Ausgabe
# verwendet werden kann.
#
# 'files'  (list)     Dateinamen-Liste
#          (str)      Verzeichnis mit Wildcards
# 'extra'  (int)      Was angezeigt werden soll [FE_NAME]
# 'filter' (str-list) Dateien, die ausgefiltert werden sollen [None]
#
# Ergebnis: Zeichenkette, die eine Auflistung der Namen enthält
################################################################################
def filelist_doc( files, extra = FE_NAME, filter = None ) :

    result = ""

    # Dateienliste
    if type( files ) == str:
        files = glob.glob( files )

    # Liste sortieren
    files.sort()

    # Index
    idx = 1

    # Alle Dateien durchlaufen...
    for filename in files:

       filename = extract( filename, extra )

       if filter == None or not filename in filter:
           result += "%3d. '%s'\n" % ( idx, filename )
           idx += 1

    return result


################################################################################
# Überprüft, ob die angegebene Datei existiert
#
# 'filename'    (str) Dateiname
#
# Ergebnis: True, wenn ja
################################################################################
def isfile( filename ) :

    return os.path.isfile( convert( filename ) )


################################################################################
# Macht eine Datei ausführbar (Linux)
#
# 'filename'    (str) Dateiname
#
# Ergebnis:
################################################################################
def make_exec( filename ) :

    st = os.stat( filename )

    os.chmod( filename,
              st.st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH )


###############################################################################
# Liest eine (Text)-Datei
# Gegenstück zu write()
#
# 'filename' (str)  Dateiname
# 'default'  (?)    Was im Fehlerfall zurückgeliefert werden soll [""]
#
# Ergebnis:  [0] (str)  Der Dateiinhalt oder 'default'
#            [1] (int)  Status
#                       ST_OK,  wenn erfolgreich
#                       ST_NO,  wenn Datei nicht vorhanden
#                       ST_PM,  wenn keine Berechtigung
#                       ST_ERR, wenn unbekannter Fehler auftrat
###############################################################################
def read( filename, default = "" ) :

    try:

        # Dateiinhalt lesen
        result = open( filename ).read()

        # Ergebnis
        return result, ST_OK

    #except getattr( __builtins__,'FileNotFoundError', IOError ) as e:
    #    return default, ST_NO

    except ( OSError, IOError ) as e:

        # Datei nicht da?
        if getattr( e, 'errno', 0 ) == errno.ENOENT:
            mode = ST_NO

        # Keine Berechtigung?
        elif getattr( e, 'errno', 0 ) == errno.EACCES:
            mode = ST_PM

        return default, mode

    except Exception as e:

        return default, ST_ERR


################################################################################
# Ermittelt einen freien Dateinamen
#
# 'filename' (str) Ausgangswert. Kompletter Dateiname mit Pfad und Erweiterung
#                  Wenn eine Datei mit diesem Namen schon existiert, wird ein
#                  freier Name im gleichen Verzeichnis ermittelt.
#
# Ergebnis      Unbenutzer Dateiname
################################################################################
def unused( filename ) :

    # Pfad mit Namen und ohne Erweiterung
    fpn = extract( filename, FE_PATH_NAME )

    # Erweiterung ohne Punkt
    ext = extract( filename, FE_EXT )

    # Ergebnis ermitteln
    result = None
    if isfile( filename ) == False:      # Datei noch nicht da?
        result = filename                # Originalname zurückliefern
    else:
        i = 1
        while result == None:                           # Noch nicht gefunden?
            filename = fpn + "_" + str(i) + "." + ext   # Dateinamen
            if isfile( filename ) == False:             # Datei nicht da?
                result = filename                       # Abbruch
            else:
                i += 1

    # Ergebnis
    return result


###############################################################################
# Speichert eine Zeichenkette in eine Datei
# Gegenstück zu read()
#
# 'filename' (str)  Dateiname
# 's'        (str)  Was gespeichert werden soll
#
# Ergebnis: ST_OK, wenn erfolgreich
###############################################################################
def write( filename, s ) :

    try:

        # Letztes Zeichen sollte Zeilenvorschub sein ...
        if len( s ) > 0 and s[ -1 ] != "\n":
            s += "\n"

        # Schreiben
        f = open( filename, "w")
        f.write( s )
        f.close()

        return ST_OK

    except ( OSError, IOError ) as e:

        # Keine Berechtigung?
        if getattr( e, 'errno', 0 ) == errno.EACCES:
            result = ST_PM
        else:
            result = ST_ERR

        return result

    except Exception as e:

        return ST_ERR
