#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# osloin
#
# Diverse Hilfsfunktionen zur Behandlung von Listen (list)
#
# get()         Sicherer Zugriff auf Listeneintrag
# get_idx()     Listeneintrag suchen
# items_ope()   Addition auf alle Listeneinträge
# to_str()      Listeneinträge in Zeichenkette konvertieren



###############################################################################
# Führt einen sicheren Zugriff auf einen Listeneintrag durch
#
# 'items'   (list) Liste, auf die zugegriffen werden sollen
# 'idx'     (int)  Wert, der addiert werden soll
# 'default' (?)    Wert, der geliefert werden soll, wenn kein entsprechender
#                  Listeneintrag existierte
#
# Ergebnis    (?)
###############################################################################
def get( items, idx, default ):

    # Listen-Eintrag vorhanden?
    if type( items ) == list and len( items ) - 1 >= idx: # Ja
        default = items[ idx ]    # Übernehmen

    return default



###############################################################################
# Durchsucht eine Liste nach dem ersten Vorkommen eines Wertes.
#
# 'items'     (list) Liste
# 'search'    (?)    Wert, der gesucht werden soll
# 'default'   (int)  Wert, der geliefert werden soll, wenn nicht gefunden [-1]
# 'compare'   (int)  Bestimmt, wie der Vergleich durchgeführt werden soll
#                    0  normal, mittels "=="
#                    1  Überprüft Listeneinträge vom Typ Zeichenk. .Ein Eintrag
#                       gilt auch als gefunden, wenn er am Anfang 'search' ent-
#                       spricht. z.B. Wenn nach "abc" gesucht wird, gilt "abcd"
#                       als gefunden.
#
# Ergebnis    (int)  Index des gefundenen Eintrags
#                    'default', wenn nicht gefunden
###############################################################################
def get_idx( items, search, default = -1, compare = 0 ):

    result = default
    found = False
    i = 0
    for item in items:

        # Normaler Vergleich
        if compare == 0 and item == search:
           found = True

        # Anfangsbereich muß entsprechen
        if compare == 1 and type( item ) == str:

            li = len( item )
            ls = len( search )

            if li == ls:
                found = item == search
            else:
                if li > ls:
                    found = item[ 0: ls ] == search

        if found:
           result = i
           break;

        i += 1

    return result


###############################################################################
# Führt eine Addition auf alle Listeneinträge aus
#
# 'l'       (list) Liste, deren Einträge konvertiert werden sollen
# 'add'     (?)    Wert, der addiert werden soll
#
# Ergebnis  (list) konvertierte Liste
###############################################################################
def items_ope( l, add ):

    # Neue Liste anlegen
    l_new = [add] * len( l )

    # Alte Liste durchlaufen
    for i in xrange( len( l ) ):

        # Eintrag konvertieren
        l_new[ i ] = l[ i ] + add

    # Neue Liste zurückliefern
    return l_new


###############################################################################
# Listeneinträge in Zeichenkette konvertieren
#
# 'items'     (list) Einträge, die formatiert werden sollen
#             (?)    Wird als eine Liste mit einen Eintrag angesehen
# 'sep'       (optional) (str / list)
#                        (str)  Trennzeichen zwischen Einträge
#                        (list) Trennzeichen mit Formatierung für Laufvariable
#                               Erscheint auch vor erstem Eintrag
#                               [0] (str)  Formatierungszeichenkette für
#                                          Laufvariable i. z.B "%d"
#                               [1] (bool) True:  0-basiert
#                                          False: 1-basiert
#                               [2] (str)  Formatierungszeichenkette für ersten
#                                          Eintrag. Wenn nicht angegeben, wird
#                                          der andere auch hier verwendet.
# 'pre'       (optional) (str / str-list)
#                        (str) Vorangestellte Zeichenkette
#                        (str-list) [0] Wenn items keinen Eintrag enthält
#                                   [1] Wenn items einen Eintrag enthält
#                                   [2] Wenn items mehr als einen Eintrag
#                                       enthält
#                                   [3] Soll [1] und [2] angehängt werden
# 'qm'        (optional) (str) Damit sollen Einträge umschlossen werden
# 'post'      (optional) (str) Nachgestellte Zeichenkette
# 'idxfilter' (optional) (?)   Auswahl der Einträge
#                        None           Keine
#                        []             Alle Indizes
#                        int            Nur der angegebene Index
#                        [int,int,..]   Nur die aufgelisteten Indizes
#
# Ergebnis    (str)
###############################################################################
def to_str( items,
            sep       = " ",
            pre       = "",
            qm        = "",
            post      = None,
            idxfilter = [] ):

    r   = ""
    i   = 0
    add = 0 # Anz der aufgenommen Einträge

    # Einzeleintrag in Liste konvertieren
    if type( items ) != list:
        items = [ items ]

    for item in items:

       # Filterbehandlung
       ok = False
       if type( idxfilter ) == list:
           if idxfilter == [] or i in idxfilter:
               ok = True
       else: # int
           ok = i == idxfilter

       if ok: # Eintrag aufnehmen
           if add == 0:   # Erster Eintrag?

               if type( pre ) == str:
                   r = pre
               else:
                   if type( pre ) == list:
                       idx = len( items )
                       if idx > 2:
                           idx = 2
                       r = pre[ idx ]
                       if len( pre ) > 3 and (idx == 1 or idx == 2):
                          r += pre[ 3 ]

           # Wenn Eintrag keine Zeichenkette ist ... konvertieren
           if type( item ) != str:
               item = str( item )

           # Trennzeichen
           if type( sep ) == str:   # Einfaches Trennzeichen?
               if i > 0:
                   r += sep
           else:                    # list ... (Formatierte Laufvar verwenden)
               ii = add
               if sep[1] == False:
                  ii += 1
               if i == 0 and len( sep ) > 2:
                  r += sep[ 2 ] % ii
               else:
                  r += sep[ 0 ] % ii

           # Akt. Eintrag zufügen
           r += qm + item + qm

           # Anz erhöhen
           add += 1

       # Laufvar erhöhen
       i += 1

    # Ggf. Nachtrag
    if i > 0 and post != None:
        r += post

    return r
