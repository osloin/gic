#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# locale.py
#
# Sprachunterstützung: Deutsch
#
# osloin. 9/2018


################################################################################
#                                   Wörter
################################################################################
# Anweisungen / Befehlsform
ACTIVATE    = "Aktiviere"
ANALYSE     = "Analysiere"
EXECUTE     = "Führe aus"
INIT        = "Initalisiere"
LOAD        = "Lade"
SAVE        = "Speichere"
TERMINATE   = "Terminiere"
# Sonst
AND         = "und"
AND_1       = " " + AND + " "
ASSUMED     = "übernommen"
BRACKET     = "Klammer"
BRIGHTNESS  = "Helligkeit"
CHARS       = "Zeichen"
CMD         = "Befehl"
CMDS        = "Befehle"
CONTRAST    = "Kontrast"
CONTROLS    = "Steuerung"
DAY         = "Tag"                     # Zeit
DEFAULT     = "Vorgabewert"
DESTPATH    = "Zielverzeichnis"
DEMO        = "Demo"                    # Beispiel
ENDED       = "Beendet"
ERROR       = "Fehler"
EVENTS      = "Ereignisse"
EXEC_TIME   = "Ausführungszeit"
FILE        = "Datei"
FILESELECT  = "Dateiauswahl"
FILEFILTER  = "Dateifilter"
FILENAME    = "Dateiname"
FILETYPE    = "Dateityp"
FIRST       = "Erster"
FLOATPOINT  = "Kommawert"
FUNCTIONS   = "Funktionen"
GIC         = "gic"
GRAPHIC     = "Grafische"
IMAGE       = "Bild"
INVALID     = "Ungültiger"
LENGTH      = "Länge"
LOADED      = "geladen"
MISSING     = "fehlt"
MODULE      = "Modul"
MODULES     = "Module"
MONTH       = "Monat"                   # Zeit
NAME        = "Name"
NOT         = "nicht"
PARAMETER   = "Parameter"
PARAMETERS  = "Parameter"
PATH        = "Pfad"
PATHNAME    = "Pfadname"
PERCENT     = "Prozent"
POSITION    = "Position"
RANGE       = "Wertebereich"
RESULT      = "Ergebnis"
SCALE       = "Skalierung"
SCRIPTS     = "Skripte"
SEC         = "Sekunde"
SECS        = "Sekunden"
SEPARATOR   = "Trennzeichen"
SHORTFORM   = "Kurzform"
SOURCEFILE  = "Quelldatei"
SOURCEPATH  = "Quellverzeichnis"
STARTED     = "Gestartet"
STRING      = "Zeichenkette"
TARGET      = "Ziel"
TEMPLATE    = "Vorlage"
TYPE        = "Typ"
UNITS       = "Maßeinheiten"
VALUE       = "Wert"
VALUES      = "Werte"
VARS        = "Variable"
VARS_1      = "Variablen"
WORKINGDIR  = "Arbeitsverzeichnis"
YEAR        = "Jahr"                    # Zeit

################################################################################
#                                   Datentypen
################################################################################

# Infos für GTYP_... (z.B. GTYP_INT hat den Index 1 )
GTY_SHORT = 0 # Kurzinfo mit einem oder max 2 Zeichen
GTY_LONG  = 1 # Langinformation
GTYPES = [ ["?", "Undefiniert"],              #  0
           ["M", "Multi"],                    #  1
           ["z", "Ganzzahl"],                 #  2
           ["f", "Fließkomma"],               #  3
           ["l", "Logischer Wert"],           #  4
           ["D", "gic-Code (D)"],             #  5
           ["t", "Zeichenkette"],             #  6
           ["x", "Horizontaler Wert"],        #  7
           ["y", "Vertikaler Wert"],          #  8
           ["B", "Bezeichner o. Anfüh.-Z."],  #  9
           ["P", "Verzeichnis"],              # 10
           ["S", "Schriftart"],               # 11
           ["V", "M-Variable"],               # 12
           ["be","Befehl"],                   # 13
           ["bg","Grafischer Befehl"],        # 14
           ["r", "Steuerungsbefehl"],         # 15
           ["Q", "Interner Befehl"],          # 16
           ["A", "Transformation"],           # 17
           ["u", "Ersetzung"],                # 18
           ["gr","Modulgruppe"],              # 19
           ["mo","Modul"],                    # 20
           ["pl","Verzeichnisse"],            # 21
           ["fc","Funktion"],                 # 22
           ["li","Liste"],                    # 23
           ["d", "Dateiname"],                # 24
           ["G", "gic-Code"],                 # 25
           ["j", "Ja/Nein"],                  # 26
           ["o", "An/Aus"],                   # 27
           ["a", "Signal"],                   # 28
           ["fa","Farbwert"],                 # 29
           ["zp","Ganzzahl, positiv"],        # 30
           ["cb","Betroffene Zellen (Ganzzahl od. Text)"]# 31
         ]

################################################################################
#                              Mathematisches
################################################################################
FLOAT_SEP = ","         # Trennzeichen in Fließkommawerten

################################################################################
#                            Boolsches / logisches
################################################################################
TRUE      = "Wahr"      # z.B. GTYP_BOOL
FALSE     = "Falsch"    # z.B. GTYP_BOOL
AND       = "und"       # Vergleichsoperation
OR        = "oder"      # Vergleichsoperation
ON        = "An"        # z.B. GTYP_BOOL_OO
OFF       = "Aus"       # z.B. GTYP_BOOL_OO
YES       = "Ja"        # z.B. GTYP_BOOL_YN
NO        = "Nein"      # z.B. GTYP_BOOL_YN

################################################################################
#                               Maßeinheiten
################################################################################
CM  = "cm"

################################################################################
#                        Bezeichnung für Python-Typen
################################################################################
PYT_TYPE_BOOL   = "bool"
PYT_TYPE_INT    = "int"
PYT_TYPE_FLOAT  = "float"
PYT_TYPE_STR    = "str"
PYT_TYPE_LIST   = "list"
PYT_TYPE_DICT   = "dict"

################################################################################
#                            Variablen-Namen
################################################################################

VAR_F_EXT  = "ext"     # Aktuelle Dateierweiterung mit führendem "."
VAR_F_NAME = "name"    # Aktueller Dateiname (ohne Erweiterung)
VAR_F_PATH = "pfad"    # Aktueller Dateipfad mit abschliessenden Trennz.
VAR_W      = "breite"  # Bildbreite
VAR_H      = "hoehe"   # Bildhöhe
VAR_DPIX   = "dpix"    # DPIX
VAR_DPIY   = "dpiy"    # DPIY
VAR_XY     = "xy"      # Zuweisungsobjekt für Koordinatentransformation

# Bezeichnet die aktuelle Gimp-Vordergrundfarbe
VAR_COL_FORE = "vordergrund"
# Bezeichnet die aktuelle Gimp-Hintergrundfarbe
VAR_COL_BACK = "hintergrund"

# VAR_WORKDIR enthält das akt. Arbeitsverzeichnis
VAR_WORKDIR   = "arbeitsverz"
# VAR_WORKEXT enthält den akt. Arbeitstyp - also die Dateierweiterung (ohne
#             Punkt), z.B. "xcf"
VAR_WORKEXT   = "arbeitstyp"

# Über VAR_RESULT können Skripte ein Ergebnis zurückliefern
VAR_RESULT = "ergebnis"

# VAR_DEST enthält im Stapelbetrieb den Zieldateinamen. Ausserdem nach dem
# Speichern eines Bildes dessen Dateinamen
VAR_DEST = TARGET.lower() # "ziel"

# VAR_POS enthält im Stapelbetrieb eine laufende Nummer, wobei der Wert
# beim ersten Bild VAR_POS_INIT + 1, beim zweiten VAR_POS_INIT + 2 usw.
# ist.
# Ausserhalb des Stapelbetriebs ist dieser Wert VAR_POS_INIT
VAR_POS      = "posnr"
VAR_POS_INIT = 0

# VAR_RECENT: Hierunter wird die letzte Benutzereingabe gespeichert
VAR_RECENT       = "letzte_eingabe"

################################################################################
#                              Befehls-Namen
################################################################################
CMD_BRIGHTNCONTR    = "HEK"     # Helligkeit / Kontrast
CMD_BRIGHTNCONTR_2  = "/"       # (Aus Kompatibilitätsgründen)
CMD_DBG             = "DBG"     # Befehl zum Testen von Python-Code
CMD_DEL             = "EXB"     # Aktuelles Bild entfernen
CMD_DELI            = "EXI"     # Initalisierungswert entfernen
CMD_DPI             = "DPI"     # DPI
CMD_DUMP_CMDS       = "INB"     # Zeige Details über einen Befehl
CMD_DUMP_VARS       = "INV"     # Zeige alle Variablen
CMD_DUPLI           = "DPL"     # Dupliziert Aktuelles Bild
CMD_EXEC_AGAIN      = "WIE"     # Durchführen: Letzte Eingabe
CMD_EXEC_DIR        = "DUV"     # Durchführen: Auf Dateien in Verzeichnis
CMD_EXEC_DEMO       = "DEMO"    # Durchführen: Demo-Skript
CMD_EXEC_GIC        = "GIC"     # Durchführen: gic auf aktuelles Bild anwenden
CMD_EXEC_SCRIPT     = "DUS"     # Durchführen: Skript
CMD_EXEC_PATH       = "DUP"     # Durchführen: Pfadnamen (Gimp)
CMD_EXEC_STR        = "DUZ"     # Durchführen: Zeichenkette
CMD_GAMMA           = "GAM"     # Gamma
CMD_GOTO            = "GEH"     # Sprungbefehl
CMD_IMG_DATA_SET    = "ETS"     # Text in Bild einbetten
CMD_INFO            = "INF"     # Info
CMD_LYR_ALIGN       = "EBA"     # Ebene ausrichten
CMD_LYR_MERGE       = "EBZ"     # Ebenen zusammenfügen
CMD_LYR_MOVE        = "EBV"     # Ebene Verschieben
CMD_LYR_TRANS       = "EBT"     # Ebenentransparenz
CMD_LEVELS          = "FAR"     # Farbwerte anpassen
CMD_LEVELS_HI       = "WEI"     # Farbwert / Weißpunkt anpassen
CMD_LIST_ADD        = "LIA"     # Neuen Eintrag aufnehmen
CMD_LOAD            = "LAD"     # Bild laden
CMD_LOADLAYER       = "LAE"     # Bild als Ebene laden
CMD_NORMALIZE       = "NOR"     # Normalisieren
CMD_RASTER          = "RAS"     # Erzeugt ein Raster
CMD_RASTER_ADD      = "RAA"     # Die durch CMD_RASTER erzeugten Recht. anpassen
CMD_RGB             = "RGB"     # Vordergrundfarbe festlegen
CMD_ROTATE          = "DRE"     # Nach rechts drehen
CMD_SAVE            = "SPE"     # Speichert / Exportiert das aktuelle Bild
CMD_SAVE_NO         = "NEE"     # Verhindert Speicherung
CMD_SCALE           = "SKA"     # Skalierung
CMD_SATURATION      = "SÄT"     # Farbsättigung (und Helligkeit)
CMD_SEL             = "AUP"     # Auswahl aus Pfad (und Zuschneiden)
CMD_SELCOPY         = "AUK"     # Auswahl kopieren (Zwischenablage)
CMD_SELCROP         = "AUZ"     # Auf Auswahl zuschneiden
CMD_SELCROP_1       = ":"       # Auswahl aus Pfad und Zuschneiden (Wenn Befehl
                                # das Zeichen enthält)
CMD_SELCUT          = "AUA"     # Auswahl ausschneiden (Zwischenablage)
CMD_SELPASTE        = "AUE"     # Auswahl einfügen (Zwischenablage)
CMD_SELNONE         = "AUS"     # Auswahl aufheben
CMD_SELRCT          = "AUR"     # Auswahl, rechteckig
CMD_SELRCTRATIO     = "AUM"     # Max. Rechteck mit bestimmten Seitenverhältnis
CMD_SET             = "SET"     # Legt den Wert einer globalen Variablen fest
CMD_SETINIT         = "SEI"     # Initalisierungswert einer globalen Var festl.
CMD_SHARPEN         = "SCH"     # Schärfen
CMD_STOP            = "STOP"    # Programmausführung beenden
CMD_TRANS_SET       = "TRA"     # Transformierungsmodul zuweisen
CMD_TXT             = "TEX"     # Textausgabe
CMD_WBALANCE        = "AWA"     # Automatischer Weißabgleich

################################################################################
#                         Namen von Spezialbefehlen
################################################################################
CMD_DUMP_TEMP       = "KAJ"     # Befehlsübersicht in Temp ausgeben
CMD_FOR             = "XMAL"    # Schleife x-mal ausführen
CMD_IF              = "WENN"    # Bedingung
CMD_PARSE_1         = "PARSE_1" # Zerlegt gic-Folge in einzelne Teile
CMD_PARSE_2         = "PARSE_2" # Zerlegt gic in einzelne Teile
CMD_WHILE           = "SOLANGE" # Solange ausführen wie Bedingung erfüllt
CMD_XXX             = "XXX"     # Ausführungsverhinderung

################################################################################
#                              Funktionsnamen
################################################################################
FCT_CON_STR         = "zk"      # Wert in Zeichenkette konvertieren
FCT_EXISTS          = "da"      # Testen, ob etwas Bestimmtes da ist
FCT_IMG_DATA_GET    = "etl"     # Eingebetteten Text lesen
FCT_INT             = "ganz"    # Ganzzahl, z.B. wird aus 3,99 die 3
FCT_GET             = "hole"    # Einen numerische Information holen. z.B. Tag
FCT_SIN             = "sin"     # Sinusfunktion
FCT_SIZE            = "groesse" # Größe abfragen
FCT_SYSPATH         = "verz"    # Verzeichnis abfragen

################################################################################
#                         Kurzformen: Befehle
################################################################################
CMDS_SHORT          = { "A":CMD_SEL,
                        "D":CMD_ROTATE,
                        "G":CMD_GAMMA,
                        "I":CMD_INFO,
                        "K":CMD_SCALE,
                        "R":CMD_SELRCT,
                        "F":CMD_SATURATION,
                        "L":CMD_LOAD,
                        "O":CMD_EXEC_DEMO,
                        "S":CMD_SAVE,
                        "T":CMD_TXT,
                        "X":CMD_XXX,
                        "Z":CMD_SELCROP }

################################################################################
#                         Kurzformen: Variable
################################################################################
VARS_SHORT          = { "b":VAR_W,
                        "h":VAR_H }

################################################################################
#                                 ID-Infos
#                              MOD_ACTION_IDDOC
################################################################################

# Steuerungsbefehle
IDDOC_GOTO         = "Springe zu"
IDDOC_IF           = "Bedingung"
IDDOC_SET          = "Legt den Wert einer Variablen fest"
IDDOC_XXX          = ["Ausführungsverhinderung","Verhindert die Ausführung "\
                      "der nachfolgenden Abschnitte" ]

# Info zu Ids
IDDOC_BRIGHTNCONTR = "Helligkeit und Kontrast einstellen"
IDDOC_CON_STR      = "Konvertiert Wert in Zeichenkette"
IDDOC_CROP         = "Auf Auswahl zuschneiden"
IDDOC_DBG          = "Befehl zum Testen von Python-Code"
IDDOC_DEL          = ["Bild entfernen", "Ein über den LAD-Befehl geladenes "\
                      "sichtbares Bild kann hierüber wieder entfernen werde"\
                      "n. Das ist allerdings nur solange möglich, bis ein w"\
                      "eiteres mal LAD ausgeführt wird oder die akt. gic-Au"\
                      "sführung beendet wird. Für unsichtbare Bilder wird E"\
                      "XB autom. ausgeführt und muss i.d.R. nicht verwendet"\
                      " werden"]
IDDOC_DELG         = ["Initalisierungswert entfernen","Initalisierungswert ein"\
                      "er Variablen entfernen - Gegenstück zu SEI"]
IDDOC_DUMP_CMDS    = ["Befehlsdetails",
                      "Zeigt Detailinfos zu dem angegebenen Befehl o. Ä."]
IDDOC_DUMP_TEMP    = ["Befehlsübersicht nach Tmp",
                      "Gibt Befehlsübersicht als Datei 'uebersicht.txt' im Tem"\
                      "porärverzeichnis aus"]
IDDOC_DUMP_VARS    = "Zeige alle Variablen"
IDDOC_DPI          = "DPI-Wert festlegen"
IDDOC_DUPLI        = ["Akt. Bild intern duplizieren","Dupliziert das aktuel"\
                      "le Bild im Speicher und macht es zum aktuellen Bild,"\
                      " so dass sich alle nachfolgende Aktionen auf dieses "\
                      "(unsichtbare) Bild auswirken"]
IDDOC_EXEC_DIR     = ["Bilder in einem Verzeichnis behandeln", "Durchläuft all"\
                      "e Bilder im angegebenen Verzeichnis und wendet den ange"\
                      "gebenen gic-Code auf jedes Bild an. Während der Ausführ"\
                      "ung stehen die Variablen 'ziel' und 'posnr' zur Verfügu"\
                      "ng"]
IDDOC_EXEC_AGAIN   = "Letzte Eingabe wiederholen"
IDDOC_EXEC_GIC     = ["Bild-gic ausführen","Führt den im aktuellen Bild enthal"\
                      "tene Standard-gic aus. Wird kein entsprechender Code ge"\
                      "funden, wird die Ausführung normal fortgesetzt. Standar"\
                      "dmäßig wird durch den GIC-Befehl der Code, der im Pfadn"\
                      "amen einen 'gic'-Text enthält, ausgeführt"]
IDDOC_EXEC_DEMO     = ["gic-Skript ausführen","Führt eines der Demo-Skripte im"\
                       " Verzeichnis 'gic/demo/gics' aus. Wird kein Name angeg"\
                       "eben, wird eine Liste der vorhandenen Skripte ausgegeb"\
                       "en"]
IDDOC_EXEC_SCRIPT  = ["gic-Skript ausführen","Führt den in einer Datei enth"\
                      "altenen gic-Code aus. Wenn kein Verzeichnis angegebe"\
                      "n wurde, muss sie sich in 'gic/scripts' befinden"]
IDDOC_EXEC_PATH    = ["Ausführen von Gimp-Pfadnamen-qic","Führt den in einem o"\
                      "der mehreren Gimp-Pfadname enthaltenen gic-Code aus. Im"\
                      " vierten Parameter kann Code angegeben werden, der ausg"\
                      "eführt werden soll, wenn kein entsprechender Pfadname g"\
                      "efunden wurde"]
IDDOC_EXEC_STR     = "gic aus Zeichenkette ausführen"
IDDOC_EXISTS       = ["Namen überprüfen","Je nach Parameter kann hierüber über"\
                      "prüft werden, ob eine Var, eine Datei oder eine gültige"\
                      " Listenposition vorhanden ist"]
IDDOC_FOR          = ["X-mal-Schleife", "Schleife x-mal ausführen"]
IDDOC_GAMMA        = "Gamma-Wert festlegen"
IDDOC_GET          = ["Etwas ermitteln","Je nach Parameter kann hierüber eine "\
                      "numerische Information abgefragt werden: Die aktuelle Z"\
                      "eit (Sek, Minute, Stunde, Tag, Monat oder Jahr), eine Z"\
                      "ufallszahl oder die gic-Version mit 4 Stellen (0 ... 3)"\
                      ". Ausserdem kann ein Sekundenschlaf durchgeführt werden"]
IDDOC_IMG_DATA_GET = "Eingebetteten Text lesen"
IDDOC_IMG_DATA_SET = ["Text in Bild einbetten","Bettet einen (unsichtbaren)"\
                      " Text in das aktuelle Bild ein. Dieser Text kann dur"\
                      "ch Bildbearbeitung oder komprimierter Speicherung ze"\
                      "rstört werden. Führt zu Verlust von Bildinformationen"]
IDDOC_INFO         = ["Info ausgeben",
                      "Hier eine Information ausgeben. Beispielsweise das Erge"\
                      "bnis einer mathematischen Berechnung, ein Funktionserge"\
                      "bnis oder den Wert einer Variablen"]
IDDOC_INT          = ["Ganzzahl liefern","Ganzzahl liefern (z.B 3,67 wird z"\
                      "u 3)"]
IDDOC_LEVELS       = "Die drei Farbwerte anpassen"
IDDOC_LEVELS_HI    = "Hohen Farbwert anpassen"
IDDOC_LIST_ADD     = "Wert in Liste eintragen"
IDDOC_LOAD         = ["Bild laden","Lädt ein Bild sichtbar oder unsichtbar"]
IDDOC_LOADLAYER    = "Bild als Ebene laden"
IDDOC_LYR_ALIGN    = "Ebene ausrichten"
IDDOC_LYR_MERGE    = "Alle Ebenen zusammenfügen"
IDDOC_LYR_MOVE     = "Ebene verschieben"
IDDOC_LYR_TRANS    = "Ebenentransparenz einstellen"
IDDOC_MASK         = ["Auswahl aus Pfad",
                      "Auswahl aus Pfad und ggf. Zuschneiden. Eine alternative"\
                      " Schreibweise ist ein beliebiger Name, der einen Doppel"\
                      "punkt beinhaltet, z.B. 16:9"]
IDDOC_MASKCROP_1   = "Auswahl aus Pfad und Zuschneiden"
IDDOC_NORMALIZE    = "Farben normalisieren"
IDDOC_PARSE        = "Zerlegt gic-Zeichenkette"
IDDOC_RASTER       = ["Raster","Erzeugt ein Raster aus Rechtecken, das dann fü"\
                      "r unterschiedliche Zwecke genutzt werden kann. Das erze"\
                      "ugt Raster kann über den RAA-Befehl weiter angepasst we"\
                      "rden. Werden die Parameter 4 bis 7 auf Null gesetzt, wi"\
                      "rd ein Raster für die gesamte Fläche des aktuellen Bild"\
                      "es erzeugt."]
IDDOC_RASTER_ADD   = "Rasteranpassung"
IDDOC_RCT          = ["Rechteckige Auswahl","Erzeugt eine rechteckige Auswahl "\
                      "mit den angegebenen Abmessungen an der angegebenen Posi"\
                      "tion"]
IDDOC_SELRCTRATIO  = ["Auswahl mit max. Seitenverhältnis","Maximalgroße Aus"\
                      "wahl mit bestimmten Seitenverhältnis. z.B. 16:9"]
IDDOC_RGB          = ["Farbe festlegen","Farbe festlegen, die dann einem Befeh"\
                      "l als Parameter übergeben werden kann."]
IDDOC_ROTATE       = ["Drehen","Bild, Auswahl oder Ebene nach rechts drehen"]
IDDOC_SAVE         = ["Aktuelles Bild speichern / exportieren","Aktuelles B"\
                      "ild speichern / exportieren. Falls der angegebene Da"\
                      "teiname schon vergeben ist, wird er erweitert."]
IDDOC_SAVE_NO      = ["Speichern verhindern","Verhindert, dass der (nächste"\
                      ") SPE-Befehl seine Arbeit verrichtet" ]
IDDOC_SCALE        = ["Skalierung","Bild od. Ebene (wenn ein Ebenenname angege"\
                      "ben wurde) auf eine bestimmte Höhe und Breite bringen. "\
                      "Nur einen Wert auf 0 setzen, um Seitenverh. beizubehalt"\
                      "en"]
IDDOC_SATURATION   = ["Farbsättigung (und Helligkeit)","Farbsättigung (und Hel"\
                      "ligkeit) des aktuellen Bildes oder einer (sofern vorhan"\
                      "denen) Auswahl einstellen"]
IDDOC_SELCOPY      = "Auswahl kopieren (Zwischenablage)"
IDDOC_SELCUT       = "Auswahl ausschneiden (Zwischenablage)"
IDDOC_SELNONE      = "Auswahl aufheben"
IDDOC_SELPASTE     = "Auswahl einfügen (Zwischenablage)"
IDDOC_SETG         = ["Initalisierungswert festlegen","Initalisierungswert ein"\
                      "er Variablen festlegen. Kann mit EXI wieder entfernt we"\
                      "rden"]
IDDOC_SHARPEN      = "Schärfen"
IDDOC_SIN          = "Sinuswert berechnen"
IDDOC_SIZE         = ["Größe abfragen","Größe einer Liste oder einer Ebene "\
                      "abfragen"]
IDDOC_STOP         = "gic-Ausführung beenden"
IDDOC_SYSPATH      = "Eines der gic-Verzeichnisse abfragen"
IDDOC_TRANS_SET    = ["Transformierungsmodul zuweisen", "Transformierungsmodul"\
                      " an eine ID oder einen ID-Parameter zuweisen"]
IDDOC_TXT          = ["Textausgabe",
                      "Gibt einen Text in der aktuellen Zeichenfarbe aus"]
IDDOC_WBALANCE     = "Automatischer Weißabgleich"
IDDOC_WHILE        = ["Schleife",
                      "Schleife ausführen, solange Bedingung erfüllt ist"]

# Info zu Ids / Variablen
IDDOC_VAR_F_EXT    = "Dateierweiterung des akt. Bildes"
IDDOC_VAR_F_PATH   = "Pfad des akt. Bildes"
IDDOC_VAR_F_NAME   = "Dateiname des akt. Bildes"
IDDOC_VAR_W        = "Breite des akt. Bildes"
IDDOC_VAR_H        = "Höhe des akt. Bildes"
IDDOC_VAR_DPIX_H   = "X-DPI des akt. Bildes"
IDDOC_VAR_DPIY_H   = "Y-DPI des akt. Bildes"
IDDOC_VAR_WORKDIR  = ["Arbeitsverzeichnis","Hierüber kann ein Arbeitsverzeichn"\
                      "is festgelegt werden, so daß dann bei Dateiangaben auf "\
                      "eine Pfadangabe verzichtet werden kann." ]
IDDOC_VAR_WORKEXT  = ["Arbeitstyp","Hierüber kann ein Arbeitstyp (Dateierweite"\
                      "rung ohne Punkt) festgelegt werden, so daß dann bei Dat"\
                      "eiangaben die Erweiterung nicht mehr angegeben werden m"\
                      "uss." ]
IDDOC_VAR_XY       = ["Maßeinheit zuweisen","Maßeinheit der Parameter 'Horizon"\
                      "taler Wert' und 'Vertikaler Wert' festlegen (z.B. xy = "\
                      "Prozent). Der ursprüngliche Zustand kann über xy=std wi"\
                      "ederhergestellt werden." ]

################################################################################
#                            MOD_ACTION_PDOC
#                            Parameter-Infos
################################################################################
PDOC_BATCH        = ["gic",
                     "Quellpfad",
                     ["Filter", "Filter, z.B. '*.jpg'" ],
                     "Zielpfad",
                     ["Ziel-Dateityp", "Ziel-Dateityp, z.B. 'jpg'" ],
                     ["Speicher-gic","Speicher-gic, z.B. 'SPE ziel'"] ]
PDOC_BRIGHTNCONTR = [ BRIGHTNESS, CONTRAST ]
PDOC_CON_STR      = [["Ausgangswert","Wert, der konvertiert werden soll"],
                     ["Formatierung","Ganzzahl, um Ganzahl auf Breite zu br"\
                      "ingen. Oder pythonkonforme Angabe, z.B '%d' "]]
PDOC_DELG         = ["Name"]
PDOC_DPI          = ["DPI"]
PDOC_DUMP_CMDS    = ["Name des Befehls"]
PDOC_EXEC_DEMO    = [["Dateiname","Dateiname ohne Dateiendung"]]
PDOC_EXEC_SCRIPT  = [["Dateiname","Dateiname ohne Dateiendung"],
                     ["Pfad","Pfadname, wenn abweichend von Std."]]
PDOC_EXEC_PATH    = ["Filter",
                     "Filtermodus",
                     "Auswählen",
                     "gic" ]
PDOC_EXEC_STR     = ["Zeichenkette mit gic-Code"]
PDOC_EXISTS       = ["Name, der überprüft werden soll","Was ist der Name"]
PDOC_FOR          = ["Anzahl der durchläufe","Anzahl der betroffenen Einträge"]
PDOC_GET          = ["Was ermittelt werden soll",
                     ["Num. Parameter","Parameter bei 'version' und 'zufall'"]]
PDOC_IF           = ["Bedingung","Anzahl der betroffenen Einträge"]
PDOC_IMG_DATA_GET = ["Eingebetteten Text lesen"]
PDOC_IMG_DATA_SET = ["Text, der eingebettet werden soll"]
PDOC_INFO         = ["Ausgabe"]
PDOC_LEVELS       = ["Niedriger Wert","Gamma","Hoher Wert"]
PDOC_LYR_ALIGN    = ["Ebene","Ebene2","H-Modus","V-Modus","H-Abstand",
                     "V-Abstand"]
PDOC_LYR_MOVE     = ["Ebenenname","Neue X-Position","Neue Y-Position"]
PDOC_LYR_TRANS    = ["Ebene","Transparenz"]
PDOC_GAMMA        = ["Gamma"]
PDOC_GOTO         = ["Text"]
PDOC_LEVELS_HI    = ["Hoher Wert"]
PDOC_LIST_ADD     = [["Liste","Liste, die den Wert bekommen soll"],
                     ["Wert","Wert, der aufgenommen werden sollen"]]
PDOC_LOAD         = ["Bilddateiname","Bild anzeigen"]
PDOC_LOADLAYER    = ["Dateiname","x-Koord.","y-Koord.","Breite","Höhe",
                     ["Name","Name der neuen Ebene"]]
PDOC_PARSE        = ["gic-Code"]
PDOC_TRANS_SET    = ["Modulname","Befehlsname","Parameterindex 0..x, -1: ID",
                     "Transf.-Param."]
PDOC_RASTER       = ["Prefix","Spalten","Zeilen","x","y","Breite","Höhe"]
PDOC_RASTER_ADD   = ["Prefix","X-Anpassung","Y-A.",
                     "Breitena.","Höhena."]
PDOC_RCT          = ["x-Koord.","y-Koord.","Breite","Höhe","Modus",
                     "Zuschneiden"]
PDOC_SELRCTRATIO  = ["a","b","Zuschneiden"]
PDOC_RGB          = ["Variable","Rot","Grün","Blau"]
PDOC_ROTATE       = ["Winkel","Ebene"]
PDOC_SATURATION   = ["Sättigung","Helligkeit"]
PDOC_SAVE         = ["Dateiname",
                     ["gic","Code, der vor dem Speichern ausgeführt werden sol"\
                      "l"],
                     "Qualität (jpg)","Kommentar (jpg)"]
PDOC_SCALE        = ["Breite","Höhe","Ebene","Interpolation"]
PDOC_SEL          = ["Zuschneiden"]
PDOC_SELCUT       = ["Name"]
PDOC_SELCOPY      = ["Name"]
PDOC_SELPASTE     = ["x-Koord.", "y-Koord.", "Name"]
PDOC_SET          = ["Name","Wert","lokal"]
PDOC_SETG         = ["Name","Wert","Sofort"]
PDOC_SHARPEN      = ["Prozentualer Schärfegrad"]
PDOC_SIZE         = ["Name","Was ist Name"]
PDOC_STOP         = ["Abbruchgrund"]
PDOC_SYSPATH      = ["Verzeichnis"]
PDOC_TXT          = ["x-Koord.","y-Koord.","Text","Größe","Schriftart",
                     "Rahmengröße","Name","Farbe"]
PDOC_XXX          = ["Anzahl der betroffenen Einträge"]
PDOC_XY           = ["Modul, das eine Konvertierung anbietet"]
PDOC_WHILE        = ["Bedingung","Anzahl der betroffenen Einträge"]

################################################################################
#                            MOD_ACTION_PDEF
#                      Vorgabewerte für Parameter
################################################################################
PDEF_EXEC_DIR     = [ None, None, "*.*", "", "jpg", "SPE ziel" ]
PDEF_TXT          = [ 1.0, 1.0, "", 20.0, "Sans", 0, "", "" ]

################################################################################
#                            MOD_ACTION_PVAL
#                            Parameter-Werte
################################################################################
PVAL_GET          = ["sek","min","stunde","tag","monat","jahr","schlaf",
                     "version","zufall"]
PVAL_EXEC_PATH_1  = ["gleich","ungleich","alle","exakt","nexakt" ]
PVAL_EXISTS       = ["var","datei","pos"]
PVAL_LYR_ALIGN_2  = ["nein","mitte","li_in","re_in","li_au","re_au"]
PVAL_LYR_ALIGN_3  = ["nein","mitte","ob_in","un_in","ob_au","un_au"]
PVAL_RCT_4        = ["zufuegen","abziehen","ersetzen","ueberschneiden"]
PVAL_SET_2        = ["lokal","global"]
                    # , NOHALO (Gimp > 2.8)
PVAL_SCALE_3      = ["nein","linear","cubic","lanczos"]
PVAL_SIZE         = ["liste","breite","hoehe"]
PVAL_SYSPATH_0    = ["temp","gic","demo"]


################################################################################
#                         Zusätzliches einzelner Befehle
################################################################################

# CMD_DUMP_CMDS
# Überschrift eines Abschnitts
DUMP_CMDS_INF = "\n%s...\n\n"
# Zusätzlicher Text in der Überschrift
DUMP_CMDS_INF_1 = " und ihre Parameter "
# Text in der Überschrift
DUMP_CMDS_INF_2 = "Demo-Skripte ... "
DUMP_CMDS_INF_3 = "Hinweis: Um beispielsweise Detailinfos zum %s-Befehl zu erh"\
                  "alten: ?%s eingeben." % ( CMD_SCALE, CMD_SCALE )

# CMD_DUMP_TEMP
# Dateiname
DUMP_TEMP_FILE =  "uebersicht.txt"

# VAR_XY - Standard wiederherstellen
VAR_XY_STD = "std"

# CMD_RASTER
# Namen, die generiert werden (Vor diesen Namen wird das angegebene Prefix
# gesetzt)
RASTER_X          = "x"
RASTER_Y          = "y"
RASTER_W          = "b"
RASTER_H          = "h"

################################################################################
#                     Namen der Bildereignisfunktionen
################################################################################
IMG_EVT_GIC = "gic"    # gic auf Bild anwenden

################################################################################
#                           Menu und Dialog-Einträge
################################################################################
# Eintrag im Hauptmenü
MNU_TOP                 = "gic"

# Untermenu in
MNU_CREATE              = "Neu"

# gic_mnu_doitagain
MNU_DOITAGAIN_LBL       = "Wiederholen"
MNU_DOITAGAIN_INFO      = "Wiederholt die letzte Eingabe"

# gic_mnu_path
MNU_PATH_LABEL          = "Pfadname"
MNU_PATH_INFO           = "Führt gic im aktiven Pfadnamen aus"

# gic_dlg_create_cmd
DLG_CREATE_CMD_LABEL    = "Befehl"
DLG_CREATE_CMD_INFO     = "Befehl"
DLG_CREATE_CMD__COMM    = "Kommentar"
DLG_CREATE_CMD__COMM_V  = "Ein Meldungsfenster darstellen"
DLG_CREATE_CMD__PARAM   = PARAMETER + " (str,int,float,bool)"
DLG_CREATE_CMD__PY_CODE1="Python-Code 1."
DLG_CREATE_CMD__PY_CODE2="Python-Code 2."
DLG_CREATE_CMD__PY_CODE3="Python-Code 3."

# gic_dlg_create_mnu
DLG_CREATE_MNU_LABEL    = "Menüpunkt"
DLG_CREATE_MNU_INFO     = "Menüpunkt"
DLG_CREATE_MNU__CMD     = "(1),(2) Befehl  (3),(4) Name"
DLG_CREATE_MNU__CMD_V   = CMD_EXEC_SCRIPT
DLG_CREATE_MNU__PARAM   = "(1) Param.-Werte (2) Vorgabewerte (3) Code"
DLG_CREATE_MNU__PARAM_V = "script"
DLG_CREATE_MNU__MODE    =  "Für was soll ein Menüpunkt erzeugt werden"
DLG_CREATE_MNU__MODE_V  =  ["(1) Befehl mit festen Parametern",
                            "(2) Befehl mit variablen Parametern",
                            "(3) Code",
                            "(4) Code aus aktuellem Gimp-Pfad"]
DLG_CREATE_MNU__MTOP    = "Erzeuge im Menü"
DLG_CREATE_MNU__MTOP_V  = "Eigenes"
DLG_CREATE_MNU__MNAME   = "Name des neuen Menüpunkts"
DLG_CREATE_MNU__MNAME_V = "Führe das Skript 'script.gic' aus"
DLG_CREATE_MNU_MSG      = "Datei zur Menüerzeugung wurde in '%s' gespeichert\n"\
                          "Zur Aktivierung ist ein Neustart von Gimp nötig!"

# gic_dlg_direct
DLG_DIRECT_LABEL        = "Eingabe"
DLG_DIRECT_INFO         = "Direkteingabe"
DLG_DIRECT__SGIC        = "gic ..."
DLG_DIRECT__SGIC_V      = ""

# gic_dlg_file
DLG_FILE_LABEL          = "Skript"
DLG_FILE_INFO           = "Führt ein gic-Skript aus"
DLG_FILE__FILENAME      = FILE
DLG_FILE__FILENAME_V    = ""

# gic_dlg_folder
DLG_FOLDER_INFO         = "Wendet gic auf Bilder in einem Verzeichnis an"
DLG_FOLDER_LABEL        = "Verzeichnis"
DLG_FOLDER__SGIC        = "gic ..."
DLG_FOLDER__EXTDEST     = "Dateityp für Ziel"

################################################################################
#                             Ereignisse / evt()
################################################################################
# Mehrfachverwendete Texte in Fehlerbeschreibungen
ERR_TXT_RUNTIME     = "Laufzeitfehler"
ERR_TXT_PARAM       = "Ungültiger Funktionsparameter"
ERR_TXT_LABEL       = "Unbekannter Bezeichner"
ERR_TXT_TYPE        = "Ungültiger Typ"
ERR_TXT_MOD_VAL_ERR = "Modul hat ungültige Daten geliefert"
ERR_TXT_MOD_VAL_NO  = "Modul hat keine Daten geliefert"

# Fehlerbeschreibungen
ERR_001  = "Dieser Variablen kann kein Wert zugewiesen werden"
ERR_003  = "Kein auswertbarer Code vorhanden"
ERR_005  = "Fehlende Modulunterstützung"
ERR_007  = "Falsches Sprungziel"
ERR_006  = "Kein Vorgabewert für Parameter vorhanden"
ERR_013  = "Fehler beim Laden eines Moduls"
ERR_015  = "Dieser Eintrag kann nicht entfernt werden"
ERR_018  = "Fehler bei Beenden eines Moduls"
ERR_019  = "Zu viele Parameter"
ERR_023  = "Ungültiger Modulname"
ERR_024  = "Statischen Werten kann kein Wert zugewiesen werden"
ERR_025  = "Diese ID ist schon vergeben"
ERR_029  = "Für diesen Befehl konnten keine Parameter erzeugt werden"
ERR_032  = "Listenparameter enthält ungültigen Eintrag"
ERR_033  = "Quell und Zielpfad dürfen nicht identisch sein"
ERR_034  = "Ungültige Zellenposition"
ERR_035  = "Ungültige Anzahl an Schleifeneinträgen angegeben"
ERR_036  = "Ungültiger Bezeichner"
ERR_038  = "Ungültiger Listenindex"
ERR_039  = "Es konnte kein Dateiname zum Speichern ermittelt werden"
ERR_040  = "Blockende nicht gefunden"
ERR_041  = "Eckige Klammern sind für diesen Bezeichner nicht zulässig"

# Ereignisbeschreibungen
EVT_000  = "" # Nachricht
EVT_001  = "Ausführung wurde gestartet"
EVT_002  = "Ausführung beendet"
EVT_003  = "Autoexec wurde ausgeführt"
EVT_004  = "Ein Befehl wurde ausgeführt"
EVT_005  = "Fehler bei Befehlsausführung durch Modul"
EVT_006  = "Wertzuweisung"
EVT_007  = "Initalisierung"
EVT_008  = "Terminierung"
EVT_009  = "Transformierungsmodul zugewiesen"
EVT_010  = "Transformierungsmodulzuweisung aufgehoben"
EVT_011  = "Verzeichnis wird nach Modulen durchsucht"
EVT_012  = "Modul wird geladen"
EVT_013  = "Modul wurde erfolgreich initalisiert"
EVT_014  = "Ressourcen Freigeben"
EVT_015  = "Initalisierungswert für eine Variable festlegen"
EVT_016  = "Initalisierungswert für eine Variable entfernen"
EVT_017  = "Datei speichern"
EVT_018  = "Beim Speichern einer Datei ist ein Fehler aufgetreten"
EVT_019  = "Datei laden"
EVT_020  = "Beim Laden einer Datei ist ein Fehler aufgetreten"
EVT_021  = "Ausführung wurde abgebrochen"
EVT_022  = "Abbruch durch %s-Befehl" % CMD_STOP
EVT_023  = "Der Ausdruck konnte nicht ausgewertet werden"
EVT_024  = "Rekursion"
EVT_025  = "Rekursion beendet"
EVT_026  = "Neuer Ordner wird angelegt"
EVT_027  = "Stapelverarbeitung"
EVT_028  = "Schleife gestartet"
EVT_030  = "Schleife beendet"
EVT_031  = "Leerer Eintrag"
EVT_032  = "Unbekannte ID"
EVT_033  = "Speichern verhindert"
EVT_034  = "Benutzereingabe ausführen"
EVT_035  = "Benutzereingabe ausgeführt"
EVT_036  = "Letzte Eingabe wiederholen"
EVT_037  = "Neue Datei wurde angelegt"
EVT_038  = "Keine Dateien für Stapelbetrieb gefunden"

EVT_100  = "Nachbehandlung für Dateinnamen"
EVT_101  = "Grafischen Befehl ausführen"

EVT_514  = "Bild soll gespeichert werden"
EVT_521  = "Bild geladen"
EVT_523  = "Gimp-Pfad wird ausgeführt"
EVT_528  = "Bild aus Speicher entfernt"
EVT_529  = "Bild wird gespeichert"
EVT_530  = "Bild gespeichert"
EVT_537  = "Vorbehandlung eines Bildes im Stapelbetrieb"
EVT_538  = "Bild wurde im Speicher dupliziert"

################################################################################
#                               Nachrichtentexte
################################################################################
MSG_STYLE_0        = " ..."        # Optisches
MSG_STYLE_1        = " ...\n"      # Optisches
MSG_ID_TAKEN       = "Der Name '%s' ist schon vergeben!"
MSG_AUTO_CREATED   = "Autom. generiert durch '%s'"
