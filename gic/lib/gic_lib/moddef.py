#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# osloin. 1/2019
#
# Deklarationen für Module


import locale   as LOC



# Schlüssel Gimp-spezifischer Einträge
VAR_DISPLAY     = "_DISPLAY"   # (gimp)

# Autom. Ausführung bei LOC.CMD_EXEC_GIC
VAR_POST_GIC    = "_POST_GIC"  # (sgic)

# Erzeugungsstatus des aktuellen Bildes
# 0: Unbekannt (durch ext. Aufruf)
# 1: Sichtbar geladen durch gic
# 2: Unsichtbar geladen durch gic
# 3: Unsichtbare Kopie eines anderen Bildes, erzeugt durch gic
VAR_IMG_STATE   = "_IMG_STATE" # (int)

# Das zuletzt durch gic-Code erzeugt (auch durch laden oder kopieren) Bild
# (kann aber schon entfernt worden sein)
VAR_IMG         = "_VAR_IMG"

# Dateiname des zuletzt duplizierten Bildes (kann aber schon entfernt worden
# sein)
VAR_IMG_FNAME   = "_VAR_IMG_FNAME"

# Unter welchem Index in 'VAR_RUN_PARA' das 'image' gespeichert wird
VAR_RUN_PARA_IMG = 0

# Unter welchem Index in 'VAR_RUN_PARA' das 'drawable' gespeichert wird
VAR_RUN_PARA_DRW = 1

# (True) Abbruch-Signal für Speichern
VAR_SAVE_NO     = "_SAVE_NO"
