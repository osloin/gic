#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Gimp-Hilfsfunktionen
#
# buffer_delete()       Entfernt den angegebenen Puffer
# scale()               Aktuelles Bild skalieren
# scale_layer()         Ebene skalieren


from gimpfu import *

import fcts     as fcts_
import convert  as con_


################################################################################
# Entfernt den angegebenen Puffer, der z.B. durch gimp_edit_named_copy()
# angelegt wurde.
#
# buffer  (str) Puffername
#
# Ergebnis: True, wenn erfolgreich
################################################################################
def buffer_delete( buffer ):

    try:

        # Unser Ergebnis
        result = False

        # Puffer-Liste abfragen. Als Filter wird der Puffername angegeben
        num_buffers, buffer_list = pdb.gimp_buffers_get_list( buffer )

        # Puffer löschen, wenn vorhanden ...
        if num_buffers > 0:
            pdb.gimp_buffer_delete( buffer )
            result = True

        # Ergebnis
        return result

    except Exception as e:

        return False


################################################################################
# Skaliert das aktuelle Bild (ggf. unter Beibehaltung des aktuellen Seiten-
# verhältnisses )
#
# image     (gimp) Bild
# w         (int)  Bild auf diese Breite bringen (*)
# h         (int)  Bild auf diese Höhe bringen (*)
#                  (*) Wird nur einer der beiden Werte auf 0 gesetzt, wird
#                      versucht, das Seitenverhältnis beizubehalten
# interpol  (int)  Siehe pdb.gimp_context_set_interpolation()
#
# Ergebnis
################################################################################
def scale( image, w, h, interpol = 3 ):

    # Es mindestens einer der beiden Werte gesetzt sein ...
    if w != 0 or h != 0:

         # Seitenverhältnis beibehalten?
         # Wenn 'w' oder 'h' gleich 0 sind, berechnen wir den anderen Wert
         # automatisch.
         if w == 0 or h == 0:

            # Seitenverhältniss
            r = float( pdb.gimp_image_width(  image ) ) / \
                float( pdb.gimp_image_height( image ) )

            if w == 0:      # Breite berechnen?
               w = h * r
            else:           # Höhe berechnen ...
               h = w / r

         # Skalierung durchführen
         # gimp 2.10 INTERPOLATION_NOHALO  (3) 4 Werte
         # gimp 2.8  INTERPOLATION-LANCZOS (3) 3 Werte
         pdb.gimp_context_set_interpolation( interpol )
         pdb.gimp_image_scale( image, w, h )



################################################################################
# Skaliert eine Bild-Ebene (ggf. unter Beibehaltung des aktuellen Seiten-
# verhältnisses )
#
# image      (gimp) Bild
# layer_name (str)  Ebenename
# w          (int)  Ebene auf diese Breite bringen (*)
# h          (int)  Ebene auf diese Höhe bringen (*)
#                   (*) Wird nur einer der beiden Werte auf 0 gesetzt, wird
#                       versucht, das Seitenverhältnis beizubehalten
# interpol   (int)  Siehe pdb.gimp_context_set_interpolation()
#
# Ergebnis
################################################################################
def scale_layer( image, layer_name, w, h, interpol = 3 ):

    # Ebene für Ebenennamen holen
    layer = pdb.gimp_image_get_layer_by_name( image, layer_name )

    if layer != None:

        # Interpolationseinstellung
        pdb.gimp_context_set_interpolation( interpol ) # 3:INTERPOLATION-LANCZOS

        # Seitenverhältnis beibehalten?
        # Wenn 'w' oder 'h' gleich 0 sind, berechnen wir den anderen Wert
        # automatisch.
        if (w == 0 or h == 0) and (w != 0 or h != 0 ):

            # Seitenverhältniss
            r = float( pdb.gimp_drawable_width(  layer ) ) / \
                float( pdb.gimp_drawable_height( layer ) )

            if w == 0:      # Breite berechnen?
               w = h * r
            else:           # Höhe berechnen ...
               h = w / r

        # Positionwiederherstellung behandeln
        xPosRestore = layer.offsets[ 0 ]        # Koordinate sichern
        yPosRestore = layer.offsets[ 1 ]        # Koordinate sichern

        # Skalieren
        pdb.gimp_layer_scale( layer, int( w ), int( h ), FALSE ) # local_origin

        # ggf. die ursprüngliche Position wiederherstellen
        pdb.gimp_layer_set_offsets( layer, xPosRestore, yPosRestore )

        # (Nachfolgender Eintrag ist notwendig, da L. manchmal unsichtbar wurde)
        pdb.gimp_item_set_visible( layer, TRUE )
