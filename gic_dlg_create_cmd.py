#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Gimp-plugin
#
# Ein Dialogfenster, um einen neuen gic-Befehl / Neues Modul zu erzeugen
#
# oli.s. 9/2018


import sys
import os

# Suchpfad für import erweitern ...
sys.path.append( sys.argv[ 0 ][ : sys.argv[ 0 ].rfind( os.sep ) ] +
				 os.sep + "gic" + os.sep + "lib" )

from gimpfu import *

import gic_lib.interpreter as gic
import gic_lib.locale      as LOC
import gic_lib.files       as fil_
import gic_lib.code        as cod_



# Dateiname der Vorlage
TEMPL_FILENAME = "module.py"

# Zeichenkette, die in der Vorlage ersetzt werden soll
TEMPL_NAME = "DUMMY"



################################################################################
# Hauptfunktion
#
# 'name'          (str) Name des neuen Befehls
#                       Es wird eine Datei mit diesem Namen und der Endung '.py'
#                       erzeugt
# 'comment'       (str) Kommentar zum neuen Befehl
# 'param'         (str) Auflistung der Parameter. z.B. "int,str"
# 'py_code1'      (str) Eine Zeile Pythoncode
# 'py_code2'      (str) Noch eine Zeile Pythoncode
# 'py_code3'      (str) Und noch eine Zeile Pythoncode
################################################################################
def gic_dlg_create( name, comment, param, py_code1, py_code2, py_code3 ):

    # Überprüfen, ob ID-Name schon vergeben
    if gic.id_doc( name ) != None:
        raise RuntimeError( LOC.MSG_ID_TAKEN % name )

    # Pfad mit Dateinamen der Vorlage
    template = gic.path_get( gic.FOLDER_TEMPLATES ) + TEMPL_FILENAME

    # Vorlage laden
    f = open( template, "r")
    codeOld = f.read()
    f.close()

    # Vorlage anpssen
    if len( codeOld ) > 0:  # Vorlage da?

      # Zielpfad
      path = gic.path_get( gic.FOLDER_MODULES_MY )
      if os.path.exists( path ) == False:     # Nicht da?
         fil_.create_folder( path )            # Ordner anlegen

      # Namen der Zieldatei ermitteln
      target = path + name.lower() + "." + gic.MOD_EXT_MOD

      # Freien Dateinamen ermitteln
      target = fil_.unused( target )

      # Modulname / MOD_ACTION_NAME
      # Überall das "DUMMY" gegen den Namen ersetzen
      mod_name = name.lower() + "_" + gic.MOD_EXT_MOD
      code = codeOld.replace("\"" + TEMPL_NAME + "\"","\""+ mod_name +"\"")

      # Initalisierungsergebnis bei MOD_ACTION_INIT ersetzen
      code = code.replace("None#<--","[\""+ name +"\"]#")

      # Kommentar bei MOD_ACTION_IDDOC ersetzen
      code = code.replace("\"?\"#<--","\""+ comment +"\"#")

      # ggf. die Parameter bei MOD_ACTION_PTYPE ersetzten
      if len( param ) > 1:

          # Python-Typen in GTYP konvertieren
          param = param.replace( "str",   "MOD_PTYP_STR"  )
          param = param.replace( "int",   "MOD_PTYP_INT"  )
          param = param.replace( "float", "MOD_PTYP_FLOAT")
          param = param.replace( "bool",  "MOD_PTYP_BOOL" )

          # Code ersetzen
          code = code.replace("[]#<--","["+ param +"]#")

      # Python-Code zusammenstellen
      code_ins = [py_code1]
      if py_code2 != "":
          code_ins.append( py_code2 )
      if py_code3 != "":
          code_ins.append( py_code3 )

      # Python-Code ersetzen/einfügen
      code = cod_.py_replace( code, "#" + TEMPL_NAME + "1#", code_ins )

      # Neue Datei erzeugen
      pdb.gimp_message( "%s ''%s'" % (LOC.SAVE,target) )
      f = open( target, "w")
      f.write( code )
      f.close()

      # Neue Datei ausführbar machen
      fil_.make_exec( target )


# Gimp-Registrierung
register(
    "python_fu_gic_dlg_create",
    LOC.DLG_CREATE_CMD_INFO,
    LOC.DLG_CREATE_CMD_INFO,
    "osloin",
    "osloin",
    "2018",
    LOC.DLG_CREATE_CMD_LABEL,
    "",
    [
        (PF_STRING, "name", \
                    LOC.NAME, \
                    "BOX"),
        (PF_STRING, "comment", \
                    LOC.DLG_CREATE_CMD__COMM, \
                    LOC.DLG_CREATE_CMD__COMM_V ),
        (PF_STRING, "param", \
                    LOC.DLG_CREATE_CMD__PARAM, \
                    "str"),
        (PF_STRING, "py_code1",\
                    LOC.DLG_CREATE_CMD__PY_CODE1,\
                    "pdb.gimp_message_set_handler( MESSAGE_BOX )"),
        (PF_STRING, "py_code2",\
                    LOC.DLG_CREATE_CMD__PY_CODE2,\
                    "pdb.gimp_message( param[0] )"),
        (PF_STRING, "py_code3",\
                    LOC.DLG_CREATE_CMD__PY_CODE3,\
                    "pdb.gimp_message_set_handler( ERROR_CONSOLE )")
    ],
    [],
    gic_dlg_create, menu="<Image>/" + LOC.MNU_TOP + "/" + LOC.MNU_CREATE
    )


main()
