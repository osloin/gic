#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Gimp-plugin
# osloin 9/2018
#
# Führt die in einer externen Textdatei enthaltenen gic-Anweisungen aus.
#
# oli.s. 9/2018


import sys
import os

# Suchpfad für import erweitern ...
sys.path.append( sys.argv[ 0 ][ : sys.argv[ 0 ].rfind( os.sep ) ] +
				 os.sep + "gic" + os.sep + "lib" )

from gimpfu import *

import gic_lib.interpreter as gic
import gic_lib.locale      as LOC
import gic_lib.files       as fil_


################################################################################
# Hauptfunktion
#
# 'image'
# 'drawable'
# 'filename'    Skript-Datei mit Pfad und Dateiendung
################################################################################
def gic_dlg_file( image, drawable, filename ) :

   	# Dateiname zerlegen
   	path = fil_.extract( filename, fil_.FE_PATH_SEP )
   	name = fil_.extract( filename, fil_.FE_NAME )

	# Befehl zusammenstellen
	# [ 1 ] 'path' soll mit Anführungszeichen umschlossen werden
	sgic = gic.code_create( LOC.CMD_EXEC_SCRIPT, [ name, path ], [ 1 ] )

   	# Ausführen ...
   	gic.run_direct( sgic, [ image, drawable ] )


# Gimp-Registrierung
register(
    "python_fu_gic_dlg_file",
    LOC.DLG_FILE_INFO,
    LOC.DLG_FILE_INFO,
    "osloin",
    "osloin",
    "2018",
    "<Image>/"+ LOC.MNU_TOP + "/" + LOC.DLG_FILE_LABEL,
    "RGB*, GRAY*",
    [
        (PF_FILE, "filename", LOC.DLG_FILE__FILENAME, LOC.DLG_FILE__FILENAME_V )
    ],
    [],
    gic_dlg_file
    )


main()
